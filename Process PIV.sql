

SELECT [Property ID],'Elec' as Utility, * FROM dbo.tbl_UC_CtrlRpt_OpenInvCurrentDay a 
INNER JOIN tbl_UC_OpenInventory b ON b.property_code = a.[Property ID]
UNION ALL 
SELECT [Property ID],'Gas' as Utility, * FROM dbo.tbl_UC_CtrlRpt_OpenInvCurrentDay a 
INNER JOIN tbl_UC_OpenInventory b ON b.property_code = a.[Property ID]

select * from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND a.Method ='Bulk Email'

SELECT *  FROM vw_UC_USP_MasterControl_Data 

---------------------------------------------------------------------------------
--Activation //Start Service

SELECT b.Start_Process as 'Start Service',a.[Gas Vendor ID],b.USP_Name as USP,a.full_Address,a.[Property ID],'Gas' as Utility, isnull(a.[Gas Status],''),replace(convert(NVARCHAR, DateUploaded, 106), ' ', '/') 
as InflowDate,a.[Gas Last Update]
from vw_UC_USP_MasterControl_Data a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.[Gas Vendor ID]
 where a.[Gas Vendor ID] IN
  (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Bulk Email')) AND 
 isnull (
 a.[Gas Status],'Research In Progress') IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND a.Method ='Bulk Email')
UNION ALL

SELECT b.Start_Process as 'Start Service',a.[Gas Vendor ID],b.USP_Name as USP,a.full_Address,a.[Property ID],'Elec' as Utility, isnull(a.[Elec Status],'Research In Progress'),replace(convert(NVARCHAR, DateUploaded, 106), ' ', '/') 
as InflowDate,a.[Gas Last Update]
from vw_UC_USP_MasterControl_Data a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.[Gas Vendor ID]
 where a.[Elec Vendor ID] IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Bulk Email')) AND 
 isnull (
 a.[Elec Status],'Research In Progress') IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND a.Method ='Bulk Email')
-----------------------------------------------
Union All

SELECT b.Start_Process as 'Start Service',a.[Gas Vendor ID],b.USP_Name as USP,a.full_Address,a.[Property ID],'Gas' as Utility, isnull(a.[Gas Status],''),replace(convert(NVARCHAR, DateUploaded, 106), ' ', '/') 
as InflowDate,a.[Gas Last Update]
from vw_UC_USP_MasterControl_Data a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.[Gas Vendor ID]
 where a.[Gas Vendor ID] IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Call')) AND 
 isnull (
 a.[Gas Status],'Research In Progress') IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND a.Method ='Call')
UNION ALL

SELECT b.Start_Process as 'Start Service',a.[Gas Vendor ID],b.USP_Name as USP,a.full_Address,a.[Property ID],'Elec' as Utility, isnull(a.[Elec Status],'Research In Progress'),replace(convert(NVARCHAR, DateUploaded, 106), ' ', '/') 
as InflowDate,a.[Gas Last Update]
from vw_UC_USP_MasterControl_Data a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.[Gas Vendor ID]
 where a.[Elec Vendor ID] IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Call')) AND 
 isnull (
 a.[Elec Status],'Research In Progress') IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND a.Method ='Call')


UNION ALL

SELECT b.Start_Process as 'Start Service',a.[Gas Vendor ID],b.USP_Name as USP,a.full_Address,a.[Property ID],'Gas' as Utility, isnull(a.[Gas Status],''),replace(convert(NVARCHAR, DateUploaded, 106), ' ', '/') 
as InflowDate,a.[Gas Last Update]
from vw_UC_USP_MasterControl_Data a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.[Gas Vendor ID]
 where a.[Gas Vendor ID] IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Online Request')) AND 
 isnull (
 a.[Gas Status],'Research In Progress') IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND a.Method ='Online Request')
UNION ALL

SELECT b.Start_Process as 'Start Service',a.[Gas Vendor ID],b.USP_Name as USP,a.full_Address,a.[Property ID],'Elec' as Utility, a.[Elec Status],replace(convert(NVARCHAR, DateUploaded, 106), ' ', '/') 
as InflowDate,a.[Gas Last Update]
from vw_UC_USP_MasterControl_Data a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.[Gas Vendor ID]
 where a.[Elec Vendor ID] IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Online Request')) AND 
 isnull (
 a.[Elec Status],'Research In Progress') IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND a.Method ='Online Request')



------------------------------------------------------------------------------------------------------------






Union ALL

SELECT b.Start_Process as 'Start Service',a.VendorId,b.USP_Name as USP,Address,[Property ID],Utility, a.PreviousStatus,replace(convert(NVARCHAR, DateUploaded, 106), ' ', '/') 
as InflowDate,Status,replace(convert(NVARCHAR, PostedDate, 106), ' ', '/')
from [view_1UC_REO_Activation] a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID
 where a.VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Call')) AND 
 a.PreviousStatus IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND a.Method ='Call')

Union ALL

SELECT b.Start_Process as 'Start Service',a.VendorId,b.USP_Name as USP,Address,[Property ID],Utility, a.PreviousStatus,replace(convert(NVARCHAR, DateUploaded, 106), ' ', '/') 
as InflowDate,Status,replace(convert(NVARCHAR, PostedDate, 106), ' ', '/')
from [view_1UC_REO_Activation] a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID
 where a.VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Online Request')) AND 
 a.PreviousStatus IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND a.Method ='Online Request')




--Deactivation //Stop Service
SELECT b.Start_Process as 'Stop Service',USPNormalizedName as USP, VendorId,[Property ID], Address ,Utility,Status,isnull(x.PreviousStatus,'Out of REO') as PreviousStatus,replace(convert(NVARCHAR, a.PostedDate , 106)
, ' ', '/') as PostedDate
FROM vw_UC_Deactivation as a outer apply 
 (SELECT TOP 1 Status as PreviousStatus FROM tbl_UC_DeactivationLogs as b where a.[Property ID]=b.PropertyCode AND a.Utility=b.Utility and a.Status<>b.Status ORDER BY b.PostedDate DESC) as x  
 INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID
  where a.VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Bulk Email')) AND 
 isnull (
 a.CRStatus,'Research In Progress') IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =2 AND a.Method ='Bulk Email')

Union ALL

SELECT b.Start_Process as 'Stop Service',USPNormalizedName as USP, VendorId,[Property ID], Address ,Utility,Status,isnull(x.PreviousStatus,'Out of REO') as PreviousStatus,replace(convert(NVARCHAR, a.PostedDate , 106)
, ' ', '/') as PostedDate
FROM vw_UC_Deactivation as a outer apply 
 (SELECT TOP 1 Status as PreviousStatus FROM tbl_UC_DeactivationLogs as b where a.[Property ID]=b.PropertyCode AND a.Utility=b.Utility and a.Status<>b.Status ORDER BY b.PostedDate DESC) as x  
 INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID
  where a.VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Call')) AND 
 isnull (
 a.CRStatus,'Research In Progress') IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =2 AND a.Method ='Call')

Union ALL

SELECT b.Start_Process as 'Stop Service',USPNormalizedName as USP, VendorId,[Property ID], Address ,Utility,Status,isnull(x.PreviousStatus,'Out of REO') as PreviousStatus,replace(convert(NVARCHAR, a.PostedDate , 106)
, ' ', '/') as PostedDate
FROM vw_UC_Deactivation as a outer apply 
 (SELECT TOP 1 Status as PreviousStatus FROM tbl_UC_DeactivationLogs as b where a.[Property ID]=b.PropertyCode AND a.Utility=b.Utility and a.Status<>b.Status ORDER BY b.PostedDate DESC) as x  
 INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID
  where a.VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Online Request')) AND 
 isnull (
 a.CRStatus,'Research In Progress') IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =2 AND a.Method ='Online Request')




---------------------------------------------------------------------------------

SELECT *
from [view_1UC_REO_Activation] a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID
 where a.VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] where Start_Process IN ('Bulk Email')) AND 
 Status IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 )






Select *FROM
[dbo].vw_UC_Capabilities where Start_Process IN ('Bulk Email','Online Request','EDI 814','Call')
SELECT b.USP_Name as USP, VendorID,Task,PropertyCode,Utility,Status,DateUploaded as InflowDate,'Online Request' as Queue 


SELECT b.Start_Process as 'Start Service',a.VendorId,b.USP_Name as USP,Address,[Property ID],Utility,Status,DateUploaded as InflowDate
from [view_1UC_REO_Activation] a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID where a.VendorID IN 
(Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] 
 where Start_Process IN ('Bulk Email')) AND 
 Status IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND Method = 'Bulk Email')

SELECT  b.Start_Process as 'Start Service',a.VendorId,b.USP_Name as USP,Address,Utility,Status,DateUploaded as InflowDate
from tbl_UC_PropertyInFlow a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID where a.VendorID IN 
(Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] 
 where Start_Process IN ('Bulk Email')) AND 
 Status IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND Method = 'Bulk Email') AND DateUploaded = '9/18/2017'






select * from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 AND Method = 'Bulk Email'



SELECT b.Invoice_Process as 'Invoice',a.VendorId,a.* 
from vw_UC_Deactivation a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID where VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] 
) AND Status IN (select Util_Name from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =3 )

[view_1UC_REO_WaterPayout]















select * from tbl_UC_USP_StartService_PRIO a 
Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where b.Start_Stop_Invoice_Bills =1 

UNION ALL

SELECT b.USP_Name as USP, VendorID,Task,PropertyCode,Utility,Status,DateUploaded as InflowDate,'Online Request' as Queue 
from tbl_UC_PropertyInFlow a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID where VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] 
Where Start_Process = 'Online Request') AND DateUploaded='9/26/2017' AND Task = 'Activation'

UNION ALL

SELECT b.USP_Name as USP, VendorID,Task,PropertyCode,Utility,Status,DateUploaded as InflowDate,'Call' as Queue 
from tbl_UC_PropertyInFlow a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID where VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] 
Where Start_Process = 'Call') AND DateUploaded='9/26/2017' AND Task = 'Activation'


Union All
--Deactivation  //Stop Service

SELECT b.USP_Name as USP, VendorID,Task,PropertyCode,Utility,Status,DateUploaded as InflowDate,'Bulk Email' as Queue 
from tbl_UC_PropertyInFlow a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID  where VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] 
Where Stop_Process = 'Bulk Email') AND DateUploaded='9/19/2017' AND Task = 'Deactivation'

UNION 

SELECT b.USP_Name as USP, VendorID,Task,PropertyCode,Utility,Status,DateUploaded as InflowDate,'Online Request' as Queue 
from tbl_UC_PropertyInFlow a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID where VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] 
Where Stop_Process = 'Online Request') AND DateUploaded='9/19/2017' AND Task = 'Deactivation'

UNION ALL

SELECT b.USP_Name as USP, VendorID,Task,PropertyCode,Utility,Status,DateUploaded as InflowDate,'Call' as Queue 
from tbl_UC_PropertyInFlow a INNER JOIN vw_UC_Capabilities b ON b.Vendor_ID = a.VendorID where VendorID IN (Select Vendor_ID FROM [dbo].[vw_UC_Capabilities] 
Where Stop_Process = 'Call') AND DateUploaded='9/19/2017' AND Task = 'Deactivation'





Select * FROM tbl_UC_USPcapabilities_Start_Service

select * FROM tbl_EM_InvoiceReconFile Where [Invoice Number] IN('WI24945940
')

Select * From tbl_UC_USP_ProcessUSP
select * FROM tbl_EM_InvoiceReconFile Where [Invoice Number] IN('WI26938345
','WI26990977')

SELECT * FROM dbo.tbl_UC_USPListOnlineRequest 


select * FROM tbl_UC_USP_Name_Vendor_ID where USP_Name IN ('National Grid','Nicor Gas','Florida Power and Light') 

Update tbl_UC_USP_Name_Vendor_ID Set Website = 'https://www.nationalgridus.com/Upstate-NY-Home/Default'
, [Login Name] = 'utilityinvoice@altisource.com' , Password = 'Utility01'
 where USP_Name in ('National Grid')

 Update tbl_UC_USP_Name_Vendor_ID Set Website = 'https://www.nicorgas.com/residential/stop-start-transfer-add-service/start-service-form'
, [Login Name] = 'NICOR.1' , Password = 'Utility01'
 where USP_Name in ('Nicor Gas')

  Update tbl_UC_USP_Name_Vendor_ID Set Website = 'https://www.fpl.com/'
, [Login Name] = 'utilityinvoice@altisource.com' , Password = 'Utility01'
 where USP_Name in ('Florida Power and Light')





 http://172.26.131.159:85/

﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class DataFeed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        [WebMethod]
        public static string[] GetEmail(string prefix)
        {
            List<string> customers = new List<string>();
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = "Select NTID,EmpName From tbl_HRMS_EmployeeMaster where EmpName like   '%" + prefix + "%'";
            dt = cls.GetData(qry);

            for (int i = 0; i < dt.Rows.Count; i++ )
            {
                customers.Add(string.Format("{0}-{1}", dt.Rows[i][1], dt.Rows[i][0]));
            }
            return customers.ToArray();
        }

        [WebMethod]
        public static string BindData()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string qry = "Select * From tbl_UC_USP_DataFeedCreated Order by id";

            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        //ArrayList myData

        [WebMethod]
        public static string InsertData(ArrayList GetTitle, ArrayList GetMessage, ArrayList GetCondition1, ArrayList GetCondition2, ArrayList GetCondition3
            , ArrayList GetCondition4, ArrayList GetCondition5)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string exm = GetTitle[1].ToString();


            string insertCreated = "INSERT INTO tbl_UC_USP_DataFeedCreated (Name,CreatedBy,CreatedDate) values('";
            insertCreated += GetTitle[0].ToString() + "','" + HttpContext.Current.Session["ntid"] + "','" + DateTime.Now.ToString("hh:mm:ss") + "'";
            insertCreated += ")";
            cls.ExecuteQuery(insertCreated);


            string insertDateFeed = "INSERT INTO tbl_UC_USP_DataFeed (id,Title,Description,AtLocation,OnDays,TimeFrom,TimeTo,TimeZone) values(";
            insertDateFeed += "(Select MAX(id) from tbl_UC_USP_DataFeedCreated)" + ",'" + GetTitle[0].ToString() + "','" + GetTitle[1].ToString() + "','" + GetTitle[2].ToString() + "','" + GetTitle[3].ToString();
            insertDateFeed += "','" + GetTitle[4].ToString() + "','" + GetTitle[5].ToString() + "','" + GetTitle[6].ToString();
            insertDateFeed += "')";
            cls.ExecuteQuery(insertDateFeed);


            string insertAlertSuccess = "INSERT INTO tbl_UC_USP_DataFeed_Alert (id,Subject,Message,isSuccess) values(";
            insertAlertSuccess += "(Select MAX(id) from tbl_UC_USP_DataFeedCreated)" + ",'" + GetMessage[0].ToString()  + "','" + GetMessage[1].ToString();
            insertAlertSuccess += "','Success'";
            insertAlertSuccess += ")";
            cls.ExecuteQuery(insertAlertSuccess);

            string insertAlertFail = "INSERT INTO tbl_UC_USP_DataFeed_Alert (id,Subject,Message,isSuccess) values(";
            insertAlertFail += "(Select MAX(id) from tbl_UC_USP_DataFeedCreated)" + ",'" + GetMessage[2].ToString() + "','" + GetMessage[3].ToString();
            insertAlertFail += "','Fail'";
            insertAlertFail += ")";
            cls.ExecuteQuery(insertAlertFail);

            string insertAlertCondition1 = "";
            string insertAlertCondition2 = "";
            string insertAlertCondition3 = "";
            string insertAlertCondition4 = "";
            string insertAlertCondition5 = "";

            if(GetCondition1.Count != 0)
            {
                insertAlertCondition1 = "INSERT INTO tbl_UC_USP_DataFeedAlertConditions (id,isActive,isRecieve,isGreaterLessEqual,Time#,TimeSet,isSuccess,Condition#) values(";
                insertAlertCondition1 += "(Select MAX(id) from tbl_UC_USP_DataFeedCreated)" + ",'" + GetCondition1[0].ToString() + "','" + GetCondition1[1].ToString() + "','";
                insertAlertCondition1 += GetCondition1[2].ToString() + "','" + GetCondition1[3].ToString() + "','" + GetCondition1[4].ToString() + "','";
                insertAlertCondition1 += GetCondition1[5].ToString() + "','" + GetCondition1[6].ToString() + "'";
                insertAlertCondition1 += ")";
                cls.ExecuteQuery(insertAlertCondition1);
            } 
            if (GetCondition2.Count != 0)
            {
                insertAlertCondition2 = "INSERT INTO tbl_UC_USP_DataFeedAlertConditions (id,isActive,isRecieve,isGreaterLessEqual,Time#,TimeSet,isSuccess,Condition#) values(";
                insertAlertCondition2 += "(Select MAX(id) from tbl_UC_USP_DataFeedCreated)" + ",'" + GetCondition2[0].ToString() + "','" + GetCondition2[1].ToString() + "','";
                insertAlertCondition2 += GetCondition2[2].ToString() + "','" + GetCondition2[3].ToString() + "','" + GetCondition2[4].ToString() + "','";
                insertAlertCondition2 += GetCondition2[5].ToString() + "','" + GetCondition2[6].ToString() + "'";
                insertAlertCondition2 += ")";
                cls.ExecuteQuery(insertAlertCondition2);
            }

            if (GetCondition3.Count != 0)
            {
                insertAlertCondition3 = "INSERT INTO tbl_UC_USP_DataFeedAlertConditions (id,isActive,isRecieve,isGreaterLessEqual,Time#,TimeSet,isSuccess,Condition#) values(";
                insertAlertCondition3 += "(Select MAX(id) from tbl_UC_USP_DataFeedCreated)" + ",'" + GetCondition3[0].ToString() + "','" + GetCondition3[1].ToString() + "','";
                insertAlertCondition3 += GetCondition3[2].ToString() + "','" + GetCondition3[3].ToString() + "','" + GetCondition3[4].ToString() + "','";
                insertAlertCondition3 += GetCondition3[5].ToString() + "','" + GetCondition3[6].ToString() + "'";
                insertAlertCondition3 += ")";
                cls.ExecuteQuery(insertAlertCondition3);
            }

            if (GetCondition4.Count != 0)
            {
                insertAlertCondition4 = "INSERT INTO tbl_UC_USP_DataFeedAlertConditions (id,isActive,isRecieve,isGreaterLessEqual,Time#,TimeSet,isSuccess,Condition#) values(";
                insertAlertCondition4 += "(Select MAX(id) from tbl_UC_USP_DataFeedCreated)" + ",'" + GetCondition4[0].ToString() + "','" + GetCondition4[1].ToString() + "','";
                insertAlertCondition4 += GetCondition4[2].ToString() + "','" + GetCondition4[3].ToString() + "','" + GetCondition4[4].ToString() + "','";
                insertAlertCondition4 += GetCondition4[5].ToString() + "','" + GetCondition4[6].ToString() + "'";
                insertAlertCondition4 += ")";
                cls.ExecuteQuery(insertAlertCondition4);
            }

            if (GetCondition5.Count != 0)
            {
                insertAlertCondition5 = "INSERT INTO tbl_UC_USP_DataFeedAlertConditions (id,isActive,isRecieve,isGreaterLessEqual,Hour,TimeSet,isSuccess,Condition#) values(";
                insertAlertCondition5 += "(Select MAX(id) from tbl_UC_USP_DataFeedCreated)" + ",'" + GetCondition5[0].ToString() + "','" + GetCondition5[1].ToString() + "','";
                insertAlertCondition5 += GetCondition5[2].ToString() + "','" + GetCondition5[3].ToString() + "','" + GetCondition5[4].ToString() + "','";
                insertAlertCondition5 += GetCondition5[5].ToString() + "','" + GetCondition5[6].ToString() + "'";
                insertAlertCondition5 += ")";
                cls.ExecuteQuery(insertAlertCondition5);
            }




            return "TRUE";
        }
    }
}
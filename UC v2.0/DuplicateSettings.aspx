﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DuplicateSettings.aspx.cs" Inherits="UC_v2._0.DuplicateSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
         input.cmn-toggle-round + label {
  padding: 2px;
  width: 36px;
  height: 15px;
  background-color: #dddddd;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  -ms-border-radius: 30px;
  -o-border-radius: 30px;
  border-radius: 30px;
}
input.cmn-toggle-round + label:before, input.cmn-toggle-round + label:after {
  display: block;
  position: absolute;
  top: 1px;
  left: 1px;
  bottom: 1px;
  content: "";
}
input.cmn-toggle-round + label:before {
  right: 1px;
  background-color: #f1f1f1;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  -ms-border-radius: 30px;
  -o-border-radius: 30px;
  border-radius: 30px;
  -webkit-transition: background 0.4s;
  -moz-transition: background 0.4s;
  -o-transition: background 0.4s;
  transition: background 0.4s;
}
input.cmn-toggle-round + label:after {
  width: 22px;
  background-color: #fff;
  -webkit-border-radius: 100%;
  -moz-border-radius: 100%;
  -ms-border-radius: 100%;
  -o-border-radius: 100%;
  border-radius: 100%;
  -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
  -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
  -webkit-transition: margin 0.4s;
  -moz-transition: margin 0.4s;
  -o-transition: margin 0.4s;
  transition: margin 0.4s;
}
input.cmn-toggle-round:checked + label:before {
  background-color: #8ce196;
}
input.cmn-toggle-round:checked + label:after {
  margin-left: 13px;
}

.cmn-toggle {
  position: absolute;
  margin-left: -9999px;
  visibility: hidden;
}
.cmn-toggle + label {
  display: block;
  position: relative;
  cursor: pointer;
  outline: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
        table tbody tr:hover {
            background: #81c341;
            cursor: pointer;
        }

        table tbody tr.selected {
            background: #81c341;
        }

        #ulSETTINGS li input[type='number'] {
            width: 51px;
            padding: 3px 8px;
            height: 28px;
        }

        select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            padding: 3px 10px;
        }
         .ultraselect-1 {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            padding: 3px 10px;
        }
    </style>
    <div class="content-wrapper">
        <div class="row">
            <%--------------------------------------------------------------%>
        
            <div class="col-sm-12"">
                 <div class="row"><div class="col-sm-12" style="background-color:#4d6578; width:1368px"><h4> Bills Processing</h4></div></div>  

                <div class="row">
                    <div class="col-md-2" style="width:259px">
                      <div class="panel panel-New">
                    <div class="panel-Newheading" style="height:30px; text-align:center; font-size:20px">Parameters</div>
                          <div class="panel-body" style="height: 604px; font-size:medium">

                               


                              </div>
                          <div class="panel-footer" style="background-color:transparent">
                              <center>
                                <asp:Button runat="server"  id="btnAdd" Text="Add New" class="btn btn-primary btn-sm"/>
                                </center>
                          </div>
                          </div>  
                    </div>
                    <div class="col-md-9" style="padding-left:3px">
                      <div class="panel panel-New" style="width:1105px" >
                    <div class="panel-Newheading" style="height:30px; text-align:center; font-size:20px"> Duplicate Setting    </div>
                          <div class="panel-body" style="height: 604px; font-size:medium; padding-left:15px; padding-right:15px">
                               <br />
                             
                                  

                              <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                                  <asp:View ID="View1" runat="server">
                                      <div class="panel" style="border-color:#595d61">
                                    <div class="panel-body" >
                                        <div class="row" style="padding-left:10px">
                                            <div class="col-md-2" style="font-size:14px">
                                                <asp:Label ID="Label1" runat="server" Text="The status of this rule is"></asp:Label></div>
                                            <div class="col-sm-1" style="padding-left:0px; padding-top:4px"><div class="switch">
        <input id="cmn-toggle-1" class="cmn-toggle cmn-toggle-round"  type="checkbox">
        <label for="cmn-toggle-1"></label>
      </div></div>
                                        </div>

                                        <div class="row" style="padding-left:10px; font-size:14px; padding-top:10px">
                                            <div class="col-md-3" style="padding-top:3px; width:190px" >
                                                <asp:Label ID="Label2" runat="server" Text="If the Parameters below is/are"></asp:Label></div>
                                            <div class="col-md-2" style="width:125px">
                                                 
                                                <asp:DropDownList ID="DropDownList69" Width="120px" Height="24px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2" style="width:100px">
                                                <asp:TextBox ID="TextBox3" runat="server" class="form-control form-group-sm"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2" style="width:79px">
                                                <asp:Label ID="Label3" runat="server" Text="for the last"></asp:Label>
                                            </div>
                                            <div class="col-md-2" style="width:100px">
                                                <asp:TextBox ID="TextBox4" runat="server" class="form-control form-group-sm"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2" style="width:125px; padding-left:2px">
                                                <asp:DropDownList ID="DropDownList1" Width="120px" Height="24px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2" style="width:105px; padding-left:2px">
                                                <asp:DropDownList ID="DropDownList2" Width="90px" Height="24px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>


                                            


                                        </div>
                                        <div class="row" style="padding-left:40px; font-size:14px; padding-top:10px">
                                            <div class="col-md-2">
                                                <asp:Label ID="Label4" runat="server" Text="and the selected USP is/are"></asp:Label>
                                            </div>
                                            <div class="col-md-2" style="width:85px; padding-left:2px">
                                                <asp:DropDownList ID="DropDownList3" Width="90px" Height="24px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2" style="width:140px">
                                                <asp:Label ID="Label5" runat="server" Text="and the States is/are"></asp:Label>
                                            </div>
                                            <div class="col-md-2" style="width:105px; padding-left:2px">
                                                <asp:DropDownList ID="DropDownList4" Width="90px" Height="24px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                                </div>

                                        <div class="row" style="padding-left:40px; font-size:14px; padding-top:10px">
                                            <div class="col-md-2" style="width:105px; padding-left:2px">
                                                <asp:DropDownList ID="DropDownList5" Width="90px" Height="24px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>

                                            
                                                
                                        
                                        </div>
                                        <div class="row" style="padding-left:40px; font-size:14px; padding-top:10px">
                                            <div class="col-md-2">
                                                <asp:Button ID="Button2" class="btn btn-link btn-sm" runat="server" Text="+Add a Parameter "  />
                                            </div>
                                        </div>
                                         
                                        <div class="row" style="padding-left:40px; font-size:14px; padding-top:10px">
                                            <div class="col-md-3" style="width:184px; padding-left:0px">
                                                <asp:Label ID="Label6" runat="server" Text="If the Condition(s) are met then"></asp:Label>
                                            </div>
                                            <div class="col-md-2" style="padding-right:0px">
                                                <asp:DropDownList ID="DropDownList6" Width="90px" Height="24px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2" style="padding-right:0px; float:right; width:100px">
                                                <asp:Button ID="Button1" class="btn btn-primary btn-sm" runat="server" Text="Edit" OnClick="Button1_Click" />
                                            </div>
                                        </div>
                                        


                                    </div>
                                </div>
                                  </asp:View>
                                  <asp:View ID="View2" runat="server">
                                      <div class="panel" style="border-color:#595d61">
                                    <div class="panel-body" >
                                        <div class="row" style="padding-left:10px">
                                            <div class="col-md-2" style="font-size:14px">
                                                <asp:Label ID="Label7" runat="server" Text="The status of this rule is"></asp:Label></div>
                                            <div class="col-sm-1" style="padding-left:0px; padding-top:4px"><div class="switch">
        <input id="cmn-toggle-1" class="cmn-toggle cmn-toggle-round"  type="checkbox">
        <label for="cmn-toggle-1"></label>
      </div></div>
                                        </div>

                                        <div class="row" style="padding-left:10px; font-size:14px; padding-top:10px">
                                            <div class="col-md-3" style="padding-top:3px; width:190px" >
                                                <asp:Label ID="Label8" runat="server" Text="If the Parameters below is/are"></asp:Label></div>
                                            <div class="col-md-2" style="width:125px">
                                                <asp:DropDownList ID="DropDownList7" Width="120px" Height="24px"  runat="server" Enabled="False">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2" style="width:100px">
                                                <asp:TextBox ID="TextBox1" runat="server" class="form-control form-group-sm" disabled></asp:TextBox>
                                                 
                                            </div>
                                            <div class="col-md-2" style="width:79px">
                                                <asp:Label ID="Label9" runat="server"  Text="for the last"></asp:Label>
                                            </div>
                                            <div class="col-md-2" style="width:100px">
                                                <asp:TextBox ID="TextBox2" runat="server" class="form-control form-group-sm" disabled></asp:TextBox>
                                            </div>
                                            <div class="col-md-2" style="width:125px; padding-left:2px">
                                                <asp:DropDownList ID="DropDownList8" Width="120px" Height="24px" Enabled="False" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2" style="width:105px; padding-left:2px">
                                                <asp:DropDownList ID="DropDownList9" Width="90px" Height="24px" Enabled="False" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>


                                            


                                        </div>
                                        <div class="row" style="padding-left:40px; font-size:14px; padding-top:10px">
                                            <div class="col-md-2">
                                                <asp:Label ID="Label10" runat="server" Text="and the selected USP is/are"></asp:Label>
                                            </div>
                                            <div class="col-md-2" style="width:85px; padding-left:2px">
                                                <asp:DropDownList ID="DropDownList10" Width="90px" Height="24px" Enabled="False" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2" style="width:140px">
                                                <asp:Label ID="Label11" runat="server" Text="and the States is/are"></asp:Label>
                                            </div>
                                            <div class="col-md-2" style="width:105px; padding-left:2px">
                                                <asp:DropDownList ID="DropDownList11" Width="90px" Height="24px" Enabled="False" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                                </div>

                                        <div class="row" style="padding-left:40px; font-size:14px; padding-top:10px">
                                            <div class="col-md-2" style="width:105px; padding-left:2px">
                                                <asp:DropDownList ID="DropDownList12" Width="90px" Height="24px" Enabled="False" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>

                                            
                                                
                                        
                                        </div>
                                        <div class="row" style="padding-left:40px; font-size:14px; padding-top:10px">
                                            <div class="col-md-2">
                                                <asp:Button ID="Button3" class="btn btn-link btn-sm" runat="server" Text="+Add a Parameter "  />
                                            </div>
                                        </div>
                                         
                                        <div class="row" style="padding-left:40px; font-size:14px; padding-top:10px">
                                            <div class="col-md-3" style="width:184px; padding-left:0px">
                                                <asp:Label ID="Label12" runat="server" Text="If the Condition(s) are met then"></asp:Label>
                                            </div>
                                            <div class="col-md-2" style="padding-right:0px">
                                                <asp:DropDownList ID="DropDownList13" Width="90px" Height="24px" Enabled="False" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                            <div class="col-md-2" style="padding-right:0px; float:right; width:100px">
                                                <asp:Button ID="Button4" class="btn btn-success btn-sm" runat="server" Text="Done" OnClick="Button4_Click" />
                                            </div>
                                        </div>
                                        


                                    </div>
                                </div>
                                  </asp:View>

                              </asp:MultiView>






                              </div>
                          <div class="panel-footer" style="background-color:transparent;">
                              <center>
                                <asp:Button runat="server"  id="btnAddPaySet" Text="Add New" class="btn btn-primary btn-sm"/> 
                            </center>
                          </div>
                          </div>  
                    </div>

                </div> 
                </div>
            </div>
          
    </div>
 
</asp:Content>


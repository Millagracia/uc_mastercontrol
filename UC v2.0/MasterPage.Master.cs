﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            clsConnection cls = new clsConnection();



            string domain = System.Web.HttpContext.Current.User.Identity.Name.Replace(@"\", "/");
            if (domain == "")
            {
                domain = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"\", "/");
            }

            string[] sUser = domain.Split('/');
            string sUserId = sUser[1].ToLower();
            Session["user"] = sUserId;
            string qryRole = "select * from tbl_UC_Access_User where ntid = '" + sUserId + "'";

            DataTable dtUser = cls.GetData(qryRole);

            if (dtUser.Rows.Count > 0)
            {
                string qryFunc = "select * from tbl_VPR_Access_Role_Type_Function where role_id = '" + dtUser.Rows[0]["role_id"].ToString() + "'";

                DataTable dtFunc = cls.GetData(qryFunc);

                if (dtFunc.Rows.Count > 0)
                {
                    string func = dtFunc.Rows[0]["function_on"].ToString();

                    string[] ids = func.Split(',');

                    ArrayList arr = new ArrayList();

                    foreach (string id in ids)
                    {
                        Control li = new Control();
                        li = FindControl("_" + id);
                        if (li != null)
                        {
                            li.Visible = true;
                        }
                    }
                }
                else
                {
                    ArrayList list = new ArrayList();
                    list.Add("_37");
                    list.Add("_38");
                    list.Add("_39");
                    list.Add("_40");
                    list.Add("_41");
                    list.Add("_42");
                    list.Add("_43");
                    list.Add("_44");
                    list.Add("_45");
                    list.Add("_46");
                    list.Add("_47");
                    list.Add("_48");
                    list.Add("_49");
                    list.Add("_50");
                    list.Add("_51");


                    foreach (string id in list)
                    {
                        Control li = new Control();
                        li = FindControl(id);
                        if (li != null)
                        {
                            li.Visible = true;
                        }
                    }
                }
            }
            else
            {
                ArrayList list = new ArrayList();
                list.Add("_37");
                list.Add("_38");
                list.Add("_39");
                list.Add("_40");
                list.Add("_41");
                list.Add("_42");
                list.Add("_43");
                list.Add("_44");
                list.Add("_45");
                list.Add("_46");
                list.Add("_47");
                list.Add("_48");
                list.Add("_49");
                list.Add("_50");
                list.Add("_51");


                foreach (string id in list)
                {
                    Control li = new Control();
                    li = FindControl(id);
                    if (li != null)
                    {
                        li.Visible = true;
                    }
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            string domain = System.Web.HttpContext.Current.User.Identity.Name.Replace(@"\", "/");
            if (domain == "")
            {
                domain = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"\", "/");
            }

            string[] sUser = domain.Split('/');
            string sUserId = sUser[1].ToLower();
            Session["ntid"] = sUserId;



            




           // usr.InnerText = Session["user"].ToString();
           // Session["ntid"] = Environment.UserName;
        }
    }
}
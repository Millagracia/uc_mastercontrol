﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="StopServiceControlSettings.aspx.cs" Inherits="UC_v2._0.StopServiceControlSettings" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="plugins/multiselect/bootstrap-multiselect.css" rel="stylesheet" />
     
    <style>
         body .divMain {
            height: 100% !important;
            /*background-color: yellow;*/
            overflow: auto;
        }

        .divMain #floatDiv {
            position: absolute;   
            left: 14%;
            margin-top: 10px;
            /*margin-left: -200px;*/
            width: 200px;
            height: 110px;
            z-index: 1;
            /*background: rgb(136, 189, 35);*/
            /*box-shadow: black 10px 10px;*/
        } 
        h4 {
            text-align: center;
            color: white;
        }

        .modal-header {
            background-color: #4d6578;
        }

        .panel-body {
            padding: 1px;
        }
        .panel-New123 > .panel-heading {
            color: #fff;
            background-color: #4d6578;
            border-color: #4d6578;
        } 
        .panel-New > .panel-heading {
            color: #fff;
            background-color: #4d6578;
            border-color: #4d6578;
        }

        .panel-New > .panel-heading1 {
            color: #fff;
            background-color: #4d6578;
            border-color: #4d6578;
        }

        .panel-New > .panel-Newheading {
            color: #fff;
            background-color: #4d6578;
            border-color: #4d6578;
        }

        .panel-Newheading {
            padding: 0px 15px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }

        .col-sm-4 {
            padding-right: -7px;
        }

        .navbar {
            position: relative;
            min-height: 26px;
            margin-bottom: 20px;
            border: 1px solid transparent;
        }

        .form-control {
            display: block;
            width: 100%;
            height: 23px;
            padding: 6px 12px;
            font-size: 16px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }


        .panel {
            margin-bottom: 0px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
        }

        table tbody tr.selected {
            background: #81c341;
        }

        .panel-footer {
            padding: 10px 15px;
            background-color: #b1b1b1;
            border-top: 1px solid #ddd;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
        }

        .content-wrapper {
            min-height: 735px !important;
        }
        input.cmn-toggle-round + label {
            padding: 2px;
            width: 50px;
            height: 15px;
            background-color: #dddddd;
            -webkit-border-radius: 30px;
            -moz-border-radius: 30px;
            -ms-border-radius: 30px;
            -o-border-radius: 30px;
            border-radius: 30px;
        } 
            input.cmn-toggle-round + label:before, input.cmn-toggle-round + label:after {
                display: block;
                position: absolute;
                top: 1px;
                left: 1px;
                bottom: 1px;
                content: "";
       } 
            input.cmn-toggle-round + label:before {
                right: 1px;
                background-color: #f1f1f1;
                -webkit-border-radius: 30px;
                -moz-border-radius: 30px;
                -ms-border-radius: 30px;
                -o-border-radius: 30px;
                border-radius: 30px;
                -webkit-transition: background 0.4s;
                -moz-transition: background 0.4s;
                -o-transition: background 0.4s;
                transition: background 0.4s;
            }

            input.cmn-toggle-round + label:after {
                width: 22px;
                background-color: #fff;
                -webkit-border-radius: 100%;
                -moz-border-radius: 100%;
                -ms-border-radius: 100%;
                -o-border-radius: 100%;
                border-radius: 100%;
                -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
                -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
                -webkit-transition: margin 0.4s;
                -moz-transition: margin 0.4s;
                -o-transition: margin 0.4s;
                transition: margin 0.4s;
            }

        input.cmn-toggle-round:checked + label:before {
            background-color: #8ce196;
        }

        input.cmn-toggle-round:checked + label:after {
            margin-left: 28px;
        }

        .cmn-toggle {
            position: absolute;
            margin-left: -9999px;
            visibility: hidden;
        }

            .cmn-toggle + label {
                display: block;
                position: relative;
                cursor: pointer;
                outline: none;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }
    </style>
     
    <style type="text/css">
        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.3;
        }

        .modalPopup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 320px;
            height: 115px;
        }

        .modalPopup2 {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 920px;
            height: 430px;
        }

        .navbar {
            position: relative;
            min-height: 26px;
            margin-bottom: 4px;
            border: 1px solid transparent;
        }

        .navbar-nav > li > a {
            padding-top: 11px;
            padding-bottom: 11px;
        }

        .navbar-text {
            margin-top: 9px;
            margin-bottom: 11px;
        }

        .navbar-inverse {
            background-color: #274761;
            border-color: #2c628a;
        }

            .navbar-inverse .navbar-nav > li > a {
                color: #d8d8d8;
            }

            .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus {
                color: #fff;
                background-color: #3d4b5d;
            }

        table tbody tr:hover {
            background: #8ce196;
            cursor: pointer;
        }

        table tbody tr.selected {
            background: #81c341;
        }

        #ulSETTINGS li input[type='text'] {
            width: 51px;
            padding: 3px 8px;
            height: 28px;
        }

        select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            padding: 3px 10px;
        }
    </style> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="content-wrapper">
<%--<div class="content-header" style="text-align:center; min-height:50px">
        <h1>Start Service</h1>
        <ol class="breadcrumb">
            <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
                <span><i class="fa fa-sitemap"></i>&nbsp;Start Service</span>
            </li> 
        </ol>
    </div>--%>
        <div class="row">
          
            <div class="col-md-2">
                <div class="panel panel-New">
                    <div class="panel-heading">
                        <center>Client</center>
                    </div>
                    <div class="panel-body" style="min-height: 690px">
                         
                        <table id="tblClientName" style="text-align:left" class="table no-border" data-util="Gas">
                            <tbody></tbody>
                        </table>
                          <%-- <asp:ListBox ID="test" runat="server"  SelectionMode="Multiple"  multiple="multiple"  ClientIDMode="Static"></asp:ListBox> --%>
                    </div>
                </div>
            </div> 
            
            <div class="col-md-10" style="padding-left: 3px; display: none;" id="divMAINPANEL"> 
                        <div class="row">
                                   <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                       <%--<Triggers>
                                           <asp:AsyncPostBackTrigger ControlID="btnSave" />
                                           
                                       </Triggers> --%>
                                       <Triggers>
                                           <asp:AsyncPostBackTrigger ControlID="ListBox11" EventName="SelectedIndexChanged" />
                                           <%--<asp:AsyncPostBackTrigger ControlID="lbedit11" EventName="SelectedIndexChanged" />--%>
                                           
                                       </Triggers> 
                    <ContentTemplate> 
                            <div class="col-md-2" style="padding-left: 14px">
                                <div class="panel panel-New">
                                    <div class="panel-heading">
                                        <center>Rule</center>
                                    </div>
                                    <div class="panel-body" style="min-height: 635px">

                                       <%-- <asp:TreeView ID="TreeView1" runat="server">
                                            <Nodes>
                                                <asp:TreeNode Text="PFC" Value="PFC" >
                                                    <asp:TreeNode Text="California Specific" Value="California Specific"></asp:TreeNode>
                                                    <asp:TreeNode Text="Maryland Specific" Value="Maryland Specific"></asp:TreeNode>
                                                </asp:TreeNode>
                                             
                                            </Nodes>

                                        </asp:TreeView>--%>
                                      

                                        <table id="tblRule" style="text-align:left" class="table no-border" data-util="Gas">
                                        <tbody></tbody>
                                        </table>


                                    </div>
                                    <div class="panel-footer text-center" style="background-color: white">
                                        <table onclick="btnNew(this)">
                                            <thead>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <button type="button" class="btn btn-primary">Add New</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                         <%--<asp:Button ID="btnNew" ClientIDMode="Static" runat="server"  class="btn btn-primary" OnClick="AddNew_Click" Text="Add New"></asp:Button>--%>
                                       
                                        <br />
                                   <%--     <table id="AddNew" onclick="AddNew(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <button id="Button12" runat="server"  class="btn btn-primary">Add New</button>
                                                                                
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>--%>
                                         </div>
                                </div>
                            </div>
                            <div class="col-md-7" style="padding-left: 3px;" id="divMAINPANEL2">
                                <div class="panel panel-New" style="min-width: 952px"> 
                                    <div class="panel-body" style="min-height: 690px">
                                        <nav class="navbar navbar-inverse" style="background-color: #4d6578">
                                            <div class="row">
                                                <div class="col-sm-3"></div>
                                                <div class="col-sm-7">
                                                    <ul class="nav navbar-nav">
                                                        <p class="navbar-text">|</p>
                                                        <li class="active"><a data-toggle="tab" href="#setting">Setting</a></li>
                                                        <p class="navbar-text">|</p>
                                                        <li><a data-toggle="tab" href="#schedule">Schedule</a></li>
                                                        <p class="navbar-text">|</p>
                                                    </ul>
                                                </div>
                                            </div>
                                        </nav>
                                        <div class="tab-content">
                                            <%--Multiview--%>
                                       









                                            <div id="setting" class="tab-pane fade active in" style=" overflow-y: scroll; width:940px">
                                                <div style="padding-left: 3px; display: none;" id="EditPanel">
                                                            <div class="row">
                                                                <div class="col-sm-4"></div>
                                                                <div class="col-sm-4">
                                                                    <center>
                                                                <b><h2><asp:Label ID="Label117" runat="server" Text="Edit Rule"></asp:Label></h2></b>
                                                                </center>
                                                                </div>
                                                                <div class="col-sm-4" style="padding-left: 195px">
                                                                    <br />
                                                  <%--                  <asp:ImageButton ID="ImageButton2" runat="server" AlternateText="ImageButton 1" ImageUrl="img/close.png"
                                                                       ClientIDMode="Static" Width="25px" Height="25px" />--%>
                                                                     <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                     <button type="button" id="ImageButton2" class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>
                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_1.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <br />
                                                                <div class="panel">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 450px">
                                                                        <br />
                                                                        <br />
                                                                        <br />
                                                                        <div class="row">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Name</b> 
                                                                            </div>
                                                                            <div class="col-md-4">

                                                                                <asp:TextBox ID="txteditName" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Service Location</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <asp:ListBox ID="lbedit1" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"> 
                                                                                </asp:ListBox>
                                                                              </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Client Status</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                               <%-- <asp:DropDownList ID="DropDownList2" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"  ClientIDMode="Static" Width="250px" runat="server" DataSourceID="SqlDataSource2"
                                                                                    DataTextField="Client_Name" DataValueField="Client_Name" multiple="multiple" size="2" AutoPostBack="True">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="lbedit2" SelectionMode="Multiple"  runat="server"  ClientIDMode="Static" Width="250px"  DataSourceID="SqlDataSource2"
                                                                                    ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource16" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Client_Name] FROM [tbl_UC_Client_Status]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">

                                                                    <%--<asp:Button ID="btneditNext" runat="server" Text="Next" class="btn btn-primary" ClientIDMode="Static" />--%>

                                                                        <table id="asdasd" onclick="btneditNext(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td> 
                                                                                       <button type="button" class="btn btn-primary">Next</button>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div> 
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                              
                                                        </div>
                                                <div id="EditPanel1" style="padding-left:3px; display:none;">
                                                            <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="Label65" runat="server" Text="Edit Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br />
                                                                 <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                    
                                                               <button type="button" id="ImageButton3" class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>
  </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                            </div>

                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_2.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <br />

                                                                <div class="panel">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 450px">
                                                                        <br />
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Country</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <asp:DropDownList  ID="DropDownList1"  Width="250px" runat="server">
                                                                                    <asp:ListItem>USA</asp:ListItem>
                                                                                </asp:DropDownList>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>State</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                               <%-- <asp:DropDownList ID="DropDownList4" Width="250px" runat="server" 
                                                                                    DataSourceID="SqlDataSource3" DataTextField="State" DataValueField="State">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="lbedit3" runat="server" SelectionMode="Multiple" ClientIDMode="Static"
                                                                                    multiple="multiple"></asp:ListBox>

                                                                               
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>City</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                             <%--   <asp:DropDownList ID="DropDownList5" Width="250px" runat="server" 
                                                                                    DataSourceID="SqlDataSource4" DataTextField="City" DataValueField="City">
                                                                                </asp:DropDownList>--%> 
                                                                                <asp:ListBox ID="lbedit4" runat="server" SelectionMode="Multiple" ClientIDMode="Static"
                                                                                   ></asp:ListBox>  
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Zip Code</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                               <%-- <asp:DropDownList ID="DropDownList6" Width="250px" runat="server" 
                                                                                    DataSourceID="SqlDataSource5" DataTextField="Zip" DataValueField="Zip">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="lbedit5" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                    ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource19" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Zip], [Val_ID]  FROM [tbl_UC_State_City_Zip]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Street Name</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <asp:DropDownList ID="DropDownList2" Width="250px" runat="server">
                                                                                    <asp:ListItem>ALL</asp:ListItem>
                                                                                   
                                                                                </asp:DropDownList>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Number of Unit</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <asp:DropDownList ID="DropDownList4" Width="250px" runat="server">
                                                                                    <asp:ListItem>ALL</asp:ListItem>
                                                                                    
                                                                                </asp:DropDownList>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>PODS</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <%--<asp:DropDownList ID="DropDownList10" Width="250px" runat="server"
                                                                                     DataSourceID="SqlDataSource6" DataTextField="POD_Name" DataValueField="POD_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="lbedit6" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                     ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource20" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [POD_Name] FROM [tbl_UC_PODS]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                  

                                                                                <div class="row">
                                                                                        <div class="col-sm-4"></div>
                                                                        <div class="col-sm-4">
                                                                        <table id="asdasd" onclick="btnback1(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <button type="button" id="btnback1" class="btn btn-primary">Back</button>
                                                                                 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table></div>
                                                                        <div class="col-sm-4">
                                                                             <table id="asdasd" onclick="btnnext1(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                     <td> 
                                                                                      <button type="button" id="btnnext1" class="btn btn-primary">Next</button>
                                                                                     </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                                        </div>            
                                                                            </div>
                                                                        
                                                                   

                                                                   

                                                                </div>



                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        </div>  
                                                <div style="padding-left:3px; display:none;" id="EditPanel3">
                                                            <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="Label66" runat="server"  Text="Edit Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br />
                                                                     <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                    
        <button type="button" id="ImageButton4" class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>

  </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                

                                                            </div>

                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_3.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <br />

                                                                <div class="panel">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 450px">
                                                                        <br />
                                                                        <%--1--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-3" style="padding-left: 80px">
                                                                                <b>VMS Board Date</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <%--<form action="/action_page.php">
                                                                                    <asp:TextBox ID="txtDatePicker" type="date" name="bday" runat="server"></asp:TextBox> 
                                                                                </form>--%>
                                                                                <asp:TextBox ID="TextBox1" ClientIDMode="Static" Height="28px" runat="server" class="form-control "  ></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-md-2" style="padding-left:2px; width:125px">
                                                                                <asp:DropDownList ID="DropDownList5" ClientIDMode="Static"  Width="120px" runat="server" >
                                                                                    
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-md-2" style="padding-left:2px">
                                                                                <asp:DropDownList ID="DropDownList6" ClientIDMode="Static"  Width="120px" runat="server"  >
                                                                                    
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--2--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>Is the property Active or Inactive?</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <asp:DropDownList ID="DropDownList9" Width="120px" runat="server" ClientIDMode="Static">
                                                                                    
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <b>Select the Property Type</b>
                                                                               <%-- <asp:DropDownList ID="DropDownList18" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource7" DataTextField="Pro_Name" DataValueField="Pro_Name" Font-Overline="False">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="lbedit7" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                    ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource12" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Pro_Name], [Val_ID] FROM [tbl_UC_Property_Type]"></asp:SqlDataSource>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        
                                                                        <%--3--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>Select the Occupancy Status</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <%--<asp:DropDownList ID="DropDownList13" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource8" DataTextField="Occu_Name" DataValueField="Occu_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="lbedit8" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                     ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource17" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Occu_Name], [Val_ID] FROM [tbl_UC_Occupancy_Status]"></asp:SqlDataSource>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <b>Select the Property Status</b>
                                                                                <%--<asp:DropDownList ID="DropDownList19" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource9" DataTextField="Property_Name" DataValueField="Property_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="lbedit10" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                  ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource18" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Property_Name], [Val_ID] FROM [tbl_UC_Property_Status]"></asp:SqlDataSource>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        <%--4--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>What is the property value</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <asp:DropDownList ID="DropDownList10" ClientIDMode="Static" Width="120px" runat="server">
                                                                                    
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-md-2" style="padding-left: 0px">
                                                                                <asp:TextBox ID="TextBox2" ClientIDMode="Static" Height="28px" class="form-control " Text="$" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        <%--5--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>What is the total preservation cost</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <asp:DropDownList ID="DropDownList11" ClientIDMode="Static" Width="120px" runat="server">
                                                                                     
                                                                                </asp:DropDownList>

                                                                            </div>
                                                                            <div class="col-md-2" style="padding-left: 0px">
                                                                                <asp:TextBox ID="TextBox3" ClientIDMode="Static" Height="28px" class="form-control " Text="$" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        <%--6--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>Select the preservation work type</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <%--<asp:DropDownList ID="DropDownList20" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource10" DataTextField="Work_Name" DataValueField="Work_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="lbedit9" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                   ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource21" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Work_Name], [Val_ID] FROM [tbl_UC_Preservation_Work_Type]"></asp:SqlDataSource>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        <%--7--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>Is the inspection completed?</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <asp:CheckBox ID="CheckBox1"  runat="server" ClientIDMode="Static" />
                                                                                &nbsp Yes
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:CheckBox ID="CheckBox2" ClientIDMode="Static" runat="server" />&nbsp No
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        <%--8--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>Inspection Completed Date greater than or equal to</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <asp:TextBox ID="TextBox4" ClientIDMode="Static" Enabled="false" Width="120px" Height="28px" class="form-control " Text="$" runat="server"></asp:TextBox>
                                                                            </div> 
                                                                           <%-- <div class="col-md-3">
                                                                                <form action="/action_page.php">
                                                                                    <asp:TextBox ID="txtCompleteDate" Enabled="false" Width="170px" Height="28px" class="form-control " type="date" name="bday" runat="server"></asp:TextBox>
 
                                                                                </form>
                                                                            </div>--%>
                                                                            <div class="col-md-2" style="padding-left:2px; width:125px">
                                                                                <asp:DropDownList ID="DropDownList13" ClientIDMode="Static" Enabled="false"  Width="120px" runat="server" >
                                                                                    
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-md-2" style="padding-left:2px">
                                                                                <asp:DropDownList ID="DropDownList16" ClientIDMode="Static" Enabled="false" Width="120px" runat="server"  >
                                                                                    
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                     <div class="row">
                                                                                        <div class="col-sm-4"></div>
                                                                        <div class="col-sm-4">
                                                                            <table id="asdasd" onclick="btnback2(this)">
                                                                                <thead>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button type="button" id="btnback2" class="btn btn-primary">Back</button>

                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table></div>
                                                                        <div class="col-sm-4">
                                                                            <table id="asdasd" onclick="btnnext2(this)">
                                                                                <thead>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button type="button" id="btnnext2" class="btn btn-primary">Next</button>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                                        </div>            
                                                                            </div>
                                                                    
                                                                </div>



                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        </div>
                                                <div style="padding-left:3px; display:none;" id="EditPanel4">
                                                            <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="Label67" runat="server" Text="Edit Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br />
                                                                 <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                    
                    <button type="button" id="ImageButton5" class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>


  </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table> 
                                                            </div>

                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_4.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row"> 
                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <br /> 
                                                                <div class="panel">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 450px">
                                                                        <br />
                                                                        <br />
                                                                        <br />
                                                                        <%--1--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="CheckBox3" runat="server" OnCheckedChanged="chkLinked_CheckedChanged" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Property Status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="TextBox5" Width="130px" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="DropDownList17" Width="90px" ClientIDMode="Static" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="DropDownList18" Width="90px" ClientIDMode="Static" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="DropDownList19" Width="90px" ClientIDMode="Static" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--2--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="CheckBox4" OnCheckedChanged="CB4_CheckedChanged" runat="server" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Utility Status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="TextBox6" ClientIDMode="Static" Enabled="false" class="form-control" Width="130px" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="DropDownList20" Width="90px" Enabled="false" runat="server">
                                                                                     <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="DropDownList21" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="DropDownList22" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--3--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="CheckBox5" runat="server" OnCheckedChanged="CheckBox5_CheckedChanged" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Work Order Type</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="TextBox7" ClientIDMode="Static" Enabled="false" Width="130px" class="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="DropDownList23" Enabled="false" Width="90px" runat="server">
                                                                                    <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="DropDownList24" Enabled="false" Width="90px" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="DropDownList25" Width="90px" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--4--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="CheckBox6" OnCheckedChanged="CheckBox6_CheckedChanged" runat="server" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Client Status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="TextBox8" ClientIDMode="Static" Enabled="false" Width="130px" class="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="DropDownList26" Width="90px" Enabled="false" runat="server">
                                                                                   <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="DropDownList27" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="DropDownList28" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--5--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="CheckBox7" OnCheckedChanged="CheckBox7_CheckedChanged" runat="server" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Utility Type</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="TextBox9" Width="130px" ClientIDMode="Static" class="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="DropDownList29" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="DropDownList30" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="DropDownList31" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--6--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="CheckBox8" OnCheckedChanged="CheckBox8_CheckedChanged" runat="server" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Receive Date</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="TextBox10" ClientIDMode="Static" Enabled="false" class="form-control" Width="130px" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="DropDownList32" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="DropDownList33" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="DropDownList34" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--7--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="CheckBox9" OnCheckedChanged="CheckBox9_CheckedChanged" runat="server" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Property boarding</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="TextBox11" ClientIDMode="Static" Enabled="false" class="form-control" Width="130px" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="DropDownList35" Width="90px" Enabled="false" runat="server">
                                                                                  <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="DropDownList36" Width="90px" Enabled="false" runat="server">
                                                                                   <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="DropDownList37" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--8--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="CheckBox10" OnCheckedChanged="CheckBox10_CheckedChanged" AutoPostBack="true" runat="server" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Referred Date</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="TextBox12" ClientIDMode="Static" class="form-control" Enabled="false" Width="130px" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="DropDownList38" Width="90px" Enabled="false" runat="server">
                                                                                   <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="DropDownList39" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="DropDownList40" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                    <div class="row">
                                                                                        <div class="col-sm-4"></div>
                                                                        <div class="col-sm-4">
                                                                        <table id="asdasd" onclick="btnback3(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                <button type="button" id="btnback3" class="btn btn-primary">Back</button>

                                                                                 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table></div>
                                                                        <div class="col-sm-4">
                                                                             <table id="asdasd" onclick="btnnext3(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                     <td> 
                                                                                       <button type="button" id="btnnext3" class="btn btn-primary">Next</button>
                                                                                     </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                                        </div>            
                                                                            </div>
                                                                    
                                                                   
                                                                </div>



                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        </div>
                                                <div style="padding-left:3px; display:none;" id="EditPanel5">
                                                        <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="Label68" runat="server"  Text="Edit Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br />
                                                                  <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                    
                    <button type="button" id="ImageButton6" class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>



  </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table> 
                                                            </div>

                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_5.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <br /> 
                                                                <div class="panel">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 450px;">
                                                                        <br />
                                                                        <div class="row" style="padding-left: 60px">
                                                                            <div class="col-sm-3"><b>Select the utility type</b></div>
                                                                            <div class="col-sm-2">
                                                                                <asp:CheckBox ID="CheckBox11" ClientIDMode="Static" runat="server" />&nbsp Gas
                                                                            </div>
                                                                            <div class="col-sm-2" style="padding-left: 40px">
                                                                                <asp:CheckBox ID="CheckBox12" ClientIDMode="Static" runat="server" />
                                                                                &nbsp Water
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:CheckBox ID="CheckBox13" ClientIDMode="Static" runat="server" />
                                                                                &nbsp Electric
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px" >
                                                                            <div class="col-sm-3">
                                                                                <b>Select the USP Name</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                          <%--      <asp:TextBox ID="TextBox1" Enabled="false" runat="server"></asp:TextBox>--%>
                                                                            <%--    <asp:DropDownList ID="DropDownList46" OnSelectedIndexChanged="DropDownList46_SelectedIndexChanged" Width="155px" runat="server" 
                                                                                    DataSourceID="SqlDataSource11" DataTextField="USP_Name" DataValueField="USP_Name" AutoPostBack="True">
                                                                                </asp:DropDownList>--%>
                                                                             <%--   <select class="form-control input-sm" id="immSlct"  >
                                                                                    <option value="DUKE ENERGY">DUKE ENERGY</option>
                                                                                    <option value="NATIONAL GRID">NATIONAL GRID</option>
                                                                                </select>--%>
                                                                               
                                                                                <asp:ListBox ID="lbedit11" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource22" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT DISTINCT [USP_Name] FROM [tbl_UC_USP_Name_Vendor_ID]"></asp:SqlDataSource>


                                                                            </div>

                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the Vendor ID</b>
                                                                            </div>
                                                                            <div class="col-sm-2 control-label" >
                                                                                <%--<asp:DropDownList ID="DropDownList47" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource12" DataTextField="USP_VendorID" DataValueField="USP_VendorID">
                                                                                </asp:DropDownList>--%>
                                                                                 
                                                                                     <asp:ListBox ID="lbedit12" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static" Width="100px" 
                                                                                   ></asp:ListBox> 
                                                                         
                                                                               
                                                                               <%--  WHERE [USP_Name] = 'NATIONAL GRID'--%>
                                                                                   <%--DataSourceID="SqlDataSource121" DataTextField="NameID" DataValueField="Val_ID"--%>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource23" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [USP_VendorID], [USP_Name], [Val_ID], [USP_VendorID] + '-' + [USP_Name] as NameID FROM [tbl_UC_USP_Name_Vendor_ID]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-3">
                                                                                <b>Select the utility status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <%--<asp:DropDownList ID="DropDownList48" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource13" DataTextField="Util_Name" DataValueField="Util_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="lbedit13" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                    ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource24" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Util_Name], [Val_ID] FROM [tbl_UC_Utility_Status]"></asp:SqlDataSource>
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the work order type</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <%--<asp:DropDownList ID="DropDownList49" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource14" DataTextField="Work_Name" DataValueField="Work_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="lbedit14" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                      ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource25" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Work_Name], [Val_ID] FROM [tbl_UC_Work_Order_Type]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-3">
                                                                                <b>Utility status Name</b>
                                                                            </div>
                                                                            <div class="col-sm-2"> 
                                                                                <asp:TextBox ID="TextBox13" ClientIDMode="Static"  class="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the work order value</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:DropDownList ID="DropDownList41" ClientIDMode="Static" Width="109px" runat="server"> 
                                                                                </asp:DropDownList>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 5px">
                                                                            <div class="col-sm-3">
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="TextBox14" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-3">
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the work order status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <%--<asp:DropDownList ID="DropDownList50" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource15" DataTextField="wStatus_Name" DataValueField="wStatus_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="lbedit15" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                    ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource26" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [wStatus_Name], [Val_ID] FROM [tbl_UC_Work_Order_Status]"></asp:SqlDataSource>
                                                                                <br />
                                                                                <br />

                                                                                <asp:Button runat="server" ID="Button11" Text="Add New" class="btn btn-primary" />

                                                                                <!-- ModalPopupExtender -->
                                                                                <cc1:ModalPopupExtender ID="ModalPopupExtender6" runat="server" PopupControlID="Panel8" TargetControlID="btnAddNew"
                                                                                    BackgroundCssClass="modalBackground">
                                                                                </cc1:ModalPopupExtender>
                                                                                <asp:Panel ID="Panel7" runat="server" CssClass="modalPopup2" align="left" Style="display: none; padding-left: 0px; padding-top: 0px">

                                                                                    <div class="panel">
                                                                                        <div class="panel-heading" style="background-color: #4d6578">
                                                                                            <div style="color: white"><b>Add New Utility and Work Item Rule</b></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="panel">
                                                                                        <div class="panel-body" style="background-color: #fff; min-height: 320px">
                                                                                            <br />
                                                                                            <div class="row" style="padding-left: 60px">
                                                                                                <div class="col-sm-3"><b>Select the utility type</b></div>
                                                                                                <div class="col-sm-1">
                                                                                                    <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />&nbsp Gas
                                                                                                </div>
                                                                                                <div class="col-sm-2" style="padding-left: 40px">
                                                                                                    <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />&nbsp Water
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />&nbsp Electric
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                <div class="col-sm-3">
                                                                                                    <b>Select the USP Name</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList42" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                    <b>Select the Vendor ID</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList43" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                <div class="col-sm-3">
                                                                                                    <b>Select the utility status</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList44" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                    <b>Select the work order type</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList45" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                <div class="col-sm-3">
                                                                                                    <b>Utility status Name</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <input type="text" class="form-control" id="usr">
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                    <b>Select the work order value</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList46" Width="109px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 5px">
                                                                                                <div class="col-sm-3">
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                </div>
                                                                                                <div class="col-sm-2">

                                                                                                    <input type="text" class="form-control" id="usr">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                <div class="col-sm-3">
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                    <b>Select the work order status</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList47" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>




                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                <div class="col-sm-3">
                                                                                                    <b>Process this as</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList48" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                    <b>else follow business rule</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="panel-footer" style="background-color: transparent">
                                                                                            <center>
              <asp:Button runat="server"  id="Button12" Text="Done" class="btn btn-primary"/>
         </center>
                                                                                        </div>
                                                                                    </div>
                                                                                    <br />
                                                                                    <br />

                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-2" style="padding-right:3px">
                                                                                <b>Process this as</b>
                                                                            </div>
                                                                            <div class="col-sm-2" style="padding-left:3px">
                                                                                <asp:DropDownList ID="DropDownList49" ClientIDMode="Static" Width="140px" runat="server"> 
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 20px">
                                                                                <b>else follow business rule</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                    <div class="row">
                                                                                        <div class="col-sm-4"></div>
                                                                        <div class="col-sm-4">
                                                                        <table id="asdasd" onclick="btnback3(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                <button type="button" id="btnback4" class="btn btn-primary">Back</button>

                                                                                 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table></div>
                                                                        <div class="col-sm-4">
                                                                             <table id="asdasd" onclick="btnnext4(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                     <td> 
                                                                                       <button type="button" id="btnnext4" class="btn btn-primary">Next</button>
                                                                                     </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                                        </div>            
                                                                            </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        </div>
                                                <div style="padding-left:3px; display:none;" id="EditPanel6">
                                                             <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="Label69" runat="server" Text="Edit Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br />
                                                                   <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                 <button type="button" id="ImageButton7" class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>



  </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table> 
                                                            </div>

                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_6.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                                <br />
                                                                <p><font color="red">&nbsp***Please Review the setting you have edit***</font></p>
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <div class="panel" style="overflow-y: scroll; height: 442px">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 440px">
                                                                        <br />
                                                                        <div class="row">
                                                                            <div class="col-sm-3" style="padding-left: 60px">
                                                                                <div class="panel">
                                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 415px">
                                                                                        <center>
                                                            <br /> <br /> 
                                                        <b>Client Level</b>
                                                            <br />    <br />   <br />    <br />       <br />
                                                            <b>Property/ Preservation Details</b>
                                                            <br /><br /><br /> <br /><br /><br />
                                                            <br /><br /><br />  <br /><br /><br /><br />
                                                            <b>SLA Deatails</b>
                                                            </center>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <div class="panel">
                                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 415px">

                                                                                        <div class="row" style="padding-top: 20px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Location_Service Location</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit1" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide1" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit3" ToolTip="" Enabled="false" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Client Status</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit4" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="TextBox19" ClientIDMode="Static" ToolTip="" Enabled="false" class="form-control" runat="server"></asp:TextBox>

                                                                                            </div>
                                                                                        </div>


                                                                                        <div class="row" style="padding-top: 22px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Property Boarding(In VMS) Date</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit5" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                    <asp:TextBox ID="txtedit6" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                    <asp:TextBox ID="txtedit7" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit8" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Property Active / Inactive</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit9" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit10" ClientIDMode="Static"   Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Property Type</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide12" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit12" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Occupancy Status</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide14" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit14" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Property Status</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide16" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit16" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Property Value</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide17" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit17" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Total Preservation Cost(TPC) incurred</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit18" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="TextBox35" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Inspection Complete</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit19" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="TextBox37" Enabled="false" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Preservation Work Type</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide21" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit21" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 22px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">

                                                                                                <asp:TextBox ID="txtedit22" Enabled="false" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="float: right">

                                                                                                <div class="btn-group">

                                                                                                    <%--<asp:Button ID="Button12" runat="server" Text="1" 
                                                                                 class="btn btn-primary" OnClick="page1_Click"/>
                                                                            <asp:Button ID="Button14" runat="server" Text="1" 
                                                                                 class="btn btn-primary disabled" />
                                                                             <asp:Button ID="Button13" runat="server" Text="2" 
                                                                                 class="btn btn-primary" OnClick="page2_Click"/>--%>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>


                                                                                    </div>
                                                                                </div>



                                                                            </div>


                                                                        </div>
                                                                        <%--------------------Scroll------------------%>
                                                                        <div class="row">
                                                                            <div class="col-sm-3" style="padding-left: 60px">
                                                                                <div class="panel">
                                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 415px">
                                                                                        <center>
                                                            <br /> <br /> 
                                                        <b>Geography</b>
                                                            <br /><br /><br /> <br /><br /><br />
                                                            <br /><br /><br />
                                                            <b>Utility and Work item Details</b>
                                                            <br />
                                                            <asp:Button ID="Button13" class="btn btn-link btn-sm" OnClick="viewMore_Click" runat="server" Text="View More" />
                                                            <br /><br /> <br /><br /><br />
                                                            <br />
                                                             
                                                            </center>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <div class="panel">
                                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 415px">

                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-2" style="padding-left: 30px">
                                                                                                <b>Country</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit23" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="TextBox42" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 30px">
                                                                                                <b>Street name</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit24" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="TextBox44" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-2" style="padding-left: 30px">
                                                                                                <b>State</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide25" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit25" ClientIDMode="Static" Enabled="false" ToolTip="" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 30px">
                                                                                                <b>Number of Units</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit26" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit27" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-2" style="padding-left: 30px">
                                                                                                <b>City</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide28" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit28" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 30px">
                                                                                                <b>POD NAME</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide30" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit30" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-2" style="padding-left: 30px">
                                                                                                <b>Zip Code</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide32" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit32" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>





                                                                                        <div class="row" style="padding-top: 32px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Utility Type</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit34" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit35" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Utility Status</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide36" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit37" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit36" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>

                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>USP NAME</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide39" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit39" Enabled="false" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Vendor ID</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide41" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit41" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Work Order Type</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide43" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit43" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Work Order Value</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide46" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit46" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Work Order Status</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txthide47" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtedit48" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtedit47" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 45px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="float: right">

                                                                                                <div class="btn-group">

                                                                                                    <%--  <asp:Button ID="Button38" runat="server" Text="1" 
                                                                                 class="btn btn-primary" OnClick="page1_Click"/>
                                                                            <asp:Button ID="Button39" runat="server" Text="2" 
                                                                                 class="btn btn-primary disabled" />
                                                                             <asp:Button ID="Button40" runat="server" Text="2" 
                                                                                 class="btn btn-primary" OnClick="page2_Click"/>--%>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                    <div class="row" style="float:right">
                                                                        <div class="col-md-5">
                                                                            </div>
                                                                        <div class="col-md-3" >
                                                                              <button type="button" id="btnback5" class="btn btn-primary">Back</button>
                                                                        </div>
                                                                        <div class="col-md-3" >
                                                                           <table id="SaveEdit" onclick="SaveEdit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                     <asp:Button ID="Button16" runat="server" Text="Save" class="btn btn-primary" ClientIDMode="Static" /> 
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                        </div>
                                                                        <div class="col-md-2"></div>
                                                                    </div>
                                                                   
                                                                   
                                                                    <%--<button id="btnSave" type="button">Save</button>--%>
                                                                    
                                                                                 <%--   <asp:Button ID="Button11" runat="server" Text="ExSave" class="btn btn-primary" OnClick="Button11_Click1"  />--%>

                                                                </div>



                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        </div>
                                                        <br />
                                                 <div id="showdata" style="padding-left: 3px;"> 
                                                        <div class="panel panel-default panel-new">
                                                            <div class="panel-heading propertyhead" style="background-color: #4a7bb9; padding-bottom: 5px">
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="padding-right: 0px; color: white">
                                                                        <h6 style="margin: 0px;"><b>keep this rule as</b></h6>
                                                                    </div>
                                                                    <div class="col-sm-1" style="padding-left: 0px">
                                                         
                                                                        <div class="switch">
                                                                            <input id="cmn-toggle-1"  class="cmn-toggle cmn-toggle-round" type="checkbox">
                                                                            <label for="cmn-toggle-1"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>




                                                            </div>
                                                            <br />
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="Label7" CssClass="vms" runat="server" Text="Label">Service Location
                                <div style="float:right">:</div>
                                                                        </asp:Label>
                                                                    </div>
                                                                    <div class="col-sm-2" style="padding-left: 10px">
                                                                        <%-- <input type="text" class="form-control form-group-sm" />--%>
                                                                        <asp:TextBox ID="txtSeclocVal" Font-Size="12px" runat="server" class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="Label8" CssClass="vms" runat="server" Text="Label">Country
                                                                             <div style="float:right">:</div>
                                                                        </asp:Label>
                                                                    </div>
                                                                    <div class="col-sm-2" style="padding-left: 10px">
                                                                            <asp:TextBox ID="txtCountVal" Font-Size="12px" runat="server" class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                    
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="Label9" runat="server" Text="Label" Style="text-align: left;">Select the Street Name 
                                                                                    <div style="float:right">:</div>
                                                                        </asp:Label>
                                                                    </div>
                                                                    <div class="col-sm-2" style="padding-left: 10px; width: 130px">
                                                                         <asp:TextBox ID="txtStreetName" Font-Size="12px" runat="server" class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-top: 5px">
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="Label16" runat="server" Text="Label">Client Status
                                                                                             <div style="float:right">:</div>
                                                                          </asp:Label>
                                                                    </div>
                                                                    <div class="col-sm-2" style="padding-left: 10px">
                                                                        <asp:TextBox ID="txtCStatus" Font-Size="12px" runat="server" class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="Label18" runat="server" Text="Label">Select the State
                                                                            <div style="float:right">:</div>
                                                                        </asp:Label>
                                                                    </div>
                                                                    <div class="col-sm-2" style="padding-left: 10px">
                                                                        <asp:TextBox ID="txtStatVal" Font-Size="12px" runat="server" class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-top: 5px">
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="Label10" runat="server" Text="Label">POD
                                    <div style="float:right">:</div>
                                                                        </asp:Label><br />
                                                                    </div>
                                                                    <div class="col-sm-2" style="padding-left: 10px">
                                                                        <asp:TextBox ID="txtpods" Font-Size="12px" runat="server" class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="Label11" runat="server" Text="Label">City
                                    <div style="float:right">:</div>
                                                                        </asp:Label><br />
                                                                    </div>
                                                                    <div class="col-sm-2" style="padding-left: 10px">
                                                                        <asp:TextBox ID="txtCitVal" Font-Size="12px" runat="server" class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="Label12" runat="server" Text="Label">Number Of Units
                                    <div style="float:right">:</div>
                                                                        </asp:Label><br />
                                                                    </div>
                                                                    <div class="col-sm-2" style="padding-left: 10px; width: 130px">
                                                                        <asp:TextBox ID="txtNU" Font-Size="12px" runat="server" class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-top: 5px">
                                                                    <div class=" col-sm-4">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="Label13" runat="server" Text="Label">   Select The Zip Code<div style="float:right">:</div></asp:Label><br />
                                                                    </div>

                                                                    <div class="col-sm-2" style="padding-left: 10px; padding-bottom: 15px">
                                                                        <asp:TextBox ID="txtZcode" Font-Size="12px" runat="server" class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel-New123">

                                                                <div class="panel-heading" style="height: 45px; align-content: center; padding-top: 2px; padding-bottom: 2px">


                                                                    <nav class="navbar navbar-inverse" style="background-color: transparent; border-color: transparent">
                                                                        <div class="row">

                                                                            <div class="col-sm-1"></div>
                                                                            <div class="col-sm-9" style="height: 20px">
                                                                                <ul class="nav navbar-nav" style="height: 30px; align-content: center">
                                                                                    <p class="navbar-text">|</p>
                                                                                    <li class="active"><a data-toggle="tab" href="#home">Property Preservation Details</a></li>
                                                                                    <p class="navbar-text">|</p>
                                                                                    <li><a data-toggle="tab" href="#sched">Utility and Work Item Details</a></li>
                                                                                    <p class="navbar-text">|</p>
                                                                                    <li><a data-toggle="tab" href="#sla">SLA Details</a></li>
                                                                                    <p class="navbar-text">|</p>
                                                                                </ul>
                                                                            </div>

                                                                        </div>

                                                                    </nav>



                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="tab-content">
                                                                        <div id="home" class="tab-pane fade in active">
                                                                            <div class="panel panel-default">
                                                                                <div class="panel-body -bodyCustom" style="height: 400px; background-color: #f1efef">
                                                                                    <br />
                                                                                    <div class="row" style="padding-left: 10px; padding-top: 30px">
                                                                                        <div class="col-sm-3">
                                                                                            <asp:Label ID="vms" CssClass="vms" runat="server" Text="Label">VMS Board Date
                                                            <div style="float:right">:</div>
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                        <div class="col-xs-2">
                                                                                            <asp:TextBox ID="txtVMSbd" Font-Size="12px" runat="server" 
                                                                                                class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-left: 0px">
                                                                                            <asp:TextBox ID="txtBus" Font-Size="12px" runat="server" 
                                                                                                class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-left: 0px">
                                                                                            <asp:TextBox ID="txtcal" Font-Size="12px" runat="server" 
                                                                                                class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row" style="padding-left: 10px; padding-top: 5px">
                                                                                        <div class="col-sm-3">
                                                                                            <asp:Label ID="Label14" CssClass="vms" runat="server" Text="Label">Is The Property Active Or Inactive?
                                                            <div style="float:right">:</div>
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                        <div class="col-xs-2">
                                                                                            <asp:TextBox ID="txtisActive" Font-Size="12px" runat="server" 
                                                                                                class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                                        </div>
                                                                                        <div class="col-md-3">
                                                                                            <asp:Label ID="Label15" CssClass="vms" runat="server" Text="Label">Select The Property Type
                                                            <div style="float:right">:</div>
                                                                                            </asp:Label>

                                                                                        </div>
                                                                                        <div class="col-xs-2">
                                                                                            <asp:TextBox ID="txtPtype" Font-Size="12px" runat="server" 
                                                                                                class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row" style="padding-left: 10px; padding-top: 5px">
                                                                                        <div class="col-sm-3">
                                                                                            <asp:Label ID="Label17" CssClass="vms" runat="server" Text="Label">Select the occupancy Status
                                                            <div style="float:right">:</div>
                                                                                            </asp:Label>

                                                                                        </div>

                                                                                        <div class="col-xs-2">
                                                                                            <asp:TextBox ID="txtOstatus" Font-Size="12px" runat="server" 
                                                                                                class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                                        </div>
                                                                                        <div class="col-md-3">
                                                                                            <asp:Label ID="Label19" CssClass="vms" runat="server" Text="Label">Select the Property Status
                                                            <div style="float:right">:</div>
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                        <div class="col-xs-2">
                                                                                            <asp:TextBox ID="txtPstatus" Font-Size="12px" runat="server" 
                                                                                                class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row" style="padding-left: 10px; padding-top: 5px">
                                                                                        <div class="col-sm-3">
                                                                                            <asp:Label ID="Label20" CssClass="vms" runat="server" Text="Label">What is the property Value
                                                            <div style="float:right">:</div>
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                        <div class="col-xs-2"> 
                                                                                            <asp:DropDownList ID="comb1" runat="server" Enabled="false"     ClientIDMode="Static">
                                                                                                <asp:ListItem>Greater Than</asp:ListItem>
                                                                                                <asp:ListItem>Less Than</asp:ListItem>
                                                                                                <asp:ListItem>Equal</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                        <div class="col-md-2">
                                                                                            <asp:TextBox ID="txtProVal" Font-Size="12px" runat="server" 
                                                                                                class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row" style="padding-left: 10px; padding-top: 5px">
                                                                                        <div class="col-sm-3">
                                                                                            <asp:Label ID="Label21" CssClass="vms" runat="server" Text="Label">What is the total preservation cost
                                                            <div style="float:right">:</div>
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                        <div class="col-sm-2">
                                                                                            <asp:DropDownList ID="comb2" runat="server" Enabled="false"  ClientIDMode="Static">
                                                                                                <asp:ListItem>Greater Than</asp:ListItem>
                                                                                                <asp:ListItem>Less Than</asp:ListItem>
                                                                                                <asp:ListItem>Equal</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                        <div class="col-sm-2">
                                                                                            <asp:TextBox ID="txtTprecost" Font-Size="12px" runat="server" 
                                                                                                class="form-control form-group-sm" ClientIDMode="Static"></asp:TextBox>
                                                                                        </div>
                                                                                        <div class="col-sm-3">
                                                                                            <asp:Label ID="Label22" CssClass="vms" runat="server" Text="Label">What is the total preservation cost
                                                            <div style="float:right">:</div>
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                        <div class="col-sm-2" style="width: 130px">
                                                                                            <input type="text" class="form-control form-group-sm" />
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row" style="padding-left: 10px; padding-top: 5px">
                                                                                        <div class="col-sm-3">
                                                                                            <asp:Label ID="Label23" CssClass="vms" runat="server" Text="Label">Is the inspection completed
                                                            <div style="float:right">:</div>
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                        <div class="col-sm-1">
                                                                                            <input type="checkbox" id="cbyes" name="vehicle2" value="Car">
                                                                                            &nbsp Yes
                                                                                        </div>
                                                                                        <div class="col-sm-2">
                                                                                            <input type="checkbox" id="cbno" name="vehicle2" value="Car">&nbsp No
                                                                                        </div>
                                                                                        <div class="col-sm-2"></div>
                                                                                        <div class="col-sm-2">
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row" style="padding-left: 10px; padding-top: 5px">
                                                                                        <div class="col-lg-5">
                                                                                            <asp:Label ID="Label24" CssClass="vms" runat="server" Text="Label">Inspection Completed Date greater than or equal to
                                                            <div style="float:right">:</div>
                                                                                            </asp:Label>

                                                                                        </div>
                                                                                        <div class="col-sm-2">
                                                                                            <input type="text" class="form-control form-group-lg" />
                                                                                        </div>
                                                                                        <div class="col-xs-2">
                                                                                            <select class="greaterthan">
                                                                                                <option value="volvo">Greater Than</option>
                                                                                                <option value="saab">Less Thans</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="col-sm-2">
                                                                                            <select>
                                                                                                <option value="volvo">Greater Than</option>
                                                                                                <option value="saab">Less Thans</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <br />
                                                                                    <br />
                                                                                    <div class="row">
                                                                                        <div class="col-sm-10"></div>
                                                                                        <div class="col-sm-2">
                                                                                            <br />
                                                                                            <br />
                                                                                            <br />
                                                                                            <br />
                                                                                            <br />
                                                                                            <br />
                                                                                             <table id="EditThis" onclick="Edit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                      <button type="button" class="btn btn-primary btn-sm">Edit</button>
                                                                                  </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>


                                                                                          

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="sched" class="tab-pane fade">
                                                                            <div class="col-sm-2" style="padding-left: 0px; padding-right: 0px; padding-top: 0px">


                                                                                <div class="panel">
                                                                                    <div class="panel-heading" style="background-color: #274761; font-size: 12px; color: white">
                                                                                        <center>
                               Utility Status Name
                           </center>
                                                                                    </div>
                                                                                    <div class="panel-body" style="min-height: 310px">
                                                                                    </div>
                                                                                    <div class="panel-footer" style="background-color: white">

                                                                                        <center> 
                                    <asp:Button runat="server"  id="Button35" Text="Add New" class="btn btn-primary btn-sm"/>
                         </center>
                                                                                        <!-- ModalPopupExtender -->
                                                                                        <cc1:ModalPopupExtender ID="ModalPopupExtender" runat="server" PopupControlID="Panel9" TargetControlID="Button35"
                                                                                            BackgroundCssClass="modalBackground">
                                                                                        </cc1:ModalPopupExtender>
                                                                                        <asp:Panel ID="Panel9" runat="server" CssClass="modalPopup2" align="left" Style="display: none; padding-left: 0px; padding-top: 0px">

                                                                                            <div class="panel">
                                                                                                <div class="panel-heading" style="background-color: #4d6578">
                                                                                                    <div style="color: white"><b>Add New Utility and Work Item Rule</b></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="panel">
                                                                                                <div class="panel-body" style="background-color: #fff; min-height: 320px">
                                                                                                    <br />
                                                                                                    <div class="row" style="padding-left: 60px">
                                                                                                        <div class="col-sm-3"><b>Select the utility type</b></div>
                                                                                                        <div class="col-sm-1">
                                                                                                            <input type="checkbox" id="cbgas" class="chk1 iCheck" data-id="11" />&nbsp Gas
                                                                                                        </div>
                                                                                                        <div class="col-sm-2" style="padding-left: 40px">
                                                                                                            <input type="checkbox" id="cbwater" class="chk1 iCheck" data-id="11" />&nbsp Water
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                            <input type="checkbox" id="cbelec" class="chk1 iCheck" data-id="11" />&nbsp Electric
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                        <div class="col-sm-3">
                                                                                                            <b>Select the USP Name</b>
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                            <asp:DropDownList ID="DropDownList64" Width="120px" runat="server">
                                                                                                                <asp:ListItem>1</asp:ListItem>
                                                                                                                <asp:ListItem>2</asp:ListItem>
                                                                                                                <asp:ListItem>3</asp:ListItem>
                                                                                                                <asp:ListItem>4</asp:ListItem>
                                                                                                                <asp:ListItem>5</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </div>
                                                                                                        <div class="col-sm-4" style="padding-left: 40px">
                                                                                                            <b>Select the Vendor ID</b>
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                            <asp:DropDownList ID="DropDownList65" Width="120px" runat="server">
                                                                                                                <asp:ListItem>1</asp:ListItem>
                                                                                                                <asp:ListItem>2</asp:ListItem>
                                                                                                                <asp:ListItem>3</asp:ListItem>
                                                                                                                <asp:ListItem>4</asp:ListItem>
                                                                                                                <asp:ListItem>5</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                        <div class="col-sm-3">
                                                                                                            <b>Select the utility status</b>
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                            <asp:DropDownList ID="DropDownList66" Width="120px" runat="server">
                                                                                                                <asp:ListItem>1</asp:ListItem>
                                                                                                                <asp:ListItem>2</asp:ListItem>
                                                                                                                <asp:ListItem>3</asp:ListItem>
                                                                                                                <asp:ListItem>4</asp:ListItem>
                                                                                                                <asp:ListItem>5</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </div>
                                                                                                        <div class="col-sm-4" style="padding-left: 40px">
                                                                                                            <b>Select the work order type</b>
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                            <asp:DropDownList ID="DropDownList67" Width="120px" runat="server">
                                                                                                                <asp:ListItem>1</asp:ListItem>
                                                                                                                <asp:ListItem>2</asp:ListItem>
                                                                                                                <asp:ListItem>3</asp:ListItem>
                                                                                                                <asp:ListItem>4</asp:ListItem>
                                                                                                                <asp:ListItem>5</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                        <div class="col-sm-3">
                                                                                                            <b>Utility status Name</b>
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                            <input type="text" class="form-control" id="usr">
                                                                                                        </div>
                                                                                                        <div class="col-sm-4" style="padding-left: 40px">
                                                                                                            <b>Select the work order value</b>
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                            <asp:DropDownList ID="DropDownList68" Width="109px" runat="server">
                                                                                                                <asp:ListItem>1</asp:ListItem>
                                                                                                                <asp:ListItem>2</asp:ListItem>
                                                                                                                <asp:ListItem>3</asp:ListItem>
                                                                                                                <asp:ListItem>4</asp:ListItem>
                                                                                                                <asp:ListItem>5</asp:ListItem>
                                                                                                            </asp:DropDownList>

                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row" style="padding-left: 60px; padding-top: 5px">
                                                                                                        <div class="col-sm-3">
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                        </div>
                                                                                                        <div class="col-sm-4" style="padding-left: 40px">
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">

                                                                                                            <input type="text" class="form-control" id="usr">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                        <div class="col-sm-3">
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                        </div>
                                                                                                        <div class="col-sm-4" style="padding-left: 40px">
                                                                                                            <b>Select the work order status</b>
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                            <asp:DropDownList ID="DropDownList69" Width="120px" runat="server">
                                                                                                                <asp:ListItem>1</asp:ListItem>
                                                                                                                <asp:ListItem>2</asp:ListItem>
                                                                                                                <asp:ListItem>3</asp:ListItem>
                                                                                                                <asp:ListItem>4</asp:ListItem>
                                                                                                                <asp:ListItem>5</asp:ListItem>
                                                                                                            </asp:DropDownList>




                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                        <div class="col-sm-3">
                                                                                                            <b>Process this as</b>
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                            <asp:DropDownList ID="DropDownList70" Width="120px" runat="server">
                                                                                                                <asp:ListItem>1</asp:ListItem>
                                                                                                                <asp:ListItem>2</asp:ListItem>
                                                                                                                <asp:ListItem>3</asp:ListItem>
                                                                                                                <asp:ListItem>4</asp:ListItem>
                                                                                                                <asp:ListItem>5</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </div>
                                                                                                        <div class="col-sm-4" style="padding-left: 40px">
                                                                                                            <b>else follow business rule</b>
                                                                                                        </div>
                                                                                                        <div class="col-sm-2">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="panel-footer" style="background-color: transparent">
                                                                                                    <center>
              <asp:Button runat="server"  id="Button36" Text="Save" class="btn btn-primary"/>
         </center>
                                                                                                </div>
                                                                                            </div>
                                                                                            <br />
                                                                                            <br />

                                                                                        </asp:Panel>









                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                            <div class="col-sm-10" style="padding-left: 0px">
                                                                                <div class="w3-container panel-New">
                                                                                    <div class="panel-body -bodyCustom bod" style="height: 400px; background-color: #f1efef">
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 30px">
                                                                                            <div class="col-sm-3">
                                                                                                <asp:Label ID="Label25" CssClass="vms" runat="server" Text="Label">Select Utility Type <div style="float:right">:</div></asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="checkbox" name="Gas" id="cbgasAA" value="Gas">Gas
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="checkbox" name="Water" id="cbWaterAA" value="Water">Water
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <input type="checkbox" name="Electric" id="cbelecAA" value="Electric">Electric
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 4px">
                                                                                            <div class="col-sm-3">
                                                                                                <asp:Label ID="Label26" CssClass="vms" runat="server" Text="Label">Select the USP Name <div style="float:right">:</div></asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <asp:Label ID="Label27" CssClass="vms text-center" runat="server" Text="Label">Select the Vendor ID <div style="float:right">:</div></asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="width: 165px">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 4px">
                                                                                            <div class="col-sm-3">
                                                                                                <asp:Label ID="Label28" CssClass="vms" runat="server" Text="Label">Select the utility status <div style="float:right">:</div></asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <asp:Label ID="Label29" CssClass="vms text-center" runat="server" Text="Label">Select the work order type
                      <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="width: 165px">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 4px">
                                                                                            <div class="col-sm-3">
                                                                                                <asp:Label ID="Label30" CssClass="vms" runat="server" Text="Label">Utility Status Name <div style="float:right">:</div></asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <asp:Label ID="Label31" CssClass="vms text-center" runat="server" Text="Label">Select the work order Value
                      <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="width: 165px">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 4px">
                                                                                            <div class="col-sm-3">
                                                                                            </div>
                                                                                            <div class="col-sm-3"></div>
                                                                                            <div class="col-sm-3">
                                                                                                <asp:Label ID="Label32" CssClass="vms text-center" runat="server" Text="Label">Select the work order Status
                                       <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="width: 165px">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 4px">
                                                                                            <div class="col-sm-3">
                                                                                                <asp:Label ID="Label33" CssClass="vms" runat="server" Text="Label">Process this  as
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-8">
                                                                                                <select class="comb2">
                                                                                                    <option value="volvo">online(Side by side)</option>
                                                                                                    <option value="saab">Less Than</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-10"></div>
                                                                                            <div class="col-sm-2">
                                                                                                <br />
                                                                                                <br />
                                                                                                <br />
                                                                                                <br />
                                                                                                <br />
                                                                                                <br />
                                                                                                <br />
                                                                                                <br />
                                                                                                <br />
                                                                                                <br />
                                                                                                <br />



                                                                                                <button type="button" class="btn btn-primary btn-sm">Edit</button>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div id="sla" class="tab-pane fade">
                                                                            <div class="col-sm-12">
                                                                                <div class="w3-container panel-New">
                                                                                    <div class="panel-body -bodyCustom bod" style="height: 400px; background-color: #f1efef">
                                                                                        <div class="row" style="padding-top: 20px; padding-left: 20px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label34" CssClass="vms" runat="server" Text="Label">Property boarding
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label37" CssClass="vms" runat="server" Text="Label">From
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 5px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label35" CssClass="vms" runat="server" Text="Label">Inspection Time
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label36" CssClass="vms" runat="server" Text="Label">From
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 5px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label38" CssClass="vms" runat="server" Text="Label">Utility Status
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label39" CssClass="vms" runat="server" Text="Label">From
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 5px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label40" CssClass="vms" runat="server" Text="Label">Work Order Type
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label41" CssClass="vms" runat="server" Text="Label">From
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 5px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label42" CssClass="vms" runat="server" Text="Label">Geography
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label43" CssClass="vms" runat="server" Text="Label">From
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 5px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label44" CssClass="vms" runat="server" Text="Label">Client Status
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label45" CssClass="vms" runat="server" Text="Label">From
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 5px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label46" CssClass="vms" runat="server" Text="Label">Property Type
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label47" CssClass="vms" runat="server" Text="Label">From
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row"></div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 5px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label48" CssClass="vms" runat="server" Text="Label">Property Value
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label49" CssClass="vms" runat="server" Text="Label">From
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 5px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label50" CssClass="vms" runat="server" Text="Label">Utility Status
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label51" CssClass="vms" runat="server" Text="Label">From
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 5px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label52" CssClass="vms" runat="server" Text="Label">Utility Type
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label53" CssClass="vms" runat="server" Text="Label">From
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 5px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label54" CssClass="vms" runat="server" Text="Label">Receive Date
                                  <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label55" CssClass="vms" runat="server" Text="Label">From
                                                                                             <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-left: 20px; padding-top: 5px">
                                                                                            <div class="col-sm-2">
                                                                                                <asp:Label ID="Label56" CssClass="vms" runat="server" Text="Label">Property Boarding
                                                                                     <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                            <div class="col-sm-1">
                                                                                                <asp:Label ID="Label57" CssClass="vms" runat="server" Text="Label">From
                                                                                                 <div style="float:right">:</div>
                                                                                                </asp:Label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="text" class="form-control form-group-sm" />
                                                                                            </div>
                                                                                        </div>

                                                                                        <br />
                                                                                        <br />
                                                                                        <br />
                                                                                        <div class="row">
                                                                                            <div class="col-sm-10"></div>
                                                                                            <div class="col-sm-2">
                                                                                                <button type="button" class="btn btn-primary btn-sm">Edit</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                            </div>
                                                <div id="Step1" style="padding-left: 3px; display: none;">
                                                    <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="lbladdnew" runat="server" Text="Add New Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br />
                                                               <%-- <asp:ImageButton ID="btnShow" runat="server" AlternateText="ImageButton 1" ImageUrl="img/close.png"
                                                                    OnClick="ImageButton_Click" Width="25px" Height="25px" />--%>
                                                                <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                     <button type="button"  class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>
                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                            </div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_1.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <br />
                                                                <div class="panel">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 450px">
                                                                        <br />
                                                                        <br />
                                                                        <br />
                                                                        <div class="row">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Name</b> 
                                                                            </div>
                                                                            <div class="col-md-4">

                                                                                <asp:TextBox ID="txtName" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Service Location</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <asp:ListBox ID="ListBox2" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                    DataSourceID="SqlDataSource1" DataTextField="Service_Name" DataValueField="Val_ID"> 
                                                                                </asp:ListBox>
                                                                               <%-- <asp:DropDownList ID="DropDownList1" ClientIDMode="Static" Width="250px" runat="server"
                                                                                     DataSourceID="SqlDataSource1" DataTextField="Service_Name" DataValueField="Service_Name">
                                                                                </asp:DropDownList>--%>
                                                                              
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Service_Name], [Val_ID] FROM [tbl_UC_Service_Location]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Client Status</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                               <%-- <asp:DropDownList ID="DropDownList2" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"  ClientIDMode="Static" Width="250px" runat="server" DataSourceID="SqlDataSource2"
                                                                                    DataTextField="Client_Name" DataValueField="Client_Name" multiple="multiple" size="2" AutoPostBack="True">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox1" SelectionMode="Multiple"  runat="server"  ClientIDMode="Static" Width="250px"  DataSourceID="SqlDataSource2"
                                                                                    DataTextField="Client_Name" DataValueField="Client_Name" multiple="multiple" ></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Client_Name] FROM [tbl_UC_Client_Status]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                    <table onclick="btnstep1(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td> 
                                                                                       <button type="button" class="btn btn-primary">Next</button>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <%--<asp:Button ID="Button1" runat="server" Text="Next" class="btn btn-primary" OnClick="Button1_Click" ClientIDMode="Static" />--%>
                                                                </div> 
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                </div>
                                                <div id="Step2" style="padding-left: 3px; display: none;">
                                                    <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="Label1" runat="server" Text="Add New Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br />
                                                               <%-- <asp:ImageButton runat="server" ID="btnShow2" AlternateText="ImageButton 1" ImageUrl="img/close.png"
                                                                    OnClick="ImageButton_Click" Width="25px" Height="25px" />--%>
                                                                <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                     <button type="button"   class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>
                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                            </div>

                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_2.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <br />

                                                                <div class="panel">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 450px">
                                                                        <br />
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Country</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <asp:DropDownList  ID="DropDownList3" ClientIDMode="Static"  Width="250px" runat="server">
                                                                                    <asp:ListItem>USA</asp:ListItem>
                                                                                </asp:DropDownList>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>State</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                               <%-- <asp:DropDownList ID="DropDownList4" Width="250px" runat="server" 
                                                                                    DataSourceID="SqlDataSource3" DataTextField="State" DataValueField="State">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox3" runat="server" SelectionMode="Multiple" ClientIDMode="Static"
                                                                                    multiple="multiple" DataSourceID="SqlDataSource3" DataTextField="State" DataValueField="State"></asp:ListBox>

                                                                              
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource3" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT DISTINCT [State] FROM [tbl_UC_State_City_Zip]"></asp:SqlDataSource>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>City</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                             <%--   <asp:DropDownList ID="DropDownList5" Width="250px" runat="server" 
                                                                                    DataSourceID="SqlDataSource4" DataTextField="City" DataValueField="City">
                                                                                </asp:DropDownList>--%> 
                                                                                <asp:ListBox ID="ListBox4" runat="server" SelectionMode="Multiple" ClientIDMode="Static"
                                                                                    DataSourceID="SqlDataSource4" DataTextField="City" DataValueField="City"></asp:ListBox>  
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource4" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT DISTINCT[City] FROM [tbl_UC_State_City_Zip]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Zip Code</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                               <%-- <asp:DropDownList ID="DropDownList6" Width="250px" runat="server" 
                                                                                    DataSourceID="SqlDataSource5" DataTextField="Zip" DataValueField="Zip">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox5" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                    DataSourceID="SqlDataSource5" DataTextField="Zip" DataValueField="Val_ID"></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource5" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Zip], [Val_ID]  FROM [tbl_UC_State_City_Zip]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Street Name</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <asp:DropDownList ID="DropDownList7" ClientIDMode="Static" Width="250px" runat="server">
                                                                                    <asp:ListItem>ALL</asp:ListItem>
                                                                                   
                                                                                </asp:DropDownList>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>Number of Unit</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <asp:DropDownList ID="DropDownList8" Width="250px" ClientIDMode="Static" runat="server">
                                                                                    <asp:ListItem>ALL</asp:ListItem>
                                                                                    
                                                                                </asp:DropDownList>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-4" style="padding-left: 150px">
                                                                                <b>PODS</b>

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <%--<asp:DropDownList ID="DropDownList10" Width="250px" runat="server"
                                                                                     DataSourceID="SqlDataSource6" DataTextField="POD_Name" DataValueField="POD_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox6" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                     DataSourceID="SqlDataSource6" DataTextField="POD_Name" DataValueField="POD_Name"></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource6" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [POD_Name] FROM [tbl_UC_PODS]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                    <div class="row">
                                                                                        <div class="col-sm-4"></div>
                                                                        <div class="col-sm-4">
                                                                            <table  onclick="btnstepB1(this)">
                                                                                <thead>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button type="button" id="btnstepB1" class="btn btn-primary">Back</button>

                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table></div>
                                                                        <div class="col-sm-4">
                                                                            <table  onclick="btnstep2(this)">
                                                                                <thead>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button type="button" id="btnstep2" class="btn btn-primary">Next</button>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                                        </div>            
                                                                            </div>
                                                       <%--             <asp:Button ID="Button3" runat="server" Text="Back" class="btn btn-primary" OnClick="Button3_Click" />
                                                                    <asp:Button ID="Button2" runat="server" Text="Next" class="btn btn-primary" OnClick="Button2_Click" />--%>
                                                                </div>



                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                </div>
                                                <div id="Step3" style="padding-left: 3px; display: none;">
                                                    <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="Label2" runat="server"  Text="Add New Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br />
                                                                <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                     <button type="button"   class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>
                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                               <%-- <asp:ImageButton runat="server" ID="btnShow3" AlternateText="ImageButton 1" ImageUrl="img/close.png"
                                                                    OnClick="ImageButton_Click" Width="25px" Height="25px" />--%>

                                                            </div>

                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_3.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <br />

                                                                <div class="panel">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 450px">
                                                                        <br />
                                                                        <%--1--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-3" style="padding-left: 80px">
                                                                                <b>VMS Board Date</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <%--<form action="/action_page.php">
                                                                                    <asp:TextBox ID="txtDatePicker" type="date" name="bday" runat="server"></asp:TextBox> 
                                                                                </form>--%>
                                                                                <asp:TextBox ID="txtVMS" ClientIDMode="Static" Height="28px" runat="server" class="form-control "  ></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-md-2"  style="padding-left:2px; width:125px">
                                                                                <asp:DropDownList ID="ddlVMS_BC" ClientIDMode="Static" Width="120px" runat="server" >
                                                                                    <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-md-2" style="padding-left:2px">
                                                                                <asp:DropDownList ID="ddlVMSday" ClientIDMode="Static"  Width="120px" runat="server"  >
                                                                                    <asp:ListItem>Day</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Year</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--2--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>Is the property Active or Inactive?</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <asp:DropDownList ID="DropDownList12" ClientIDMode="Static"   Width="120px" runat="server"  >
                                                                                    <asp:ListItem>ACTIVE</asp:ListItem>
                                                                                    <asp:ListItem>INACTIVE</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <b>Select the Property Type</b>
                                                                               <%-- <asp:DropDownList ID="DropDownList18" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource7" DataTextField="Pro_Name" DataValueField="Pro_Name" Font-Overline="False">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox7" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                    DataSourceID="SqlDataSource7" DataTextField="Pro_Name" DataValueField="Val_ID" Font-Overline="False"></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource7" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Pro_Name], [Val_ID] FROM [tbl_UC_Property_Type]"></asp:SqlDataSource>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        <%--3--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>Select the Occupancy Status</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <%--<asp:DropDownList ID="DropDownList13" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource8" DataTextField="Occu_Name" DataValueField="Occu_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox8" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                     DataSourceID="SqlDataSource8" DataTextField="Occu_Name" DataValueField="Val_ID"></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource8" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Occu_Name], [Val_ID] FROM [tbl_UC_Occupancy_Status]"></asp:SqlDataSource>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <b>Select the Property Status</b>
                                                                                <%--<asp:DropDownList ID="DropDownList19" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource9" DataTextField="Property_Name" DataValueField="Property_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox9" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                   DataSourceID="SqlDataSource9" DataTextField="Property_Name" DataValueField="Val_ID"></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource9" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Property_Name], [Val_ID] FROM [tbl_UC_Property_Status]"></asp:SqlDataSource>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        <%--4--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>What is the property value</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <asp:DropDownList ID="DropDownList14" Width="120px" ClientIDMode="Static" runat="server">
                                                                                    <asp:ListItem>Greater Than</asp:ListItem>
                                                                                    <asp:ListItem>Less Than</asp:ListItem>
                                                                                    <asp:ListItem>Equal To</asp:ListItem>

                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-md-2" style="padding-left: 0px">
                                                                                <asp:TextBox ID="txtProVal1" Height="28px" class="form-control " ClientIDMode="Static" Text="$" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        <%--5--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>What is the total preservation cost</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <asp:DropDownList ID="DropDownList15" Width="120px" ClientIDMode="Static" runat="server">
                                                                                    <asp:ListItem>Greater Than</asp:ListItem>
                                                                                    <asp:ListItem>Less Than</asp:ListItem>
                                                                                    <asp:ListItem>Equal To</asp:ListItem>
                                                                                </asp:DropDownList>

                                                                            </div>
                                                                            <div class="col-md-2" style="padding-left: 0px">
                                                                                <asp:TextBox ID="txtPreCost" Height="28px" class="form-control " ClientIDMode="Static" Text="$" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        <%--6--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>Select the preservation work type</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <%--<asp:DropDownList ID="DropDownList20" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource10" DataTextField="Work_Name" DataValueField="Work_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox10" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                    DataSourceID="SqlDataSource10" DataTextField="Work_Name" DataValueField="Val_ID"></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource10" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Work_Name], [Val_ID] FROM [tbl_UC_Preservation_Work_Type]"></asp:SqlDataSource>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        <%--7--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>Is the inspection completed?</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <asp:CheckBox ID="CheckBox14" ClientIDMode="Static"   runat="server" />
                                                                                &nbsp Yes
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:CheckBox ID="CheckBox15" ClientIDMode="Static" Checked="true"  runat="server" />&nbsp No
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>
                                                                        <%--8--%>
                                                                        <div class="row" style="padding-top: 10px">
                                                                            <div class="col-md-5" style="padding-left: 80px">
                                                                                <b>Inspection Completed Date greater than or equal to</b>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <asp:TextBox ID="txtComplete" ClientIDMode="Static" Enabled="false" Width="120px" Height="28px" class="form-control " Text="$" runat="server"></asp:TextBox>
                                                                            </div> 
                                                                           <%-- <div class="col-md-3">
                                                                                <form action="/action_page.php">
                                                                                    <asp:TextBox ID="txtCompleteDate" Enabled="false" Width="170px" Height="28px" class="form-control " type="date" name="bday" runat="server"></asp:TextBox>
 
                                                                                </form>
                                                                            </div>--%>
                                                                            <div class="col-md-2" style="padding-left:2px; width:125px">
                                                                                <asp:DropDownList ID="ddlComplete_BC" Enabled="false" ClientIDMode="Static"  Width="120px" runat="server" >
                                                                                    <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-md-2" style="padding-left:2px">
                                                                                <asp:DropDownList ID="ddlComplete_DMY" Enabled="false" ClientIDMode="Static" Width="120px" runat="server"  >
                                                                                    <asp:ListItem>Day</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Year</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                    <div class="row">
                                                                                        <div class="col-sm-4"></div>
                                                                        <div class="col-sm-4">
                                                                            <table  onclick="btnstepB2(this)">
                                                                                <thead>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button type="button" id="btnstepB2" class="btn btn-primary">Back</button>

                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table></div>
                                                                        <div class="col-sm-4">
                                                                            <table  onclick="btnstep3(this)">
                                                                                <thead>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button type="button" id="btnstep3" class="btn btn-primary">Next</button>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                                        </div>            
                                                                            </div>
                                                                    <%--<asp:Button ID="Button4" runat="server" Text="Back" class="btn btn-primary" OnClick="Button4_Click" />
                                                                    <asp:Button ID="Button5" runat="server" Text="Next" class="btn btn-primary" OnClick="Button5_Click" />--%>
                                                                </div>



                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                </div>
                                                <div id="Step4" style="padding-left: 3px; display: none;">
                                                    <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="Label3" runat="server" Text="Add New Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br />
                                                                <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                     <button type="button"   class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>
                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                               <%-- <asp:ImageButton runat="server" ID="btnShow4" AlternateText="ImageButton 1" ImageUrl="img/close.png"
                                                                    OnClick="ImageButton_Click" Width="25px" Height="25px" />--%>

                                                            </div>

                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_4.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row"> 
                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <br /> 
                                                                <div class="panel">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 450px">
                                                                        <br />
                                                                        <br />
                                                                        <br />
                                                                        <%--1--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="chc1" runat="server" OnCheckedChanged="chkLinked_CheckedChanged" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Property Status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="txtProperty" Width="130px" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="ddl1" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="ddl2" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="ddl3" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--2--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="CB4" OnCheckedChanged="CB4_CheckedChanged" runat="server" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Utility Status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="txtUtil" Enabled="false" class="form-control" Width="130px" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="ddl25" Width="90px" Enabled="false" runat="server">
                                                                                     <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="ddl26" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="ddl27" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--3--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="cb5" runat="server" OnCheckedChanged="CheckBox5_CheckedChanged" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Work Order Type</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="txtWork" Enabled="false" Width="130px" class="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="ddl28" Enabled="false" Width="90px" runat="server">
                                                                                    <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="ddl29" Enabled="false" Width="90px" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="ddl30" Width="90px" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--4--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="cb6" OnCheckedChanged="CheckBox6_CheckedChanged" runat="server" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Client Status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="txtClient" Enabled="false" Width="130px" class="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="ddl31" Width="90px" Enabled="false" runat="server">
                                                                                   <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="ddl32" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="ddl33" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--5--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="cb7" OnCheckedChanged="CheckBox7_CheckedChanged" runat="server" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Utility Type</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="txtType" Width="130px" class="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="ddl34" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="ddl35" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="ddl36" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--6--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="cb8" OnCheckedChanged="CheckBox8_CheckedChanged" runat="server" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Receive Date</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="txtReceive" Enabled="false" class="form-control" Width="130px" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="ddl37" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="ddl38" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="ddl39" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--7--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="cb9" OnCheckedChanged="CheckBox9_CheckedChanged" runat="server" AutoPostBack="True" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Property boarding</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtboarding" Enabled="false" class="form-control" Width="130px" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="ddl40" Width="90px" Enabled="false" runat="server">
                                                                                  <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="ddl41" Width="90px" Enabled="false" runat="server">
                                                                                   <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="ddl42" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <%--8--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-1" style="padding-left: 60px">
                                                                                <asp:CheckBox ID="cb10" OnCheckedChanged="CheckBox10_CheckedChanged" AutoPostBack="true" runat="server" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <b>Referred Date</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="txtReferred" class="form-control" Enabled="false" Width="130px" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="ddl43" Width="90px" Enabled="false" runat="server">
                                                                                   <asp:ListItem>Business</asp:ListItem>
                                                                                    <asp:ListItem>Calendar</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList ID="ddl44" Width="90px" Enabled="false" runat="server">
                                                                                    <asp:ListItem>Days</asp:ListItem>
                                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                                    <asp:ListItem>Years</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <b>from</b> &nbsp
                                                        <asp:DropDownList ID="ddl45" Width="90px" Enabled="false" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                    <div class="row">
                                                                                        <div class="col-sm-4"></div>
                                                                        <div class="col-sm-4">
                                                                            <table  onclick="btnstepB4(this)">
                                                                                <thead>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button type="button" id="btnstepB4" class="btn btn-primary">Back</button>

                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table></div>
                                                                        <div class="col-sm-4">
                                                                            <table  onclick="btnstep4(this)">
                                                                                <thead>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button type="button" id="btnstep4" class="btn btn-primary">Next</button>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                                        </div>            
                                                                            </div>
                                                                  <%--  <asp:Button ID="Button6" runat="server" Text="Back" class="btn btn-primary" OnClick="Button6_Click" />
                                                                    <asp:Button ID="Button7" runat="server" Text="Next" class="btn btn-primary" OnClick="Button7_Click" />--%>
                                                                </div>



                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                </div>
                                                <div id="Step5" style="padding-left: 3px; display: none;">
                                                    <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="Label4" runat="server"  Text="Add New Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br />
                                                                <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                     <button type="button"   class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>
                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                               <%-- <asp:ImageButton runat="server" ID="btnShow5" AlternateText="ImageButton 1" ImageUrl="img/close.png"
                                                                    OnClick="ImageButton_Click" Width="25px" Height="25px" />--%>

                                                            </div>

                                                        </div>
                                                    <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_5.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                    <div class="row">
                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <br /> 
                                                                <div class="panel">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 450px;">
                                                                        <br />
                                                                        <div class="row" style="padding-left: 60px">
                                                                            <div class="col-sm-3"><b>Select the utility type</b></div>
                                                                            <div class="col-sm-1">
                                                                                <asp:CheckBox ID="CheckBox16" ClientIDMode="Static" runat="server" />
                                                                                &nbsp Gas
                                                                            </div>
                                                                            <div class="col-sm-2" style="padding-left: 40px">
                                                                                <asp:CheckBox ID="CheckBox17" ClientIDMode="Static" runat="server" />
                                                                                &nbsp Water
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:CheckBox ID="cbElectric" ClientIDMode="Static" runat="server" />
                                                                                &nbsp Electric
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px" >
                                                                            <div class="col-sm-3">
                                                                                <b>Select the USP Name</b>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                          <%--      <asp:TextBox ID="TextBox1" Enabled="false" runat="server"></asp:TextBox>--%>
                                                                            <%--    <asp:DropDownList ID="DropDownList46" OnSelectedIndexChanged="DropDownList46_SelectedIndexChanged" Width="155px" runat="server" 
                                                                                    DataSourceID="SqlDataSource11" DataTextField="USP_Name" DataValueField="USP_Name" AutoPostBack="True">
                                                                                </asp:DropDownList>--%>
                                                                             <%--   <select class="form-control input-sm" id="immSlct"  >
                                                                                    <option value="DUKE ENERGY">DUKE ENERGY</option>
                                                                                    <option value="NATIONAL GRID">NATIONAL GRID</option>
                                                                                </select>--%>
                                                                               
                                                                                <asp:ListBox ID="ListBox11" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                DataSourceID="SqlDataSource11" DataTextField="USP_Name" DataValueField="USP_Name"></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource11" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT DISTINCT [USP_Name] FROM [tbl_UC_USP_Name_Vendor_ID]"></asp:SqlDataSource>


                                                                            </div>

                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the Vendor ID</b>
                                                                            </div>
                                                                            <div class="col-sm-2 control-label" >
                                                                                <%--<asp:DropDownList ID="DropDownList47" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource12" DataTextField="USP_VendorID" DataValueField="USP_VendorID">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox12" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static" Width="100px" 
                                                                                   ></asp:ListBox>
                                                                               <%--  WHERE [USP_Name] = 'NATIONAL GRID'--%>
                                                                                   <%--DataSourceID="SqlDataSource121" DataTextField="NameID" DataValueField="Val_ID"--%>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource121" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [USP_VendorID], [USP_Name], [Val_ID], [USP_VendorID] + '-' + [USP_Name] as NameID FROM [tbl_UC_USP_Name_Vendor_ID]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-3">
                                                                                <b>Select the utility status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <%--<asp:DropDownList ID="DropDownList48" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource13" DataTextField="Util_Name" DataValueField="Util_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox13" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                    DataSourceID="SqlDataSource13" DataTextField="Util_Name" DataValueField="Val_ID"></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource13" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Util_Name], [Val_ID] FROM [tbl_UC_Utility_Status]"></asp:SqlDataSource>
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the work order type</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <%--<asp:DropDownList ID="DropDownList49" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource14" DataTextField="Work_Name" DataValueField="Work_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox14" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                     DataSourceID="SqlDataSource14" DataTextField="Work_Name" DataValueField="Val_ID"></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource14" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Work_Name], [Val_ID] FROM [tbl_UC_Work_Order_Type]"></asp:SqlDataSource>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-3">
                                                                                <b>Utility status Name</b>
                                                                            </div>
                                                                            <div class="col-sm-2"> 
                                                                                <asp:TextBox ID="txtUtilStatName" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the work order value</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:DropDownList ID="DropDownList51" Width="109px" ClientIDMode="Static" runat="server">
                                                                                    <asp:ListItem>Greater Than</asp:ListItem>
                                                                                    <asp:ListItem>Less Than</asp:ListItem>
                                                                                    <asp:ListItem>Equal to</asp:ListItem>
                                                                                </asp:DropDownList>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 5px">
                                                                            <div class="col-sm-3">
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txt51" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-3">
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the work order status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <%--<asp:DropDownList ID="DropDownList50" Width="120px" runat="server" 
                                                                                    DataSourceID="SqlDataSource15" DataTextField="wStatus_Name" DataValueField="wStatus_Name">
                                                                                </asp:DropDownList>--%>
                                                                                <asp:ListBox ID="ListBox15" runat="server" SelectionMode="Multiple"  multiple="multiple" ClientIDMode="Static"
                                                                                    DataSourceID="SqlDataSource15" DataTextField="wStatus_Name" DataValueField="Val_ID"></asp:ListBox>
                                                                                <asp:SqlDataSource runat="server" ID="SqlDataSource15" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [wStatus_Name], [Val_ID] FROM [tbl_UC_Work_Order_Status]"></asp:SqlDataSource>
                                                                                <br />
                                                                                <br />

                                                                                <asp:Button runat="server" ID="btnAddNew" Text="Add New" class="btn btn-primary" />

                                                                                <!-- ModalPopupExtender -->
                                                                                <cc1:ModalPopupExtender ID="ModalPopupExtender7" runat="server" PopupControlID="Panel8" TargetControlID="btnAddNew"
                                                                                    BackgroundCssClass="modalBackground">
                                                                                </cc1:ModalPopupExtender>
                                                                                <asp:Panel ID="Panel8" runat="server" CssClass="modalPopup2" align="left" Style="display: none; padding-left: 0px; padding-top: 0px">

                                                                                    <div class="panel">
                                                                                        <div class="panel-heading" style="background-color: #4d6578">
                                                                                            <div style="color: white"><b>Add New Utility and Work Item Rule</b></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="panel">
                                                                                        <div class="panel-body" style="background-color: #fff; min-height: 320px">
                                                                                            <br />
                                                                                            <div class="row" style="padding-left: 60px">
                                                                                                <div class="col-sm-3"><b>Select the utility type</b></div>
                                                                                                <div class="col-sm-1">
                                                                                                    <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />&nbsp Gas
                                                                                                </div>
                                                                                                <div class="col-sm-2" style="padding-left: 40px">
                                                                                                    <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />&nbsp Water
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />&nbsp Electric
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                <div class="col-sm-3">
                                                                                                    <b>Select the USP Name</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList52" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                    <b>Select the Vendor ID</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList54" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                <div class="col-sm-3">
                                                                                                    <b>Select the utility status</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList55" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                    <b>Select the work order type</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList56" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                <div class="col-sm-3">
                                                                                                    <b>Utility status Name</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <input type="text" class="form-control" id="usr">
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                    <b>Select the work order value</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList57" Width="109px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 5px">
                                                                                                <div class="col-sm-3">
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                </div>
                                                                                                <div class="col-sm-2">

                                                                                                    <input type="text" class="form-control" id="usr">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                <div class="col-sm-3">
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                    <b>Select the work order status</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList58" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>




                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                                                <div class="col-sm-3">
                                                                                                    <b>Process this as</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:DropDownList ID="DropDownList59" Width="120px" runat="server">
                                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                                <div class="col-sm-4" style="padding-left: 40px">
                                                                                                    <b>else follow business rule</b>
                                                                                                </div>
                                                                                                <div class="col-sm-2">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="panel-footer" style="background-color: transparent">
                                                                                            <center>
              <asp:Button runat="server"  id="Button34" Text="Done" class="btn btn-primary"/>
         </center>
                                                                                        </div>
                                                                                    </div>
                                                                                    <br />
                                                                                    <br />

                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-2" style="padding-right:3px">
                                                                                <b>Process this as</b>
                                                                            </div>
                                                                            <div class="col-sm-2" style="padding-left:3px">
                                                                                <asp:DropDownList ID="DropDownList53" Width="140px" ClientIDMode="Static" runat="server">
                                                                                    <asp:ListItem>Online Side By Side</asp:ListItem>
                                                                                    <asp:ListItem>Callout</asp:ListItem>
                                                                                    <asp:ListItem>Bulk Email</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 20px">
                                                                                <b>else follow business rule</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                    <div class="row">
                                                                                        <div class="col-sm-4"></div>
                                                                        <div class="col-sm-4">
                                                                            <table  onclick="btnstepB5(this)">
                                                                                <thead>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button type="button" id="btnstepB5" class="btn btn-primary">Back</button>

                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table></div>
                                                                        <div class="col-sm-4">
                                                                            <table  onclick="btnstep5(this)">
                                                                                <thead>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button type="button" id="btnstep5" class="btn btn-primary">Next</button>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                                        </div>            
                                                                            </div>
                                                                   <%-- <asp:Button ID="Button8" runat="server" Text="Back" class="btn btn-primary" OnClick="Button8_Click" />
                                                                    <asp:Button ID="Button9" runat="server" Text="Next" class="btn btn-primary" OnClick="Button9_Click" />--%>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                </div>
                                                <div id="Step6" style="padding-left: 3px; display: none;">
                                                    <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="Label5" runat="server" Text="Add New Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br />
                                                                <table id="1" onclick="exit(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                     <button type="button"   class="btn btn-danger"><i class="fa fa-times fa-1x" ></i></button>
                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                              <%--  <asp:ImageButton runat="server" ID="btnShow6" AlternateText="ImageButton 1" ImageUrl="img/close.png"
                                                                    OnClick="ImageButton_Click" Width="25px" Height="25px" />--%>

                                                            </div>

                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_6.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                                <br />
                                                                <p><font color="red">&nbsp***Please Review the setting you have created***</font></p>
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <div class="panel" style="overflow-y: scroll; height: 442px">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 440px">
                                                                        <br />
                                                                        <div class="row">
                                                                            <div class="col-sm-3" style="padding-left: 60px">
                                                                                <div class="panel">
                                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 415px">
                                                                                        <center>
                                                            <br /> <br /> 
                                                        <b>Client Level</b>
                                                            <br />    <br />   <br />    <br />       <br />
                                                            <b>Property/ Preservation Details</b>
                                                            <br /><br /><br /> <br /><br /><br />
                                                            <br /><br /><br />  <br /><br /><br /><br />
                                                            <b>SLA Deatails</b>
                                                            </center>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <div class="panel">
                                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 415px">

                                                                                        <div class="row" style="padding-top: 20px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Location_Service Location</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtNameVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtServiceLocVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtServiceLoc" ClientIDMode="Static" ToolTip="" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Client Status</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtClientStatVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtClientStat" ClientIDMode="Static" ToolTip="" Enabled="false" class="form-control" runat="server"></asp:TextBox>

                                                                                            </div>
                                                                                        </div>


                                                                                        <div class="row" style="padding-top: 22px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Property Boarding(In VMS) Date</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtPropertyVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                    <asp:TextBox ID="txtPropertyBusCal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                    <asp:TextBox ID="txtPropertyDMY" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtPropertyB" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Property Active / Inactive</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtActiveVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtActive" ForeColor="White" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Property Type</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtProTypeVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtProType" Enabled="false" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Occupancy Status</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtOccuVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtOccu" Enabled="false" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Property Status</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtProStatusVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtProStatus" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Property Value</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtProValueVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtProValue" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Total Preservation Cost(TPC) incurred</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtPreserVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtPreser" Enabled="false" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Inspection Complete</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtInspectionVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtInspection" Enabled="false" class="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                                <b>Preservation Work Type</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtPWTVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtPWT" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 22px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">

                                                                                                <asp:TextBox ID="txtSLA" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="float: right">

                                                                                                <div class="btn-group">

                                                                                                    <%--<asp:Button ID="Button12" runat="server" Text="1" 
                                                                                 class="btn btn-primary" OnClick="page1_Click"/>
                                                                            <asp:Button ID="Button14" runat="server" Text="1" 
                                                                                 class="btn btn-primary disabled" />
                                                                             <asp:Button ID="Button13" runat="server" Text="2" 
                                                                                 class="btn btn-primary" OnClick="page2_Click"/>--%>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>


                                                                                    </div>
                                                                                </div>



                                                                            </div>


                                                                        </div>
                                                                        <%--------------------Scroll------------------%>
                                                                        <div class="row">
                                                                            <div class="col-sm-3" style="padding-left: 60px">
                                                                                <div class="panel">
                                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 415px">
                                                                                        <center>
                                                            <br /> <br /> 
                                                        <b>Geography</b>
                                                            <br /><br /><br /> <br /><br /><br />
                                                            <br /><br /><br />
                                                            <b>Utility and Work item Details</b>
                                                            <br />
                                                            <asp:Button ID="btnView" class="btn btn-link btn-sm" OnClick="viewMore_Click" runat="server" Text="View More" />
                                                            <br /><br /> <br /><br /><br />
                                                            <br />
                                                             
                                                            </center>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <div class="panel">
                                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 415px">

                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-2" style="padding-left: 30px">
                                                                                                <b>Country</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtCountryVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtCountry" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 30px">
                                                                                                <b>Street name</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtStreetVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtStreet" Enabled="false" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-2" style="padding-left: 30px">
                                                                                                <b>State</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtStateVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtState" ClientIDMode="Static" Enabled="false" ToolTip="" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 30px">
                                                                                                <b>Number of Units</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtNumberVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtNumber" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-2" style="padding-left: 30px">
                                                                                                <b>City</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtCityVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtCity" Enabled="false" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 30px">
                                                                                                <b>POD NAME</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtPODVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtPOD" Enabled="false" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-2" style="padding-left: 30px">
                                                                                                <b>Zip Code</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtZipVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtZip" Enabled="false" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>





                                                                                        <div class="row" style="padding-top: 32px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Utility Type</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtUtilTypeVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtUtilType" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Utility Status</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtUtilNameVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtUtilStatVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtUtilStat" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>

                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>USP NAME</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtUSPnameVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtUSPname" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Vendor ID</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtVendorIDVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtVendorID" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Work Order Type</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtWOTVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtWOT" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Work Order Value</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtWOVVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtWOV" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 5px">
                                                                                            <div class="col-sm-4" style="padding-left: 30px">
                                                                                                <b>Work Order Status</b>
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="padding-left: 0px">
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtWOSVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <div style="display: none">
                                                                                                    <asp:TextBox ID="txtProcessVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                </div>
                                                                                                <asp:TextBox ID="txtWOS" ClientIDMode="Static" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row" style="padding-top: 45px">
                                                                                            <div class="col-sm-5" style="padding-left: 30px">
                                                                                            </div>
                                                                                            <div class="col-sm-3" style="float: right">

                                                                                                <div class="btn-group">

                                                                                                    <%--  <asp:Button ID="Button38" runat="server" Text="1" 
                                                                                 class="btn btn-primary" OnClick="page1_Click"/>
                                                                            <asp:Button ID="Button39" runat="server" Text="2" 
                                                                                 class="btn btn-primary disabled" />
                                                                             <asp:Button ID="Button40" runat="server" Text="2" 
                                                                                 class="btn btn-primary" OnClick="page2_Click"/>--%>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                  <div class="row">
                                                                                        <div class="col-sm-4"></div>
                                                                        <div class="col-sm-4">
                                                                            <table  onclick="btnstepB6(this)">
                                                                                <thead>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <button type="button" id="btnstepB6" class="btn btn-primary">Back</button>

                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table></div>
                                                                        <div class="col-sm-4">
                                                                            <table id="asdasd" onclick="SaveRule(this)">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                     <asp:Button ID="Button3" runat="server" Text="Save" class="btn btn-primary" ClientIDMode="Static" /> 
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                                        </div>            
                                                                            </div>
                                                                   
                                                                   
                                                                    <%--<button id="btnSave" type="button">Save</button>--%>
                                                                    
                                                                                 <%--   <asp:Button ID="Button11" runat="server" Text="ExSave" class="btn btn-primary" OnClick="Button11_Click1"  />--%>

                                                                </div>



                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>

                                                    
                                                    
                                                </div>

                                                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="6">
                                                    <asp:View ID="View1" runat="server">
                                                        

                                                    </asp:View>
                                                    <asp:View ID="View2" runat="server">
                                                        
                                                    </asp:View>
                                                    <asp:View ID="View3" runat="server">
                                                        
                                                    </asp:View>
                                                    <asp:View ID="View4" runat="server">
                                                        
                                                    </asp:View>
                                                    <asp:View ID="View5" runat="server">
                                                        
                                                    </asp:View>
                                                    <asp:View ID="View6" runat="server">
                                                         
                                                    </asp:View>
                                                    <asp:View ID="View7" runat="server">
                                                      

                                                        
                                                        

                                                        
                                                    </asp:View>
                                                    <asp:View ID="View8" runat="server">
                                                        <div class="row">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-4">
                                                                <center>
                        <b><h2><asp:Label ID="Label6" runat="server"  Text="Add New Rule"></asp:Label></h2></b>
                            </center>
                                                            </div>
                                                            <div class="col-sm-4" style="padding-left: 195px">
                                                                <br /> 
                                                                <asp:ImageButton runat="server" ID="ImageButton1" AlternateText="ImageButton 1" ImageUrl="img/close.png"
                                                                    OnClick="ImageButton_Click" Width="25px" Height="25px" /> 
                                                            </div> 
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6" style="padding-left: 55px">
                                                                <br />
                                                                <img src="img/Step_6.PNG" class="img-rounded" alt="Cinque Terre" width="800" height="55" />
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-11" style="padding-left: 60px">
                                                                <br />
                                                                <%--<div class="panel" style="padding-left: 0px">
                                                                    <div class="panel-heading" style="padding-left: 0px; padding-bottom: 0px; width: 847px">
                                                                        <nav class="navbar navbar-inverse" style="background-color: #4d6578">
                                                                            <ul class="nav navbar-nav">
                                                                                <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp| &nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                                                <li class="active"><a data-toggle="tab" href="#USP">For Research</a></li>
                                                                                <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                                                <li><a data-toggle="tab" href="#Utility">For Inspection</a></li>
                                                                                <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>


                                                                            </ul>
                                                                        </nav>
                                                                    </div>
                                                                </div>--%>
                                                                <div class="panel" style="padding-top: 0px">
                                                                    <div class="panel-body" style="background-color: #f1f1f1; min-height: 380px">
                                                                        <br />
                                                                        <div class="row" style="padding-left: 60px">
                                                                            <div class="col-sm-3"><b>Select the utility type</b></div>
                                                                            <div class="col-sm-1">
                                                                                <asp:CheckBox ID="cbGas2" runat="server" />&nbsp Gas
                                                                            </div>
                                                                            <div class="col-sm-2" style="padding-left: 40px">
                                                                                <asp:CheckBox ID="cbWater2" runat="server" />&nbsp Water
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:CheckBox ID="cbElec2" runat="server" />&nbsp Electric
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-3">
                                                                                <b>Select the USP Name</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtUSP" ClientIDMode="Static" Enabled="false" runat="server" class="form-control form-group-sm"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the Vendor ID</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtVenID" ClientIDMode="Static" Enabled="false" runat="server" class="form-control form-group-sm"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-3">
                                                                                <b>Select the utility status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtUtilTy" ClientIDMode="Static" Enabled="false" runat="server" class="form-control form-group-sm"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the work order type</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtWOT2" ClientIDMode="Static" Enabled="false" runat="server" class="form-control form-group-sm"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-3">
                                                                                <b>Utility status Name</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                               <asp:TextBox ID="txtUtilName" ClientIDMode="Static" Enabled="false" runat="server" class="form-control form-group-sm"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the work order value</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtWOV2" ClientIDMode="Static" runat="server" Enabled="false" class="form-control form-group-sm"></asp:TextBox>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 5px">
                                                                            <div class="col-sm-3">
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="txtWOV3" ClientIDMode="Static" runat="server" Enabled="false" class="form-control form-group-sm"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-3">
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>Select the work order status</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtWOS2" ClientIDMode="Static" runat="server" Enabled="false" class="form-control form-group-sm"></asp:TextBox>

                                                                                <br />
                                                                                <br />
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 60px; padding-top: 10px">
                                                                            <div class="col-sm-3">
                                                                                <b>Process this as</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtProcess" ClientIDMode="Static" Enabled="false" runat="server" class="form-control form-group-sm"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-4" style="padding-left: 40px">
                                                                                <b>else follow business rule</b>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer" style="background-color: #ffffff; float: right">
                                                                    <asp:Button ID="Button14" runat="server" Text="Back" class="btn btn-primary" OnClick="Button9_Click" />

                                                                </div>
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                    </asp:View>
                                                </asp:MultiView>

                                               <%-- <!-- ModalPopupExtender -->
                                                <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="btnShow"
                                                    BackgroundCssClass="modalBackground">
                                                </cc1:ModalPopupExtender>
                                                <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                    <br />
                                                    <b>Are you sure do you want you close?</b>
                                                    <br />
                                                    <br />
                                                    <asp:Button ID="Button21" class="btn btn-primary" runat="server" OnClick="closeView_Click" Text="Yes" />
                                                    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="btnClose" class="btn btn-primary" runat="server" Text="No" />
                                                </asp:Panel>

                                                <!-- ModalPopupExtender -->
                                                <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel2" TargetControlID="btnShow2"
                                                    BackgroundCssClass="modalBackground">
                                                </cc1:ModalPopupExtender>
                                                <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" align="center" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                    <br />
                                                    <b>Are you sure do you want you close?</b>
                                                    <br />
                                                    <br />
                                                    <asp:Button ID="Button22" class="btn btn-primary" runat="server" OnClick="closeView_Click" Text="Yes" />
                                                    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button23" class="btn btn-primary" runat="server" Text="No" />
                                                </asp:Panel>
                                                <!-- ModalPopupExtender -->
                                                <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel3" TargetControlID="btnShow3"
                                                    BackgroundCssClass="modalBackground">
                                                </cc1:ModalPopupExtender>
                                                <asp:Panel ID="Panel3" runat="server" CssClass="modalPopup" align="center" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                    <br />
                                                    <b>Are you sure do you want you close?</b>
                                                    <br />
                                                    <br />
                                                    <asp:Button ID="Button24" class="btn btn-primary" runat="server" OnClick="closeView_Click" Text="Yes" />
                                                    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button25" class="btn btn-primary" runat="server" Text="No" />
                                                </asp:Panel>
                                                <!-- ModalPopupExtender -->
                                                <cc1:ModalPopupExtender ID="ModalPopupExtender3" runat="server" PopupControlID="Panel4" TargetControlID="btnShow4"
                                                    BackgroundCssClass="modalBackground">
                                                </cc1:ModalPopupExtender>
                                                <asp:Panel ID="Panel4" runat="server" CssClass="modalPopup" align="center" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                    <br />
                                                    <b>Are you sure do you want you close?</b>
                                                    <br />
                                                    <br />
                                                    <asp:Button ID="Button26" class="btn btn-primary" runat="server" OnClick="closeView_Click" Text="Yes" />
                                                    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button27" class="btn btn-primary" runat="server" Text="No" />
                                                </asp:Panel>
                                                <!-- ModalPopupExtender -->
                                                <cc1:ModalPopupExtender ID="ModalPopupExtender4" runat="server" PopupControlID="Panel5" TargetControlID="btnShow5"
                                                    BackgroundCssClass="modalBackground">
                                                </cc1:ModalPopupExtender>
                                                <asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" align="center" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                    <br />
                                                    <b>Are you sure do you want you close?</b>
                                                    <br />
                                                    <br />
                                                    <asp:Button ID="Button28" class="btn btn-primary" runat="server" OnClick="closeView_Click" Text="Yes" />
                                                    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button29" class="btn btn-primary" runat="server" Text="No" />
                                                </asp:Panel>
                                                <!-- ModalPopupExtender -->
                                                <cc1:ModalPopupExtender ID="ModalPopupExtender5" runat="server" PopupControlID="Panel6" TargetControlID="btnShow6"
                                                    BackgroundCssClass="modalBackground">
                                                </cc1:ModalPopupExtender>
                                                <asp:Panel ID="Panel6" runat="server" CssClass="modalPopup" align="center" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                    <br />
                                                    <b>Are you sure do you want you close?</b>
                                                    <br />
                                                    <br />
                                                    <asp:Button ID="Button30" class="btn btn-primary" runat="server" OnClick="closeView_Click" Text="Yes" />
                                                    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button31" class="btn btn-primary" runat="server" Text="No" />
                                                </asp:Panel>--%>







                                            </div>
                        

                                            <div id="schedule" class="tab-pane fade">
                                                <div class="panel panel-default panel-new">
                                                    <div class="panel-heading propertyhead" style="background-color: #4a7bb9; padding-bottom: 5px">
                                                        <div class="row">
                                                            <div class="col-sm-2" style="padding-right: 0px; color: white">
                                                                <h6 style="margin: 0px;"><b>keep this rule as</b></h6>
                                                            </div>
                                                            <div class="col-sm-1" style="padding-left: 0px">
                                                                <div class="switch">
                                                                    <input id="cmn-toggle-2" class="cmn-toggle cmn-toggle-round" type="checkbox">
                                                                    <label for="cmn-toggle-2"></label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row" style="padding-top: 30px">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-2">
                                                            <asp:Label ID="Label58" CssClass="vms" runat="server" Text="Label">Schedule
                                                            <div style="float:right">:</div>
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <asp:DropDownList ID="DropDownList60" Width="120px" runat="server">
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                                <asp:ListItem>5</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top: 5px">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-2">
                                                            <asp:Label ID="Label59" CssClass="vms" runat="server" Text="Label">Period
                                                            <div style="float:right">:</div>
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <asp:DropDownList ID="DropDownList61" Width="120px" runat="server">
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                                <asp:ListItem>5</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top: 5px">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-2">
                                                            <asp:Label ID="Label60" CssClass="vms" runat="server" Text="Label">Start to run rule
                                                            <div style="float:right">:</div>
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-2" style="width: 137px">
                                                            <input type="text" class="form-control" style="height: 29px" id="usr">
                                                        </div>
                                                        <div class="col-md-2" style="padding-left: 3px">

                                                            <asp:DropDownList ID="DropDownList62" Width="120px" runat="server">
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                                <asp:ListItem>5</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top: 5px">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-2">
                                                            <asp:Label ID="Label61" CssClass="vms" runat="server" Text="Label">Stop to run rule after
                                                            <div style="float:right">:</div>
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-2" style="width: 137px">
                                                            <input type="text" class="form-control" style="height: 29px" id="usr">
                                                        </div>
                                                        <div class="col-md-2" style="padding-left: 3px">

                                                            <asp:DropDownList ID="DropDownList63" Width="120px" runat="server">
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                                <asp:ListItem>5</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top: 5px">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-2">
                                                            <asp:Label ID="Label62" CssClass="vms" runat="server" Text="Label">Read from
                                                            <div style="float:right">:</div>
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-2" style="width: 137px">
                                                        </div>
                                                        <div class="col-md-2" style="padding-left: 3px">
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top: 5px">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-2">
                                                            <asp:Label ID="Label63" CssClass="vms" runat="server" Text="Label">Save to
                                                            <div style="float:right">:</div>
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-2" style="width: 137px">
                                                        </div>
                                                        <div class="col-md-2" style="padding-left: 3px">
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top: 5px">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-2">
                                                            <asp:Label ID="Label64" CssClass="vms" runat="server" Text="Label">Send Error To
                                                            <div style="float:right">:</div>
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-2" style="width: 137px">
                                                            <input type="text" class="form-control" style="height: 29px" id="usr">
                                                        </div>
                                                        <div class="col-md-2" style="padding-left: 3px">
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top: 5px">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-2">
                                                        </div>
                                                        <div class="col-md-2" style="width: 137px">
                                                        </div>
                                                        <div class="col-md-2" style="padding-left: 3px; float: right">
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <asp:Button runat="server" class="btn btn-primary" Text="Edit"></asp:Button>
                                                        </div>
                                                    </div>
                                                     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
             
                            

                        </div>
                                      </ContentTemplate>
                  </asp:UpdatePanel>
                        </div>
          

               

            </div>

        </div>
     
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
    <script src="plugins/multiselect/bootstrap-multiselect.js"></script>
    <script src="../js/StopServiceSettings.js"></script>
</asp:Content>


﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class DashBoard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["NTID"].ToString() == "millagra" || Session["NTID"].ToString() == "doriajam" || Session["NTID"].ToString() == "blashehe")
            {
              //  btnRUN.Visible = true;
            }
            else
            {
             //   btnRUN.Visible = false;
                Response.Redirect("USPCapabilities.aspx");
            }
        }
        protected void btnRUN_Click(object sender, EventArgs e)
        {
            try
            {

                //For Start Process
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.FileName = Server.MapPath(@"\exe\Copy_USP_TO_PIV.exe");
                p.StartInfo.WorkingDirectory = Server.MapPath(@"\");
                p.StartInfo.RedirectStandardOutput = false;
                p.Start();
                p.WaitForExit();


                //For Stop Process
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.FileName = Server.MapPath(@"\exe\Process_StopService.exe");
                p.StartInfo.WorkingDirectory = Server.MapPath(@"\");
                p.StartInfo.RedirectStandardOutput = false;
                p.Start();
                p.WaitForExit();


                //For Invoice Process
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.FileName = Server.MapPath(@"\exe\Process_USP.exe");
                p.StartInfo.WorkingDirectory = Server.MapPath(@"\");
                p.StartInfo.RedirectStandardOutput = false;
                p.Start();
                p.WaitForExit();

                //For BillPayment Process
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.FileName = Server.MapPath(@"\exe\Process_BIllsPayment.exe");
                p.StartInfo.WorkingDirectory = Server.MapPath(@"\");
                p.StartInfo.RedirectStandardOutput = false;
                p.Start();
                p.WaitForExit();
            }
            catch (Exception ex)
            {
            }
        }
        [WebMethod]
        public static string GetList()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select * FROM [dbo].[tbl_UC_USP_Name_Vendor_ID] a Inner Join tbl_UC_USP_ProcessUSP b ON b.USP_ID = a.USP_ID Order By a.USP_Name";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetStart()
        {
            string connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";
            DataTable dt = new DataTable();
            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                string qry = "Select * FROm [vw_UC_USP_InflowSettings]";
                
                cnn.Open();
                SqlCommand command = new SqlCommand(qry, cnn);
               
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    dt.Load(reader);
                  
                }
                cnn.Close();
            }
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetStop()
        {
            string connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";
            DataTable dt = new DataTable();
            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                string qry = "Select * FROm [vw_UC_USP_InflowSettings_StopService]";

                cnn.Open();
                SqlCommand command = new SqlCommand(qry, cnn);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    dt.Load(reader);

                }
                cnn.Close();
            }
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
    }
}
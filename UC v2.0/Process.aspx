﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Process.aspx.cs" Inherits="UC_v2._0.Process" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary" style="height: 717px; overflow-y: scroll;">
                        <div class="panel-body">
                            <div class="col-lg-2">
                                <asp:DropDownList ID="ddlPro" class="form-control" runat="server" ClientIDMode="Static">
                                    <asp:ListItem></asp:ListItem>
                                    <asp:ListItem Value="2">All Status</asp:ListItem>
                                    <asp:ListItem Value="1">RIP,Out of REO,NULL</asp:ListItem>
                                    <asp:ListItem Value="3">SBS</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <br />
                            <br />
                            <table id="tblBulkEmail" class="table no-border">
                                <thead>
                                    <tr>
                                        <th data-field="Task" data-sortable="true">Task</th>
                                        <th data-field="USP" data-sortable="true">USP</th>
                                        <th data-field="PropertyID" data-sortable="true">Property ID</th>
                                        <th data-field="Utility" data-sortable="true">Utility</th>
                                        <th data-field="Address" data-sortable="true">Address</th>
                                        <th data-field="Status" data-sortable="true">Status</th>
                                        <th data-field="VendorID" data-sortable="true">Vendor ID</th>
                                        <th data-field="AccountNo" data-sortable="true">AccountNo</th>
                                        <th data-field="NoOfUnits" data-sortable="true">No of Units</th>
                                        <th data-field="Category" data-sortable="true">Category</th>
                                    </tr>
                                </thead>
                            </table>
                            <br />
                            <br />
                            <table id="tblsbs" class="table no-border">
                                <thead>
                                    <tr>
                                        <th data-field="Task" data-sortable="true">Task</th>
                                        <th data-field="USP" data-sortable="true">USP</th>
                                        <th data-field="VendorId" data-sortable="true">VendorId</th>
                                        <th data-field="PropertyCode" data-sortable="true">PropertyCode</th>
                                        <th data-field="Utility" data-sortable="true">Utility</th>
                                        <th data-field="Address" data-sortable="true">Address</th>
                                        <th data-field="Status" data-sortable="true">Status</th>
                                        <th data-field="DateUploaded" data-sortable="true">DateUploaded</th>
                                        <th data-field="AgentNTID" data-sortable="true">AgentNTID</th>
                                        <th data-field="OnlineFlag" data-sortable="true">OnlineFlag</th>
                                        <th data-field="Updateflag" data-sortable="true">Updateflag</th>
                                        <th data-field="Skip" data-sortable="true">Skip</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/process.js"></script>
</asp:Content>

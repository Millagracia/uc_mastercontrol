﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class DataListUSP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            //string qry = "Select EmpLevel FROm tbl_HRMS_EmployeeMaster where NTID = '" + Session["ntid"].ToString() + "'";
            //dt = cls.GetData(qry);

            //if (dt.Rows[0][0].ToString() == "SR. MGR" || dt.Rows[0][0].ToString() == "MGR" || dt.Rows[0][0].ToString() == "MANAGER" || dt.Rows[0][0].ToString() == "AM"
            //    || dt.Rows[0][0].ToString() == "DIR" || dt.Rows[0][0].ToString() == "VP" || dt.Rows[0][0].ToString() == "TL")
            //{

            //}
            //else if (Session["ntid"].ToString() == "solimanj" || Session["ntid"].ToString() == "torrmary" || Session["ntid"].ToString() == "mendiola"
            //  || Session["ntid"].ToString() == "lopezlad" || Session["ntid"].ToString() == "llorenge" || Session["ntid"].ToString() == "chavezar")
            //{

            //}
            //else
            //{
            //    Response.Redirect("Usersettings.aspx");
            //}
        }
        //Bind USP DataList to Table
        [WebMethod]
        public static string GetList()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select * From tbl_UC_USP_Name_Vendor_ID Where isDeleted = 0 Order BY USP_Name";
            string qryExport = "Select USP_Name as 'USP Name', [Email Address],Website,StopWebsite,[Login Name],Password From tbl_UC_USP_Name_Vendor_ID Where isDeleted = 0 Order BY USP_Name";
            DataTable dt = new DataTable();
            DataTable dtExport = new DataTable();
            dt = cls.GetData(qry);
            dtExport = cls.GetData(qryExport);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd2 = dt } });
        }

        //Get Selected Vendor ID
        [WebMethod]
        public static string GetVendorList(string Alpha)
        {
            clsConnection cls = new clsConnection();

            Alpha = Alpha.Replace(",", "','");
            Alpha = Alpha.Replace("__", " ");
            string qry = "Select * From tbl_UC_USP_Normalization Where isTag = 0 AND isActive = 1 and SUBSTRING(VendorName, 1, 1) IN ('" + Alpha + "')";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetAlpha()
        {
            clsConnection cls = new clsConnection();
            string qry = @"Select Distinct REPLACE(SUBSTRING(VendorName, 1, 1),' ','__') As NewColumnfrom from tbl_UC_USP_Normalization 
                           where isTag = 0 AND isActive = 1 order by NewColumnfrom ";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetEditAlpha()
        {
            clsConnection cls = new clsConnection();
            string qry = @"Select Distinct REPLACE(SUBSTRING(VendorName, 1, 1),' ','__') As NewColumnfrom from tbl_UC_USP_Normalization 
                           order by NewColumnfrom ";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        //Delete Selected USP From Datalist
        [WebMethod]
        public static string DeleteUSP(string USP_ID)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dtUSP = new DataTable();
            DataTable dtCount = new DataTable();
            ArrayList arrPages = new ArrayList();
            List<string> VendorIDRemove = new List<string>();
            string getData = "Select * FROM tbl_UC_USP_Name_Vendor_ID where USP_ID = '" + USP_ID + "'";
            dt = cls.GetData(getData);

            string selectCount = "Select Count (*) as CountID From tbl_UC_USPcapabilities_Start_Service Where USP_ID = '" + USP_ID + "'";
            dtCount = cls.GetData(selectCount);
            string SelectUSP = "Select * From tbl_UC_USPcapabilities_Start_Service Where USP_ID = '" + USP_ID + "'";
            dtUSP = cls.GetData(SelectUSP);
            string UpdateTag = "";
            for (int i = 0; i < Convert.ToInt32(dtCount.Rows[0][0]); i++)
            {
                UpdateTag += "Update tbl_UC_USP_Normalization SET isTag = 0 Where VendorID = '" + dtUSP.Rows[i][1].ToString() + "';";
            }

            cls.ExecuteQuery(UpdateTag);

            arrPages.Add("USP General Info");
            arrPages.Add("-");
            cls.FUNC.Audit("Deleted USP : " + dt.Rows[0][1].ToString(), arrPages);
            

            string DeleteUSP = "Update tbl_UC_USP_Name_Vendor_ID Set isDeleted ='1' Where USP_ID = '" + USP_ID + "'";
            string DeleteStart = "Update tbl_UC_USPcapabilities_Start_Service Set isDeleted ='1' Where USP_ID = '" + USP_ID + "'";
            string DeleteStop = "Update tbl_UC_USPcapabilities_Stop_Service Set isDeleted ='1' Where USP_ID = '" + USP_ID + "'";
            string DeleteInvoicing = "Update tbl_UC_USPcapabilities_Invoicing Set isDeleted ='1' Where USP_ID = '" + USP_ID + "'";
            string DeleteBills = "Update tbl_UC_USPcapabilities_BillsPayment Set isDeleted ='1' Where USP_ID = '" + USP_ID + "'";
            string DeleteProcess = "Update tbl_UC_USP_ProcessUSP Set isDeleted ='1' Where USP_ID = '" + USP_ID + "'";

            //Execute Query
            cls.ExecuteQuery(DeleteUSP);
            cls.ExecuteQuery(DeleteStart);
            cls.ExecuteQuery(DeleteStop);
            cls.ExecuteQuery(DeleteInvoicing);
            cls.ExecuteQuery(DeleteBills);
            cls.ExecuteQuery(DeleteProcess);

            return "True";
        }
        //get Selected VendorID every USP
        //[WebMethod]
        //public static string vendorID(string itemSelec)
        //{
        //    string[] arrItem;
        //    string newItem = "";
        //    arrItem = itemSelec.Split(',');

        //    for (int i = 0; i <= arrItem.Count(); i++)
        //    {
        //        if (i == 0)
        //        {
        //            newItem = "'" + arrItem[i] + "'";
        //        }
        //        else if (i == arrItem.Count())
        //        {

        //        }
        //        else
        //        {
        //            newItem = newItem + ",'" + arrItem[i] + "'";
        //        }
        //    }
        //    clsConnection cls = new clsConnection();
        //    DataTable dt = new DataTable();
        //    string selectqry = " Select * FROM tbl_UC_USP_Normalization Where VID IN (" + newItem + ")";
        //    dt = cls.GetData(selectqry);

        //    return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        //}
        //Bind Data to Modal for Edit
        [WebMethod]
        public static string GetUSPvalue(string editVal)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dtVendor = new DataTable();
            string selectqry = @"Select REPLACE(SUBSTRING(VendorName, 1, 1),' ','__') As NewColumnfrom,a.USP_ID,USP_Name, [Email Address] as Email_Address, 
            Website,StopWebsite, [Login Name] as Login_Name, Password
            , Vendor_ID,c.VID FROM [tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [tbl_UC_USPcapabilities_Start_Service] b ON b.USP_ID = a.USP_ID
            Inner Join tbl_UC_USP_Normalization c on c.VendorID = b.Vendor_ID
             Where a.USP_ID = '" + editVal + "'";
            dt = cls.GetData(selectqry);
                 
            string alphaSTR = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (!alphaSTR.Contains(dt.Rows[i]["NewColumnfrom"].ToString()))
                {
                    if(alphaSTR != "")
                    {
                        alphaSTR += ",'" + dt.Rows[i]["NewColumnfrom"].ToString() + "'";
                    }
                    else
                    {
                        alphaSTR = "'" + dt.Rows[i]["NewColumnfrom"].ToString() + "'";
                    }
                }
            }


            string selectVendor = @" Select * FROM tbl_UC_USP_Normalization where VendorID IN (Select Vendor_ID FROM [tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [tbl_UC_USPcapabilities_Start_Service] b ON b.USP_ID = a.USP_ID
                                     Inner Join tbl_UC_USP_Normalization c on c.VendorID = b.Vendor_ID Where a.USP_ID =  '"
                + dt.Rows[0]["USP_ID"].ToString() + "') or SUBSTRING(VendorName,1,1) IN (" + alphaSTR + ") and isTag = 0";

            HttpContext.Current.Session["USP_ID"] = dt.Rows[0]["USP_ID"].ToString(); 
            dtVendor = cls.GetData(selectVendor);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd2 = dtVendor } });
        }
        [WebMethod]
        public static string GetSelectedVID(string alphaSTR)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dtVendor = new DataTable();
            string selectqry = @"Select REPLACE(SUBSTRING(VendorName, 1, 1),' ','__') As NewColumnfrom,a.USP_ID,USP_Name, [Email Address] as Email_Address, 
            Website,StopWebsite, [Login Name] as Login_Name, Password
            , Vendor_ID,c.VID FROM [tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [tbl_UC_USPcapabilities_Start_Service] b ON b.USP_ID = a.USP_ID
            Inner Join tbl_UC_USP_Normalization c on c.VendorID = b.Vendor_ID
             Where a.USP_ID = '" + HttpContext.Current.Session["USP_ID"] + "'";
            dt = cls.GetData(selectqry);

            alphaSTR = alphaSTR.Replace(",","','");
            string selectVendor = @" Select * FROM tbl_UC_USP_Normalization where VendorID IN (Select Vendor_ID FROM [tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [tbl_UC_USPcapabilities_Start_Service] b ON b.USP_ID = a.USP_ID
                                     Inner Join tbl_UC_USP_Normalization c on c.VendorID = b.Vendor_ID Where a.USP_ID =  '"
                + HttpContext.Current.Session["USP_ID"] + "') or SUBSTRING(VendorName,1,1) IN ('" + alphaSTR + "') and isTag = 0 order by VendorName";

            
            dtVendor = cls.GetData(selectVendor);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd2 = dtVendor } });
        }
        //Save USP DataList in Start,Stop,Invoicing and Bills Payment
        [WebMethod]
        public static string Save(string USPname, string Email, string Website, string StopWebsite, string UserName, string Password, string itemSelec)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string USPID = "";
            string[] arrItem;
            string newItem = "";
            arrItem = itemSelec.Split(',');
            ArrayList arrPages = new ArrayList();
            List<string> vendorid = new List<string>();
            //Update Normalization isTag to 1
            for (int i = 0; i < arrItem.Count(); i++)
            {
                vendorid.Add(arrItem[i]);
                string UpdateTag = "Update tbl_UC_USP_Normalization SET isTag = 1 Where VendorID = '" + arrItem[i] + "'";
                cls.ExecuteQuery(UpdateTag);
            }
            //string qryAudit = "Insert Into tbl_UC_Audit Set Page = 'USP General Info', action = 'Create New USP : ' " + USPname + "',";
            //qryAudit += "action_by = '" + HttpContext.Current.Session["ntid"] + "', action_date ='" + DateTime.Now.ToString("dd/MM/yyyy") + "',";

            arrPages.Add("USP General Info");
            arrPages.Add("USP General Info");
            cls.FUNC.Audit("Create New USP : " + USPname, arrPages);

            //Insert New Data in USP Datalist
            string insert = "Insert INTO [tbl_UC_USP_Name_Vendor_ID]  (USP_Name,[Email Address],[Website],[StopWebsite],[Login Name],[Password],[NTID],[Date_Created]) values ('" + USPname + "'";
            insert += ",'" + Email.Trim() + "'";
            insert += ",'" + Website.Trim() + "'";
            insert += ",'" + StopWebsite.Trim() + "'";
            insert += ",'" + UserName.Trim() + "'";
            insert += ",'" + Password.Trim() + "'";
            insert += ",'" + HttpContext.Current.Session["ntid"] + "'";
            insert += ",'" + DateTime.Now.ToString() + "')";
            cls.ExecuteQuery(insert);


            //Select Data from USP Table Database
            string selectID = "Select * FROM [dbo].[tbl_UC_USP_Name_Vendor_ID] Where USP_Name = '" + USPname + "'";
            dt = cls.GetData(selectID);


            //Insert Value in Start Service Capabilities
            insert = "Insert INTO [tbl_UC_USPcapabilities_Start_Service]  (USP_ID,Vendor_ID,[Bulk Email USP],[Bulk Email ASPS],[Online Request USP]" +
                         ",[Online Request ASPS],[EDI 814 USP],[EDI 814 ASPS],[Call USP],[Call ASPS]) values ";
            for (int i = 0; i < vendorid.Count; i++)
            {
                insert += "('" + dt.Rows[0][0].ToString() + "'";
                insert += ",'" + vendorid[i].ToString() + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'),";

            }
            insert = insert.Remove(insert.Length - 1, 1);
            cls.ExecuteQuery(insert);


            //Insert Value in Stop Service Capabilities
            insert = "Insert INTO [tbl_UC_USPcapabilities_Stop_Service]  (USP_ID,Vendor_ID,[Bulk Email USP],[Bulk Email ASPS],[Online Request USP]" +
                        ",[Online Request ASPS],[EDI 814 USP],[EDI 814 ASPS],[Call USP],[Call ASPS]) values ";

            for (int i = 0; i < vendorid.Count; i++)
            {
                insert += "('" + dt.Rows[0][0].ToString() + "'";
                insert += ",'" + vendorid[i].ToString() + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'),";

            }
            insert = insert.Remove(insert.Length - 1, 1);
            cls.ExecuteQuery(insert);


            //Insert Value in Invoicing Capabilities
            insert = "Insert INTO [tbl_UC_USPcapabilities_Invoicing]  (USP_ID,Vendor_ID,[Summary Billing USP],[Summary Billing ASPS],[E-Bill w/ Password USP]" +
                         ",[E-Bill w/ Password ASPS],[E-Bill w/o Password USP],[E-Bill w/o Password ASPS],[E-Bill Via Link USP],[E-Bill Via Link ASPS] " +
                         ",[EDI 810 USP],[EDI 810 ASPS],[Paper Bill USP],[Paper Bill ASPS]) values ";
            for (int i = 0; i < vendorid.Count; i++)
            {

                insert += "('" + dt.Rows[0][0].ToString() + "'";
                insert += ",'" + vendorid[i].ToString() + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'),";
            }
            insert = insert.Remove(insert.Length - 1, 1);
            cls.ExecuteQuery(insert);

            //Insert Value in BillsPayment Capabilities
            insert = "Insert INTO [tbl_UC_USPcapabilities_BillsPayment]  (USP_ID,Vendor_ID,[ACH USP],[ACH ASPS],[Wire Transfer USP]" +
                         ",[Wire Transfer ASPS],[Credit Card USP],[Credit Card ASPS],[Check USP],[Check ASPS]) values ";

            for (int i = 0; i < vendorid.Count; i++)
            {

                insert += " ('" + dt.Rows[0][0].ToString() + "'";
                insert += ",'" + vendorid[i].ToString() + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'";
                insert += ",'" + 0 + "'),";

            }
            insert = insert.Remove(insert.Length - 1, 1);
            cls.ExecuteQuery(insert);
            return "True";
        }
        //Update Selected USP in DataList
        [WebMethod]
        public static string Update(string USP_ID, string USPname, string Email, string Website, string StopWebsite, string UserName, string Password, string chkUSP, string itemSelec)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dtCount = new DataTable();
            DataTable dtUSP = new DataTable();
            string USPID = "";
            string[] arrItem;
            string newItem = "";
            ArrayList arrPages = new ArrayList();
            arrItem = itemSelec.Split(',');

            List<string> vendorid = new List<string>();
            List<string> VendorIDRemove = new List<string>();


            arrPages.Add("USP General Info");
            arrPages.Add("-");
            cls.FUNC.Audit("Update USP : " + USPname, arrPages);



            //Select USP Count
            string selectCount = "Select Count (*) as CountID From tbl_UC_USPcapabilities_Start_Service Where USP_ID = '" + USP_ID + "'";
            dtCount = cls.GetData(selectCount);

            if (chkUSP == "0")
            {
                string insert = "Update [tbl_UC_USP_Name_Vendor_ID] Set USP_Name = '" + USPname + "',";
                insert += "[Email Address] ='" + Email + "'";
                insert += ",[Website] = '" + Website + "'";
                insert += ",[StopWebsite] = '" + StopWebsite + "'";
                insert += ",[Login Name] = '" + UserName + "' ";
                insert += ",[Password] = '" + Password + "'";
                insert += ",[NTID] = '" + HttpContext.Current.Session["ntid"] + "'";
                insert += ",[Last_Modified] = '" + DateTime.Now.ToString() + "'";
                insert += " Where USP_ID = '" + USP_ID + "'";
                cls.ExecuteQuery(insert);
            }
            else
            {
                for (int i = 0; i < Convert.ToInt32(dtCount.Rows[0][0]); i++)
                {
                    string SelectUSP = "Select * From tbl_UC_USPcapabilities_Start_Service Where USP_ID = '" + USP_ID + "'";
                    dtUSP = cls.GetData(SelectUSP);
                    VendorIDRemove.Add(dtUSP.Rows[i][1].ToString());
                }

                string UpdateTag = "";

                for (int i = 0; i < VendorIDRemove.Count(); i++)
                {
                    UpdateTag += "Update tbl_UC_USP_Normalization SET isTag = 0 Where VendorID = '" + VendorIDRemove[i] + "';";
                }

                cls.ExecuteQuery(UpdateTag);

                UpdateTag = "";

                for (int i = 0; i < arrItem.Count(); i++)
                {
                    UpdateTag += "Update tbl_UC_USP_Normalization SET isTag = 1 Where VendorID = '" + arrItem[i] + "';";
                }
                cls.ExecuteQuery(UpdateTag);

                string UpdateNameVendorID = "Update [tbl_UC_USP_Name_Vendor_ID] Set USP_Name = '" + USPname + "',";
                UpdateNameVendorID += "[Email Address] ='" + Email + "'";
                UpdateNameVendorID += ",[Website] = '" + Website + "'";
                UpdateNameVendorID += ",[StopWebsite] = '" + StopWebsite + "'";
                UpdateNameVendorID += ",[Login Name] = '" + UserName + "' ";
                UpdateNameVendorID += ",[Password] = '" + Password + "'";
                UpdateNameVendorID += ",[NTID] = '" + HttpContext.Current.Session["ntid"] + "'";
                UpdateNameVendorID += " Where USP_ID = '" + USP_ID + "'";
                cls.ExecuteQuery(UpdateNameVendorID);

                string selectID = "Select * FROM [dbo].[tbl_UC_USP_Name_Vendor_ID] Where USP_ID = '" + USP_ID + "'";
                dt = cls.GetData(selectID);

                string DeleteStart = "Delete [tbl_UC_USPcapabilities_Start_Service] Where USP_ID = '" + USP_ID + "'";
                string DeleteStop = "Delete [tbl_UC_USPcapabilities_Stop_Service] Where USP_ID = '" + USP_ID + "'";
                string DeleteInvoicing = "Delete [tbl_UC_USPcapabilities_Invoicing] Where USP_ID = '" + USP_ID + "'";
                string DeleteBills = "Delete [tbl_UC_USPcapabilities_BillsPayment] Where USP_ID = '" + USP_ID + "'";
                cls.ExecuteQuery(DeleteStart);
                cls.ExecuteQuery(DeleteStop);
                cls.ExecuteQuery(DeleteInvoicing);
                cls.ExecuteQuery(DeleteBills);

                string insert = "Insert INTO [tbl_UC_USPcapabilities_Start_Service]  (USP_ID,Vendor_ID,[Bulk Email USP],[Bulk Email ASPS],[Online Request USP]" +
                             ",[Online Request ASPS],[EDI 814 USP],[EDI 814 ASPS],[Call USP],[Call ASPS]) values ";
                for (int i = 0; i < arrItem.Count(); i++)
                {
                    insert += "('" + USP_ID + "'";
                    insert += ",'" + arrItem[i].ToString() + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'),";

                }
                insert = insert.Remove(insert.Length - 1, 1);
                cls.ExecuteQuery(insert);

                insert = "Insert INTO [tbl_UC_USPcapabilities_Stop_Service]  (USP_ID,Vendor_ID,[Bulk Email USP],[Bulk Email ASPS],[Online Request USP]" +
                            ",[Online Request ASPS],[EDI 814 USP],[EDI 814 ASPS],[Call USP],[Call ASPS]) values ";

                for (int i = 0; i < arrItem.Count(); i++)
                {
                    insert += "('" + dt.Rows[0][0].ToString() + "'";
                    insert += ",'" + arrItem[i].ToString() + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'),";

                }
                insert = insert.Remove(insert.Length - 1, 1);
                cls.ExecuteQuery(insert);

                insert = "Insert INTO [tbl_UC_USPcapabilities_Invoicing]  (USP_ID,Vendor_ID,[Summary Billing USP],[Summary Billing ASPS],[E-Bill w/ Password USP]" +
                             ",[E-Bill w/ Password ASPS],[E-Bill w/o Password USP],[E-Bill w/o Password ASPS],[E-Bill Via Link USP],[E-Bill Via Link ASPS] " +
                             ",[EDI 810 USP],[EDI 810 ASPS],[Paper Bill USP],[Paper Bill ASPS]) values ";

                for (int i = 0; i < arrItem.Count(); i++)
                {

                    insert += " ('" + dt.Rows[0][0].ToString() + "'";
                    insert += ",'" + arrItem[i].ToString() + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'),";
                }
                insert = insert.Remove(insert.Length - 1, 1);
                cls.ExecuteQuery(insert);

                insert = "Insert INTO [tbl_UC_USPcapabilities_BillsPayment]  (USP_ID,Vendor_ID,[ACH USP],[ACH ASPS],[Wire Transfer USP]" +
                             ",[Wire Transfer ASPS],[Credit Card USP],[Credit Card ASPS],[Check USP],[Check ASPS]) values ";
                for (int i = 0; i < arrItem.Count(); i++)
                {

                    insert += "('" + dt.Rows[0][0].ToString() + "'";
                    insert += ",'" + arrItem[i].ToString() + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'";
                    insert += ",'" + 0 + "'),";
                }
                insert = insert.Remove(insert.Length - 1, 1);
                cls.ExecuteQuery(insert);
            }



            return "True";
        }

        [WebMethod]
        public static string fileUploadExcel(string fileUploadExcel)
        {

            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();


            string filePathStr = "C:\\Users\\" + HttpContext.Current.Session["ntid"] + "\\Desktop\\" + fileUploadExcel;
            string qry = "";

            string ExcelContentType = "application/vnd.ms-excel";
            string Excel2010ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Check the Content Type of the file 
            try
            {
                //Save file path 
                string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", filePathStr);

                // Create Connection to Excel Workbook 
                using (OleDbConnection connection =
                             new OleDbConnection(excelConnectionString))
                {
                    OleDbCommand command = new OleDbCommand
                            ("Select * FROM [Sheet1$]", connection);
                    //("Select * FROM [Sheet1$]", connection);
                    connection.Open();

                    // Create DbDataReader to Data Worksheet 
                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        string VendorIDs = "";
                        // SQL Server Connection String 
                        //string sqlConnectionString = @"Data Source=piv8dbmisnp01;Initial Catalog=MIS_ALTI;Integrated Security=True";
                        string sqlConnectionString = @"Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";
                        
                        dt.Load(dr);
                        // dt.Columns.Remove("USP Name");

                        foreach (DataRow Datar in dt.Rows)
                        {
                            if (Datar["ID"].ToString() == "")
                            {
                                Datar.Delete();
                            }
                        }
                        dt.AcceptChanges();
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i != dt.Rows.Count - 1)
                            {
                                VendorIDs += "'" + dt.Rows[i][1].ToString() + "',";
                            }
                            else
                            {
                                VendorIDs += "'" + dt.Rows[i][1].ToString() + "'";
                            }
                        }




                        DataTable dtRemove = new DataTable();
                        DataTable dtInsertVIDStart = new DataTable();
                        DataTable dtInsertVIDStop = new DataTable();
                        DataTable dtInsertVIDInvoice = new DataTable();
                        DataTable dtInsertVIDBills = new DataTable();
                        DataTable dtInsertInfo = new DataTable();
                        string checkNull = "";
                        string qryCheckNormaliz = "Select Distinct Vendor_ID FRom tbl_UC_USPcapabilities_Start_Service ";
                        dtRemove = cls.GetData(qryCheckNormaliz);

                        dtInsertVIDStart.Columns.Add("USP_ID");
                        dtInsertVIDStart.Columns.Add("Vendor_ID");

                        List<string> lsVendor = new List<string>();
                        List<string> lsID = new List<string>();


                        for (int i = 0; i < dtRemove.Rows.Count; i++)
                        {
                            lsVendor.Add(dtRemove.Rows[i][0].ToString());
                        }

                        int iii = 0;
                        foreach (DataRow Datar in dt.Rows)
                        {
                            string VID = Datar["VendorID"].ToString();
                            if (lsVendor.Contains(VID))
                            {

                            }
                            else
                            {
                                DataRow drNew = dtInsertVIDStart.NewRow();
                                drNew[0] = dt.Rows[iii][0].ToString();
                                drNew[1] = dt.Rows[iii][1].ToString();
                                dtInsertVIDStart.Rows.Add(drNew.ItemArray);
                            }

                            iii++;
                        }

                        dt.Columns.Remove("VendorID");
                        dt.Columns.Remove("F8");
                        DataTable distinctTable = dt.DefaultView.ToTable( /*distinct*/ true);


                        //for(int i = 0; i < distinctTable.Rows.Count; i++)
                        //{
                        //    if (lsID.Contains(distinctTable.Rows[i][0].ToString()))
                        //    {

                        //    }
                        //    else
                        //    {
                        //        lsID.Add(distinctTable.Rows[i][0].ToString());
                        //    }
                        //}

                        int iv = 0;
                        List<string> lsIdDelete = new List<string>();
                        foreach (DataRow Datar in distinctTable.Rows)
                        {


                            if (lsID.Contains(Datar["ID"].ToString()))
                            {
                                lsIdDelete.Add(iv.ToString());

                            }
                            else
                            {
                                lsID.Add(Datar["ID"].ToString());
                            }
                            iv++;
                        }

                        DataTable dtInsertNameVID = new DataTable();
                        dtInsertNameVID.Columns.Add("ID");
                        dtInsertNameVID.Columns.Add("USP Name");
                        dtInsertNameVID.Columns.Add("Email Address");
                        dtInsertNameVID.Columns.Add("Website");
                        dtInsertNameVID.Columns.Add("Username");
                        dtInsertNameVID.Columns.Add("Password");

                        for (int i = 0; i < distinctTable.Rows.Count; i++)
                        {
                            //   string  passVal = lsIdDelete[i];
                            // distinctTable.Rows[Int32.Parse(passVal)].Delete();

                            if (lsIdDelete.Contains(i.ToString()))
                            {

                            }
                            else
                            {
                                dtInsertNameVID.Rows.Add(distinctTable.Rows[i][0], distinctTable.Rows[i][1], distinctTable.Rows[i][2], distinctTable.Rows[i][3], distinctTable.Rows[i][4], distinctTable.Rows[i][5]);

                            }

                        }

                        string qryUpdate = "";
                        for (int i = 0; i < dtInsertNameVID.Rows.Count; i++)
                        {
                            qryUpdate += " Update tbl_UC_USP_Name_Vendor_ID Set ";
                            qryUpdate += " USP_Name = '" + dtInsertNameVID.Rows[i][1] + "'";
                            qryUpdate += ", [Email Address] = '" + dtInsertNameVID.Rows[i][2] + "' ";
                            qryUpdate += ", [Website] = '" + dtInsertNameVID.Rows[i][3] + "' ";
                            qryUpdate += ", [Login Name] = '" + dtInsertNameVID.Rows[i][4] + "' ";
                            qryUpdate += ", Password = '" + dtInsertNameVID.Rows[i][5] + "' ";
                            qryUpdate += ", NTID = '" + HttpContext.Current.Session["ntid"] + "' ";
                            qryUpdate += " Where USP_ID = '" + dtInsertNameVID.Rows[i][0] + "'; ";
                        }

                        cls.ExecuteQuery(qryUpdate);




                        DataTable dtDistinct = dtInsertVIDStart.DefaultView.ToTable( /*distinct*/ true);

                        cls.SQLBulkCopy("atbl_UC_USPcapabilities_Start_Service", dtDistinct);
                        cls.SQLBulkCopy("atbl_UC_USPcapabilities_Stop_Service", dtDistinct);
                        cls.SQLBulkCopy("atbl_UC_USPcapabilities_BillsPayment", dtDistinct);
                        cls.SQLBulkCopy("atbl_UC_USPcapabilities_Invoicing", dtDistinct);


                    }
                }
                //return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd2 = dt2 } });
                return "True";
            }


            catch (Exception ex)
            {
                return "False";
            }


        }
    }
}
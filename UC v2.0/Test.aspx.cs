﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class Test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetVendorList(string Alpha)
        {
            clsConnection cls = new clsConnection();

            Alpha = Alpha.Replace(",", "','");
            Alpha = Alpha.Replace("__", " ");
            string qry = "Select * From tbl_UC_USP_Normalization Where isTag = 0 AND isActive = 1 and SUBSTRING(VendorName, 1, 1) IN ('" + Alpha + "')";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetAlpha()
        {
            clsConnection cls = new clsConnection();
            string qry = @"Select Distinct REPLACE(SUBSTRING(VendorName, 1, 1),' ','__') As NewColumnfrom from tbl_UC_USP_Normalization 
                           where isTag = 0 AND isActive = 1 order by NewColumnfrom ";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
    }
}
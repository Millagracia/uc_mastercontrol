﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DataFeed.aspx.cs" Inherits="UC_v2._0.DataFeed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        .weekDays-selector input {
            display: none !important;
        }

            .weekDays-selector input[type=checkbox] + label {
                display: inline-block;
                border-radius: 6px;
                background: #dddddd;
                height: 40px;
                width: 30px;
                margin-right: 3px;
                line-height: 40px;
                text-align: center;
                cursor: pointer;
            }

            .weekDays-selector input[type=checkbox]:checked + label {
                background: #2AD705;
                color: #ffffff;
            }
    </style>
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
<link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css"
    rel="Stylesheet" type="text/css" />
    <script src="http://yandex.st/json2/2011-10-19/json2.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/json2/20160511/json2.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3>&nbsp Data Feed</h3>
                <div class="panel panel-primary" style="background-color: #ecf0f5; margin-left: 3px">
                    <div class="panel-body" style="background-color: white">
                        <div id="dvTable">
                            <div class="col-xs-11" style="padding-left: 0px; height: 605px">
                                <table id="tblDataFeed" class="table">
                                    <thead>
                                        <tr>
                                            <th data-field="Name" data-sortable="true">Name</th>
                                            <th data-field="CreatedBy" data-sortable="true">Created By</th>
                                            <th data-field="CreatedDate" data-sortable="true">Created Date</th>
                                            <th data-field="id" data-sortable="true">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <button type="button" id="btnAddnew" class="btn btn-primary btn-sm">AddNew</button>
                        </div>
                        <div id="dvAddNew" style="display: none">
                            <div class="row">
                                <div class="col-xs-1 text-right" style="padding-top: 6px">
                                    Title/ File Name:
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" id="txtTitle">
                                </div>
                                <div class="col-lg-1 text-right" style="padding-top: 6px">
                                    On Days :
                                </div>
                            </div>
                            <div class="row" style="padding-top: 5px">
                                <div class="col-xs-1 text-right" style="padding-top: 6px">
                                    Description :
                                </div>
                                <div class="col-lg-3">
                                    <textarea class="form-control" rows="5" id="txtDesc"></textarea>
                                </div>
                                <div class="col-lg-1"></div>
                                <div class="col-lg-3">
                                    <div class="weekDays-selector">
                                        <input type="checkbox" id="weekday-mon" class="weekday" />
                                        <label for="weekday-mon">M</label>
                                        <input type="checkbox" id="weekday-tue" class="weekday" />
                                        <label for="weekday-tue">T</label>
                                        <input type="checkbox" id="weekday-wed" class="weekday" />
                                        <label for="weekday-wed">W</label>
                                        <input type="checkbox" id="weekday-thu" class="weekday" />
                                        <label for="weekday-thu">TH</label>
                                        <input type="checkbox" id="weekday-fri" class="weekday" />
                                        <label for="weekday-fri">F</label>
                                        <input type="checkbox" id="weekday-sat" class="weekday" />
                                        <label for="weekday-sat">S</label>
                                        <input type="checkbox" id="weekday-sun" class="weekday" />
                                        <label for="weekday-sun">SU</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 5px">
                                <div class="col-xs-1 text-right" style="padding-top: 6px">
                                    At Location :
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" id="filePathLoc" class="form-control" />
                                </div>
                                <div class="col-lg-1 text-right" style="padding-top: 6px">
                                    At Time :
                                </div>
                                <div class="col-lg-2 ">
                                    <asp:TextBox ID="txtTime" ClientIDMode="Static" runat="server" TextMode="Time" class="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-1 text-left" style="padding-top: 6px; width: 12px">
                                    to
                                </div>
                                <div class="col-lg-2 ">
                                    <asp:TextBox ID="txtTimeTo" ClientIDMode="Static" runat="server" TextMode="Time" class="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-2 " style="padding-left: 0px">
                                    <asp:DropDownList ID="ddlTimezone" runat="server" class="form-control" ClientIDMode="Static">
                                        <asp:ListItem>Select TimeZone</asp:ListItem>
                                        <asp:ListItem>AST</asp:ListItem>
                                        <asp:ListItem>CST</asp:ListItem>
                                        <asp:ListItem>EST</asp:ListItem>
                                        <asp:ListItem>HST</asp:ListItem>
                                        <asp:ListItem>MST</asp:ListItem>
                                        <asp:ListItem>PST</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <br />
                            <div class="row">
                                <div class="col-lg-12 text-bold">
                                    <h3>ALERTS</h3>

                                    <svg height="20" width="1500">
                                        <line x1="0" y1="10" x2="20000" y2="10" style="stroke: #006add; stroke-width: 2" />
                                    </svg>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 " style="padding-right: 0px">
                                    <button type="button" class="btn btn-primary" id="btnAlertMessage" data-toggle="modal" data-target="#myModal">Alert Message</button>
                                </div>
                            </div>
                            <br />
                            <div class="row" id="dvCondition1">
                                <div class="col-lg-3 " style="padding-right: 0px">
                                    <div class="col-lg-2 " style="padding-right: 0px; padding-top: 6px">
                                    </div>
                                    <div class="col-lg-1 " style="padding-right: 0px; padding-top: 6px">
                                        <asp:CheckBox ID="cbIsActive1" ClientIDMode="Static" runat="server" />
                                    </div>
                                    <div class="col-lg-3 " style="padding-right: 0px; padding-top: 6px">
                                        If file is
                                    </div>
                                    <div class="col-lg-6 " style="padding-left: 0px">
                                        <asp:DropDownList ID="ddlAlertRecei1" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Received</asp:ListItem>
                                            <asp:ListItem>Not Received</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 " style="padding-right: 0px; padding-left: 0px">
                                    <div class="col-lg-5 " style="padding-left: 0px">
                                        <asp:DropDownList ID="ddAlertGLE1" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Greater Than</asp:ListItem>
                                            <asp:ListItem>Less Than</asp:ListItem>
                                            <asp:ListItem>Equal to</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-3 " style="padding-left: 0px">
                                        <asp:TextBox ID="txtHour1" ClientIDMode="Static" class="form-control" runat="server" TextMode="Number"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-4 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:DropDownList ID="ddlHMS1" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Minutes</asp:ListItem>
                                            <asp:ListItem>Hours</asp:ListItem>
                                            <asp:ListItem>Days</asp:ListItem>
                                            <asp:ListItem>Weeks</asp:ListItem>
                                            <asp:ListItem>Months</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 " style="padding-left: 0px; padding-right: 0px">
                                    <div class="col-lg-3 " style="padding-right: 0px; padding-top: 6px">
                                        then send
                                    </div>
                                    <div class="col-lg-6 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:DropDownList ID="ddlAlertSuc1" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Success Alert</asp:ListItem>
                                            <asp:ListItem>Failure Alert</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-2 text-center" style="padding-right: 0px; padding-top: 6px">
                                        to
                                    </div>


                                </div>
                                <div class="col-lg-3 " style="padding-left: 0px">
                                    <div class="col-lg-7 " style="padding-left: 0px; padding-right: 0px">

                                        <asp:TextBox ID="txtTo1" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                        <label id="lblTo1"></label>
                                    </div>
                                    <div class="col-lg-2 " style="padding-left: 5px; padding-right: 0px">
                                        <button type="button" id="btnPlus1" class="btn btn-default">
                                            <span>
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </span>
                                        </button>
                                    </div>
                                    <div class="col-lg-3 " style="padding-left: 0px; padding-right: 0px">
                                        <div id="dvAddNew1">
                                            <button type="button" class="btn btn-primary form-control" id="btnAddNew1">Add New</button>

                                        </div>

                                        <div id="dvRemove1" style="display: none">
                                            <button type="button" class="btn btn-danger form-control" id="btnRemove1">Remove</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br />
                            <div class="row" id="dvCondition2" style="display: none">
                                <div class="col-lg-3 " style="padding-right: 0px">
                                    <div class="col-lg-2 " style="padding-right: 0px; padding-top: 6px">
                                    </div>
                                    <div class="col-lg-1 " style="padding-right: 0px; padding-top: 6px">
                                        <asp:CheckBox ID="cbIsActive2" ClientIDMode="Static" runat="server" />
                                    </div>
                                    <div class="col-lg-3 " style="padding-right: 0px; padding-top: 6px">
                                        If file is
                                    </div>
                                    <div class="col-lg-6 " style="padding-left: 0px">
                                        <asp:DropDownList ID="ddlRecei2" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Received</asp:ListItem>
                                            <asp:ListItem>Not Received</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 " style="padding-right: 0px; padding-left: 0px">
                                    <div class="col-lg-5 " style="padding-left: 0px">
                                        <asp:DropDownList ID="ddlGLE2" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Greater Than</asp:ListItem>
                                            <asp:ListItem>Less Than</asp:ListItem>
                                            <asp:ListItem>Equal to</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-3 " style="padding-left: 0px">
                                        <asp:TextBox ID="txtAlertHour2" ClientIDMode="Static" class="form-control" runat="server" TextMode="Number"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-4 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:DropDownList ID="ddlHMS2" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Minutes</asp:ListItem>
                                            <asp:ListItem>Hours</asp:ListItem>
                                            <asp:ListItem>Days</asp:ListItem>
                                            <asp:ListItem>Weeks</asp:ListItem>
                                            <asp:ListItem>Months</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 " style="padding-left: 0px; padding-right: 0px">
                                    <div class="col-lg-3 " style="padding-right: 0px; padding-top: 6px">
                                        then send
                                    </div>
                                    <div class="col-lg-6 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:DropDownList ID="ddlSucc2" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Success Alert</asp:ListItem>
                                            <asp:ListItem>Failure Alert</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-2 text-center" style="padding-right: 0px; padding-top: 6px">
                                        to
                                    </div>


                                </div>
                                <div class="col-lg-3 " style="padding-left: 0px">
                                    <div class="col-lg-7 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:TextBox ID="txtTo2" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                        <label id="lblTo2"></label>
                                    </div>
                                    <div class="col-lg-2 " style="padding-left: 5px; padding-right: 0px">
                                        <button type="button" id="btnPlus2" class="btn btn-default">
                                            <span>
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </span>
                                        </button>
                                    </div>
                                    <div class="col-lg-3 " style="padding-left: 0px; padding-right: 0px">
                                        <div id="dvAddNew2">
                                            <button type="button" class="btn btn-primary form-control" id="btnAddNew2">Add New</button>

                                        </div>

                                        <div id="dvRemove2">
                                            <button type="button" class="btn btn-danger form-control" id="btnRemove2">Remove</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br />
                            <div class="row" id="dvCondition3" style="display: none">
                                <div class="col-lg-3 " style="padding-right: 0px">
                                    <div class="col-lg-2 " style="padding-right: 0px; padding-top: 6px">
                                    </div>
                                    <div class="col-lg-1 " style="padding-right: 0px; padding-top: 6px">
                                        <asp:CheckBox ID="cbIsActive3" ClientIDMode="Static" runat="server" />
                                    </div>
                                    <div class="col-lg-3 " style="padding-right: 0px; padding-top: 6px">
                                        If file is
                                    </div>
                                    <div class="col-lg-6 " style="padding-left: 0px">
                                        <asp:DropDownList ID="ddlRecei3" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Received</asp:ListItem>
                                            <asp:ListItem>Not Received</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 " style="padding-right: 0px; padding-left: 0px">
                                    <div class="col-lg-5 " style="padding-left: 0px">
                                        <asp:DropDownList ID="ddlGLE3" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Greater Than</asp:ListItem>
                                            <asp:ListItem>Less Than</asp:ListItem>
                                            <asp:ListItem>Equal to</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-3 " style="padding-left: 0px">
                                        <asp:TextBox ID="txtAlertHour3" ClientIDMode="Static" class="form-control" runat="server" TextMode="Number"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-4 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:DropDownList ID="ddlHMS3" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Minutes</asp:ListItem>
                                            <asp:ListItem>Hours</asp:ListItem>
                                            <asp:ListItem>Days</asp:ListItem>
                                            <asp:ListItem>Weeks</asp:ListItem>
                                            <asp:ListItem>Months</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 " style="padding-left: 0px; padding-right: 0px">
                                    <div class="col-lg-3 " style="padding-right: 0px; padding-top: 6px">
                                        then send
                                    </div>
                                    <div class="col-lg-6 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:DropDownList ID="ddlSucc3" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Success Alert</asp:ListItem>
                                            <asp:ListItem>Failure Alert</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-2 text-center" style="padding-right: 0px; padding-top: 6px">
                                        to
                                    </div>


                                </div>
                                <div class="col-lg-3 " style="padding-left: 0px">
                                    <div class="col-lg-7 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:TextBox ID="txtTo3" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                        <label id="lblTo3"></label>
                                    </div>
                                    <div class="col-lg-2 " style="padding-left: 5px; padding-right: 0px">
                                        <button type="button" id="btnPlus3" class="btn btn-default">
                                            <span>
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </span>
                                        </button>
                                    </div>
                                    <div class="col-lg-3 " style="padding-left: 0px; padding-right: 0px">
                                        <div id="dvAddNew3">
                                            <button type="button" class="btn btn-primary form-control" id="btnAddNew3">Add New</button>
                                        </div>
                                        <div id="dvRemove3">
                                            <button type="button" class="btn btn-danger form-control" id="btnRemove3">Remove</button>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <br />
                            <div class="row" id="dvCondition4" style="display: none">
                                <div class="col-lg-3 " style="padding-right: 0px">
                                    <div class="col-lg-2 " style="padding-right: 0px; padding-top: 6px">
                                    </div>
                                    <div class="col-lg-1 " style="padding-right: 0px; padding-top: 6px">
                                        <asp:CheckBox ID="cbIsActive4" ClientIDMode="Static" runat="server" />
                                    </div>
                                    <div class="col-lg-3 " style="padding-right: 0px; padding-top: 6px">
                                        If file is
                                    </div>
                                    <div class="col-lg-6 " style="padding-left: 0px">
                                        <asp:DropDownList ID="ddlRecei4" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Received</asp:ListItem>
                                            <asp:ListItem>Not Received</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 " style="padding-right: 0px; padding-left: 0px">
                                    <div class="col-lg-5 " style="padding-left: 0px">
                                        <asp:DropDownList ID="ddlGLE4" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Greater Than</asp:ListItem>
                                            <asp:ListItem>Less Than</asp:ListItem>
                                            <asp:ListItem>Equal to</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-3 " style="padding-left: 0px">
                                        <asp:TextBox ID="txtAlertHour4" ClientIDMode="Static" class="form-control" runat="server" TextMode="Number"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-4 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:DropDownList ID="ddlHMS4" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Minutes</asp:ListItem>
                                            <asp:ListItem>Hours</asp:ListItem>
                                            <asp:ListItem>Days</asp:ListItem>
                                            <asp:ListItem>Weeks</asp:ListItem>
                                            <asp:ListItem>Months</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 " style="padding-left: 0px; padding-right: 0px">
                                    <div class="col-lg-3 " style="padding-right: 0px; padding-top: 6px">
                                        then send
                                    </div>
                                    <div class="col-lg-6 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:DropDownList ID="ddlSucc4" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Success Alert</asp:ListItem>
                                            <asp:ListItem>Failure Alert</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-2 text-center" style="padding-right: 0px; padding-top: 6px">
                                        to
                                    </div>


                                </div>
                                <div class="col-lg-3 " style="padding-left: 0px">
                                    <div class="col-lg-7 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:TextBox ID="txtTo4" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                        <label id="lblTo4"></label>
                                    </div>
                                    <div class="col-lg-2 " style="padding-left: 5px; padding-right: 0px">
                                        <button type="button" id="btnPlus4" class="btn btn-default">
                                            <span>
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </span>
                                        </button>
                                    </div>
                                    <div class="col-lg-3 " style="padding-left: 0px; padding-right: 0px">
                                        <div id="dvAddNew4">
                                            <button type="button" class="btn btn-primary form-control" id="btnAddNew4">Add New</button>
                                        </div>
                                        <div id="dvRemove4">
                                            <button type="button" class="btn btn-danger form-control" id="btnRemove4">Remove</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br />
                            <div class="row" id="dvCondition5" style="display: none">
                                <div class="col-lg-3 " style="padding-right: 0px">
                                    <div class="col-lg-2 " style="padding-right: 0px; padding-top: 6px">
                                    </div>
                                    <div class="col-lg-1 " style="padding-right: 0px; padding-top: 6px">
                                        <asp:CheckBox ID="cbIsActive5" ClientIDMode="Static" runat="server" />
                                    </div>
                                    <div class="col-lg-3 " style="padding-right: 0px; padding-top: 6px">
                                        If file is
                                    </div>
                                    <div class="col-lg-6 " style="padding-left: 0px">
                                        <asp:DropDownList ID="ddlRecei5" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Received</asp:ListItem>
                                            <asp:ListItem>Not Received</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 " style="padding-right: 0px; padding-left: 0px">
                                    <div class="col-lg-5 " style="padding-left: 0px">
                                        <asp:DropDownList ID="ddlGLE5" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Greater Than</asp:ListItem>
                                            <asp:ListItem>Less Than</asp:ListItem>
                                            <asp:ListItem>Equal to</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-3 " style="padding-left: 0px">
                                        <asp:TextBox ID="txtAlertHour5" ClientIDMode="Static" class="form-control" runat="server" TextMode="Number"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-4 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:DropDownList ID="ddlHMS5" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Minutes</asp:ListItem>
                                            <asp:ListItem>Hours</asp:ListItem>
                                            <asp:ListItem>Days</asp:ListItem>
                                            <asp:ListItem>Weeks</asp:ListItem>
                                            <asp:ListItem>Months</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 " style="padding-left: 0px; padding-right: 0px">
                                    <div class="col-lg-3 " style="padding-right: 0px; padding-top: 6px">
                                        then send
                                    </div>
                                    <div class="col-lg-6 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:DropDownList ID="ddlSucc5" ClientIDMode="Static" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Success Alert</asp:ListItem>
                                            <asp:ListItem>Failure Alert</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-2 text-center" style="padding-right: 0px; padding-top: 6px">
                                        to
                                    </div>


                                </div>
                                <div class="col-lg-3 " style="padding-left: 0px">
                                    <div class="col-lg-7 " style="padding-left: 0px; padding-right: 0px">
                                        <asp:TextBox ID="txtTo5" ClientIDMode="Static" class="form-control" runat="server"></asp:TextBox>
                                        <label id="lblTo5"></label>
                                    </div>
                                    <div class="col-lg-2 " style="padding-left: 5px; padding-right: 0px">
                                        <button type="button" id="btnPlus5" class="btn btn-default">
                                            <span>
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </span>
                                        </button>
                                    </div>
                                    <div class="col-lg-3 " style="padding-left: 0px; padding-right: 0px">
                                        <div id="dvAddNew5">
                                            <button type="button" class="btn btn-primary form-control" id="btnAddNew5">Add New</button>
                                        </div>
                                        <div id="dvRemove5">
                                            <button type="button" class="btn btn-danger form-control" id="btnRemove5">Remove</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-5"></div>
                                <div class="col-lg-1">
                                    <button type="button" class="btn btn-primary" id="btnApply">Apply</button>
                                </div>
                                <div class="col-lg-1">
                                    <button type="button" class="btn btn-primary" id="btnReset">Reset</button>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Alert Message</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-2">
                            Success Alert
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-2 text-right" style="padding-top: 6px">
                            Subject :
                        </div>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="txtSuccAlert" />
                        </div>

                        <div class="col-lg-2 text-right" style="padding-top: 6px">
                            Subject :
                        </div>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="txtFailAlert" />
                        </div>
                    </div>

                    <div class="row" style="padding-top: 10px">
                        <div class="col-lg-2 text-right" style="padding-top: 6px">
                            Message :
                        </div>
                        <div class="col-lg-4">
                            <textarea class="form-control" rows="5" id="txtSuccAreaMessage"></textarea>
                        </div>
                        <div class="col-lg-2 text-right" style="padding-top: 6px">
                            Message :
                        </div>
                        <div class="col-lg-4">
                            <textarea class="form-control" rows="5" id="txtFailAreaMessage"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer " style="text-align: center">
                    <button type="button" class="btn btn-primary" id="btnOkay" data-dismiss="modal">Okay</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/DataFeed.js"></script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="OnlineRequestSetUp.aspx.cs" Inherits="UC_v2._0.OnlineRequestSetUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-8">
                <br />
                <br />
                <div class="box box-success">
                    <div class="row">
                        <div class="col-lg-4">
                            <br />
                            <br />
                            USP Name :
                    <asp:DropDownList ID="ddlUSP" class="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList>
                            <br />
                            <br />
                        </div>
                        <div class="col-lg-2">
                            <br />
                            <br />
                            Task :
                            <asp:DropDownList ID="ddlTask" class="form-control" ClientIDMode="Static" runat="server">
                                <asp:ListItem>Start Service</asp:ListItem>
                                <asp:ListItem>Stop Service</asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <br />
                        </div>
                        <div class="col-lg-2">
                            <br />
                            <br />
                            No of Pages :
                    <input type="text" class="form-control" id="txtPages">
                            <br />
                            <br />
                        </div>
                        <div class="col-lg-2">
                            <br />
                            <br />
                            Input Pages :
                            <asp:ListBox ID="lbIpages" runat="server" class="form-control" SelectionMode="Multiple" multiple="multiple" ClientIDMode="Static">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                                <asp:ListItem>9</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>21</asp:ListItem>
                                <asp:ListItem>22</asp:ListItem>
                                <asp:ListItem>23</asp:ListItem>
                                <asp:ListItem>24</asp:ListItem>
                                <asp:ListItem>25</asp:ListItem>
                                <asp:ListItem>26</asp:ListItem>
                                <asp:ListItem>27</asp:ListItem>
                                <asp:ListItem>28</asp:ListItem>
                                <asp:ListItem>29</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                            </asp:ListBox>
                            <br />
                            <br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <br />
                            <br />
                            <br />
                            <br />
                        </div>
                        <div class="col-lg-4">
                            <button type="button" id="btnSave" onclick="Save(this)" class="btn btn-primary btn-sm form-control">Save</button>
                        </div>
                        <div class="col-lg-4">

                            <br />
                            <br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="tblUSP" data-search="true" class="table" >
                                <thead>
                                    <tr>
                                        <th data-field="USP_Name">USP Name </th>
                                        <th data-field="Task">Task</th>
                                        <th data-field="Website">Website</th>
                                        <th data-field="NoOfPage">No of Pages</th>
                                        <th data-field="iPages" class="text-center">iPages</th>
                                        <th data-field="Login Name">Login Name</th>
                                        <th data-field="Password" class="text-center">Password</th>
                                        <th data-field="USP_ID" class="text-center" data-formatter="Edit"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <div class="modal fade" id="myModalEdit" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align: center">Edit USP</h4>
                </div>
                <div class="modal-body">
                     <div class="box box-success">
                    <div class="row">
                        <div class="col-lg-4">
                            <br />
                            <br />
                            USP Name :
                    <asp:DropDownList ID="ddlEditUSP" class="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList>
                            <br />
                            <br />
                        </div>
                        <div class="col-lg-2">
                            <br />
                            <br />
                            Task :
                            <asp:DropDownList ID="ddlEditTask" class="form-control" ClientIDMode="Static" runat="server">
                                <asp:ListItem>Start Service</asp:ListItem>
                                <asp:ListItem>Stop Service</asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <br />
                        </div>
                        <div class="col-lg-2">
                            <br />
                            <br />
                            No of Pages :
                    <input type="text" class="form-control" id="txtEditNoPages">
                            <br />
                            <br />
                        </div>
                        <div class="col-lg-2">
                            <br />
                            <br />
                            Input Pages :
                            <asp:ListBox ID="lbEditIpages" runat="server" class="form-control" SelectionMode="Multiple" multiple="multiple" ClientIDMode="Static">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                                <asp:ListItem>9</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>21</asp:ListItem>
                                <asp:ListItem>22</asp:ListItem>
                                <asp:ListItem>23</asp:ListItem>
                                <asp:ListItem>24</asp:ListItem>
                                <asp:ListItem>25</asp:ListItem>
                                <asp:ListItem>26</asp:ListItem>
                                <asp:ListItem>27</asp:ListItem>
                                <asp:ListItem>28</asp:ListItem>
                                <asp:ListItem>29</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                            </asp:ListBox>
                            <br />
                            <br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <br />
                            <br />
                            <br />
                            <br />
                        </div>
                        <div class="col-lg-4">

                            <br />
                            <br />
                        </div>
                    </div>
                </div>
                </div>
                <div class="modal-footer" style="text-align: center">
                    <button type="button" id="btnUpdate" onclick="Update(this)" class="btn btn-success btn-sm">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/OnlineRequestSetup.js"></script>
</asp:Content>

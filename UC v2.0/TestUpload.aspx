﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="TestUpload.aspx.cs" Inherits="UC_v2._0.TestUpload" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content-wrapper" >
         <input id="inUploadFile" type="file" class="file input-sm" data-show-preview="false" />

        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <button id="btnUploadPDF" type="button" class="btn btn-primary btn-sm">Upload</button>
        </div>
    </div>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $('#btnUploadPDF').click(function () {

          
                var fileUpload = $("#inUploadFile").get(0);
                var files = fileUpload.files;
                var test = new FormData();
                for (var i = 0; i < files.length; i++) {
                    test.append('Signature', files[i]);
                    var textFile = files[i];
                }

                $.ajax({
                    url: "FileUpload.ashx",
                    type: "POST",
                    contentType: false,
                    processData: false,
                    data: test,
                    success: function (result) {
                        alertify.alert(result);
                        $('#modalUpload').modal('hide');
                        $('#modalLoading').modal('hide');
                    },
                    error: function (err) {
                        alertify.alert(err.responseText);
                    }
                });
        
            });
           
    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Audit.aspx.cs" Inherits="UC_v2._0.Audit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 733px">
    <div class="content-header">
        <h1>Audit Trail</h1>
        <ol class="breadcrumb">
            <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
                <span><i class="fa fa-bar-search"></i>&nbsp;Audit Trail</span>
            </li>
        </ol>
    </div>

    <div class="content">
        <div class="box box-default">
            <div class="box-body">
                <table class="table no-border">
                    <tr>
                        <th>Time Frame:</th>
                        <td>
                            <select id="slctTime" class="form-control input-sm">
                                <option value="year">This Year</option>
								<option value="month">This Month</option>
								<option value="week">This Week</option>
								<option value="yesterday">Yesterday</option>
								<option value="today">Today</option>
                            </select>
                        </td>
                        <th>or</th>
                        <th>From</th>
                        <td>
                            <input id="inDateFrom" type="text" class="form-control input-sm datepicker" />
                        </td>
                        <th>To</th>
                        <td>
                            <input id="inDateTo" type="text" class="form-control input-sm datepicker" />
                        </td>
                    </tr>
                    <tr>
                        <th>By:</th>
                        <td>
                            <select id="slcNTID" class="form-control input-sm">
                                <option>All</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <div class="text-right">
                    <button id="btnApply" type="button" class="btn btn-primary btn-sm"><i class="fa fa-check"></i>&nbsp;Apply</button>
                </div>

                <div class="col-xs-12" style="margin-top: 1%;">
                    <table id="tblAudit" class="table table-bordered table-hover" data-single-select="true" style="display: none">
                        <thead>
                            <tr>
                                <th data-sortable="true" data-field="page">Page</th>
                                <th data-sortable="true" data-field="sub_page">Sub Page</th>
                                <th data-sortable="true" data-field="sub_sub_page">Sub-Sub Page</th>
                                <th data-sortable="true" data-field="action">Action</th>
                                <th data-sortable="true" data-field="action_by">Action By</th>
                                <th data-sortable="true" data-field="action_date">Action Date</th>
                                <th data-sortable="true" data-field="action_time">Action Time</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
     <script src="js/Audit.js"></script>
</asp:Content>

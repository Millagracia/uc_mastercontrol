﻿$(document).ready(function () {

    $('#slctPDF').select2({
        multiple: false

    });
    getPDF();
    getSource();
    getsusp1();

});

$('[data-toggle=popover]').popover({
    html: true,
    trigger: 'hover',
    placement: 'bottom',
    content: function () {
        return $('#popover-content').html();
    }
});



function getsusp1() {
    $.ajax({
        type: 'POST',
        url: 'PDFMap.aspx/Getusps',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#DrpStartStop').empty;
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.records;
                $.each(records, function (idx, val) {
                    $('#DrpStartStop').append(
                        '<option value="' + val.USPName + '">' + val.USPName + '</option>'
                    );
                });
                
              

            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnUpload').click(function () {
    $('.filter').select2({
        multiple: false

    });
    $('#modalUpload').modal({ backdrop: 'static', keyboard: false });
    $('#inUploadFile').fileinput('reset');
    $('#modalUpload input').val('');
});

$('#btnTransfer').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'PDFMap.aspx/Transfer',
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            $('#modalLoading').modal('hide');
            console.log(response.responseText);
        },
    });
});
$('#btnUploadPDF').click(function () {


    if ($('#DrpInput').val() == 'M') {
        var s = $('#DrpStartStop').val() + '_' + $('#DRPusppdf').val() + '_' + $('#Inppdf').val() + ('_') + $('#DrpInput').val();

    } else {
        var s = $('#DrpStartStop').val() + '_' + $('#DRPusppdf').val() + '_' + $('#Inppdf').val();
    }


    $('#inFileName').val(s);


    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });

    var usr = $('#usr').text();
    var filename = $('#inFileName').val();

    $.ajax({
        type: 'POST',
        url: 'PDFMap.aspx/UploadFile',
        data: '{filename: "' + filename + '", usr: "' + usr + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //console.log(data);

            if (data.d == '0') {
                var fileUpload = $("#inUploadFile").get(0);
                var files = fileUpload.files;
                var test = new FormData();
                for (var i = 0; i < files.length; i++) {
                    test.append(filename, files[i]);
                }
                $.ajax({
                    url: "FileUpload.ashx",
                    type: "POST",
                    contentType: false,
                    processData: false,
                    data: test,
                    success: function (result) {
                        alertify.alert(result);
                        getPDF();
                        $('#modalUpload').modal('hide');

                        $('#modalLoading').modal('hide');
                    },
                    error: function (err) {
                        alertify.alert(err.responseText);
                    }
                });
            } else if (data.d == 'Error') {
                alertify.alert(data.d);
                $('#modalLoading').modal('hide');
            } else if (data.d == '1') {
                alertify.confirm('Filename already exists! Overwrite existing file?',
                      function () {
                          var fileUpload = $("#inUploadFile").get(0);
                          var files = fileUpload.files;
                          var test = new FormData();
                          for (var i = 0; i < files.length; i++) {
                              test.append(filename, files[i]);
                          }

                          $.ajax({
                              url: "FileUpload.ashx",
                              type: "POST",
                              contentType: false,
                              processData: false,
                              data: test,
                              success: function (result) {
                                //  alertify.alert(result);
                                  getPDF();
                                  $('#modalUpload').modal('hide');

                                  $('#modalLoading').modal('hide');
                              },
                              error: function (err) {
                                  alertify.alert(err.responseText);
                              }
                          });

                      },
                      function () {

                      });
                //alert('Filename already exists!');
                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnSlect').click(function () {
    $('#modalSource').modal({ backdrop: 'static', keyboard: false });
});

$('#btnApplySrc').click(function () {
    var srcTitle = [];
    var groupid = "";
    var groupname = "";
    var chkCount = 0;

    $.each($('#tblRepository tbody tr'), function (idx, val) {
        if ($(this).find('input[type="checkbox"]').is(':checked')) {
            chkCount = 1;
            srcTitle.push($(this).find('td:eq(1)').html());
            groupid += $(this).find('input[type="checkbox"]').attr('data-id') + ',';

            $('#tblSource tbody').append(
				'<tr>' +
					'<td nowrap>' + $(this).find('td:eq(1)').html() + '</td>' +
					'<td nowrap>Data Map</td>' +
				'</tr>'
			);
        }
    });

    $.each($('#tblCustom tbody tr'), function (idx, val) {
        if ($(this).find('input[type="checkbox"]').is(':checked')) {
            chkCount = 1;
            srcTitle.push($(this).find('td:eq(1)').html());
            groupname += $(this).find('input[type="checkbox"]').attr('data-id') + ',';

            $('#tblSource tbody').append(
				'<tr>' +
					'<td nowrap>' + $(this).find('td:eq(1)').html() + '</td>' +
					'<td nowrap>Custom Data</td>' +
				'</tr>'
			);
        }
    });

    groupid = groupid.slice(0, -1);
    groupname = groupname.slice(0, -1);

    $('#GroupId').text(groupid);
    $('#GroupName').text(groupname);

    if (chkCount == 0) {
        alertify.alert("Please select one data mapping file!");
    } else {

        if (srcTitle.length > 2) {
            $('#lblSrc').text('(' + srcTitle.length + ') selected');
        } else {
            var str = '';
            $.each(srcTitle, function (idx, val) {
                str += val + ', ';
            });

            str = str.slice(0, -2);

            $('#lblSrc').text(str);
        }

        $('#modalSource').modal('hide');

        var data = groupid + ',' + groupname;

        getFields(data);

        $('#btnAddCustom').prop('disabled', false);
    }
});

$('#btnAddCustom').click(function () {

    var boxCount = $('#divFields div.box').length;

    $(this).prop('disabled', true);

    $('#divFields div.box').addClass('collapsed-box');
    $('#divFields div.box div.box-header div.box-tools button i').removeClass('fa-minus');
    $('#divFields div.box div.box-header div.box-tools button i').addClass('fa-plus');

    $('#divFields').append(
		'<div class="box box-default custom">' +
			'<div class="box-header text-center">' +
				'<div class="col-xs-4 col-xs-push-4"><input id="inGroup" type="text" class="form-control input-sm" /></div>' +
				'<div class="box-tools pull-right">' +
					'<button type="button" class="btn btn-box-tool" data-widget="collapse">' +
						'<i class="fa fa-minus"></i>' +
					'</button>' +
				'</div>' +
			'</div>' +
			'<div class="box-body">' +
				'<table id="tblFields_' + (boxCount + 1) + '" class="table">' +
					'<thead>' +
						'<tr>' +
							'<th class="text-center">Headers</th>' +
							'<th class="text-center">Values</th>' +
						'</tr>' +
					'</thead>' +
					'<tbody class="text-center">' +
						'<tr>' +
							'<td><input type="text" class="form-control input-sm" /></td>' +
							'<td><input type="text" class="form-control input-sm" /></td>' +
							'<td><button type="button" class="btn btn-link btn-sm" onclick="addRow(this);"><i class="fa fa-plus"></i></button></td>' +
						'</tr>' +
					'</tbody>' +
					'<tfoot>' +
						'<tr class="text-right">' +
							'<td colspan="2"><button type="button" class="btn btn-primary btn-sm" onclick="saveCustom(this);">Save Changes</button></td>' +
							'<td colspan="2"><button type="button" class="btn btn-primary btn-sm" onclick="removeCustom(this);">Remove</button></td>' +
						'</tr>' +
					'</tfoot>' +
				'</table>' +
			'</div>' +
		'</div>'
	);
});

function GetWeb(pdf)
{
  
    $.ajax({
        type: 'POST',
        url: 'PDFMap.aspx/GetWebsite',
        data: '{USP: "' + pdf + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.records;

                $.each(records, function (idx, val) {
                    $('#lblWebsite').text(val.Website);
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}


$('#slctPDF').on('change', function () {
    var pdf = $(this).val() + ".pdf";
    getHeaders(pdf)
    GetWeb(pdf)
    var srcTitle = [];
    var groupid = "";
    var groupname = "";
    var chkCount = 0;

    


    $.each($('#tblRepository tbody tr'), function (idx, val) {
        if ($(this).find('input[type="checkbox"]').is(':checked')) {
            chkCount = 1;
            srcTitle.push($(this).find('td:eq(1)').html());
            groupid += $(this).find('input[type="checkbox"]').attr('data-id') + ',';

            $('#tblSource tbody').append(
				'<tr>' +
					'<td nowrap>' + $(this).find('td:eq(1)').html() + '</td>' +
					'<td nowrap>Data Map</td>' +
				'</tr>'
			);
        }
    });
    $.each($('#tblCustom tbody tr'), function (idx, val) {
        if ($(this).find('input[type="checkbox"]').is(':checked')) {
            chkCount = 1;
            srcTitle.push($(this).find('td:eq(1)').html());
            groupname += $(this).find('input[type="checkbox"]').attr('data-id') + ',';

            $('#tblSource tbody').append(
				'<tr>' +
					'<td nowrap>' + $(this).find('td:eq(1)').html() + '</td>' +
					'<td nowrap>Custom Data</td>' +
				'</tr>'
			);
        }
    });

    groupid = groupid.slice(0, -1);
    groupname = groupname.slice(0, -1);

    $('#GroupId').text(groupid);
    $('#GroupName').text(groupname);

    if (chkCount == 0) {
        alertify.alert("Please select one data mapping file!");
    } else {

        if (srcTitle.length > 2) {
            $('#lblSrc').text('(' + srcTitle.length + ') selected');
        } else {
            var str = '';
            $.each(srcTitle, function (idx, val) {
                str += val + ', ';
            });

            str = str.slice(0, -2);

            $('#lblSrc').text(str);
        }

        $('#modalSource').modal('hide');

        var data = groupid + ',' + groupname;

        getFields(data);

        $('#btnAddCustom').prop('disabled', false);
    }


    




});

$('#btnPreview').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });

    var arr = [];
    var obj = {};

    $.each($('#divHeaders #divDrop .passed'), function (idx, val) {
        var key = $(this).closest('#divDrop').find('.original').text();
        var value = $(this).attr('data-id');

        obj[key] = value;
    })

    arr.push(obj);

    var arr2 = [];
    var obj2 = {};
    $.each($('#divHeaders #divDrop .original[style]'), function (idx, val) {
        var key = $(this).html();
        var value = $(this).closest('#divDrop').find('.passed').text();

        obj2[key] = value;
    });
    arr2.push(obj2);

    var pdf = $('#slctPDF option:selected').val() + ".pdf";
    var filename = $('#slctPDF option:selected').text();

    $.ajax({
        type: 'POST',
        url: 'PDFMap.aspx/PreviewPDF',
        data: '{fields: ' + JSON.stringify(arr2) + ', values: ' + JSON.stringify(arr) + ', pdf: "' + pdf + '", filename: "' + filename + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //console.log(data);
            $('#pdf').attr('src', '..' + data.d);
            $('#btnSave').prop('disabled', false);
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });

});

$('#btnSave').click(function () {
    $('#modalSaveDesc').modal({ backdrop: 'static', keyboard: false });
    //$('#divMap').show();
    $('#taDescription').attr('data-id', 'SaveMap')

    //selectState();
});

$('#slctState').change(function (idx, val) {
    var state = $(this).val();

    selectMunicipality(state);
});

//$('#slctMunicipality').change(function (idx, val) {
//    var state = $('#slctState').val();
//    var municipality = $(this).val();

//    selectZip(state, municipality);
//});

function selectState() {
    $.ajax({
        type: 'POST',
        url: 'PDFMap.aspx/SelectState',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#slctState').empty;
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                $.each(records, function (idx, val) {
                    $('#slctState').append(
                        '<option value="' + val.State + '">' + val.State + '</option>'
                    );
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function selectMunicipality(state) {
    $.ajax({
        type: 'POST',
        url: 'PDFMap.aspx/SelectMunicipality',
        data: '{state: "' + state + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#slctMunicipality').empty;
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                $.each(records, function (idx, val) {
                    $('#slctMunicipality').append(
                        '<option value="' + val.Municipality + '">' + val.Municipality + '</option>'
                    );
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

//function selectZip(state, municipality) {
//    $.ajax({
//        type: 'POST',
//        url: 'PDFMap.aspx/SelectZip',
//        data: '{state: "' + state + '", municipality: "' + municipality + '"}',
//        contentType: 'application/json; charset=utf-8',
//        success: function (data) {
//            $('#slctZip').empty;
//            //$('#slctZip').append('<option value="">--Select One--</option>');
//            var d = $.parseJSON(data.d);

//            if (d.Success) {
//                var records = d.data.record;

//                $.each(records, function (idx, val) {
//                    $('#slctZip').append(
//                        '<option value="' + val.Zip + '">' + val.Zip + '</option>'
//                    );
//                });
//            }
//        },
//        error: function (response) {
//            console.log(response.responseText);
//        }
//    });
//}

function savePDF(myData) {
    $('#modalLoading').modal({ bakcdrop: 'static', keyboard: false });
    var usr = $('#usr').text();
    $.ajax({
        type: 'POST',
        url: 'PDFMap.aspx/SavePDF',
        data: '{myData: ' + JSON.stringify(myData) + ', usr: "' + usr + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            if (data.d == 2) {
                alertify.confirm('Do you want to overwrite the template?',
                    function () {
                        var filename = $('#slctPDF option:selected').text();
                        var description = $('#taDescription').val();
                        //var municipality = $('#slctMunicipality').val();
                        //var client = $('#slctClient').val();

                        var myData = [filename, description, 'overwrite'];

                        savePDF(myData);
                    },
                    function () {
                        $('#modalSaveDesc').modal('hide');
                        $('#modalLoading').modal('hide');
                    });
            } else if (data.d == 3) {
                myData[2] = 'save';
                savePDF(myData);
            } else if (data.d == 1) {
            alertify.alert('Update Successful!');
            $('#modalSaveDesc').modal('hide');
            $('#modalLoading').modal('hide');
            }

            //console.log(data);
            //alert(data.d);
            $('#modalSaveDesc').modal('hide');
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnSaveDesc').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var descType = $('#taDescription').attr('data-id');

    if (descType == 'SaveMap') {
        var filename = $('#slctPDF option:selected').text();
        var description = $('#taDescription').val();
        //var municipality = $('#slctMunicipality').val();
        //var client = $('#slctClient').val();

        var myData = [filename, description, 'new'];

        savePDF(myData);

    } else if (descType == 'SaveCust') {
        var id = $('#lblSrc').attr('data-id');
        var group = $('#inGroup').val();
        var description = $('#taDescription').val();
        var groups = [];

        if ($('#divFields .custom').length > 0) {
            $.each($('#divFields .custom'), function (idx, val) {
                groups.push($(this).find('.box-header label').text());
            });
        }

        if ($('#divFields .box').not('.custom').length > 0) {
            $.each($('#divFields .custom'), function (idx, val) {
                groups.push($(this).find('.box-header label').text());
            });
        }

        groups.push(group);

        var rows = $('#divFields .custom:last-child').find('table tbody tr');
        var headers = "", values = "";
        $.each(rows, function (idx, val) {
            headers += $(this).find('td:eq(0) input').val() + ',';
            values += $(this).find('td:eq(1) input').val() + ',';
        });

        headers = headers.slice(0, -1);
        values = values.slice(0, -1);

        var usr = $('#usr').text();
        var myData = [id, group, headers, values, description]

        $.ajax({
            type: 'POST',
            url: 'PDFMap.aspx/SaveCustom',
            data: '{myData: ' + JSON.stringify(myData) + ', usr: "' + usr + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);
                getFields(groups);
                $('#modalSaveDesc').modal('hide');
                $('#btnAddCustom').prop('disabled', false);

                $('#modalLoading').modal('hide');
            },
            error: function (response) {
                console.log(response.responseText);
            }
        });

    }
});

function getHeaders(pdf) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'PDFMap.aspx/GetHeaders',
        data: '{pdf: "' + pdf + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            var headers = data.d.split(',');

            showHeaders(headers);

            $('#pdf').attr('src', '..' + headers[0]);
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function showHeaders(headers) {

    $('#divHeaders').empty();

    var selected = $('#slctPDF option:selected').val();

    $('#divHeaders').append(
		'<div class="col-xs-12">' +
			'<div class="col-xs-8 noPadMar">' +
				'<input id="inSrchHeader" type="text" class="form-control input-sm" placeholder="Search" onkeyup="revert(this);" />' +
			'</div>' +
			'<div class="col-xs-2 noPadMar">' +
				'<button type="button" class="btn btn-link" style="color:#333; padding-top: 0;" onclick="searchHeader();">' +
					'<i class="fa fa-search"></i>' +
				'</button>' +
			'</div>' +
			'<div class="col-xs-2 noPadMar">' +
				'<button type="button" class="btn btn-link" style="color:#333; padding-top: 0;" onclick="revertHeaders();">' +
					'<i class="fa fa-refresh"></i>' +
				'</button>' +
			'</div>' +
		'</div>'
	);

    $.each(headers, function (idx, val) {
        if (idx != 0) {
            $('#divHeaders').append(
				'<div id="divDrop" class="col-xs-12 " onclick="headerActive(this);"><span class="original">' + val + '</span></div>'
			);
        }
    });
}

function revertHeaders() {
    $.each($('#divHeaders div'), function () {
        $(this).removeClass('activeHeader');
        $(this).find('.passed').remove();
        $(this).find('.original').show();
        $(this).show();
    });
}

function searchHeader() {
    var header = $('#inSrchHeader').val().toLowerCase();

    $.each($('#divHeaders #divDrop'), function (idx, val) {
        var header2 = $(this).find('.original').text().toLowerCase();
        if (header2.indexOf(header) >= 0) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}

function revert(val) {
    if ($(val).val() == "") {
        $.each($('#divHeaders #divDrop'), function (idx, val) {
            $(this).show();
        });
    }
}

function getPDF() {
    $.ajax({
        type: 'POST',
        url: 'PDFMap.aspx/GetPDF',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                $('#slctPDF').empty();
                $('#slctPDF').append(
					'<option value="" selected="selected">-- Select One --</option>'
				);
                $.each(records, function (idx, val) {
                    $('#slctPDF').append(
						'<option value="' + val.filename + '">' + val.filename + '</option>'
					);
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getSource() {
    $.ajax({
        type: 'POST',
        url: 'PDFMap.aspx/GetMapping',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            console.log(d);
            if (d.Success) {
                var dataMap = d.data.dataMap;
              
                $('#tblRepository').bootstrapTable('destroy');
                $('#tblRepository').bootstrapTable({
                    data: dataMap,
                    onPageChange: function () {
                        $('.iCheck').iCheck({
                            checkboxClass: 'icheckbox_flat-blue',
                            radioClass: 'iradio_flat-blue'
                        });
                    }
                });

                var customData = d.data.customData;

                $('#tblCustom').bootstrapTable('destroy');
                $('#tblCustom').bootstrapTable({
                    data: customData,
                    onPageChange: function () {
                        $('.iCheck').iCheck({
                            checkboxClass: 'icheckbox_flat-blue',
                            radioClass: 'iradio_flat-blue'
                        });
                    }
                });

                //iCheck for checkbox and radio inputs
                $('.iCheck').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    radioClass: 'iradio_flat-blue'
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    })
}

function saveCustom(val) {
    $('#modalSaveDesc').modal({ backdrop: 'static', keyboard: false });
    $('#divMap').hide();
    $('#taDescription').attr('data-id', 'SaveCust');
}

function removeCustom(val) {
    $(val).closest('div.box').remove();
    if ($('#divFields').find('div.box').length = 1) {
        $('#btnAddCustom').prop('disabled', false);
    }
}

function rdID(val) {
    return '<input type="checkbox" class="iCheck" checked="checked" data-id="' + val + '" name="rdID" />';
}

function actionFormat(val) {
    var edt = '<button type="button" class="btn btn-link" data-id="' + val + '"><i class="fa fa-pencil-square-o fa-2x"></i></button>';
    var del = '<button type="button" class="btn btn-link" data-id="' + val + '"><i class="fa fa-trash-o fa-2x"></i></button>';

    return edt + '&nbsp;' + del;
}

function addRow(val) {

    if ($(val).closest('tr').find('td.del').length == 0) {
        $(val).closest('tr').append('<td class="del"><button type="button" class="btn btn-link btn-sm" onclick="delRow(this);"><i class="fa fa-trash"></i></button></td>');
    }

    $(val).closest('tbody').append(
		'<tr>' +
			'<td><input type="text" class="form-control input-sm" /></td>' +
			'<td><input type="text" class="form-control input-sm" /></td>' +
			'<td><button type="button" class="btn btn-link btn-sm" onclick="addRow(this);"><i class="fa fa-plus"></i></button></td>' +
			'<td class="del"><button type="button" class="btn btn-link btn-sm" onclick="delRow(this);"><i class="fa fa-trash"></i></button></td>' +
		'</tr>'
	);

    $(val).closest('td').remove();
}

function delRow(val) {
    var prevRow = $(val).closest('tr').prev();
    var len = $(val).closest('tbody').find('tr').length;

    if ($(val).closest('tr').is(':last-child')) {
        prevRow.find('td:eq(1)').after('<td><button type="button" class="btn btn-link btn-sm" onclick="addRow(this);"><i class="fa fa-plus"></i></button></td>');
        if (len == 2) {
            prevRow.find('td.del').remove();
        }
        $(val).closest('tr').remove();
    } else if ($(val).closest('tr').is(':first-child')) {
        $(val).closest('tr').next().find('td.del').remove();
        $(val).closest('tr').remove();
    } else {
        $(val).closest('tr').remove();
    }
}

function revertFields(val) {
    var boxBody = $(val).closest('.box-body');

    $.each(boxBody.find('div.col-xs-12:gt(0)'), function (idx, val) {
        $(this).show();
    });
}

function searchFields(val) {
    var header = $(val).closest('div.col-xs-12').find('#inSrchFields').val().toLowerCase();
    var boxBody = $(val).closest('.box-body');

    $.each(boxBody.find('div.col-xs-12:gt(0)'), function (idx, val) {
        var headerText = $(this).find('.hdrs div').text().toLowerCase();

        if (headerText.indexOf(header) >= 0) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}

function revertF(val) {
    var boxBody = $(val).closest('.box-body');
    if ($(val).val() == "") {
        $.each(boxBody.find('div.col-xs-12:gt(0)'), function (idx, val) {
            $(this).show();
        });
    }
}

function getFields(val) {
    var arr = val.split(',');
    $.ajax({
        type: 'POST',
        url: 'PDFMap.aspx/GetFields',
        data: '{id: ' + JSON.stringify(arr) + '}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                //console.log(records);

                var icon = (records.length > 1) ? "fa-plus" : "fa-minus";

                $('#divFields').empty();
                var headers = [];
                var values = [];

                var group = [];
                $.each(records, function (indx, grp) {
                    if ($.inArray(grp.GroupName, group) == -1) {
                        if (grp.GroupName != '') {
                            group.push(grp.GroupName);

                            var tblCount = (indx + 1);
                            var cust = (grp.custom_tag == 'True') ? 'custom' : '';
                            $('#divFields').append(
                                '<div class="box box-default ' + cust + '">' +
                                    '<div class="box-header text-left">' +
                                        '<label class="box-title">' + grp.GroupName + '</label>' +
                                        '<div class="box-tools pull-right">' +
                                            '<button type="button" class="btn btn-box-tool" data-widget="collapse">' +
                                                '<i class="fa ' + icon + '"></i>' +
                                            '</button>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div id="divGroupFields_' + tblCount + '" class="box-body">' +
                                            '<div class="col-xs-12">' +
                                                '<div class="col-xs-8 noPadMar">' +
				                                    '<input id="inSrchFields" type="text" class="form-control input-sm" placeholder="Search" onkeyup="revertF(this);" />' +
			                                    '</div>' +
			                                    '<div class="col-xs-2 noPadMar">' +
				                                    '<button type="button" class="btn btn-link" style="color:#333; padding-top: 0;" onclick="searchFields(this);">' +
					                                    '<i class="fa fa-search"></i>' +
				                                    '</button>' +
			                                    '</div>' +
			                                    '<div class="col-xs-2 noPadMar">' +
				                                    '<button type="button" class="btn btn-link" style="color:#333; padding-top: 0;" onclick="revertFields(this);">' +
					                                    '<i class="fa fa-refresh"></i>' +
				                                    '</button>' +
			                                    '</div>' +
                                            '</div>' +
                                            '<div class="col-xs-6 text-left">' +
                                                '<label>Headers</label>' +
                                            '</div>' +

                                            '<div class="col-xs-6 text-left">' +
                                                '<label>Values</label>' +
                                            '</div>' +
                                    '</div>' +
                                '</div>'
                            );

                            $.each(records, function (idx, val) {
                                if (grp.GroupName == val.GroupName) {
                                    var height = (val.FieldData == '') ? 'style="height: 20px;"' : '';
                                    $('#divGroupFields_' + tblCount).append(
                                        '<div class="col-xs-12">' +
                                            '<div class="col-xs-6 hdrs"><div class="text-left" onclick="passDatafield(this);">' + val.FieldName + '</div></div>' +
                                            '<div class="col-xs-6 text-left">' + val.FieldData + '</div>' +
                                        '</div>'
                                    );
                                }
                            });
                            //$('#divGroupFields_' + tblCount).append(
                            //    '<div class="col-xs-12">' +
                            //        clList() +
                            //    '</div>'
                            //);
                        }
                    }
                });

                if (group.length > 1) {
                    var collapsed = (records.length > 1) ? "collapsed-box" : "";

                    $.each($('#divFields .box'), function (idx, val) {
                        $(this).addClass(collapsed);
                    });
                } else {
                    $.each($('#divFields .box'), function (idx, val) {
                        $(this).removeClass(collapsed);
                    });
                }

            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function headerActive(val) {
    $.each($('#divHeaders').find('div'), function () {
        if ($(this).hasClass('activeHeader')) {
            $(this).removeClass('activeHeader');
        } else {
            $(val).addClass("activeHeader");
        }
    });
}

function passDatafield(val) {
    var header = $(val).text();
    var value = $(val).closest('.hdrs').next('.col-xs-6').text();
    $.each($('#divHeaders').find('div'), function () {
        if ($(this).hasClass('activeHeader')) {
            $(this).find('span').hide();
            $(this).find('span.passed').remove();
            $(this).append('<span class="passed" data-id="' + value + '">' + header + '</span>');
            $(this).css('background-color', '#88bd23');
            $(this).css('color', '#fff');
        }
    });
}

function clList() {
    var cl = '<div class=col-xs-12 style="padding-left:0px">' +
    '<div class="col-xs-6 hdrs"><div class="text-left"  onclick="passDatafield(this);">CL_Ocwen</div></div>' +
	'<div class="col-xs-6 text-left">Ocwen Loan Servicing, LLC</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Ocwen2"  onclick="passDatafield(this);">CL_Ocwen2</div></div>' +
	'<div class="col-xs-6 text-left">c/o Ocwen Loan Servicing, LLC</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Ocwen Contact"  onclick="passDatafield(this);">CL_Ocwen Contact</div></div>' +
	'<div class="col-xs-6 text-left">Judy Credit</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Ocwen Contact2"  onclick="passDatafield(this);">CL_Ocwen Contact2</div></div>' +
	'<div class="col-xs-6 text-left">- Judy Credit</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Ocwen Address"  onclick="passDatafield(this);">CL_Ocwen Address</div></div>' +
	'<div class="col-xs-6 text-left">1661 Worthington Road, Suite 100, West Palm Beach, FL 33409</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Ocwen Street Address"  onclick="passDatafield(this);">CL_Ocwen Street Address</div></div>' +
	'<div class="col-xs-6 text-left">1661 Worthington Road, Suite 100</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Ocwen City Address"  onclick="passDatafield(this);">CL_Ocwen City Address</div></div>' +
	'<div class="col-xs-6 text-left">West Palm Beach</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Ocwen State Address"  onclick="passDatafield(this);">CL_Ocwen State Address</div></div>' +
	'<div class="col-xs-6 text-left">FL</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Ocwen Zipcode"  onclick="passDatafield(this);">CL_Ocwen Zipcode</div></div>' +
	'<div class="col-xs-6 text-left">33409</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Ocwen Phone Number"  onclick="passDatafield(this);">CL_Ocwen Phone Number</div></div>' +
	'<div class="col-xs-6 text-left">(800) 746-2936</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Ocwen Email Address"  onclick="passDatafield(this);">CL_Ocwen Email Address</div></div>' +
	'<div class="col-xs-6 text-left">PropertyRegistration@ocwen.com</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Altisource"  onclick="passDatafield(this);">CL_Altisource</div></div>' +
	'<div class="col-xs-6 text-left">Altisource Solutions, Inc.</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Altisource2" onclick="passDatafield(this);">CL_Altisource2</div></div>' +
	'<div class="col-xs-6 text-left">c/o Altisource Solutions, Inc.</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Altisource Contact" onclick="passDatafield(this);">CL_Altisource Contact</div></div>' +
	'<div class="col-xs-6 text-left">*designated Regional Field Services Manager (Property Manager)* or *Asset Manager (Owner)*</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Altisource Address" onclick="passDatafield(this);">CL_Altisource Address</div></div>' +
	'<div class="col-xs-6 text-left">1000 Abernathy Rd Northpark Town Center, Building 400 Suite 200 Atlanta, GA 30328</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Altisource Street Address"  onclick="passDatafield(this);">CL_Altisource Street Address</div></div>' +
	'<div class="col-xs-6 text-left">1000 Abernathy Rd Northpark Town Center, Building 400 Suite 200</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Altisource City Address"  onclick="passDatafield(this);">CL_Altisource City Address</div></div>' +
	'<div class="col-xs-6 text-left">Atlanta</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Altisource State Address" onclick="passDatafield(this);">CL_Altisource State Address</div></div>' +
	'<div class="col-xs-6 text-left">GA</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Altisource Zipcode" onclick="passDatafield(this);">CL_Altisource Zipcode</div></div>' +
	'<div class="col-xs-6 text-left">30328</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Altisource Phone Number"  onclick="passDatafield(this);">CL_Altisource Phone Number</div></div>' +
	'<div class="col-xs-6 text-left">(866) 952-6514</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_Altisource Email Address"  onclick="passDatafield(this);">CL_Altisource Email Address</div></div>' +
	'<div class="col-xs-6 text-left">VPR@altisource.com</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_RESI" onclick="passDatafield(this);">CL_RESI</div></div>' +
	'<div class="col-xs-6 text-left">Altisource Solutions, Inc.</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_RESI Altisource Contact" onclick="passDatafield(this);">CL_RESI Altisource Contact</div></div>' +
	'<div class="col-xs-6 text-left">*Asset Manager*</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_RESI Altisource Address" onclick="passDatafield(this);">CL_RESI Altisource Address</div></div>' +
	'<div class="col-xs-6 text-left">402 Strand Street, Frederiksted, USVI 00840</div>' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_RESI Street Address" onclick="passDatafield(this);">CL_RESI Street Address</div></div>' +
	'<div class="col-xs-6 text-left">402 Strand Street</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_RESI City Address" onclick="passDatafield(this);">CL_RESI City Address</div></div>' +
	'<div class="col-xs-6 text-left">Frederiksted</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_RESI State Address" onclick="passDatafield(this);">CL_RESI State Address</div></div>' +
	'<div class="col-xs-6 text-left">USVI</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_RESI Zipcode" onclick="passDatafield(this);">CL_RESI Zipcode</div></div>' +
	'<div class="col-xs-6 text-left">840</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_RESI Altisource Phone Number" onclick="passDatafield(this);">CL_RESI Altisource Phone Number</div></div>' +
	'<div class="col-xs-6 text-left">(855)-662-8989</div>' +
    '</div>' +
    '<div class=col-xs-12 style="padding-left:0px">' +
	'<div class="col-xs-6 hdrs"><div class="text-left " myval="CL_RESI Altisource Email Address" onclick="passDatafield(this);">CL_RESI Altisource Email Address</div></div>' +
	'<div class="col-xs-6 text-left">CustomerService@altisourcerentals.com / VPR@altisource.com</div>'
    '</div>'

    return cl;
}
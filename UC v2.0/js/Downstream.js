﻿$(document).ready(function () {

    getClientBusiness();
});

function getClientBusiness() {
    $.ajax({
        type: 'POST',
        url: 'BusinessRequirements.aspx/GetClient',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                var records2 = d.data.asd2;
                $('#tblClient tbody').empty();
                $('#tblBusinessRule').bootstrapTable('destroy');
                $.each(records, function (idx, val) {
                    $('#tblClient tbody').append(
                        '<tr  class="rowHover" onclick="ClientChange(this)">' +
                            '<td>' + val.Client_Name + '</td>' +
                        '</tr>'
                    );
                });
                $('#tblBusinessRule').bootstrapTable({
                    data: records
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function edit(value) {
    // return '<button type="button" id="editPanel"  data-id="' + value + '" value="' + value + '" class="btn btn-default btn-sm"><i class="fa fa-edit"></i>&nbsp;Edit</button>'
    return '<p data-placement="top" type="button" data-toggle="tooltip" title="Edit"><button data-id="' + value + '" value="' + value
        + '" class="btn btn-primary btn-xs" ><span class="glyphicon glyphicon-pencil"></span></button></p>' + 
        '<p data-placement="top" data-toggle="tooltip" title="Delete"><button data-id="' + value + '" value="' + value
        + '" class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>'
}

$('#btnAddNew').click(function () {
    dvRules.style.display = 'none';
    dvAddnew.style.display = 'none';
    if ($("#dvNewRules:visible").length == 0) {
        $("#dvNewRules").show();
        $("#dvDone").show();
    }
    getStatusddl();
});
$('#btnBack').click(function () {
    dvNewRules.style.display = 'none';
    
    dvDone.style.display = 'none';
    if ($("#dvRules:visible").length == 0) {
        $("#dvRules").show();
        $("#dvAddnew").show();
    }
});
function ClientChange(Util) {
    sessionStorage.setItem("Client", $(Util).closest("tr").find("td:lt(1)").text());
    $(Util).closest("table").find("tr").removeClass("selected");
    dvNewRules.style.display = 'none';
    dvDone.style.display = 'none';
    $(Util).addClass("selected");
    if ($("#dvRules:visible").length == 0) {
        $("#dvRules").show();
        $("#dvAddnew").show();
    }
}
$('#btnDone').click(function () {

    $("#ddlTask option:selected").text()
    $("#ddlStatus option:selected").text()
    $("#ddlUtility option:selected").text()
    $("#ddlworkItemYN option:selected").text()
    $("#ddlCategory option:selected").text()
    $("#ddlWorkItemStatus option:selected").text()

    $("#ddlNo option:selected").text() + '_' + $("#ddlBusCal option:selected").text() + '_' + $("#ddlDWMY option:selected").text()

    $.ajax({
        type: 'POST',
        url: 'BusinessRequirements.aspx/InsertData',
        data: "{'Client':'" + sessionStorage.getItem('Client')
            + "','Task':'" + $("#ddlTask option:selected").text()
            + "','Status':'" + $("#ddlStatus option:selected").text()
            + "','Utility':'" + $("#ddlUtility option:selected").text()
            + "','WorkItem':'" + $("#ddlworkItemYN option:selected").text()
            + "','Category':'" + $("#ddlCategory option:selected").text()
            + "','WorkItemStatus':'" + $("#ddlWorkItemStatus option:selected").text()
            + "','Days':'" + $("#ddlNo option:selected").text() + '_'
            + $("#ddlBusCal option:selected").text() + '_'
            + $("#ddlDWMY option:selected").text()
            + "'}",
     
        contentType: 'application/json; charset=utf-8',
        
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
    });
});
function getStatusddl() {
    $.ajax({
        type: 'POST',
        url: 'BusinessRequirements.aspx/GetStatus',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                var records2 = d.data.asd2;
                $('#ddlStatus').empty();
                ta2 = ' <option value="" disabled="disabled" selected="selected">Select Status</option>'
                $('#ddlStatus').append(ta2);
                $.each(records, function (idx, val) {
                    ta2 = "<option value='" + val.Status + "' >" + val.Status + "</option>";
                    $('#ddlStatus').append(ta2);
                });
           
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
$('#ddlAndOr').on('change', function () {
    if ($("#dvCondition1:visible").length == 0) {
        $("#dvCondition1").show();
        $("#dvCondition1").show();
    }
})
$('#ddlAndOR1').on('change', function () {
    if ($("#dvCondition2:visible").length == 0) {
        $("#dvCondition2").show();
        $("#dvCondition2").show();
    }
})
$('#ddlAndOR2').on('change', function () {
    if ($("#dvCondition3:visible").length == 0) {
        $("#dvCondition3").show();
        $("#dvCondition3").show();
    }
})
$('#ddlAndOR3').on('change', function () {
    if ($("#dvCondition4:visible").length == 0) {
        $("#dvCondition4").show();
        $("#dvCondition4").show();
    }
})

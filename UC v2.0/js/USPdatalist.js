﻿$(document).ready(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    getDisplay();
    $('#modalLoading').modal('hide');


    $(document).on('click', ".table .btn", function (e) {
        e.preventDefault();
        id = $(this).attr('value')
        Edit(id);
        sessionStorage.setItem('USP_ID', id);
    });
});


function Edit(editVal) {

    var strval = '';
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"editVal" : "' + editVal + '"}',
        url: 'DataListUSP.aspx/GetUSPvalue',
        dataType: "json",
        success: function (data) {
            $('#lbEditUSP').empty();
            $('#lbEditUSP').multiselect('destroy');
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                var VendorRecords = d.data.asd2;
                var VendorSplit = "";
                $.each(records, function (idx, val) {
                    document.getElementById("txtEditUSPName").value = val.USP_Name;
                    document.getElementById("txtEditEmailAdd").value = val.Email_Address;
                    document.getElementById("txtEditWebsite").value = val.Website;
                    document.getElementById("txtEditStopWebsite").value = val.StopWebsite;
                    document.getElementById("txtEditUserName").value = val.Login_Name;
                    document.getElementById("txtEditPassword").value = val.Password;
                    if (VendorSplit == "") {
                        VendorSplit = val.Vendor_ID
                    }
                    else {
                        VendorSplit = VendorSplit + "," + val.Vendor_ID
                    }
                    strval = strval + ',' + val.NewColumnfrom;
                    //ta3 = "<option value='" + val.VID + "' selected='selected'>" + val.Vendor_ID + "</option>";
                    //$('#lbEditVendorID').append(ta3);
                });
                ddlEditAplha(strval);
                $.each(VendorRecords, function (idx, val) {
                    var selectSplit = "";
                    selectSplit = VendorSplit.split(',');

                    if ($.inArray(val.VendorID.toString(), selectSplit) !== -1) {
                        var name = val.VendorName.toUpperCase()
                        ta2 = "<option value='" + val.VendorID + "' selected='selected'>" + val.VendorID + "&nbsp&nbsp&nbsp--&nbsp&nbsp&nbsp   " + val.VendorName.toUpperCase() + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + "</option>";
                    } else {
                        ta2 = "<option value='" + val.VendorID + "' >" + val.VendorID + "&nbsp&nbsp&nbsp--&nbsp&nbsp&nbsp   " + val.VendorName.toUpperCase() + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + "</option>";
                    }
                    console.log(val.VendorName);
                    $('#lbEditUSP').append(ta2);

                  
                });
              
                $('#lbEditUSP').multiselect({
                    numberDisplayed: 0,
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    maxHeight: 200
                });

            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getDisplay() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DataListUSP.aspx/GetList',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#tblUSP').bootstrapTable('destroy');
                $('#tblUSPExport').bootstrapTable('destroy');
                var records = d.data.asd;
                var records2 = d.data.asd2;
                $('#tblUSP').bootstrapTable({
                    data: records,
                    height: 560,
                    width: 1000,
                    pagination: true
                });
                $('#tblUSPExport').bootstrapTable({
                    data: records2,
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function DeleteThis() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DataListUSP.aspx/DeleteUSP',
        data: '{"USP_ID" : "' + sessionStorage.getItem('USP_ID') + '"}',
        success: function (data) {
            getDisplay();
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
    });
    $('#delete').modal('hide');
}


function edit(value) {
    // return '<button type="button" id="editPanel"  data-id="' + value + '" value="' + value + '" class="btn btn-default btn-sm"><i class="fa fa-edit"></i>&nbsp;Edit</button>'
    return '<p data-placement="top" data-toggle="tooltip" title="Edit"><button data-id="' + value + '" value="' + value
        + '" class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#myModalEdit" ><span class="glyphicon glyphicon-pencil"></span></button></p>'
}
function btnDelete(value) {
    return '<p data-placement="top" data-toggle="tooltip" title="Delete"><button data-id="' + value + '" value="' + value
        + '" class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>'
}

function Update() {
    var checkBox = '';

    var select = document.getElementById("lbEditUSP");
    var itemSelec = "";
    var listLength = select.options.length;
    for (var i = 0; i < listLength; i++) {
        if (select.options[i].selected)
            if (itemSelec == "") {
                itemSelec = select.options[i].value;
            }
            else {
                itemSelec = itemSelec + ',' + select.options[i].value;
            }
    }

    if (document.getElementById("chkUSPEdit").checked == true) {
        checkBox = 1;
    }
    else {
        checkBox = 0;
    }

    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        data: '{"USP_ID" : "' + sessionStorage.getItem('USP_ID') +
            '","USPname" : "' + document.getElementById("txtEditUSPName").value +
            '","Email" : "' + document.getElementById("txtEditEmailAdd").value +
            '","Website" : "' + document.getElementById("txtEditWebsite").value +
            '","StopWebsite" : "' + document.getElementById("txtEditStopWebsite").value +
            '","UserName" : "' + document.getElementById("txtEditUserName").value +
            '","Password" : "' + document.getElementById("txtEditPassword").value +
            '","chkUSP" : "' + checkBox +
            '","itemSelec" : "' + itemSelec + '"}',
        contentType: 'application/json; charset=utf-8',
        url: 'DataListUSP.aspx/Update',
        success: function (data) {
            getDisplay();
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
    });
    $('#myModalEdit').modal('hide');
}
function Save() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var USPname = document.getElementById("txtUSPName").value;
    var Email = document.getElementById("txtEmailAdd").value;
    var Website = document.getElementById("txtWebsite").value;
    var StopWebsite = document.getElementById("txtStopWebsite").value;
    var UserName = document.getElementById("txtUserName").value;
    var Password = document.getElementById("txtPassword").value;
    var select = document.getElementById("lbUSP");
    var itemSelec = "";
    var listLength = select.options.length;
    for (var i = 0; i < listLength; i++) {
        if (select.options[i].selected)
            if (itemSelec == "") {
                itemSelec = select.options[i].value;
            }
            else {
                itemSelec = itemSelec + ',' + select.options[i].value;
            }
    }
    $.ajax({
        type: 'POST',
        data: '{"USPname" : "' + USPname +
            '","Email" : "' + Email +
            '","Website" : "' + Website +
            '","StopWebsite" : "' + StopWebsite +
            '","UserName" : "' + UserName +
            '","Password" : "' + Password +
            '","itemSelec" : "' + itemSelec + '"}',
        contentType: 'application/json; charset=utf-8',
        url: 'DataListUSP.aspx/Save',
        success: function (data) {
            getDisplay();
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
    $('#myModal').modal('hide');
}
function ddlVendor() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var select = document.getElementById("lblAlpha");
    var itemSelec = "";
    var listLength = select.options.length;
    for (var i = 0; i < listLength; i++) {
        if (select.options[i].selected)
            if (itemSelec == "") {
                itemSelec = select.options[i].value;
            }
            else {
                itemSelec = itemSelec + ',' + select.options[i].value;
            }
    }


    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DataListUSP.aspx/GetVendorList',
        dataType: "json",
        data: '{"Alpha" : "' + itemSelec + '"}',
        success: function (data) {

            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#lbUSP').empty();
                $('#lbUSP').multiselect('destroy');
                var records = d.data.asd;
                $.each(records, function (idx, val) {
                    var selected = 'selected="selected"';
                    ta2 = "<option value='" + val.VendorID + "' >" + val.VendorID + "&nbsp&nbsp&nbsp--&nbsp&nbsp&nbsp   " + val.VendorName.toUpperCase() + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + "</option>";
                    $('#lbUSP').append(ta2);
                });
                $('#lbUSP').multiselect({
                    numberDisplayed: 0,
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    maxHeight: 200
                });
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}
function ddlEditAplha(val) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var strSplit = val.split(',');
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DataListUSP.aspx/GetEditAlpha',
        dataType: "json",
        success: function (data) {
           
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#lblEditAlpha').empty();
                $('#lblEditAlpha').multiselect('destroy');
                var records = d.data.asd;
                $.each(records, function (idx, val) {
                    var selected = 'selected="selected"';
                    var valueStr = val.NewColumnfrom;

                    if ($.inArray(val.NewColumnfrom.toString(), strSplit) !== -1) {
                        ta2 = "<option value='" + val.NewColumnfrom + "' selected='selected'>" + val.NewColumnfrom + "</option>";
                    }
                    else
                    {
                        ta2 = "<option value='" + val.NewColumnfrom + "' >" + val.NewColumnfrom + "</option>";
                    }
                    
                    $('#lblEditAlpha').append(ta2);
                });
                $('#lblEditAlpha').multiselect({
                    onChange: function (option, checked, select) {
                        var select = document.getElementById("lblEditAlpha");
                        var itemSelec = "";
                        var listLength = select.options.length;
                        for (var i = 0; i < listLength; i++) {
                            if (select.options[i].selected)
                                if (itemSelec == "") {
                                    itemSelec = select.options[i].value;
                                }
                                else {
                                    itemSelec = itemSelec + ',' + select.options[i].value;
                                }
                        }
                        Editslc(itemSelec);
                    },
                    numberDisplayed: 0,
                    includeSelectAllOption: false,
                    maxHeight: 200
                });
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}
function Editslc(editVal) {

    var strval = '';
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"alphaSTR" : "' + editVal + '"}',
        url: 'DataListUSP.aspx/GetSelectedVID',
        dataType: "json",
        success: function (data) {
            $('#lbEditUSP').empty();
            $('#lbEditUSP').multiselect('destroy');
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                var VendorRecords = d.data.asd2;
                var VendorSplit = "";
                $.each(records, function (idx, val) {
                    if (VendorSplit == "") {
                        VendorSplit = val.Vendor_ID
                    }
                    else {
                        VendorSplit = VendorSplit + "," + val.Vendor_ID
                    }
                });
                $.each(VendorRecords, function (idx, val) {
                    var selectSplit = "";
                    selectSplit = VendorSplit.split(',');

                    if ($.inArray(val.VendorID.toString(), selectSplit) !== -1) {
                        var name = val.VendorName.toUpperCase()
                        ta2 = "<option value='" + val.VendorID + "' selected='selected'>" + val.VendorID + "&nbsp&nbsp&nbsp--&nbsp&nbsp&nbsp   " + val.VendorName.toUpperCase() + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + "</option>";
                    } else {
                        ta2 = "<option value='" + val.VendorID + "' >" + val.VendorID + "&nbsp&nbsp&nbsp--&nbsp&nbsp&nbsp   " + val.VendorName.toUpperCase() + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + "</option>";
                    }
                    console.log(val.VendorName);
                    $('#lbEditUSP').append(ta2);


                });

                $('#lbEditUSP').multiselect({
                    numberDisplayed: 0,
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    maxHeight: 200
                });

            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function ddlAplha() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DataListUSP.aspx/GetAlpha',
        dataType: "json",

        success: function (data) {
            $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#lblAlpha').empty();
                $('#lblAlpha').multiselect('destroy');
                var records = d.data.asd;
                $.each(records, function (idx, val) {
                    var selected = 'selected="selected"';
                    var valueStr = val.NewColumnfrom;
                    ta2 = "<option value='" + val.NewColumnfrom + "' >" + val.NewColumnfrom + "</option>";
                    $('#lblAlpha').append(ta2);
                });
                $('#lblAlpha').multiselect({
                    onChange: function (option, checked, select) {
                        ddlVendor();
                    },
                    numberDisplayed: 0,
                    includeSelectAllOption: false,
                    maxHeight: 200
                });
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}
function AddNew() {
    document.getElementById("txtUSPName").value = "";
    document.getElementById("txtEmailAdd").value = "";
    document.getElementById("txtWebsite").value = "";
    document.getElementById("txtUserName").value = "";
    document.getElementById("txtPassword").value = "";

    $('#lbUSP').multiselect({
        numberDisplayed: 0,
        includeSelectAllOption: true,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        maxHeight: 200
    });
    ddlAplha();

    //$.ajax({
    //    type: 'post',
    //    contenttype: 'application/json; charset=utf-8',
    //    url: 'datalistusp.aspx/getvendorlist',
    //    datatype: "json",
    //    success: function (data) {
    //        $('#modalloading').modal({ backdrop: 'static', keyboard: false });
    //        var d = $.parsejson(data.d);
    //        if (d.success) {
    //            //$('#lbusp,#lbvendorid').empty();
    //            //$('#lbusp,#lbvendorid').multiselect('destroy');
    //            //var records = d.data.asd;
    //            //$.each(records, function (idx, val) {
    //            //    var selected = 'selected="selected"';
    //            //    ta2 = "<option value='" + val.vid + "' >" + val.vendorname + "</option>";
    //            //    $('#lbusp').append(ta2);
    //            //}); 
    //            //$('#lbusp,#lbvendorid').multiselect({
    //            //    numberdisplayed: 0,
    //            //    includeselectalloption: true,
    //            //    enablefiltering: true,
    //            //    maxheight: 200
    //            //});
    //            $('#lbusp').empty();
    //            $('#lbusp').multiselect('destroy');
    //            var records = d.data.asd;
    //            $.each(records, function (idx, val) {
    //                var selected = 'selected="selected"';
    //                ta2 = "<option value='" + val.vendorid + "' >" + val.vendorid + "&nbsp&nbsp&nbsp--&nbsp&nbsp&nbsp   " + val.vendorname.touppercase() + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + "</option>";
    //                $('#lbusp').append(ta2);
    //            }); 
    //            $('#lbusp').multiselect({
    //                numberdisplayed: 0,
    //                includeselectalloption: true,
    //                enablefiltering: true,
    //                enablecaseinsensitivefiltering: true,
    //                maxheight: 200
    //            });
    //        }
    //        $('#modalloading').modal('hide');
    //    }, error: function (response) {
    //        console.log(response.responsetext);
    //    }
    //});
}


$('#btnUpload').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var fileNameReplace = $('#fileUpload').val();
    fileNameReplace = fileNameReplace.replace("C:\\fakepath\\", "");
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DataListUSP.aspx/fileUploadExcel',
        dataType: "json",
        data: "{'fileUploadExcel' : '" + fileNameReplace + "'}",
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
            $('#modalLoading').modal('hide');
        }
    });
});
var tablesToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
    , tmplWorkbookXML = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">'
      + '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>'
      + '<Styles>'
      + '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>'
      + '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>'
      + '</Styles>'
      + '{worksheets}</Workbook>'
    , tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>'
    , tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>'
    , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (tables, wsnames, wbname, appname) {
        var ctx = "";
        var workbookXML = "";
        var worksheetsXML = "";
        var rowsXML = "";

        for (var i = 0; i < tables.length; i++) {
            if (!tables[i].nodeType) tables[i] = document.getElementById(tables[i]);
            for (var j = 0; j < tables[i].rows.length; j++) {
                rowsXML += '<Row>'
                for (var k = 0; k < tables[i].rows[j].cells.length; k++) {
                    var dataType = tables[i].rows[j].cells[k].getAttribute("data-type");
                    var dataStyle = tables[i].rows[j].cells[k].getAttribute("data-style");
                    var dataValue = tables[i].rows[j].cells[k].getAttribute("data-value");
                    dataValue = (dataValue) ? dataValue : tables[i].rows[j].cells[k].innerHTML;
                    var dataFormula = tables[i].rows[j].cells[k].getAttribute("data-formula");
                    dataFormula = (dataFormula) ? dataFormula : (appname == 'Calc' && dataType == 'DateTime') ? dataValue : null;
                    ctx = {
                        attributeStyleID: (dataStyle == 'Currency' || dataStyle == 'Date') ? ' ss:StyleID="' + dataStyle + '"' : ''
                           , nameType: (dataType == 'Number' || dataType == 'DateTime' || dataType == 'Boolean' || dataType == 'Error') ? dataType : 'String'
                           , data: (dataFormula) ? '' : dataValue
                           , attributeFormula: (dataFormula) ? ' ss:Formula="' + dataFormula + '"' : ''
                    };
                    rowsXML += format(tmplCellXML, ctx);
                }
                rowsXML += '</Row>'
            }
            ctx = { rows: rowsXML, nameWS: wsnames[i] || 'Sheet' + i };
            worksheetsXML += format(tmplWorksheetXML, ctx);
            rowsXML = "";
        }

        ctx = { created: (new Date()).getTime(), worksheets: worksheetsXML };
        workbookXML = format(tmplWorkbookXML, ctx);



        var link = document.createElement("A");
        link.href = uri + base64(workbookXML);
        link.download = wbname || 'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
})();
function fnExcelReport() {
    var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j = 0;
    tab = document.getElementById('tblUSPExport'); // id of table

    for (j = 0 ; j < tab.rows.length ; j++) {
        tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text = tab_text + "</table>";
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html", "replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
    }
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

    return (sa);
}
//$('#lbUSP').on('change', function (evt, params) {

//    var select = document.getElementById("lbUSP");
//    var itemSelec = "";
//    var listLength = select.options.length;
//    for (var i = 0; i < listLength; i++) {
//        if (select.options[i].selected)
//            if (itemSelec == "") {
//                itemSelec = select.options[i].value;
//            }
//            else {
//                itemSelec = itemSelec + ',' + select.options[i].value;
//            }
//    }
//    $.ajax({
//        type: 'POST',
//        url: 'DataListUSP.aspx/vendorID',
//        data: '{"itemSelec" : "' + itemSelec + '"}',
//        contentType: 'application/json; charset=utf-8',
//        success: function (data) {
//            $('#lbVendorID').empty();
//            $('#lbVendorID').multiselect('destroy');
//            var d = $.parseJSON(data.d);
//            if (d.Success) {
//                var records = d.data.asd;
//                $.each(records, function (idx, val) {
//                    ta2 = "<option value='" + val.VID + "'>" + val.VendorID + "</option>";
//                    $('#lbVendorID').append(ta2);
//                });
//                $('#lbVendorID').multiselect({
//                    numberDisplayed: 0,
//                    includeSelectAllOption: true,
//                    enableFiltering: true,
//                    maxHeight: 200
//                });
//            }
//        },
//        error: function (response) {
//            console.log(response.responseText);
//        }
//    });
//});
//$('#lbEditUSP').on('change', function (evt, params) {

//    var select = document.getElementById("lbEditUSP");
//    var itemSelec = "";
//    var listLength = select.options.length;
//    for (var i = 0; i < listLength; i++) {
//        if (select.options[i].selected)
//            if (itemSelec == "") {
//                itemSelec = select.options[i].value;
//            }
//            else {
//                itemSelec = itemSelec + ',' + select.options[i].value;
//            }
//    }
//    $.ajax({
//        type: 'POST',
//        data: '{"itemSelec" : "' + itemSelec + '"}',
//        contentType: 'application/json; charset=utf-8',
//        url: 'DataListUSP.aspx/vendorID',
//        success: function (data) {
//            $('#lbEditVendorID').empty();
//            $('#lbEditVendorID').multiselect('destroy');
//            var d = $.parseJSON(data.d);
//            if (d.Success) {
//                var records = d.data.asd;
//                $.each(records, function (idx, val) {
//                    ta2 = "<option value='" + val.VID + "'>" + val.VendorID + "</option>";
//                    $('#lbEditVendorID').append(ta2);
//                });
//                $('#lbEditVendorID').multiselect({
//                    numberDisplayed: 0,
//                    includeSelectAllOption: true,
//                    maxHeight: 200
//                });
//            }
//        },
//        error: function (response) {
//            console.log(response.responseText);
//        }
//    });
//});
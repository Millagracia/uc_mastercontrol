﻿$(document).ready(function () {
  
});

window.onload = function ()
{
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('#tblDataFeed').bootstrapTable('destroy');
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DataFeed.aspx/BindData',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblDataFeed').bootstrapTable({
                    data: records,
                    height: 560,
                    width: 1000,
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnApply').click(function () {

    //Get Title
    var GetTitle = [];
    var Days = '';
    if ($('#weekday-mon').is(':checked') == true)
    {
        Days += 'Monday'
    }

    if ($('#weekday-tue').is(':checked') == true) {
        if (Days != '')
        {
            Days += ',Tuesday'
        }
        else
        {
            Days += 'Tuesday'
        }
        
    }

    if ($('#weekday-wed').is(':checked') == true) {

        if (Days != '') {
            Days += ',Wednesday'
        }
        else {
            Days += 'Wednesday'
        }
    }

    if ($('#weekday-thu').is(':checked') == true) {
        if (Days != '') {
            Days += ',Thursday'
        }
        else {
            Days += 'Thursday'
        }
    }

    if ($('#weekday-fri').is(':checked') == true) {
        if (Days != '') {
            Days += ',Friday'
        }
        else {
            Days += 'Friday'
        }
    }

    if ($('#weekday-sat').is(':checked') == true) {
        if (Days != '') {
            Days += ',Saturday'
        }
        else {
            Days += 'Saturday'
        }
    }

    if ($('#weekday-sun').is(':checked') == true) {
        if (Days != '') {
            Days += ',Sunday'
        }
        else {
            Days += 'Sunday'
        }
    
    }
    GetTitle.push($('#txtTitle').val());
    GetTitle.push($('#txtDesc').val());
    GetTitle.push($('#filePathLoc').val());
    GetTitle.push(Days);
    GetTitle.push($('#txtTime').val());
    GetTitle.push($('#txtTimeTo').val());
    GetTitle.push($('#ddlTimezone :selected').text());

    //Get Alert Message
    //Success Message
    var GetMessage = [];
    GetMessage.push($('#txtSuccAlert').val());
    GetMessage.push($('#txtSuccAreaMessage').val());
    //Fail Message
    GetMessage.push($('#txtFailAlert').val());
    GetMessage.push($('#txtFailAreaMessage').val());
    
    

    //Get Condition
    var NoOfCondition = 1;
    var GetCondition1 = [];
    var GetCondition2 = [];
    var GetCondition3 = [];
    var GetCondition4 = [];
    var GetCondition5 = [];


    if ($("#dvCondition1:visible").length == 1) {
        GetCondition1.push($('#cbIsActive1').is(':checked'));
        GetCondition1.push($('#ddlAlertRecei1').val());
        GetCondition1.push($('#ddAlertGLE1').val());
        GetCondition1.push($('#txtHour1').val());
        GetCondition1.push($('#ddlHMS1').val());
        GetCondition1.push($('#ddlAlertSuc1').val());
        //GetCondition1.push($('#txtTo1').val());
        GetCondition1.push($('#lblTo1').text());
        
    }


    if ($("#dvCondition2:visible").length == 1)
    {
        NoOfCondition = 2;
        GetCondition2.push($('#cbIsActive2').is(':checked'));
        GetCondition2.push($('#ddlRecei2').val());
        GetCondition2.push($('#ddlGLE2').val());
        GetCondition2.push($('#txtAlertHour2').val());
        GetCondition2.push($('#ddlHMS2').val());
        GetCondition2.push($('#ddlSucc2').val());
        //GetCondition2.push($('#txtTo2').val());
        GetCondition2.push($('#lblTo2').text());
    }

    if ($("#dvCondition3:visible").length == 1) {
        NoOfCondition = 3;
        GetCondition3.push($('#cbIsActive3').is(':checked'));
        GetCondition3.push($('#ddlRecei3').val());
        GetCondition3.push($('#ddlGLE3').val());
        GetCondition3.push($('#txtAlertHour3').val());
        GetCondition3.push($('#ddlHMS3').val());
        GetCondition3.push($('#ddlSucc3').val());
        //GetCondition3.push($('#txtTo3').val());
        GetCondition3.push($('#lblTo3').text());
    }

    if ($("#dvCondition4:visible").length == 1) {
        NoOfCondition = 4;
        GetCondition4.push($('#cbIsActive4').is(':checked'));
        GetCondition4.push($('#ddlRecei4').val());
        GetCondition4.push($('#ddlGLE4').val());
        GetCondition4.push($('#txtAlertHour4').val());
        GetCondition4.push($('#ddlHMS4').val());
        GetCondition4.push($('#ddlSucc4').val());
        //GetCondition4.push($('#txtTo4').val());
        GetCondition4.push($('#lblTo4').text());
    }

    if ($("#dvCondition5:visible").length == 1) {
        NoOfCondition = 5;
        GetCondition5.push($('#cbIsActive5').is(':checked'));
        GetCondition5.push($('#ddlRecei5').val());
        GetCondition5.push($('#ddlGLE5').val());
        GetCondition5.push($('#txtAlertHour5').val());
        GetCondition5.push($('#ddlHMS5').val());
        GetCondition5.push($('#ddlSucc5').val());
        //GetCondition5.push($('#txtTo5').val());
        GetCondition5.push($('#lblTo5').text());
    }

    
  
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        data: '{GetTitle : ' + JSON.stringify(GetTitle) +
            ',GetMessage : ' + JSON.stringify(GetMessage)  +
            ',GetCondition1 : ' + JSON.stringify(GetCondition1)  +
            ',GetCondition2 : ' + JSON.stringify(GetCondition2)  +
            ',GetCondition3 : ' + JSON.stringify(GetCondition3)  +
            ',GetCondition4 : ' + JSON.stringify(GetCondition4)  +
            ',GetCondition5 : ' + JSON.stringify(GetCondition5) + '}',
        contentType: 'application/json; charset=utf-8',
        url: 'DataFeed.aspx/InsertData',
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
    });



    dvAddNew.style.display = 'none';
    if ($("#dvTable:visible").length == 0) {
        $("#dvTable").show();

    }
});

$(function () {
    $("[id$=txtTo1]").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: 'DataFeed.aspx/GetEmail',
                data: "{ 'prefix': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('-')[0],
                            val: item.split('-')[1]
                        }
                    }))
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("[id$=hfCustomerId]").val(i.item.val);
        },
        minLength: 1
    });
    $("[id$=txtTo2]").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: 'DataFeed.aspx/GetEmail',
                data: "{ 'prefix': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('-')[0],
                            val: item.split('-')[1]
                        }
                    }))
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("[id$=hfCustomerId]").val(i.item.val);
        },
        minLength: 1
    });
    $("[id$=txtTo3]").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: 'DataFeed.aspx/GetEmail',
                data: "{ 'prefix': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('-')[0],
                            val: item.split('-')[1]
                        }
                    }))
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("[id$=hfCustomerId]").val(i.item.val);
        },
        minLength: 1
    });
    $("[id$=txtTo4]").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: 'DataFeed.aspx/GetEmail',
                data: "{ 'prefix': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('-')[0],
                            val: item.split('-')[1]
                        }
                    }))
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("[id$=hfCustomerId]").val(i.item.val);
        },
        minLength: 1
    });
    $("[id$=txtTo5]").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: 'DataFeed.aspx/GetEmail',
                data: "{ 'prefix': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('-')[0],
                            val: item.split('-')[1]
                        }
                    }))
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("[id$=hfCustomerId]").val(i.item.val);
        },
        minLength: 1
    });
});
$('#btnPlus1').click(function () {
    if (document.getElementById('txtTo1').value != '')
    {
        if ($('#lblTo1').text() == '')
        {
            $("#lblTo1").append(document.getElementById('txtTo1').value);
            document.getElementById('txtTo1').value = '';
        }
        else
        {
            $("#lblTo1").append('; '+document.getElementById('txtTo1').value);
            document.getElementById('txtTo1').value = '';
        }
        //$("#lblTo1").append(document.getElementById('txtTo1').value + '@altisource.com ');
    }

    
});
$('#btnPlus2').click(function () {
    //if (document.getElementById('txtTo2').value != '') {
    //    $("#lblTo2").append(document.getElementById('txtTo2').value + '@altisource.com ');
    //    document.getElementById('txtTo2').value = '';
    //}

    if (document.getElementById('txtTo2').value != '') {
        if ($('#lblTo2').text() == '') {
            $("#lblTo2").append(document.getElementById('txtTo2').value);
            document.getElementById('txtTo2').value = '';
        }
        else {
            $("#lblTo2").append('; ' + document.getElementById('txtTo2').value);
            document.getElementById('txtTo2').value = '';
        }
        //$("#lblTo1").append(document.getElementById('txtTo1').value + '@altisource.com ');
    }
});
$('#btnPlus3').click(function () {
    //if (document.getElementById('txtTo3').value != '') {
    //    $("#lblTo3").append(document.getElementById('txtTo3').value + '@altisource.com ');
    //    document.getElementById('txtTo3').value = '';
    //}

    if (document.getElementById('txtTo3').value != '') {
        if ($('#lblTo3').text() == '') {
            $("#lblTo3").append(document.getElementById('txtTo3').value);
            document.getElementById('txtTo3').value = '';
        }
        else {
            $("#lblTo3").append('; ' + document.getElementById('txtTo3').value);
            document.getElementById('txtTo3').value = '';
        }
        //$("#lblTo1").append(document.getElementById('txtTo1').value + '@altisource.com ');
    }
});
$('#btnPlus4').click(function () {
    //if (document.getElementById('txtTo4').value != '') {
    //    $("#lblTo4").append(document.getElementById('txtTo4').value + '@altisource.com ');
    //    document.getElementById('txtTo4').value = '';
    //}

    if (document.getElementById('txtTo4').value != '') {
        if ($('#lblTo4').text() == '') {
            $("#lblTo4").append(document.getElementById('txtTo4').value);
            document.getElementById('txtTo4').value = '';
        }
        else {
            $("#lblTo4").append('; ' + document.getElementById('txtTo4').value);
            document.getElementById('txtTo4').value = '';
        }
        //$("#lblTo1").append(document.getElementById('txtTo1').value + '@altisource.com ');
    }

});
$('#btnPlus5').click(function () {
    //if (document.getElementById('txtTo5').value != '') {
    //    $("#lblTo5").append(document.getElementById('txtTo5').value + '@altisource.com ');
    //    document.getElementById('txtTo5').value = '';
    //}

    if (document.getElementById('txtTo5').value != '') {
        if ($('#lblTo5').text() == '') {
            $("#lblTo5").append(document.getElementById('txtTo5').value);
            document.getElementById('txtTo5').value = '';
        }
        else {
            $("#lblTo5").append('; ' + document.getElementById('txtTo5').value);
            document.getElementById('txtTo5').value = '';
        }
        //$("#lblTo1").append(document.getElementById('txtTo1').value + '@altisource.com ');
    }
});

$('#btnAddNew1').click(function () {
    dvAddNew1.style.display = 'none';
    if ($("#dvCondition2:visible").length == 0) {
        $("#dvCondition2").show();
        $("#dvRemove1").show();
    }
});
$('#btnAddNew2').click(function () {
    dvAddNew2.style.display = 'none';
    if ($("#dvCondition3:visible").length == 0) {
        $("#dvCondition3").show();
        $("#dvRemove2").show();
    }
});
$('#btnAddNew3').click(function () {
    dvAddNew3.style.display = 'none';
    if ($("#dvCondition4:visible").length == 0) {
        $("#dvCondition4").show();
        $("#dvRemove3").show();
    }
});
$('#btnAddNew4').click(function () {
    dvAddNew4.style.display = 'none';
    if ($("#dvCondition5:visible").length == 0) {
        $("#dvCondition5").show();
        $("#dvRemove4").show();
    }
});
$('#btnAddNew5').click(function () {
    if ($("#dvCondition1:visible").length == 0)
    {
        $("#dvCondition1").show();
        $("#dvRemove1").show();
        dvAddNew1.style.display = 'none';
    }
    else if($("#dvCondition2:visible").length == 0)
    {
        $("#dvCondition2").show();
        $("#dvRemove2").show();
        dvAddNew2.style.display = 'none';
    }
    else if ($("#dvCondition3:visible").length == 0) {
        $("#dvCondition3").show();
        $("#dvRemove3").show();
        dvAddNew3.style.display = 'none';
    }
    else{
        $("#dvCondition4").show();
        $("#dvRemove4").show();
        dvAddNew4.style.display = 'none';
    }
   
});
$('#btnRemove1').click(function () {
    dvCondition1.style.display = 'none';
    $("#lblTo1").empty();
});
$('#btnRemove2').click(function () {
    dvCondition2.style.display = 'none';
    dvRemove1.style.display = 'none';
        $("#dvAddNew1").show();
        $("#lblTo2").empty();
    
});
$('#btnRemove3').click(function () {
    dvCondition3.style.display = 'none';
    $("#dvAddNew2").show();
    $("#lblTo3").empty();
});
$('#btnRemove4').click(function () {
    dvCondition4.style.display = 'none';
    $("#dvAddNew3").show();
    $("#lblTo4").empty();
});
$('#btnRemove5').click(function () {
    dvCondition5.style.display = 'none';
    $("#dvAddNew4").show();
    $("#lblTo5").empty();
});
$('#btnAddnew').click(function () {
    dvTable.style.display = 'none';
    if ($("#dvAddNew:visible").length == 0) {
        $("#dvAddNew").show();
    }
});
$('#btnReset').click(function () {

    dvAddNew.style.display = 'none';
    if ($("#dvTable:visible").length == 0) {
        $("#dvTable").show();
    }
});

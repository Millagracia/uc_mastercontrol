﻿$(document).ready(function () {

    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //auditTable();

    $('.datepicker').datepicker({
        autoclose: true
    });

    ddlNTIDappend();
});

$('#btnApply').click(function () {
    auditTable();
});

function auditTable() {
    var time = $('#slctTime').val();
    var dtFrom = $('#inDateFrom').val();
    var dtTo = $('#inDateTo').val();
    var ntid = $('#slcNTID').val();
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'Audit.aspx/auditList',
        data: '{time: "' + time + '", dtFrom: "' + dtFrom + '", dtTo: "' + dtTo + '", ntid: "' + ntid + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            $('#tblAudit').bootstrapTable('destroy');

            $('#tblAudit').bootstrapTable({
                data: data.d.tblAuditList,
                height: 500,
                width: 1000,
                pagination: true
            });

            $('#tblAudit').show();

            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}


function ddlNTIDappend() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'Audit.aspx/ddlNTID',
        dataType: 'json',
        success: function(data)
        {
            var d = $.parseJSON(data.d);
            if(d.Success)
            {
                var records = d.data.asd;
                $('#slcNTID').empty();
                $('#slcNTID').append("<option value='' >All</option>");
                $.each(records, function (ids, val) {
                    $('#slcNTID').append("<option value='" + val.action_by + "' >" + val.action_by + "</option>");
                });
            }
        }
    });
}



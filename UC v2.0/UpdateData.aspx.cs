﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class UpdateData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetStart()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select DISTINCT USP_Name, [Bulk Email] as ebulk, [Online Request] as online,[EDI 814] as edi,[Call] From vw_UC_Capabilities_StartService";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

        [WebMethod]
        public static string Update()
        {
            

            string IDClient = "";
            List<string> uspID = new List<string>();
            List<string> bulk = new List<string>();
            List<string> online = new List<string>();
            List<string> edi = new List<string>();
            List<string> call = new List<string>();
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                con.Open();
                SqlCommand command = new SqlCommand("Select DISTINCT USP_ID, [Bulk Email], [Online Request], [EDI 814], Call From tbl_UC_USPcapabilities_Start_Service", con);
                //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDClient = reader["USP_ID"].ToString();
                    uspID.Add(IDClient);
                    bulk.Add(reader["Bulk Email"].ToString());
                    online.Add(reader["Online Request"].ToString());
                    edi.Add(reader["EDI 814"].ToString());
                    call.Add(reader["Call"].ToString());
                }
                con.Close();
            }
            clsConnection cls = new clsConnection();
            for (int i = 0; i < uspID.Count; i++ )
            {
                string update = "UPDATE tbl_UC_USPcapabilities_Start_Service SET [Bulk Email] = '" + bulk[i].ToString() 
                    +"',[Online Request] = '"+ online[i].ToString() +"',[EDI 814] = '"+ edi[i].ToString() +"',[Call] = '"+ call[i].ToString() +"'"
                    +"WHERE USP_ID = '" + uspID[i].ToString() + "'";
                cls.ExecuteQuery(update);
            }



          
            string qry = "Select DISTINCT USP_Name, [Bulk Email] as ebulk, [Online Request] as online,[EDI 814] as edi,[Call] From vw_UC_Capabilities_StartService";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
    }
}
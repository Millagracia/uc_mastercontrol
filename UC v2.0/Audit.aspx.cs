﻿using Master_Control;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class Audit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static AuditList auditList(string time, string dtFrom, string dtTo, string ntid)
        {
            clsConnection cls = new clsConnection();
            AuditList al = new AuditList();
            List<AuditClass> acl = new List<AuditClass>();
            DataTable dt = new DataTable();

            string param = "";
            DateTime now = DateTime.Now;

            if (time == "year" && (dtFrom == "" || dtTo == ""))
            {
                if (ntid == "" || ntid == null)
                {
                    param = "where YEAR(action_date) = '" + now.Year.ToString() + "';";
                }
                else
                {
                    param = "where YEAR(action_date) = '" + now.Year.ToString() + "' and action_by = '" + ntid +"';";
                }
                
            }
            else if (time == "month" && (dtFrom == "" || dtTo == ""))
            {
                if (ntid == "" || ntid == null)
                {
                    param = "where MONTH(action_date) = '" + now.Month.ToString() + "';";
                }
                else
                {
                    param = "where MONTH(action_date) = '" + now.Month.ToString() + "' and action_by = '" + ntid + "';";
                }
                
            }
            else if (time == "week" && (dtFrom == "" || dtTo == ""))
            {
                //DateTime today = DateTime.Today;
                int currentDayOfWeek = (int)now.DayOfWeek + 1;
                DateTime sunday = now.AddDays(-currentDayOfWeek);
                DateTime monday = sunday.AddDays(1);
                // If we started on Sunday, we should actually have gone *back*
                // 6 days instead of forward 1...
                if (currentDayOfWeek == 0)
                {
                    monday = monday.AddDays(-7);
                }
                var dates = Enumerable.Range(0, 7).Select(days => monday.AddDays(days)).ToList();


                if (ntid == "" || ntid == null)
                {
                    param = "where action_date between '" + dates[0].ToShortDateString() + "' and '" + dates[6].ToShortDateString() + "';";
                }
                else
                {
                    param = "where action_date between '" + dates[0].ToShortDateString() + "' and '" + dates[6].ToShortDateString() + "' and action_by = '" + ntid + "';";
                }


                
            }
            else if (time == "yesterday" && (dtFrom == "" || dtTo == ""))
            {
                if (ntid == "" || ntid == null)
                {
                    param = "where action_date = '" + now.AddDays(-1).ToShortDateString() + "';";
                }
                else
                {
                    param = "where action_date = '" + now.AddDays(-1).ToShortDateString() + "' and action_by = '" + ntid + "';";
                }
            }
            else if (time == "today" && (dtFrom == "" || dtTo == ""))
            {
                if (ntid == "" || ntid == null)
                {
                    param = "where action_date = '" + now.ToShortDateString() + "';";
                }
                else
                {
                    param = "where action_date = '" + now.ToShortDateString() + "' and action_by = '" + ntid + "';";
                }
            }
            else
            {
                
                if (ntid == "" || ntid == null)
                {
                    param = "where action_date between  '" + dtFrom + "' and '" + dtTo + "'";
                }
                else
                {
                    param = "where action_date between  '" + dtFrom + "' and '" + dtTo + "' and action_by = '" + ntid + "';";
                }
            }


            dt = cls.GetData("select id,page,case when sub_page is null then '-' else sub_page end, case when sub_sub_page is null then '-' else sub_sub_page end, " +
                            "action,action_by,action_date,Convert(time(0),action_time) from tbl_UC_Audit " + param);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AuditClass ac = new AuditClass();
                ac.id = dt.Rows[i][0].ToString();
                ac.page = dt.Rows[i][1].ToString();
                ac.sub_page = dt.Rows[i][2].ToString();
                ac.sub_sub_page = dt.Rows[i][3].ToString();
                ac.action = dt.Rows[i][4].ToString();
                ac.action_by = dt.Rows[i][5].ToString();
                ac.action_date = Convert.ToDateTime(dt.Rows[i][6].ToString()).ToShortDateString();
                ac.action_time = dt.Rows[i][7].ToString();
                acl.Add(ac);
            }

            al.tblAuditList = acl;

            return al;

        }

        [WebMethod]
        public static string ddlNTID()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string qry = "Select distinct action_by from tbl_UC_Audit order by action_by";
            dt = cls.GetData(qry);




            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dt
                }
            });
        }




    }
}
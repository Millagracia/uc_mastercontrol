﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class USPNormalize : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }






        [WebMethod]
        public static string fileUploadCSV(string fileUploadExcel)
        {

            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();

            // string csvData = File.ReadAllText(fileUploadExcel);

            //foreach (string row in csvData.Split(ControlChars.Lf)) { 

            string filePathStr = "C:\\Users\\" + HttpContext.Current.Session["ntid"] + "\\Desktop\\" + fileUploadExcel;
            string qry = "";



            //}
            string ExcelContentType = "application/vnd.ms-excel";
            string Excel2010ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Check the Content Type of the file 
            try
            {
                //Save file path 
                string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", filePathStr);

                // Create Connection to Excel Workbook 
                using (OleDbConnection connection =
                             new OleDbConnection(excelConnectionString))
                {
                    OleDbCommand command = new OleDbCommand
                            ("Select * FROM [USP Normalization List$]", connection);
                    //("Select * FROM [Sheet1$]", connection);
                    connection.Open();

                    //Create DbDataReader to Data Worksheet 
                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        // SQL Server Connection String 
                        string sqlConnectionString = @"Data Source=piv8dbmisnp01;Initial Catalog=MIS_ALTI;Integrated Security=True";
                        dt.Load(dr);
                        cls.SQLBulkCopy("tbl_UC_USP_TempoDelete", dt);
                        // Bulk Copy to SQL Server 
                        DataTable dtCheck = new DataTable();
                        string qryInsert = "Insert Into tbl_UC_USP_Normalization (VendorID,Vendorname,isTag) values ";
                        string qrySelect = " ";
                        string VendorIDs = "";

                        DataTable dtDelete = new DataTable();


                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i != dt.Rows.Count - 1)
                            {
                                VendorIDs += "'" + dt.Rows[i][0].ToString() + "',";
                            }
                            else
                            {
                                VendorIDs += "'" + dt.Rows[i][0].ToString() + "'";
                            }
                        }
                                     
                        qrySelect = "Select VendorID From tbl_UC_USP_Normalization ";
                        //qrySelect += "VendorID IN (" + VendorIDs + ")";
                        dtCheck = cls.GetData(qrySelect);
                        string VendorIDs2 = "";
                        for (int i = 0; i < dtCheck.Rows.Count; i++)
                        {
                            if (i != dtCheck.Rows.Count - 1)
                            {
                                VendorIDs2 += "'" + dtCheck.Rows[i][0].ToString() + "',";
                            }
                            else
                            {
                                VendorIDs2 += "'" + dtCheck.Rows[i][0].ToString() + "'";

                            }
                        }





                        string qryDelete = "Delete tbl_UC_USP_TempoDelete where VID IN (" + VendorIDs2 + ") OR VID is NULL";
                        cls.ExecuteQuery(qryDelete);

                        DataTable dtVID = new DataTable();
                        string qryGetVID = "Select VID,[Vendor Name],isTag FROM tbl_UC_USP_TempoDelete";
                        dtVID = cls.GetData(qryGetVID);
                        dtVID.Columns.Add("NewColumn", typeof(System.Int32)).SetOrdinal(0);
                        cls.SQLBulkCopy("tbl_UC_USP_Normalization", dtVID);




                        string qryUpdateActive = "Update tbl_UC_USP_Normalization Set isActive = 1 Where VendorID IN (" + VendorIDs + "); ";
                        qryUpdateActive += "Update tbl_UC_USP_Normalization Set isActive = 0 Where NOT VendorID IN (" + VendorIDs + ")";
                        cls.ExecuteQuery(qryUpdateActive);



                        string DeleteVID = "Delete [tbl_UC_USPcapabilities_Start_Service] Where NOT VendorID IN (" + VendorIDs + "); ";
                        DeleteVID += "Delete [tbl_UC_USPcapabilities_Stop_Service] Where NOT VendorID IN (" + VendorIDs + "); ";
                        DeleteVID += "Delete [tbl_UC_USPcapabilities_BillsPayment] Where NOT VendorID IN (" + VendorIDs + "); ";
                        DeleteVID += "Delete [tbl_UC_USPcapabilities_Invoicing] Where NOT VendorID IN (" + VendorIDs + "); ";
                        cls.ExecuteQuery(DeleteVID);




                        qryDelete = "Delete tbl_UC_USP_TempoDelete";
                        cls.ExecuteQuery(qryDelete);
                    }
                }
                //return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd2 = dt2 } });
                return "True";
            }
            catch (Exception ex)
            {
                return "False";
            }

           
        }
    }
}
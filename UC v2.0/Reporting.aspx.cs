﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class Reporting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        public static string GetClosed()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string qry = "Select DISTINCT 'Bulk Email' as 'Solution' , ";
            qry += "(SELECT Count(*) as 'Utility On' FROM tbl_UC_USP_Historical where OnlineFlag = '3' and Status = 'Utility On' and ";
            qry += "convert(NVARCHAR, Historical_Date, 110) Between '10-27-2017' AND '" + DateTime.Now.ToString("MM-dd-yyyy") +"') as 'Utility_On' ";
            qry += ",(SELECT Count(*) as 'Utility Off' FROM tbl_UC_USP_Historical where OnlineFlag = '3' and Status = 'Utility Off' and ";
            qry += "convert(NVARCHAR, Historical_Date, 110) Between '10-27-2017' AND '" + DateTime.Now.ToString("MM-dd-yyyy") + "') as 'Utility_Off' ";
            qry += "FRom tbl_UC_USP_Historical where OnlineFlag = '3' and Status IN ('Utility On','Utility Off') ";
            qry += " Union All ";
            qry += "Select DISTINCT 'Online Request' as 'Solution' , ";
            qry += "(SELECT Count(*) as 'Utility On' FROM tbl_UC_USP_Historical where OnlineFlag = '2' and Status = 'Utility On' and  ";
            qry += "convert(NVARCHAR, Historical_Date, 110) Between '10-27-2017' AND '" + DateTime.Now.ToString("MM-dd-yyyy") + "') as 'Utility_On' ";
            qry += ",(SELECT Count(*) as 'Utility Off' FROM tbl_UC_USP_Historical where OnlineFlag = '2' and Status = 'Utility Off' and  ";
            qry += "convert(NVARCHAR, Historical_Date, 110) Between '10-27-2017' AND '" + DateTime.Now.ToString("MM-dd-yyyy") + "') as 'Utility_Off' ";
            qry += "FRom tbl_UC_USP_Historical where OnlineFlag = '2' and Status IN ('Utility On','Utility Off') ";
            qry += "Union All ";
            qry += "Select DISTINCT 'Callout' as 'Solution' , ";
            qry += "(SELECT Count(*) as 'Utility On' FROM tbl_UC_USP_Historical where OnlineFlag = '0' and Status = 'Utility On' and ";
            qry += "convert(NVARCHAR, Historical_Date, 110) Between '10-27-2017' AND '" + DateTime.Now.ToString("MM-dd-yyyy") + "') as 'Utility_On' ";
            qry += ",(SELECT Count(*) as 'Utility Off' FROM tbl_UC_USP_Historical where OnlineFlag = '0' and Status = 'Utility Off' and ";
            qry += "convert(NVARCHAR, Historical_Date, 110) Between '10-27-2017' AND '" + DateTime.Now.ToString("MM-dd-yyyy") + "') as 'Utility_Off' ";
            qry += " FRom tbl_UC_USP_Historical where OnlineFlag = '0' and Status IN ('Utility On','Utility Off') ";
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

        [WebMethod]
        public static string Graph()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dtShow = new DataTable();
            string qry = "";


            DateTime countDays = DateTime.ParseExact("10-27-2017", "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
            string date1 = DateTime.Now.ToString("MM-dd-yyyy");
            DateTime TodayDate = DateTime.ParseExact(date1, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

            double daysTotal = TodayDate.Subtract(countDays).TotalDays;
            int daysTotalInt = (int)daysTotal;

            TodayDate = TodayDate.AddDays(-1);
            string DayName = TodayDate.DayOfWeek.ToString();


            for (int i = 0; i <= daysTotalInt; i ++ )
            {

                if ( countDays.AddDays(i).DayOfWeek.ToString() == "Sunday")
                {

                }
                else
                {

                        qry += "Select DISTINCT 'Bulk Email' as 'Solution' , ";
                        qry += "(SELECT Count(*) as 'Utility On' FROM tbl_UC_USP_Historical where OnlineFlag = '3' and Status = 'Utility On' and ";
                        qry += "convert(NVARCHAR, Historical_Date, 110) = '" + countDays.AddDays(i).ToString("MM-dd-yyyy") + "') as 'Utility_On' ";
                        qry += ",(SELECT Count(*) as 'Utility Off' FROM tbl_UC_USP_Historical where OnlineFlag = '3' and Status = 'Utility Off' and ";
                        qry += "convert(NVARCHAR, Historical_Date, 110) = '" + countDays.AddDays(i).ToString("MM-dd-yyyy") + "') as 'Utility_Off', ";
                        qry += "'" + countDays.AddDays(i).ToString("yyyy-MM-dd") + "' as 'Date' ";
                        qry += "FRom tbl_UC_USP_Historical where OnlineFlag = '0' and Status IN ('Utility On','Utility Off') ";
                        qry += " Union all ";
                   
                }
            }
            qry = qry.Substring(0, qry.Length - 10);

            dt = cls.GetData(qry);



            //dtShow.Columns.Add("Solution");
            //dtShow.Columns.Add("Utility_On");
            //dtShow.Columns.Add("Utility_Off");
            //dtShow.Columns.Add("Date");


            //foreach(DataRow dr in dt.Rows)
            //{
            //    dtShow.Rows.Add(dr.ItemArray);
            //}
            

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GraphChange(string selectedShow)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dtShow = new DataTable();
            string qry = "";
            int Process = 0;

            if(selectedShow == "Bulk Email")
            {
                Process = 3;
            }
            else if(selectedShow == "Online Request")
            {
                Process = 2;
            }
            else if (selectedShow == "CallOut")
            {
                Process = 0;
            }
            DateTime countDays = DateTime.ParseExact("10-27-2017", "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
            string date1 = DateTime.Now.ToString("MM-dd-yyyy");
            DateTime TodayDate = DateTime.ParseExact(date1, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

            double daysTotal = TodayDate.Subtract(countDays).TotalDays;
            int daysTotalInt = (int)daysTotal;

            TodayDate = TodayDate.AddDays(-1);
            string DayName = TodayDate.DayOfWeek.ToString();


            for (int i = 0; i <= daysTotalInt; i++)
            {

                if (countDays.AddDays(i).DayOfWeek.ToString() == "Saturday" || countDays.AddDays(i).DayOfWeek.ToString() == "Sunday")
                {

                }
                else
                {

                    qry += "Select DISTINCT 'Bulk Email' as 'Solution' , ";
                    qry += "(SELECT Count(*) as 'Utility On' FROM tbl_UC_USP_Historical where OnlineFlag = '"+ Process +"' and Status = 'Utility On' and ";
                    qry += "convert(NVARCHAR, Historical_Date, 110) = '" + countDays.AddDays(i).ToString("MM-dd-yyyy") + "') as 'Utility_On' ";
                    qry += ",(SELECT Count(*) as 'Utility Off' FROM tbl_UC_USP_Historical where OnlineFlag = '" + Process + "' and Status = 'Utility Off' and ";
                    qry += "convert(NVARCHAR, Historical_Date, 110) = '" + countDays.AddDays(i).ToString("MM-dd-yyyy") + "') as 'Utility_Off', ";
                    qry += "'" + countDays.AddDays(i).ToString("yyyy-MM-dd") + "' as 'Date' ";
                    qry += "FRom tbl_UC_USP_Historical where OnlineFlag = '" + Process + "' and Status IN ('Utility On','Utility Off') ";
                    qry += " Union all ";

                }
            }
            qry = qry.Substring(0, qry.Length - 10);

            dt = cls.GetData(qry);


            //dtShow.Columns.Add("Solution");
            //dtShow.Columns.Add("Utility_On");
            //dtShow.Columns.Add("Utility_Off");
            //dtShow.Columns.Add("Date");


            //foreach(DataRow dr in dt.Rows)
            //{
            //    dtShow.Rows.Add(dr.ItemArray);
            //}


            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }


        [WebMethod]
        public static string GetMovement()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            DataTable dtStatus = new DataTable();


            DataTable dtAdd = new DataTable();
            DataTable dtCer = new DataTable();
            DataTable dtCity = new DataTable();
            DataTable dtDoc = new DataTable();
            DataTable dtException = new DataTable();
            DataTable dtExcAllElec = new DataTable();
            DataTable dtExcProFO = new DataTable();
            DataTable dtExcDeedRe = new DataTable();
            DataTable dtExcHOA = new DataTable();
            DataTable dtExcCity = new DataTable();
            DataTable dtExcWellWater = new DataTable();
            DataTable dtPayPenCliApp = new DataTable();
            DataTable dtPayReqCC = new DataTable();
            DataTable dtPayReqCheck = new DataTable();
            DataTable dtPayReqPastDues = new DataTable();
            DataTable dtPendingAnoUtilAct = new DataTable();
            DataTable dtRIP = new DataTable();
            DataTable dtRepUnd = new DataTable();
            DataTable dtRepairsComMove = new DataTable();
            DataTable dtRepairsPenClient = new DataTable();
            DataTable dtReqDocu = new DataTable();
            DataTable dtUtilityOff = new DataTable();
            DataTable dtUtilityOffSaleAs = new DataTable();
            DataTable dtUtilityOn = new DataTable();
            DataTable dtUtlityOrder = new DataTable();
            DataTable dtVendorPresenceReq = new DataTable();
            DataTable dtOutOfREO = new DataTable();
            DataTable dtNull = new DataTable();
            DataTable dtHUD = new DataTable();
            DataTable dtLetterOf = new DataTable();
            DataTable dtSettBal = new DataTable();
            DataTable dtTurnOffReq = new DataTable();
            DataTable dtExcCityChiWaterDeact = new DataTable();
            string qry = "Select Address,[Property ID], Utility,Status from tbl_UC_USP_Historical where convert(NVARCHAR, Historical_Date, 110) = '10-27-2017' Order By Status";
            dt = cls.GetData(qry);

            for (int i = 0; i < dt.Rows.Count; i++ )
            {
                if (dt.Rows[i][0] == "Address Verification Required by Vendor")
                {

                }
                else if (dt.Rows[i][0] == "Certified Inspection")
                {

                }
                else if (dt.Rows[i][0] == "City Inspection")
                {

                }
                else if (dt.Rows[i][0] == "Documents Required by USP")
                {

                }
                else if (dt.Rows[i][0] == "Exception")
                {

                }
                else if (dt.Rows[i][0] == "Exception - All Electric")
                {

                }
                else if (dt.Rows[i][0] == "Exception - Propane Fuel Oil")
                {

                }
                else if (dt.Rows[i][0] == "Exception – Deeds Required")
                {

                }
                else if (dt.Rows[i][0] == "Exception - HOA Managed")
                {

                }
                else if (dt.Rows[i][0] == "Exception - City of Chicago Water (Deactivation Only)")
                {

                }
                else if (dt.Rows[i][0] == "Exception - Well Water")
                {

                }
                else if (dt.Rows[i][0] == "Payment Pending Client Approval")
                {

                }
                else if (dt.Rows[i][0] == "Payment Requested - CC")
                {

                }
                else if (dt.Rows[i][0] == "Payment Requested - Check")
                {

                }
                else if (dt.Rows[i][0] == "Payment Requested (to be used for multiple past dues)")
                {

                }
                else if (dt.Rows[i][0] == "Pending Another Utility Activation")
                {

                }
                else if (dt.Rows[i][0] == "Research In Progress")
                {

                }
                else if (dt.Rows[i][0] == "Repair - Underway")
                {

                }
                else if (dt.Rows[i][0] == "Repairs Complete - Move to Renovate")
                {

                }
                else if (dt.Rows[i][0] == "Repair -  Pending Client Approval")
                {

                }
                else if (dt.Rows[i][0] == "Required Documents Sent to USP")
                {

                }
                else if (dt.Rows[i][0] == "Utility Off")
                {

                }
                else if (dt.Rows[i][0] == "Utility Off-Sale As Is")
                {

                }
                else if (dt.Rows[i][0] == "Utility On")
                {

                }
                else if (dt.Rows[i][0] == "Utility Ordered")
                {

                }
                else if (dt.Rows[i][0] == "Vendor Presence Required")
                {

                }
                else if (dt.Rows[i][0] == "Out of REO")
                {

                }
                else if (dt.Rows[i][0] == "Null")
                {

                }
                else if (dt.Rows[i][0] == @"HUD\Proof of Sale")
                {

                }
                else if (dt.Rows[i][0] == "Letter of request required")
                {

                }
                else if (dt.Rows[i][0] == "Settlement of Balance")
                {

                }
                else if (dt.Rows[i][0] == "Turn Off form Required")
                {

                }
                else if (dt.Rows[i][0] == "Exception - City of Chicago Water (Deactivation Only)")
                {

                }
                else 
                {

                }
                
            }

            


            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }






    }
}
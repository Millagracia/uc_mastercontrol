﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Principal;
using System.Management;

namespace UC_v2._0
{
    public partial class BusinessRequirements : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }
        [WebMethod]
        public static string GetUSP()
        {
            clsConnection cls = new clsConnection();

            string qry = "Select DISTINCT USP_Name FROM tbl_UC_USP_Name_Vendor_ID Where isDeleted = 0;";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetClient()
        {
            clsConnection cls = new clsConnection();

            string qry = "Select * From tbl_UC_Client Order By Client_Name;";
            string qryBusiness = "Select * From tbl_UC_Client Order By Client_Name;";
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            dt = cls.GetData(qry);
            dt2 = cls.GetData(qryBusiness);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd2 = dt2 } });
        }
        [WebMethod]
        public static string GetStatus()
        {
            clsConnection cls = new clsConnection();

            string qry = "select id,Start_Desc as 'Status' from tbl_UC_USP_Start_Service_Status ";
            qry += "Union All ";
            qry += "select id,Stop_Desc as 'Status' from tbl_UC_USP_Stop_Service_Status ";
            string qryBusiness = "Select * From tbl_UC_Client Order By Client_Name;";
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            dt = cls.GetData(qry);
            dt2 = cls.GetData(qryBusiness);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd2 = dt2 } });
        }
        [WebMethod]
        public static string GetState()
        {
            clsConnection cls = new clsConnection();

            string qry = "Select DISTINCT State FROM tbl_UC_State_City_Zip;";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetUSPsettings(string USP)
        {
            clsConnection cls = new clsConnection();
            string qry = "Select a.Business_ID, a.USP_Name, b.Requirements_ID,a.Address_ID, b.[Deposit Amount] as Deposit, b.[Activation Fee] as Activation,"
                          + "b.[Lien Search Fee] as lien, a.[Email Address] as email, a.[Attention To] as attention, a.[FAX Text] as fax,"
                          + "a.[Foreclosure Deed] as foreclosure, a.[Investor-Ocwen POA] as investor, a.[Ocwen-Altisource POA] as ocwen"
                          + ", a.[Application Form] as application, a.[Letter of Authorization] as letter, a.[ASFI W9] as asfi"
                          + ", a.[Listing Agreement] as listing, a.[Report/Clearance] as report,a.[City Email Address] as cityE, "
                          + "a.[City Attention To] as cityA, a.[City Fax Text] cityF, a.Activate_Elec_Water, a.Activate_Elec_Gas,"
                          + "a.Activate_Gas_Water, a.Activate_Gas_Elec, a.Activate_Water_Elec, a.Activate_Water_Gas "
                          + ", a.[Inspection Requirements] as inspection, a.ZipCodes ,c.Physical_Street, c.Physical_City, c.Physical_State , c.Physical_Zipcode,"
                          + "c.Payment_Street, c.Payment_City,c.Payment_State, c.Payment_Zipcode, c.PO_Street, c.PO_City, c.PO_State, c.PO_Zipcode"
                          + ", a.[Date Created], a.NTID"
                          + " FROM [tbl_UC_USP_BusinessReq] a JOIN tbl_UC_BusinessReq_Requirements b ON a.Requirements_ID  = b.Requirements_ID"
                          + " JOIN tbl_UC_BusinessReq_Address c ON c.Address_ID = a.Address_ID"
                          + " Where USP_Name = '" + USP + "';";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetCity(string State)
        {
            clsConnection cls = new clsConnection();

            string qry = "Select DISTINCT City FROM tbl_UC_State_City_Zip Where State = '" + State + "';";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            string set = dt.Rows[0].ToString();
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string SaveRequirements(string Deposit, string Activation, string Lien, string USP, string Email, string Attention,
            string Fax, string Deed, string Request, string POA, string Autho, string OcwenPOA, string ASFI, string Form, string Listing, string Report, string CityEmail,
            string Attention1, string CityFax, string ElecWater, string ElecGas, string GasWater, string GasElec, string WaterElec, string WaterGas, string PStreet, string PCity,
            string PState, string PZip, string PayStreet, string PayCity, string PayState, string PayZip, string POstreet, string POcity, string POstate, string POzip, string Zip,
            string Require)
        {
            string ReqID = "";
            string BusinessID = "";
            string AddressID = "";
            string ReqIDnew = "";
            string verify = "";
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                //SqlCommand command = new SqlCommand("Select Requirements_ID From tbl_UC_BusinessReq_Requirements Where [Deposit Amount] = '" + Deposit + "' AND " +
                //    "[Activation Fee] = '" + Activation + "' AND [Lien Search Fee] = '" + Lien + "'", con);
                //con.Open();
                //SqlDataReader reader = command.ExecuteReader();
                //while (reader.Read())
                //{
                //    ReqID = reader["Requirements_ID"].ToString();
                //}
                SqlCommand command = new SqlCommand("select * FROM tbl_UC_USP_BusinessReq AS a "
                    + "INNER JOIN  tbl_UC_BusinessReq_Requirements AS b ON a.Requirements_ID = b.Requirements_ID  "
                    + "INNER JOIN  tbl_UC_BusinessReq_Address AS c ON a.Address_ID = c.Address_ID"
                    + " Where a.USP_Name = '" + USP + "'", con);
                con.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BusinessID = reader["Business_ID"].ToString();
                    AddressID = reader["Address_ID"].ToString();
                    ReqID = reader["Requirements_ID"].ToString();
                }
                con.Close();

                if (BusinessID == "")
                {
                    SqlCommand updateBulk = new SqlCommand("INSERT INTO tbl_UC_BusinessReq_Requirements  ([Deposit Amount], [Activation Fee], [Lien Search Fee]) values('" + Deposit
                    + "','" + Activation + "','" + Lien + "')", con);
                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();
                        con.Close();
                    }

                    SqlCommand insertAdd = new SqlCommand("INSERT INTO tbl_UC_BusinessReq_Address  ([Physical_Street], [Physical_City], [Physical_State], " +
                        "[Physical_Zipcode],[Payment_Street],[Payment_City],[Payment_State],[Payment_Zipcode],[PO_Street],[PO_City],[PO_State],[PO_Zipcode]) values('" + PStreet
                    + "','" + PCity + "','" + PState + "','" + PZip + "','" + PayStreet + "','" + PayCity + "','" + PayState + "','" + PayZip + "','" + POstreet
                    + "','" + POcity + "','" + POstate + "','" + POzip + "')", con);
                    {
                        con.Open();
                        insertAdd.ExecuteNonQuery();
                        con.Close();
                    }
                    //Last Update
                    SqlCommand command3 = new SqlCommand("SELECT TOP 1 *  FROM [dbo].[tbl_UC_BusinessReq_Address] ORDER BY Address_ID DESC", con);
                    con.Open();
                    SqlDataReader reader3 = command3.ExecuteReader();
                    while (reader3.Read())
                    {
                        verify = reader3["Address_ID"].ToString();
                    }
                    con.Close();

                    SqlCommand command2 = new SqlCommand("Select Requirements_ID From tbl_UC_BusinessReq_Requirements Where [Deposit Amount] = '" + Deposit + "' AND " +
                    "[Activation Fee] = '" + Activation + "' AND [Lien Search Fee] = '" + Lien + "'", con);
                    con.Open();
                    SqlDataReader reader2 = command2.ExecuteReader();
                    while (reader2.Read())
                    {
                        ReqIDnew = reader2["Requirements_ID"].ToString();
                    }
                    con.Close();
                    SqlCommand updateReq = new SqlCommand("INSERT INTO tbl_UC_USP_BusinessReq ([USP_Name] ,[Requirements_ID], " +
                        "[Email Address],[Attention To], [FAX Text],[Foreclosure Deed],[Investor-Ocwen POA],[Ocwen-Altisource POA],[Application Form]," +
                        "[Letter OF Request],[Letter of Authorization], [ASFI W9],[Listing Agreement], " +
                        "[Report/Clearance],[City Email Address], [City Attention To],[City Fax Text], " +
                        "[Activate_Elec_Water],[Activate_Elec_Gas],[Activate_Gas_Water],[Activate_Gas_Elec],[Activate_Water_Elec], " +
                        "[Activate_Water_Gas],[Inspection Requirements],[Address_ID],[ZipCodes], [Date Created],[NTID]  ) values('"
                        + USP + "','" + ReqIDnew + "','"
                        + Email + "','" + Attention + "','" + Fax + "','" + Deed + "','"
                        + POA + "','" + OcwenPOA + "','" + Form + "','" + Request + "','"
                        + Autho + "','" + ASFI + "','" + Listing + "','"
                        + Report + "','" + CityEmail + "','" + Attention1 + "','" + CityFax + "','"
                        + ElecWater + "','" + ElecGas + "','" + GasWater + "','" + GasElec + "','"
                        + WaterElec + "','" + WaterGas + "','" + Require + "','" + verify + "','" + Zip + "','"
                        + DateTime.Now.ToString() + "','" + HttpContext.Current.Session["ntid"].ToString()
                        + "')", con);
                    {
                        con.Open();
                        updateReq.ExecuteNonQuery();
                        con.Close();
                    }
                }
                else
                {
                    SqlCommand updateBusiness = new SqlCommand("UPDATE tbl_UC_USP_BusinessReq SET " +
                     "[USP_Name] = '" + USP + "',"
                   + "[Email Address] = '" + Email + "'," + "[Attention To] = '" + Attention + "',"
                   + "[FAX Text] = '" + Fax + "'," + "[Foreclosure Deed] = '" + Deed + "',"
                   + "[Investor-Ocwen POA] = '" + POA + "',"
                   + "[Ocwen-Altisource POA] = '" + OcwenPOA + "'," + "[Application Form] = '" + Form + "',"
                   + "[Report/Clearance] = '" + Report + "'," + "[City Email Address] = '" + CityEmail + "',"
                   + "[City Attention To] = '" + Attention1 + "'," + "[City Fax Text] = '" + CityFax + "',"
                   + "[Activate_Elec_Water] = '" + ElecWater + "'," + "[Activate_Elec_Gas] = '" + ElecGas + "',"
                   + "[Activate_Gas_Water] = '" + GasWater + "'," + "[Activate_Gas_Elec] = '" + GasElec + "',"
                   + "[Activate_Water_Elec] = '" + WaterElec + "'," + "[Activate_Water_Gas] = '" + WaterGas + "',"
                   + "[Inspection Requirements] = '" + Require + "',"
                   + "[ZipCodes] = '" + Zip + "'," + "[Date Created] = '" + DateTime.Now.ToString() + "',"
                   + "[NTID] = '" + Environment.UserName + "'"
                   + " FROM tbl_UC_USP_BusinessReq "
                   + " WHERE USP_Name = '" + USP + "'", con);

                    {
                        con.Open();
                        updateBusiness.ExecuteNonQuery();
                        con.Close();
                    }

                    SqlCommand updateRequirements = new SqlCommand("UPDATE tbl_UC_BusinessReq_Requirements SET "
                   + "[Deposit Amount] = '" + Deposit + "'," + "[Activation Fee] = '" + Activation + "',"
                   + "[Lien Search Fee] = '" + Lien + "'"
                   + " FROM tbl_UC_BusinessReq_Requirements "
                   + " WHERE Requirements_ID = '" + ReqID + "'", con);
                    {
                        con.Open();
                        updateRequirements.ExecuteNonQuery();
                        con.Close();
                    }
                    SqlCommand updateAddress = new SqlCommand("UPDATE tbl_UC_BusinessReq_Address SET "
                        + "[Physical_Street] = '" + PStreet + "'," + "[Physical_City] = '" + PCity + "',"
                        + "[Physical_State] = '" + PState + "'," + "[Physical_Zipcode] = '" + PZip + "',"
                        + "[Payment_Street] = '" + PayStreet + "'," + "[Payment_City] = '" + PayCity + "',"
                        + "[Payment_State] = '" + PState + "'," + "[Payment_Zipcode] = '" + PayZip + "',"
                        + "[PO_Street] = '" + POstate + "'," + "[PO_City] = '" + POcity + "',"
                        + "[PO_State] = '" + POstate + "'," + "[PO_Zipcode] = '" + POzip + "'"
                        + " FROM tbl_UC_BusinessReq_Address "
                        + " WHERE Address_ID = '" + AddressID + "'", con);
                    {
                        con.Open();
                        updateAddress.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            return "true";
        }

        [WebMethod]
        public static string InsertData(string Client, string Task, string Status, string Utility, string WorkItem, string Category, string WorkItemStatus, string Days)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qryClient = "Select * From tbl_UC_Client where Client_Name IN ('" + Client + "')";
            dt = cls.GetData(qryClient);

            string insert = "Insert INTO [tbl_UC_BusinessRule]  (Client_ID,Task,Status,Utility,Requires_Work_Item" +
                            ",Category_Name,WI_Status,Follow_UP) values ";
            insert += "('" + dt.Rows[0][0].ToString() + "'";
            insert += ",'" + Task + "'";
            insert += ",'" + Status + "'";
            insert += ",'" + Utility + "'";
            insert += ",'" + WorkItem + "'";
            insert += ",'" + Category + "'";
            insert += ",'" + WorkItemStatus + "'";
            insert += ",'" + Days + "'),";




            return "True";
        }
    }
}
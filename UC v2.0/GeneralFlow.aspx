﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="GeneralFlow.aspx.cs" Inherits="UC_v2._0.GeneralFlow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 

   <div class="content-wrapper" style="min-height:733px">
        <%--<div class="box box-default">--%>
            <div class="box-body">
                <div class="row">

                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_2" data-toggle="tab" aria-expanded="false">USP WF Map</a></li>
                                <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="true">Prioritization</a></li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane" id="tab_1">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_2_1" data-toggle="tab" aria-expanded="false">Start Service</a></li>
                                            <li><a href="#tab_2_2" data-toggle="tab" aria-expanded="false">Stop service</a></li>
                                            <li><a href="#tab_2_3" data-toggle="tab" aria-expanded="false">Invoice</a></li>
                                            <li><a href="#tab_2_4" data-toggle="tab" aria-expanded="false">Payments</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_2_1">
                                                <div class="panel panel-primary">
                                                    <div class="panel-body">
                                                        <div class="row form-group">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered tblPRIORITY" >
                                                                    <thead>
                                                                        <tr>
                                                                            <th>PRIO
                                                                            </th>
                                                                            <th>METHOD
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                         <tr>
                                                                            <td>1
                                                                            </td>
                                                                            <td>EDI 814 Start
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td>2
                                                                            </td>
                                                                            <td>Bulk Start
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td>3
                                                                            </td>
                                                                            <td>Online Payment Start
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td>4
                                                                            </td>
                                                                            <td>Bulk Online Request
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-12 divADD">
                                                                <a class="lnkADDNEW" style="cursor: pointer; font-style: italic;"><i class="fa fa-plus"></i>&nbsp;Add New</a>
                                                                <a class="lnkMODIFY" style="cursor: pointer; font-style: italic;" class="modify"><i class="fa fa-edit"></i>&nbsp;Modify</a>
                                                            </div>
                                                            <div class="col-md-12 divSAVE" style="display: none;">
                                                                <a class="lnkSAVE" style="cursor: pointer; font-style: italic;"><i class="fa fa-save"></i>&nbsp;Save</a>
                                                                <a class="lnkCANCEL" style="cursor: pointer; font-style: italic;"><i class="fa fa-close"></i>&nbsp;Cancel</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_2_2">
                                                <div class="panel panel-primary">
                                                    <div class="panel-body">
                                                        <div class="row form-group">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered tblPRIORITY" >
                                                                    <thead>
                                                                        <tr>
                                                                            <th>PRIO
                                                                            </th>
                                                                            <th>METHOD
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                       <tr>
                                                                            <td>1
                                                                            </td>
                                                                            <td>EDI 814 Stop
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td>2
                                                                            </td>
                                                                            <td>Bulk Stop
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td>3
                                                                            </td>
                                                                            <td>Online Payment Stop
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td>4
                                                                            </td>
                                                                            <td>Bulk Online Request
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-12 divADD">
                                                                <a class="lnkADDNEW" style="cursor: pointer; font-style: italic;"><i class="fa fa-plus"></i>&nbsp;Add New</a>
                                                                <a class="lnkMODIFY" style="cursor: pointer; font-style: italic;" class="modify"><i class="fa fa-edit"></i>&nbsp;Modify</a>
                                                            </div>
                                                            <div class="col-md-12 divSAVE" style="display: none;">
                                                                <a class="lnkSAVE" style="cursor: pointer; font-style: italic;"><i class="fa fa-save"></i>&nbsp;Save</a>
                                                                <a class="lnkCANCEL" style="cursor: pointer; font-style: italic;"><i class="fa fa-close"></i>&nbsp;Cancel</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_2_3">
                                                <div class="panel panel-primary">
                                                    <div class="panel-body">
                                                        <div class="row form-group">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered tblPRIORITY">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>PRIO
                                                                            </th>
                                                                            <th>METHOD
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                       <tr>
                                                                            <td>1
                                                                            </td>
                                                                            <td>EDI 810 Invoice
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                      
                                                                         <tr>
                                                                            <td>2
                                                                            </td>
                                                                            <td>Summary Billing
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td>3
                                                                            </td>
                                                                            <td>Paper Bill
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td>4
                                                                            </td>
                                                                            <td>E-Bill
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-12 divADD">
                                                                <a class="lnkADDNEW" style="cursor: pointer; font-style: italic;"><i class="fa fa-plus"></i>&nbsp;Add New</a>
                                                                <a class="lnkMODIFY" style="cursor: pointer; font-style: italic;" class="modify"><i class="fa fa-edit"></i>&nbsp;Modify</a>
                                                            </div>
                                                            <div class="col-md-12 divSAVE" style="display: none;">
                                                                <a class="lnkSAVE" style="cursor: pointer; font-style: italic;"><i class="fa fa-save"></i>&nbsp;Save</a>
                                                                <a class="lnkCANCEL" style="cursor: pointer; font-style: italic;"><i class="fa fa-close"></i>&nbsp;Cancel</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_2_4">
                                                <div class="panel panel-primary">
                                                    <div class="panel-body">
                                                        <div class="row form-group">
                                                            <div class="col-md-12">
                                                                <table class="table table-bordered tblPRIORITY">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>PRIO
                                                                            </th>
                                                                            <th>METHOD
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                       <tr>
                                                                            <td>1
                                                                            </td>
                                                                            <td>ACH
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td>2
                                                                            </td>
                                                                            <td>Wire Transfer
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td>3
                                                                            </td>
                                                                            <td>Credit Card
                                                                            </td>
                                                                            <td style="border-style: none; display: none;" class="btnFOREDITING">
                                                                                <button type="button" class="btn btn-btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                                                                                <button type="button" class="btn btn-btn btn-danger" title="Remove"><i class="fa fa-remove"></i></button>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                           <div class="col-md-12 divADD">
                                                                <a class="lnkADDNEW" style="cursor: pointer; font-style: italic;"><i class="fa fa-plus"></i>&nbsp;Add New</a>
                                                                <a class="lnkMODIFY" style="cursor: pointer; font-style: italic;" class="modify"><i class="fa fa-edit"></i>&nbsp;Modify</a>
                                                            </div>
                                                            <div class="col-md-12 divSAVE" style="display: none;">
                                                                <a class="lnkSAVE" style="cursor: pointer; font-style: italic;"><i class="fa fa-save"></i>&nbsp;Save</a>
                                                                <a class="lnkCANCEL" style="cursor: pointer; font-style: italic;"><i class="fa fa-close"></i>&nbsp;Cancel</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane active" id="tab_2">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered" id="tblWF">
                                                        <thead>
                                                            <tr>
                                                                <th>USP
                                                                </th>
                                                                <th>EDI 810 Invoice
                                                                </th>
                                                                <th>EDI 814 Start/Stop
                                                                </th>
                                                                <th>EDI 820 Remittance
                                                                </th>
                                                                <th>Summary Billing
                                                                </th>
                                                                <th>E-Bill
                                                                </th>
                                                                <th>Online Payment
                                                                </th>
                                                                <th>Bulk Online Request
                                                                </th>
                                                                <th>Bulk Start/Stop Email Request
                                                                </th>
                                                                <th>Online Stop/Start
                                                                </th>
                                                                <th>Wire Transfer
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Duke Energy </td>

                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Florida Power and Light </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>ComEd  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Baltimore Gas and Electric  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>National Grid  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>PSE and G  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Nicor Gas  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>PG and E  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <tr>
                                                                    <td>DTE Energy  </td>
                                                                    <td class='text-center'>
                                                                        <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                    </td>
                                                                    <td class='text-center'>
                                                                        <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                    </td>
                                                                    <td class='text-center'>
                                                                        <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                    </td>
                                                                    <td class='text-center'>
                                                                        <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                    </td>
                                                                    <td class='text-center'>
                                                                        <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                    </td>
                                                                    <td class='text-center'>
                                                                        <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                    </td>
                                                                    <td class='text-center'>
                                                                        <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                    </td>
                                                                    <td class='text-center'>
                                                                        <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                    </td>
                                                                    <td class='text-center'>
                                                                        <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                    </td>
                                                                    <td class='text-center'>
                                                                        <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                    </td>
                                                                </tr>
                                                            <tr>
                                                                <td>Eversource  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Consumers Energy  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Columbia Gas  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>TXU Energy  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Puget Sound Energy  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Centerpoint Energy  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Dominion Virginia Power  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Xcel Energy  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>TECO Peoples Gas  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Atmos Energy Corp  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Southern California Gas  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>PECO  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>AEP  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Southwest Gas  </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Ameren Illinois </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked='checked' />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                                <td class='text-center'>
                                                                    <input type='checkbox' data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" checked="checked" />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane " id="tab_3">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    <div class="checkbox" style="margin: 0;">
                                                        <label>
                                                            <input type="checkbox">
                                                            What is the Website ?
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" style="width: 200px;" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    <div class="checkbox" style="margin: 0;">
                                                        <label>
                                                            <input type="checkbox">
                                                            What is the Paypal Account ?
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" style="width: 200px;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane " id="tab_4">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    Who is the Person to contact?

                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" style="width: 200px;" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    What is the Phone Number?
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" style="width: 200px;" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    What is the Email Address?
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" style="width: 200px;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <%--</div>--%>
    </div>
    <div class="modal fade" id="mdlADD">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add Utility Status</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Status</label>
                        <input type="text" class="form-control" placeholder="" id="txtSTATUS">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" rows="3" placeholder="" id="txtDESCRIPTION"></textarea>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="chkGAS">
                                    Gas
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="chkELECTRICITY">
                                    Electricity
                                </label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnADDUTILITY">Submit</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script type="text/javascript">
        var newcount, newdata;
        $(document).ready(function () {
            $('.toggle').bootstrapToggle('destroy');
            $('.toggle').bootstrapToggle({
                on: 'Active',
                off: 'Inactive'
            });
        });

        var asc = 'asc';
        tbody = $("#tblWF").find('tbody');

        tbody.find('tr').sort(function (a, b) {
            if (asc) {
                return $('td:first', a).text().localeCompare($('td:first', b).text());
            } else {
                return $('td:first', b).text().localeCompare($('td:first', a).text());
            }
        }).appendTo(tbody);
        $("#tblEXCLUDEDSTATES").on("keyup", ".target", function () {

            $(this).closest("tr").find("td label").html($(this).val());
        });

        $(".tblPRIORITY tbody ").sortable({
            items: "tr",
            update: function (event, ui) {

                var counter = 1;
                $.each($(this).find("tr"), function () {

                    $(this).find("td:first").html(counter);
                    counter++;
                });
            }
        });
        $(".tblPRIORITY tbody").disableSelection();
        $(".lnkADDNEW").on("click", function () {
            var count = $(this).closest(".panel-body").find(".tblPRIORITY tbody tr").length + 1;
            newcount = count;
            $(this).closest(".panel-body").find(".tblPRIORITY tbody").append("<tr class='trNEW'><td>" + count + "</td><td><input type='text' class='txtNEW'></input ></td><td style='border-style:none;'></td> <td style='border-style:none;display:none;' class='btnFOREDITING'><button type='button' class='btn btn-btn btn-success' title='Edit'><i class='fa fa-edit'></i></button><button type='button' class='btn btn-btn btn-danger' title='Remove'><i class='fa fa-remove'></i></button></td></tr>");
            $(this).closest(".panel-body").find(".divADD").hide();
            $(this).closest(".panel-body").find(".divSAVE").show();
        });
        $(".lnkCANCEL").on("click", function () {
            $(this).closest(".panel-body").find(".divSAVE").hide();
            $(this).closest(".panel-body").find(".divADD").show();
            $(this).closest(".panel-body").find(".trNEW").remove();
        });
        $(".lnkSAVE").on("click", function () {
            $(this).closest(".panel-body").find(".divSAVE").hide();
            $(this).closest(".panel-body").find(".divADD").show();
            newdata = $.trim($(this).closest(".panel-body").find(".txtNEW").val());
            $(this).closest(".panel-body").find(".tblPRIORITY tbody .trNEW").remove();
            $(this).closest(".panel-body").find(".tblPRIORITY tbody").append("<tr><td>" + newcount + "</td><td>" + newdata + "</td><td style='border-style: none;display:none' class='btnFOREDITING'> <button type='button' class='btn btn-btn btn-success' title='Edit'><i class='fa fa-edit'></i></button> <button type='button' class='btn btn-btn btn-danger' title='Remove'><i class='fa fa-remove'></i></button> </td></tr>");
           // $("#tblWF thead tr").append("<th>" + newdata + "</th>");
            //$("#tblWF tbody tr").append("<td class='text-center'> <input type='checkbox' data-toggle='toggle' data-style='ios' class='chkBoxActive toggle' checked='checked'>  </td>");
           
        });
        $(".divADD").on("click", ".modify", function () {
            $(this).html('<i class="fa fa-remove"></i>&nbsp;Cancel').removeClass("modify").addClass("cancel");

            $(".btnFOREDITING").show();

        });
        $(".divADD").on("click", ".cancel", function () {
            $(this).html('<i class="fa fa-edit"></i>&nbsp;Modify').removeClass("cancel").addClass("modify");

            $(".btnFOREDITING").hide();

        });
        $("#tblPRIORITY tbody").on("click", "tr .btn-danger", function () {

            var index = $("#tblPRIORITY tbody tr").index($(this).closest("tr")) + 1;
            $(this).closest("tr").remove();

            $("#tblWF thead tr th:eq(" + index + ")").remove();
            $.each($("#tblWF tbody tr"), function () {
                $(this).find("td:eq(" + index + ")").remove();
            });
            var counter = 1;
            $.each($("#tblPRIORITY tbody tr"), function () {
                $(this).find("td:first").html(counter);
                counter++;
            });
        });
    </script>

</asp:Content>

﻿$(document).ready(function () {
    //getPara();
    getRule();
   
    

    $('#tblParameter tbody').sortable().disableSelection();

    $('#Button4').click(function () {

        var txtParaName = document.getElementById('lblParaName').value;
        var txtLogicDesc = document.getElementById('lblParaDesc').value;

        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'InventoryControl.aspx/SaveTo',
            data: "{'ParaName':'" + txtParaName + "','LogicDesc':'" + txtLogicDesc + "'}",
            async: false,
            success: function (response) {
                $('#txtParaName').val('');
                $('#txtLogicDesc').val(''); 
                alert("Record Has been Saved in Database");
            },
            error: function () {
                console.log('there is some error');
                alert("BBBBBBBBB");
            }

        });
        return false;
    });

 
    


    
});
function getAllID() {
    var tab = document.getElementById('tblParameter');
    var l = tab.rows.length;

    var s = '';
    for (var i = 1; i < l; i++) {
        s = '';
        var tr = tab.rows[i];
        var cll = tr.cells[3];
        s += cll.innerText;
        setPrio(s, i);
    }
   // alert("Priority has been set!");
    
}
function setPrio(IDrule, preference) {

    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'InventoryControl.aspx/SetPrio',
        data: "{'IDrule':'" + IDrule + "','preference':'" + preference + "'}",
        async: false,
        success: function (response) { 
           
        },
        error: function () {
            console.log('there is some error'); 
        }

    }); 
}
 
function Change(Util) {
    getAllID();
    getRuleInfoDisplay(sessionStorage.getItem("getVal"));
}
 
function getRuleInfoDisplay(getVal) {

    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'InventoryControl.aspx/GetRuleInfoDisplay',
        data: "{'Client':'" + getVal + "'}", 
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblParameter tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblParameter tbody').append( 
                             '<tr>' +
                                 '<td>' + val.PriorityID +
                                 '<td>' + val.Pre_Name +
                                 '<td>' + val.Pre_Desc +
                                 '<td style="display: none;">' + val.ID_Pre +
                             '</tr>'
                    );
                });
            } 
        },

        error: function () { 
            console.log('there is some error');
        }
    });
     
} 
function getRuleInfo(getVal) {
   
 
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'InventoryControl.aspx/GetRuleInfo',
        data: "{'Client':'" + getVal + "'}",
        async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

              

                $.each(records, function (idx, val) {
                    document.getElementById('txtdisTitle').value = val.rule_Name;
                    
                    document.getElementById('txtdisDesc').value = val.rule_desc;
                    document.getElementById('txtdisSched').value = val.rule_sched;
                    document.getElementById('txtdisPer').value = val.Period_WDM;
                    document.getElementById('txtdisHM').value = val.Period + " " + val.Time ; 
                    document.getElementById('txtdisStart').value = val.Start_Run_Rule; 
                    document.getElementById('txtdisStop').value = val.Stop_Run_Rule; 
                });
            }

             
        },

        error: function () {
           
            console.log('there is some error');
        }
    });

}
 
function getRule() {

    $.ajax({
        type: 'POST',
        url: 'InventoryControl.aspx/GetRule', 
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblRules tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblRules tbody').append(
                         
                             '<tr onclick="ChangeUtil(this)">' + 
                                 '<td>' + val.rule_Name +
                                 '<td>' + val.Start_Run_Rule +
                                 '<td>' + val.Time +
                             '</tr>'
                      
                    );
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}


function ChangeUtil(Util) {

    var getVal = $(Util).closest("tr").find("td:lt(1)").text();
    sessionStorage.setItem("getVal", getVal);
    getRuleInfo(getVal);
    getRuleInfoDisplay(getVal);
    document.getElementById('txtValName').value = getVal;
  
    if ($("#divMAINPANEL:visible").length == 0) {
        $("#divMAINPANEL").show();
    } 
    document.getElementById('divMAINPANEL2').setAttribute("style", "display:none");
    $(Util).closest("table").find("tr").removeClass("selected"); 
    $(Util).addClass("selected");
    //document.location = "?facets='" + getVal;
 
}
function ChangeUtil2(Util2) {
     
    if ($("#divMAINPANEL2:visible").length == 0) {
        $("#divMAINPANEL2").show();
       
    } 
    document.getElementById('divMAINPANEL').setAttribute("style", "display:none");

    if(document.getElementById('txtTitle').value == "" || document.getElementById('txtDesc').value == "")
    {
        document.getElementById('txtTitle').value = "";
        document.getElementById('txtDesc').value = "";
    }
    else
    {
        alert("Discard changes?");
    }
    document.getElementById('txtTitle').value = "";
    document.getElementById('txtDesc').value = "";
}

function ChangeUtil3(Util2) {

    if ($("#divMAINPANEL2:visible").length == 0) {
        $("#divMAINPANEL2").show(); 
    }
    document.getElementById('divMAINPANEL').setAttribute("style", "display:none");

    document.getElementById('txtTitle').value = document.getElementById('txtdisTitle').value;
    document.getElementById('txtDesc').value = document.getElementById('txtdisDesc').value;
 
}

﻿$(document).ready(function () {

    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
   
   
    $('#tblStartPRIO tbody').sortable().disableSelection();
    $('#tblStopPRIO tbody').sortable().disableSelection();
    $('#tblInvoicePRIO tbody').sortable().disableSelection();
    $('#tblBillsPRIO tbody').sortable().disableSelection();
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    ClientStatus();
    $('#modalLoading').modal('hide');
    //$(document).on('click', ".table .chk1", function (e) {
    //    e.preventDefault();
    //    status = $(this).attr('value')
    //    UpdateStatus(status);
    //    sessionStorage.setItem('Status', status);
    //});
    //$("#chkStart").click(function () {
    //    alert("checked");
    //});

   
    




});

//(function ($) {
//    $(document).ready(function () {
        
//    });
//})(jQuery);




function UpdateStatus(editVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"editVal" : "' + editVal + '"}',
        url: 'USPCapabilities.aspx/UpdateStatus',
        dataType: "json",
        success: function (data) {
            //ClientStatus();
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function UpdateStatusPFC(editVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"editVal" : "' + editVal + '"}',
        url: 'USPCapabilities.aspx/UpdateStatusPFC',
        dataType: "json",
        success: function (data) {
            //ClientStatus();
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function getRuleInfoDisplay() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetPrioStart',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                $('#tblStartPRIO').bootstrapTable('destroy');
                var records = d.data.asd;
                $('#tblStartPRIO').bootstrapTable({
                    data: records
                });
            }
            $('#tblStartPRIO tbody').sortable().disableSelection();
            $('.multiselect-ui').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 0,
                maxWidth: 200
            });
            fnUtility();
           
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function getRuleStop() {

    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetPrioStop',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                $('#tblStopPRIO').bootstrapTable('destroy');
                var records = d.data.asd;
                $('#tblStopPRIO').bootstrapTable({
                    data: records
                });
            }
            $('#tblStopPRIO tbody').sortable().disableSelection();
            $('.multiselect-ui').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 0,
                maxWidth: 200
            });
            fnUtilityStop();
          
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function getRuleInvoice() {

    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetPrioInvoice',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                $('#tblInvoicePRIO').bootstrapTable('destroy');
                var records = d.data.asd;
                $('#tblInvoicePRIO').bootstrapTable({
                    data: records
                });
            }
            $('#tblInvoicePRIO tbody').sortable().disableSelection();
            $('.multiselect-ui').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 0,
                maxWidth: 200
            });
            fnUtilityInvoice();
           
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function getRuleBills() {
    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetPrioBills',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                $('#tblBillsPRIO').bootstrapTable('destroy');
                var records = d.data.asd;
                $('#tblBillsPRIO').bootstrapTable({
                    data: records
                });
            }
            $('#tblBillsPRIO tbody').sortable().disableSelection();
            $('.multiselect-ui').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 0,
                maxWidth: 200
            });
            fnUtilityBills();
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function selected(val, idx) {
    $.ajax({
        success: function (data) {
            var arr = [];

            $.each($('#ddl:eq() option'), function (idx, val) {
                if ($.inArray($(this).text(), arr) > -1) {
                    $(this).prop('checked', true);
                }
            });
        }
    });
}
function fnUtility() {
    var arr = [];
    var arr2 = [];
    var arr3 = [];
    var arr4 = [];
    var PrioID = [];
    var PrioID2 = [];
    var PrioID3 = [];
    var PrioID4 = [];
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'USPCapabilities.aspx/GetUtilityStatusDDl',
        dataType: "json",
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#Prio1,#Prio2,#Prio3,#Prio4').empty();
                $('#Prio1,#Prio2,#Prio3,#Prio4').multiselect('destroy');
                var records = d.data.asd;
                var records1 = d.data.asd1;
                $.each(records1, function (idx, val) {
                    if(val.Prio_ID == 1)
                    {
                        arr.push(val.Util_Name);
                        PrioID.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 2)
                    {
                        arr2.push(val.Util_Name);
                        PrioID2.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 3) {
                        arr3.push(val.Util_Name);
                        PrioID3.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 4) {
                        arr4.push(val.Util_Name);
                        PrioID4.push(val.Prio_ID);
                    }
                    else
                    {
                       
                    }
                   
                });
                $.each(records, function (idx, val) {
                    var selected = 'selected="selected"';
                    if ($.inArray(1, PrioID) !== -1)
                    {
                        if ($.inArray(val.Start_Desc, arr) !== -1) {
                            ta2 = "<option value='" + val.Start_Desc + "' data-id='1'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta2 = "<option value='" + val.Start_Desc + "'data-id='1' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#Prio1').append(ta2);
                    }
                    else {
                        ta2 = "<option value='" + val.Start_Desc + "'data-id='1' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#Prio1').append(ta2);
                    }

                    if ($.inArray(2, PrioID2) !== -1)
                    {
                        if ($.inArray(val.Start_Desc, arr2) !== -1) {
                            ta3 = "<option value='" + val.Start_Desc + "' data-id='2'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta3 = "<option value='" + val.Start_Desc + "'data-id='2' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#Prio2').append(ta3);
                    }
                    else {
                        ta3 = "<option value='" + val.Start_Desc + "'data-id='2' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#Prio2').append(ta3);
                    }


                    if ($.inArray(3, PrioID3) !== -1) {
                        if ($.inArray(val.Start_Desc, arr3) !== -1) {
                            ta4 = "<option value='" + val.Start_Desc + "' data-id='3'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta4 = "<option value='" + val.Start_Desc + "'data-id='3' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#Prio3').append(ta4);
                    }
                    else {
                        ta4 = "<option value='" + val.Start_Desc + "'data-id='3' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#Prio3').append(ta4);
                    }

                    if ($.inArray(4, PrioID4) !== -1) {
                        if ($.inArray(val.Start_Desc, arr4) !== -1) {
                            ta5 = "<option value='" + val.Start_Desc + "' data-id='4'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta5 = "<option value='" + val.Start_Desc + "'data-id='4' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#Prio4').append(ta5);
                    }
                    else
                    {
                        ta5 = "<option value='" + val.Start_Desc + "'data-id='4' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#Prio4').append(ta5);
                    }
                   
                  
                });
                $('#Prio1,#Prio2,#Prio3,#Prio4').multiselect({
                    numberDisplayed: 0,
                    includeSelectAllOption: true,
                    maxHeight: 200
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}




function fnUtilityStop() {
    var arr = [];
    var arr2 = [];
    var arr3 = [];
    var arr4 = [];
    var PrioID = [];
    var PrioID2 = [];
    var PrioID3 = [];
    var PrioID4 = [];
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'USPCapabilities.aspx/GetUtilityStatusDDlStop',
        dataType: "json",
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#StopPrio1,#StopPrio2,#StopPrio3,#StopPrio4').empty();
                $('#StopPrio1,#StopPrio2,#StopPrio3,#StopPrio4').multiselect('destroy');
                var records = d.data.asd;
                var records1 = d.data.asd1;
                $.each(records1, function (idx, val) {
                    if (val.Prio_ID == 1) {
                        arr.push(val.Util_Name);
                        PrioID.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 2) {
                        arr2.push(val.Util_Name);
                        PrioID2.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 3) {
                        arr3.push(val.Util_Name);
                        PrioID3.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 4) {
                        arr4.push(val.Util_Name);
                        PrioID4.push(val.Prio_ID);
                    }
                    else {

                    }

                });
                $.each(records, function (idx, val) {
                    var selected = 'selected="selected"';
                    if ($.inArray(1, PrioID) !== -1) {
                        if ($.inArray(val.Start_Desc, arr) !== -1) {
                            ta2 = "<option value='" + val.Start_Desc + "' data-id='1'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta2 = "<option value='" + val.Start_Desc + "'data-id='1' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#StopPrio1').append(ta2);
                    }
                    else {
                        ta2 = "<option value='" + val.Start_Desc + "'data-id='1' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#StopPrio1').append(ta2);
                    }

                    if ($.inArray(2, PrioID2) !== -1) {
                        if ($.inArray(val.Start_Desc.toString(), arr2) !== -1) {
                            ta3 = "<option value='" + val.Start_Desc + "' data-id='2'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta3 = "<option value='" + val.Start_Desc + "'data-id='2' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#StopPrio2').append(ta3);
                    }
                    else {
                        ta3 = "<option value='" + val.Start_Desc + "'data-id='2' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#StopPrio2').append(ta3);
                    }


                    if ($.inArray(3, PrioID3) !== -1) {
                        if ($.inArray(val.Start_Desc, arr3) !== -1) {
                            ta4 = "<option value='" + val.Start_Desc + "' data-id='3'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta4 = "<option value='" + val.Start_Desc + "'data-id='3' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#StopPrio3').append(ta4);
                    }
                    else {
                        ta4 = "<option value='" + val.Start_Desc + "'data-id='3' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#StopPrio3').append(ta4);
                    }

                    if ($.inArray(4, PrioID4) !== -1) {
                        if ($.inArray(val.Start_Desc, arr4) !== -1) {
                            ta5 = "<option value='" + val.Start_Desc + "' data-id='4'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta5 = "<option value='" + val.Start_Desc + "'data-id='4' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#StopPrio4').append(ta5);
                    }
                    else {
                        ta5 = "<option value='" + val.Start_Desc + "'data-id='4' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#StopPrio4').append(ta5);
                    }


                });
                $('#StopPrio1,#StopPrio2,#StopPrio3,#StopPrio4').multiselect({
                    numberDisplayed: 0,
                    includeSelectAllOption: true,
                    maxHeight: 200
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}
function fnUtilityInvoice() {
    var arr = [];
    var arr2 = [];
    var arr3 = [];
    var arr4 = [];
    var arr5 = [];
    var arr6 = [];
    var PrioID = [];
    var PrioID2 = [];
    var PrioID3 = [];
    var PrioID4 = [];
    var PrioID5 = [];
    var PrioID6 = [];
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'USPCapabilities.aspx/GetUtilityStatusDDlInvoice',
        dataType: "json",
        success: function (data) {
            
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#InvoicePrio1,#InvoicePrio2,#InvoicePrio3,#InvoicePrio4,#InvoicePrio5,#InvoicePrio6').empty();
                $('#InvoicePrio1,#InvoicePrio2,#InvoicePrio3,#InvoicePrio4,#InvoicePrio5,#InvoicePrio6').multiselect('destroy');
                var records = d.data.asd;
                var records1 = d.data.asd1;
                $.each(records1, function (idx, val) {
                    if (val.Prio_ID == 1) {
                        arr.push(val.Util_Name);
                        PrioID.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 2) {
                        arr2.push(val.Util_Name);
                        PrioID2.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 3) {
                        arr3.push(val.Util_Name);
                        PrioID3.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 4) {
                        arr4.push(val.Util_Name);
                        PrioID4.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 5) {
                        arr5.push(val.Util_Name);
                        PrioID5.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 6) {
                        arr6.push(val.Util_Name);
                        PrioID6.push(val.Prio_ID);
                    }
                    else {

                    }
                });
                $.each(records, function (idx, val) {
                    var selected = 'selected="selected"';
                    if ($.inArray(1, PrioID) !== -1) {
                        if ($.inArray(val.Start_Desc, arr) !== -1) {
                            ta2 = "<option value='" + val.Start_Desc + "' data-id='1'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta2 = "<option value='" + val.Start_Desc + "'data-id='1' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#InvoicePrio1').append(ta2);
                    }
                    else {
                        ta2 = "<option value='" + val.Start_Desc + "'data-id='1' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#InvoicePrio1').append(ta2);
                    }

                    if ($.inArray(2, PrioID2) !== -1) {
                        if ($.inArray(val.Start_Desc, arr2) !== -1) {
                            ta3 = "<option value='" + val.Start_Desc + "' data-id='2'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta3 = "<option value='" + val.Start_Desc + "'data-id='2' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#InvoicePrio2').append(ta3);
                    }
                    else {
                        ta3 = "<option value='" + val.Start_Desc + "'data-id='2' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#InvoicePrio2').append(ta3);
                    }


                    if ($.inArray(3, PrioID3) !== -1) {
                        if ($.inArray(val.Start_Desc, arr3) !== -1) {
                            ta4 = "<option value='" + val.Start_Desc + "' data-id='3'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta4 = "<option value='" + val.Start_Desc + "'data-id='3' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#InvoicePrio3').append(ta4);
                    }
                    else {
                        ta4 = "<option value='" + val.Start_Desc + "'data-id='3' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#InvoicePrio3').append(ta4);
                    }

                    if ($.inArray(4, PrioID4) !== -1) {
                        if ($.inArray(val.Start_Desc, arr4) !== -1) {
                            ta5 = "<option value='" + val.Start_Desc + "' data-id='4'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta5 = "<option value='" + val.Start_Desc + "'data-id='4' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#InvoicePrio4').append(ta5);
                    }
                    else {
                        ta5 = "<option value='" + val.Start_Desc + "'data-id='4' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#InvoicePrio4').append(ta5);
                    }

                    if ($.inArray(5, PrioID5) !== -1) {
                        if ($.inArray(val.Start_Desc, arr5) !== -1) {
                            ta6 = "<option value='" + val.Start_Desc + "' data-id='5'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta6 = "<option value='" + val.Start_Desc + "'data-id='5' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#InvoicePrio5').append(ta6);
                    }
                    else {
                        ta6 = "<option value='" + val.Start_Desc + "'data-id='5' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#InvoicePrio5').append(ta6);
                    }

                    if ($.inArray(6, PrioID5) !== -1) {
                        if ($.inArray(val.Start_Desc, arr6) !== -1) {
                            ta7 = "<option value='" + val.Start_Desc + "' data-id='6'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta7 = "<option value='" + val.Start_Desc + "'data-id='6' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#InvoicePrio6').append(ta7);
                    }
                    else {
                        ta7 = "<option value='" + val.Start_Desc + "'data-id='6' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#InvoicePrio6').append(ta7);
                    }


                });
                $('#InvoicePrio1,#InvoicePrio2,#InvoicePrio3,#InvoicePrio4,#InvoicePrio5,#InvoicePrio6').multiselect({
                    numberDisplayed: 0,
                    includeSelectAllOption: true,
                    maxHeight: 200
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}
function fnUtilityBills() {
    var arr = [];
    var arr2 = [];
    var arr3 = [];
    var arr4 = [];
    var PrioID = [];
    var PrioID2 = [];
    var PrioID3 = [];
    var PrioID4 = [];
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'USPCapabilities.aspx/GetUtilityStatusDDlBills',
        dataType: "json",
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#BillsPrio1,#BillsPrio2,#BillsPrio3,#BillsPrio4').empty();
                $('#BillsPrio1,#BillsPrio2,#BillsPrio3,#BillsPrio4').multiselect('destroy');
                var records = d.data.asd;
                var records1 = d.data.asd1;
                $.each(records1, function (idx, val) {
                    if (val.Prio_ID == 1) {
                        arr.push(val.Util_Name);
                        PrioID.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 2) {
                        arr2.push(val.Util_Name);
                        PrioID2.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 3) {
                        arr3.push(val.Util_Name);
                        PrioID3.push(val.Prio_ID);
                    }
                    else {

                    }

                    if (val.Prio_ID == 4) {
                        arr4.push(val.Util_Name);
                        PrioID4.push(val.Prio_ID);
                    }
                    else {

                    }

                });
                $.each(records, function (idx, val) {
                    var selected = 'selected="selected"';
                    if ($.inArray(1, PrioID) !== -1) {
                        if ($.inArray(val.Start_Desc, arr) !== -1) {
                            ta2 = "<option value='" + val.Start_Desc + "' data-id='1'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta2 = "<option value='" + val.Start_Desc + "'data-id='1' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#BillsPrio1').append(ta2);
                    }
                    else {
                        ta2 = "<option value='" + val.Start_Desc + "'data-id='1' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#BillsPrio1').append(ta2);
                    }

                    if ($.inArray(2, PrioID2) !== -1) {
                        if ($.inArray(val.Start_Desc, arr2) !== -1) {
                            ta3 = "<option value='" + val.Start_Desc + "' data-id='2'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta3 = "<option value='" + val.Start_Desc + "'data-id='2' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#BillsPrio2').append(ta3);
                    }
                    else {
                        ta3 = "<option value='" + val.Start_Desc + "'data-id='2' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#BillsPrio2').append(ta3);
                    }


                    if ($.inArray(3, PrioID3) !== -1) {
                        if ($.inArray(val.Start_Desc, arr3) !== -1) {
                            ta4 = "<option value='" + val.Start_Desc + "' data-id='3'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta4 = "<option value='" + val.Start_Desc + "'data-id='3' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#BillsPrio3').append(ta4);
                    }
                    else {
                        ta4 = "<option value='" + val.Start_Desc + "'data-id='3' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#BillsPrio3').append(ta4);
                    }

                    if ($.inArray(4, PrioID4) !== -1) {
                        if ($.inArray(val.Start_Desc, arr4) !== -1) {
                            ta5 = "<option value='" + val.Start_Desc + "' data-id='4'" + selected + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        else {
                            ta5 = "<option value='" + val.Start_Desc + "'data-id='4' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        }
                        $('#BillsPrio4').append(ta5);
                    }
                    else {
                        ta5 = "<option value='" + val.Start_Desc + "'data-id='4' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + val.Start_Desc + "</option>";
                        $('#BillsPrio4').append(ta5);
                    }


                });
                $('#BillsPrio1,#BillsPrio2,#BillsPrio3,#BillsPrio4').multiselect({
                    numberDisplayed: 0,
                    includeSelectAllOption: true,
                    maxHeight: 200
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}
function ddlPrio(value)
{
    return '<select id="Prio'+ value +'"  class="multiselect-ui" multiple="multiple" ></select>' 
}
function ddlPrioStop(value) {
    return '<select id="StopPrio' + value + '"  class="multiselect-ui" multiple="multiple" ></select>'
}
function ddlPrioInvoice(value) {
    return '<select id="InvoicePrio' + value + '"  class="multiselect-ui" multiple="multiple" ></select>'
}
function ddlPrioBills(value) {
    return '<select id="BillsPrio' + value + '"  class="multiselect-ui" multiple="multiple" ></select>'
}
function chkStart(value, row) {
    if(value == 'Yes')
    {
        return '<input type="checkbox" onclick="asd($(this))" id="chkStart"  checked="checked" value="No_Start,' + row.Val_ID + '" class="chk1 iCheck" data-id="11" />'
    }
    else
    {
        return '<input type="checkbox" onclick="asd($(this))"  id="chkStart" value="Yes_Start,' + row.Val_ID + '" class="chk1 iCheck" data-id="11" />'
    }
}
function chkStartPFC(value, row) {
    if (value == 'Yes') {
        return '<input type="checkbox" onclick="asdPFC($(this))" id="chkStartPFC"  checked="checked" value="No_Start,' + row.Val_ID + '" class="chk1 iCheck" data-id="11" />'
    }
    else {
        return '<input type="checkbox" onclick="asdPFC($(this))"  id="chkStartPFC" value="Yes_Start,' + row.Val_ID + '" class="chk1 iCheck" data-id="11" />'
    }
}



function asdPFC(val) {
    UpdateStatusPFC($(val).attr('value'));
}


function asd(val)
{
    UpdateStatus($(val).attr('value'));
}
function chkStop(value, row) {
    if (value == 'Yes') {
        return '<input type="checkbox" id="chkStop" onclick="asd($(this))" value="No_Stop,' + row.Val_ID + '" checked="checked" class="chk1 iCheck" data-id="11" />'
    }
    else {
        return '<input type="checkbox" id="chkStop" onclick="asd($(this))" value="Yes_Stop,' + row.Val_ID + '"  class="chk1 iCheck" data-id="11" />'
    }
}
function chkStopPFC(value, row) {
    if (value == 'Yes') {
        return '<input type="checkbox" id="chkStopPFC" onclick="asdPFC($(this))" value="No_Stop,' + row.Val_ID + '" checked="checked" class="chk1 iCheck" data-id="11" />'
    }
    else {
        return '<input type="checkbox" id="chkStopPFC" onclick="asdPFC($(this))" value="Yes_Stop,' + row.Val_ID + '"  class="chk1 iCheck" data-id="11" />'
    }
}
function chkStatus(value, row) {
    if (value == 'Yes') {
        return '<input type="checkbox" id="chkStatus" onclick="chkUpdateStatus($(this))" value="No,' + row.id + '" checked="checked" class="chk1 iCheck"  />'
    }
    else {
        return '<input type="checkbox" id="chkStatus" onclick="chkUpdateStatus($(this))" value="Yes,' + row.id + '"  class="chk1 iCheck" />'
    }
}
function chkStatusStop(value, row) {
    if (value == 'Yes') {
        return '<input type="checkbox" id="chkStatus" onclick="chkUpdateStatusStop($(this))" value="No,' + row.id + '" checked="checked" class="chk1 iCheck"  />'
    }
    else {
        return '<input type="checkbox" id="chkStatus" onclick="chkUpdateStatusStop($(this))" value="Yes,' + row.id + '"  class="chk1 iCheck" />'
    }
}
function chkTask(value, row) {
    if (value == 'Yes') {
        return '<input type="checkbox" id="chk1Task" onclick="chkUpdateTask($(this))" value="No,' + row.id + '" checked="checked" class="chk1 iCheck"  />'
    }
    else {
        return '<input type="checkbox" id="chk1Task" onclick="chkUpdateTask($(this))" value="Yes,' + row.id + '"  class="chk1 iCheck" />'
    }
}
function chkUtility(value, row) {
    if (value == 'Yes') {
        return '<input type="checkbox" id="chk1Utility" onclick="chkUpdateUtility($(this))" value="No,' + row.id + '" checked="checked" class="chk1 iCheck"  />'
    }
    else {
        return '<input type="checkbox" id="chk1Utility" onclick="chkUpdateUtility($(this))" value="Yes,' + row.id + '"  class="chk1 iCheck" />'
    }
}

                '<a class="btn btn-app">'
                '<i class="fa fa-save"></i> Save'
                '</a>'


function btnSave(value, row) {
    //return '<a class="btn btn-app" type="button" ' + 'value="' + value + ' onclick="btnSavePrio($(this))">' +
    //' <i class="fa fa-save"></i> Save'+
    //' </a>'

    return '<button type="button" value="' + value
        + '" class="btn btn-primary btn-xs"  onclick="btnSavePrio($(this))"><i class="fa fa-save"></i> SAVE</button>'
}
function btnSaveStop(value, row) {
    return '<button type="button" value="' + value
        + '" class="btn btn-primary btn-xs"  onclick="btnSavePrioStop($(this))"><span class="fa fa-save"></span> SAVE</button>'
}
function btnSaveInvoice(value, row) {
    return '<button type="button" value="' + value
        + '" class="btn btn-primary btn-xs"  onclick="btnSavePrioInvoice($(this))"><span class="fa fa-save"></span> SAVE</button>'
}
function btnSaveBills(value, row) {
    return '<button type="button" value="' + value
        + '" class="btn btn-primary btn-xs"  onclick="btnSavePrioBills($(this))"><span class="fa fa-save"></span> SAVE</button>'
}
function btnSavePrio(val, row) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var select = "";
    var itemSelec = "";
    var listLength = "";
    var ID = "";
    if ($(val).attr('value') == 1) {
        select = document.getElementById("Prio1");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();

    }
    else if ($(val).attr('value') == 2) {
        select = document.getElementById("Prio2");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    else if ($(val).attr('value') == 3) {
        select = document.getElementById("Prio3");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    else if ($(val).attr('value') == 4) {
        select = document.getElementById("Prio4");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"PrioID" : "' + ID +
            '","itemSelec" : "' + itemSelec + '"}',
        url: 'USPCapabilities.aspx/InsertUtilityStatus',
        dataType: "json",
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function btnSavePrioStop(val, row) {
    var select = "";
    var itemSelec = "";
    var listLength = "";
    var ID = "";
    if ($(val).attr('value') == 1) {
        select = document.getElementById("StopPrio1");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();

    }
    else if ($(val).attr('value') == 2) {
        select = document.getElementById("StopPrio2");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    else if ($(val).attr('value') == 3) {
        select = document.getElementById("StopPrio3");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    else if ($(val).attr('value') == 4) {
        select = document.getElementById("StopPrio4");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"PrioID" : "' + ID +
            '","itemSelec" : "' + itemSelec + '"}',
        url: 'USPCapabilities.aspx/InsertUtilityStatusStop',
        dataType: "json",
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function btnSavePrioInvoice(val, row) {
    var select = "";
    var itemSelec = "";
    var listLength = "";
    var ID = "";
    if ($(val).attr('value') == 1) {
        select = document.getElementById("InvoicePrio1");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();

    }
    else if ($(val).attr('value') == 2) {
        select = document.getElementById("InvoicePrio2");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    else if ($(val).attr('value') == 3) {
        select = document.getElementById("InvoicePrio3");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    else if ($(val).attr('value') == 4) {
        select = document.getElementById("InvoicePrio4");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"PrioID" : "' + ID +
            '","itemSelec" : "' + itemSelec + '"}',
        url: 'USPCapabilities.aspx/InsertUtilityStatusInvoice',
        dataType: "json",
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function btnSavePrioBills(val, row) {
    var select = "";
    var itemSelec = "";
    var listLength = "";
    var ID = "";
    if ($(val).attr('value') == 1) {
        select = document.getElementById("BillsPrio1");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();

    }
    else if ($(val).attr('value') == 2) {
        select = document.getElementById("BillsPrio2");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    else if ($(val).attr('value') == 3) {
        select = document.getElementById("BillsPrio3");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    else if ($(val).attr('value') == 4) {
        select = document.getElementById("BillsPrio4");
        itemSelec = "";
        listLength = select.options.length;
        for (var i = 0; i < listLength; i++) {
            if (select.options[i].selected)
                if (itemSelec == "") {
                    itemSelec = select.options[i].value;
                }
                else {
                    itemSelec = itemSelec + ',' + select.options[i].value;
                }
        }
        ID = $(val).attr('value').toString();
    }
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"PrioID" : "' + ID +
            '","itemSelec" : "' + itemSelec + '"}',
        url: 'USPCapabilities.aspx/InsertUtilityStatusBills',
        dataType: "json",
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function chkUpdateStatusStop(val, row) {
    UpdateStopStatus($(val).attr('value'));
}
function chkUpdateStatus(val, row) {
    UpdateStartStatus($(val).attr('value'));
}
function chkUpdateUtility(val, row) {
    UpdateUtilityType($(val).attr('value'));
}
function chkUpdateTask(val, row) {

    UpdateTask($(val).attr('value'));
}
function UpdateTask(editVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"editVal" : "' + editVal + '"}',
        url: 'USPCapabilities.aspx/UpdateTask',
        dataType: "json",
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function UpdateUtilityType(editVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"editVal" : "' + editVal + '"}',
        url: 'USPCapabilities.aspx/UpdateUtilityType',
        dataType: "json",
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function UpdateStartStatus(editVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"editVal" : "' + editVal + '"}',
        url: 'USPCapabilities.aspx/UpdateStartStatus',
        dataType: "json",
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function UpdateStopStatus(editVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"editVal" : "' + editVal + '"}',
        url: 'USPCapabilities.aspx/UpdateStopStatus',
        dataType: "json",
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function ClientStatus()
{
    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetClientStatus',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                $('#tblREO').bootstrapTable('destroy');
                $('#tblResi').bootstrapTable('destroy');
                $('#tblpfc').bootstrapTable('destroy');
                var records = d.data.asd;
                var records2 = d.data.asd2;
                var records3 = d.data.asd3;
                $('#tblREO').bootstrapTable({
                    data: records,
                    width: 1000,
                    pagination: true
                });
                $('#tblResi').bootstrapTable({
                    data: records2,
                    width: 1000,
                    pagination: true
                });
                $('#tblpfc').bootstrapTable({
                    data: records3,
                    width: 1000,
                    pagination: true
                });
            }
       
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
    $('#modalLoading').modal('hide');
}
function Status() {
    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetStartStatus',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                
                $('#tblUtilityType').bootstrapTable('destroy');
                var records = d.data.asd;
                $('#tblUtilityType').bootstrapTable({
                    data: records,
                    width: 1000
                });
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
    $('#modalLoading').modal('hide');
}
function UtilityType() {
    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetUtilityType',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#tblUtilityTypeStatus').bootstrapTable('destroy');
                var records = d.data.asd;
                $('#tblUtilityTypeStatus').bootstrapTable({
                    data: records,
                    width: 1000
                });
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
    $('#modalLoading').modal('hide');
}
function Task() {
    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetTask',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#tblTask').bootstrapTable('destroy');
                var records = d.data.asd;
                $('#tblTask').bootstrapTable({
                    data: records,
                    width: 1000
                });
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
    $('#modalLoading').modal('hide');
}
function StopStatus() {
    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetStopStatus',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                $('#tblUtilityTypeStop').bootstrapTable('destroy');
                var records = d.data.asd;
                $('#tblUtilityTypeStop').bootstrapTable({
                    data: records,
                    width: 1000
                });

            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
    $('#modalLoading').modal('hide');
}



function getAllID() {

    var tab = "";
    if (sessionStorage.getItem('prio') == "Start")
    {
        tab = document.getElementById('tblStartPRIO');

    }
    else if (sessionStorage.getItem('prio') == "Stop")
    {
        tab = document.getElementById('tblStopPRIO');
    }
    else if (sessionStorage.getItem('prio') == "Invoice")
    {
        tab = document.getElementById('tblInvoicePRIO');
    }
    else if (sessionStorage.getItem('prio') == "Bills")
    {
        tab = document.getElementById('tblBillsPRIO');
    }

    var l = tab.rows.length;
    var s = '';
    for (var i = 1; i < l; i++) {
        s = '';
        var tr = tab.rows[i];
        var cll = tr.cells[1];
        s += cll.innerText;
        setPrio(s, i);
    }
    if (sessionStorage.getItem('prio') == "Start") {
        getRuleInfoDisplay();


    }
    else if (sessionStorage.getItem('prio') == "Stop") {
        getRuleStop();

    }
    else if (sessionStorage.getItem('prio') == "Invoice") {
        getRuleInvoice();
    }
    else if (sessionStorage.getItem('prio') == "Bills") {
        getRuleBills();
    }

   
}
function setPrio(Name, preference) {

    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'USPCapabilities.aspx/SetPrio',
        data: "{'Name':'" + Name + "','preference':'" + preference +
            "','PRIO':'" + sessionStorage.getItem('prio') + "'}",
        async: false,
        success: function (response) {
        },
        error: function () {
            console.log('there is some error');
        }
    });
}
// Last
function Change(Util) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    sessionStorage.setItem('prio', 'Start');
    getAllID();
    $('#modalLoading').modal('hide');
    // getRuleInfoDisplay(sessionStorage.getItem("getVal"));
}
function Setprio2(Util) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    sessionStorage.setItem('prio','Stop');
    getAllID();
    $('#modalLoading').modal('hide');
    // getRuleInfoDisplay(sessionStorage.getItem("getVal"));
}
function Setprio3(Util) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    sessionStorage.setItem('prio', 'Invoice');
    getAllID();
    $('#modalLoading').modal('hide');
    // getRuleInfoDisplay(sessionStorage.getItem("getVal"));
}
function Setprio4(Util) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    sessionStorage.setItem('prio', 'Bills');
    getAllID();
    $('#modalLoading').modal('hide');
    // getRuleInfoDisplay(sessionStorage.getItem("getVal"));
}
var tablesToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
    , tmplWorkbookXML = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">'
      + '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>'
      + '<Styles>'
      + '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>'
      + '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>'
      + '</Styles>'
      + '{worksheets}</Workbook>'
    , tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>'
    , tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>'
    , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (tables, wsnames, wbname, appname) {
        var ctx = "";
        var workbookXML = "";
        var worksheetsXML = "";
        var rowsXML = "";

        for (var i = 0; i < tables.length; i++) {
            if (!tables[i].nodeType) tables[i] = document.getElementById(tables[i]);
            for (var j = 0; j < tables[i].rows.length; j++) {
                rowsXML += '<Row>'
                for (var k = 0; k < tables[i].rows[j].cells.length; k++) {
                    var dataType = tables[i].rows[j].cells[k].getAttribute("data-type");
                    var dataStyle = tables[i].rows[j].cells[k].getAttribute("data-style");
                    var dataValue = tables[i].rows[j].cells[k].getAttribute("data-value");
                    dataValue = (dataValue) ? dataValue : tables[i].rows[j].cells[k].innerHTML;
                    var dataFormula = tables[i].rows[j].cells[k].getAttribute("data-formula");
                    dataFormula = (dataFormula) ? dataFormula : (appname == 'Calc' && dataType == 'DateTime') ? dataValue : null;
                    ctx = {
                        attributeStyleID: (dataStyle == 'Currency' || dataStyle == 'Date') ? ' ss:StyleID="' + dataStyle + '"' : ''
                           , nameType: (dataType == 'Number' || dataType == 'DateTime' || dataType == 'Boolean' || dataType == 'Error') ? dataType : 'String'
                           , data: (dataFormula) ? '' : dataValue
                           , attributeFormula: (dataFormula) ? ' ss:Formula="' + dataFormula + '"' : ''
                    };
                    rowsXML += format(tmplCellXML, ctx);
                }
                rowsXML += '</Row>'
            }
            ctx = { rows: rowsXML, nameWS: wsnames[i] || 'Sheet' + i };
            worksheetsXML += format(tmplWorksheetXML, ctx);
            rowsXML = "";
        }

        ctx = { created: (new Date()).getTime(), worksheets: worksheetsXML };
        workbookXML = format(tmplWorkbookXML, ctx);

        var link = document.createElement("A");
        link.href = uri + base64(workbookXML);
        link.download = wbname || 'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
       
        document.body.removeChild(link);
    }
})();
var tableToExcelForIE = (function () {

    var uri = 'data:application/vnd.ms-excel;base64,'
      , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
      , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
      , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
        var blob = new Blob([format(template, ctx)]);
        var blobURL = window.URL.createObjectURL(blob);
        //IE
        if (isIE()) {
            csvData = table.innerHTML;
            if (window.navigator.msSaveBlob) {
                var blob = new Blob([format(template, ctx)], {
                    type: "text/html"
                });
                navigator.msSaveBlob(blob, '' + excelfileName + '.xls');

            }

        }
    }
})()
function isIE() {
    var isIE11 = navigator.userAgent.indexOf(".NET CLR") > -1;
    var isIE11orLess = isIE11 || navigator.appVersion.indexOf("MSIE") != -1;
    return isIE11orLess;
}
function getStart() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetStart',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
             //   $('#tblStartService').bootstrapTable('destroy');
                var records = d.data.asd;
                var bulkToggle = '';
                var OnlineToggle = '';
                var EDItoggle = '';
                var calltoggle = '';

                var bulkToggleASPS = '';
                var OnlineToggleASPS = '';
                var EDItoggleASPS = '';
                var calltoggleASPS = '';
                $('#tblStartService tbody').empty();
                //$('#tblStartService thead').append(
                //        '<tr >' +
                //            '<th class="text-center">USP Name </th>' +
                //            '<th class="text-center">Bulk Email </th>' +
                //            '<th class="text-center">Online Request </th>' +
                //            '<th class="text-center">EDI 814 </th>' +
                //            '<th class="text-center">Call </th>' +
                //        '</tr>'
                //    );
                //$('#tblStartService').bootstrapTable({
                //    data: records,
                //    height: 560,
                //    width: 1000,
                //    pagination: true
                //});
                $.each(records, function (idx, val) {
                    if (val.bulkUSP == '1') {
                        bulkToggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.bulkUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBulkToggle" checked="checked"/>';
                    }
                    else {
                        bulkToggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.bulkUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBulkToggle"/>';
                    }
                    if (val.onlineUSP == '1') {
                        OnlineToggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.onlineUSP + '" value="' + val.USP_Name + ',' + val.onlineUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkOnlineToggle" checked="checked"/>';
                    }
                    else {
                        OnlineToggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.onlineUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkOnlineToggle"/>';
                    }

                    if (val.ediUSP == '1') {
                        EDItoggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.ediUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDIToggle" checked="checked"/>';
                    }
                    else {
                        EDItoggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.ediUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDIToggle"/>';
                    }
                    if (val.CallUSP == '1') {
                        calltoggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.CallUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkCallToggle" checked="checked"/>';
                    }
                    else {
                        calltoggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.CallUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkCallToggle"/>';
                    }

                    //ASPS

                    if (val.bulkASPS == '1') {
                        bulkToggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.bulkASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBulkASPSToggle" checked="checked"/>';
                    }
                    else {
                        bulkToggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.bulkASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBulkASPSToggle"/>';
                    }
                    if (val.onlineASPS == '1') {
                        OnlineToggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.onlineASPS + '" value="' + val.USP_Name + ',' + val.onlineASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkOnlineASPSToggle" checked="checked"/>';
                    }
                    else {
                        OnlineToggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.onlineASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkOnlineASPSToggle"/>';
                    }

                    if (val.ediASPS == '1') {
                        EDItoggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.ediASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDIASPSToggle" checked="checked"/>';
                    }
                    else {
                        EDItoggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.ediASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDIASPSToggle"/>';
                    }
                    if (val.CallASPS == '1') {
                        calltoggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.CallASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkCallASPSToggle" checked="checked"/>';
                    }
                    else {
                        calltoggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.CallASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkCallASPSToggle"/>';
                    }
                    $('#tblStartService tbody').append(
                        '<tr class="something">' +
                            '<td class="text-left">' + val.USP_Name + '</td>' +
                            '<td class="text-center" >' + bulkToggle + bulkToggleASPS + '</td>' +
                            '<td class="text-center" >' + OnlineToggle + OnlineToggleASPS + '</td>' +
                            //'<td class="text-center" >' + OnlineToggleASPS + '</td>' +
                            '<td class="text-center">' + EDItoggle + EDItoggleASPS + '</td>' +
                            //'<td class="text-center">' + EDItoggleASPS + '</td>' +
                            '<td class="text-center">' + calltoggle + calltoggleASPS + '</td>' +
                           // '<td class="text-center">' + calltoggleASPS + '</td>' +
                        '</tr>'
                    );

                    //$('#tblStartService').bootstrapTable({
                    //    data: records,
                    //    height: 560,
                    //    width: 1000,
                    //    pagination: true
                    //});
                });

                $('.toggle').bootstrapToggle('destroy');
                $('.toggle').bootstrapToggle({
                    on: 'YES',
                    off: 'NO'
                });

                $('.chkBulkToggle').change(function () {

                    var val = $(this).attr('value');
                    //var splitVal = val.split(',');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSP',
                        data: "{'val':'" + val
                            + "','USP':'" + "Bulk" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });

                });
                $('.chkOnlineToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSP',
                        data: "{'val':'" + val
                            + "','USP':'" + "Online" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });
                $('.chkEDIToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSP',
                        data: "{'val':'" + val
                            + "','USP':'" + "EDI" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });
                $('.chkCallToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSP',
                        data: "{'val':'" + val
                            + "','USP':'" + "Call" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });


                //ASPS UPDATE
                $('.chkBulkASPSToggle').change(function () {

                    var val = $(this).attr('value');
                    //var splitVal = val.split(',');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSP',
                        data: "{'val':'" + val
                            + "','USP':'" + "BulkASPS" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });

                });
                $('.chkOnlineASPSToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSP',
                        data: "{'val':'" + val
                            + "','USP':'" + "OnlineASPS" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });
                $('.chkEDIASPSToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSP',
                        data: "{'val':'" + val
                            + "','USP':'" + "EDIASPS" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });
                $('.chkCallASPSToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSP',
                        data: "{'val':'" + val
                            + "','USP':'" + "CallASPS" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });
                $('#modalLoading').modal('hide');
            }


        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

//function bulkFormat(val) {
//    var uspname = $('#tblStop tbody tr:eq(0)').innerText();
//    if (val == '1') {
//        bulkToggle = '<input type="checkbox" value="' + uspname + ',' + val + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBulk1Toggle" checked="checked"/>';
//    }
//    else {
//        bulkToggle = '<input type="checkbox" value="' + uspname + ',' + val + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBulk1Toggle"/>';
//    }
//}
window.onload = function getExtract() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'USPCapabilities.aspx/ExtractExcel',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#tblStartExcel').bootstrapTable('destroy');
                $('#tblStopExcel').bootstrapTable('destroy');
                $('#tblInvoiceExcel').bootstrapTable('destroy');
                $('#tblBillsExcel').bootstrapTable('destroy');
                var records = d.data.asd;
                var records2 = d.data.asd2;
                var records3 = d.data.asd3;
                var records4 = d.data.asd4;
                $('#tblStartExcel').bootstrapTable({
                    data: records,
                });
                $('#tblStopExcel').bootstrapTable({
                    data: records2,
                });
                $('#tblInvoiceExcel').bootstrapTable({
                    data: records3,
                });
                $('#tblBillsExcel').bootstrapTable({
                    data: records4,
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function getStop() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });

    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetStop',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                var bulkToggle = '';
                var OnlineToggle = '';
                var EDItoggle = '';
                var calltoggle = '';

                var bulkToggleASPS = '';
                var OnlineToggleASPS = '';
                var EDItoggleASPS = '';
                var calltoggleASPS = '';





               // $('#tblStop thead').empty();
                $('#tblStop tbody').empty();
                //$('#tblStop').bootstrapTable('destroy');
                //$('#tblStop').bootstrapTable();
                //$('#tblStop thead').append(
                //        '<tr >' +
                //            '<th class="text-center">USP Name </th>' +
                //            '<th class="text-center">Bulk Email </th>' +
                //            '<th class="text-center">Online Request </th>' +
                //            '<th class="text-center">EDI 814 </th>' +
                //            '<th class="text-center">Call </th>' +
                //        '</tr>'
                //    );
                $.each(records, function (idx, val) {
                    if (val.bulkUSP == '1') {
                        bulkToggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.bulkUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBulk1Toggle" checked="checked"/>';
                    }
                    else {
                        bulkToggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.bulkUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBulk1Toggle"/>';
                    }
                    if (val.onlineUSP == '1') {
                        OnlineToggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.onlineUSP + '" value="' + val.USP_Name + ',' + val.onlineUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkOnline1Toggle" checked="checked"/>';
                    }
                    else {
                        OnlineToggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.onlineUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkOnline1Toggle"/>';
                    }

                    if (val.ediUSP == '1') {
                        EDItoggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.ediUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDI1Toggle" checked="checked"/>';
                    }
                    else {
                        EDItoggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.ediUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDI1Toggle"/>';
                    }
                    if (val.CallUSP == '1') {
                        calltoggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.CallUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkCall1Toggle" checked="checked"/>';
                    }
                    else {
                        calltoggle = '<input type="checkbox" value="' + val.USP_Name + ',' + val.CallUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkCall1Toggle"/>';
                    }

                    //ASPS

                    if (val.bulkASPS == '1') {
                        bulkToggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.bulkASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBulk1ASPSToggle" checked="checked"/>';
                    }
                    else {
                        bulkToggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.bulkASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBulk1ASPSToggle"/>';
                    }
                    if (val.onlineASPS == '1') {
                        OnlineToggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.onlineASPS + '" value="' + val.USP_Name + ',' + val.onlineASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkOnline1ASPSToggle" checked="checked"/>';
                    }
                    else {
                        OnlineToggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.onlineASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkOnline1ASPSToggle"/>';
                    }

                    if (val.ediASPS == '1') {
                        EDItoggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.ediASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDI1ASPSToggle" checked="checked"/>';
                    }
                    else {
                        EDItoggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.ediASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDI1ASPSToggle"/>';
                    }
                    if (val.CallASPS == '1') {
                        calltoggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.CallASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkCall1ASPSToggle" checked="checked"/>';
                    }
                    else {
                        calltoggleASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.CallASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkCall1ASPSToggle"/>';
                    }
                    $('#tblStop tbody').append(
                        '<tr class="something">' +
                            '<td class="text-left">' + val.USP_Name + '</td>' +

                            '<td class="text-center">' + bulkToggle + bulkToggleASPS + '</td>' +
                        //    '<td class="text-center">' + bulkToggleASPS + '</td>' +
                            '<td class="text-center" >' + OnlineToggle + OnlineToggleASPS + '</td>' +
                         //   '<td class="text-center" >' + OnlineToggleASPS + '</td>' +
                            '<td class="text-center">' + EDItoggle + EDItoggleASPS + '</td>' +
                        //    '<td class="text-center">' + EDItoggleASPS + '</td>' +
                            '<td class="text-center">' + calltoggle + calltoggleASPS + '</td>' +
                         //   '<td class="text-center">' + calltoggleASPS + '</td>' +
                        '</tr>'
                    );
                });
                $('.toggle').bootstrapToggle('destroy');
                $('.toggle').bootstrapToggle({
                    on: 'YES',
                    off: 'NO'
                });
                $('.chkBulk1Toggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPstop',
                        data: "{'val':'" + val
                            + "','USP':'" + "Bulk" + "'}",
                        //async: false,
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {
                            console.log(response.responseText);
                        }
                    });
                });
                $('.chkOnline1Toggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPstop',
                        data: "{'val':'" + val
                            + "','USP':'" + "Online" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });
                $('.chkEDI1Toggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPstop',
                        data: "{'val':'" + val
                            + "','USP':'" + "EDI" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });
                $('.chkCall1Toggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPstop',
                        data: "{'val':'" + val
                            + "','USP':'" + "Call" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });

                //ASPS
                $('.chkBulk1ASPSToggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPstop',
                        data: "{'val':'" + val
                            + "','USP':'" + "BulkASPS" + "'}",
                        //async: false,
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {
                            console.log(response.responseText);
                        }
                    });
                });
                $('.chkOnline1ASPSToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPstop',
                        data: "{'val':'" + val
                            + "','USP':'" + "OnlineASPS" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });
                $('.chkEDI1ASPSToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPstop',
                        data: "{'val':'" + val
                            + "','USP':'" + "EDIASPS" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });
                $('.chkCall1ASPSToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPstop',
                        data: "{'val':'" + val
                            + "','USP':'" + "CallASPS" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');

                        },
                        error: function (response) {

                            console.log(response.responseText);
                        }
                    });
                });
                $('#modalLoading').modal('hide');
            }


        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function getInvoice() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });

    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetInvoice',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                var summary = '';
                var password1 = '';
                var password2 = '';
                var link = '';
                var edi = '';
                var bill = '';
                //ASPS
                var summaryASPS = '';
                var password1ASPS = '';
                var password2ASPS = '';
                var linkASPS = '';
                var ediASPS = '';
                var billASPS = '';
              //  $('#tblInvoicing thead').empty();
                $('#tblInvoicing tbody').empty();

                $.each(records, function (idx, val) {



                    if (val.summaryUSP == '1') {
                        summary = '<input type="checkbox" value="' + val.USP_Name + ',' + val.summaryUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkSummaryToggle" checked="checked"/>';
                    }
                    else {
                        summary = '<input type="checkbox" value="' + val.USP_Name + ',' + val.summaryUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkSummaryToggle"/>';
                    }

                    if (val.password1USP == '1') {
                        password1 = '<input type="checkbox" value="' + val.USP_Name + ',' + val.password1USP + '"  data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkPassword1Toggle" checked="checked"/>';
                    }
                    else {
                        password1 = '<input type="checkbox" value="' + val.USP_Name + ',' + val.password1USP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkPassword1Toggle"/>';
                    }

                    if (val.password2USP == '1') {
                        password2 = '<input type="checkbox" value="' + val.USP_Name + ',' + val.password2USP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkPassword2Toggle" checked="checked"/>';
                    }
                    else {
                        password2 = '<input type="checkbox" value="' + val.USP_Name + ',' + val.password2USP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkPassword2Toggle"/>';
                    }
                    if (val.LinkUSP == '1') {
                        link = '<input type="checkbox" value="' + val.USP_Name + ',' + val.LinkUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkLinkToggle" checked="checked"/>';
                    }
                    else {
                        link = '<input type="checkbox" value="' + val.USP_Name + ',' + val.LinkUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkLinkToggle"/>';
                    }
                    if (val.ediUSP == '1') {
                        edi = '<input type="checkbox" value="' + val.USP_Name + ',' + val.ediUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDI810Toggle" checked="checked"/>';
                    }
                    else {
                        edi = '<input type="checkbox" value="' + val.USP_Name + ',' + val.ediUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDI810Toggle"/>';
                    }
                    if (val.billUSP == '1') {
                        bill = '<input type="checkbox" value="' + val.USP_Name + ',' + val.billUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBillToggle" checked="checked"/>';
                    }
                    else {
                        bill = '<input type="checkbox" value="' + val.USP_Name + ',' + val.billUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBillToggle"/>';
                    }


                    //ASPS
                    if (val.summaryASPS == '1') {
                        summaryASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.summaryASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkSummaryASPSToggle" checked="checked"/>';
                    }
                    else {
                        summaryASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.summaryASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkSummaryASPSToggle"/>';
                    }

                    if (val.password1ASPS == '1') {
                        password1ASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.password1ASPS + '"  data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkPassword1ASPSToggle" checked="checked"/>';
                    }
                    else {
                        password1ASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.password1ASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkPassword1ASPSToggle"/>';
                    }

                    if (val.password2ASPS == '1') {
                        password2ASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.password2ASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkPassword2ASPSToggle" checked="checked"/>';
                    }
                    else {
                        password2ASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.password2ASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkPassword2ASPSToggle"/>';
                    }
                    if (val.LinkASPS == '1') {
                        linkASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.LinkASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkLinkASPSToggle" checked="checked"/>';
                    }
                    else {
                        linkASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.LinkASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkLinkASPSToggle"/>';
                    }
                    if (val.ediASPS == '1') {
                        ediASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.ediASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDI810ASPSToggle" checked="checked"/>';
                    }
                    else {
                        ediASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.ediASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkEDI810ASPSToggle"/>';
                    }
                    if (val.billASPS == '1') {
                        billASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.billASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBillASPSToggle" checked="checked"/>';
                    }
                    else {
                        billASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ',' + val.billASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkBillASPSToggle"/>';
                    }
                    $('#tblInvoicing tbody').append(
                        '<tr >' +
                            '<td class="text-left" style="width:190px">' + val.USP_Name + '</td>' +
                            '<td class="text-center" style="width:190px">' + summary + summaryASPS + '</td>' +
                            '<td class="text-center" style="width:190px">' + password1 + password1ASPS + '</td>' +
                            '<td class="text-center" style="width:190px">' + password2 + password2ASPS + '</td>' +
                            '<td class="text-center" style="width:190px">' + link + linkASPS + '</td>' +
                            '<td class="text-center" style="width:190px">' + edi + ediASPS + '</td>' +
                            '<td class="text-center" style="width:190px">' + bill + billASPS + '</td>' +
                        '</tr>'
                    );
                });
                //$('#tblInvoicing thead').append(
                //        '<tr >' +
                //            '<th class="text-center" style="width:122px">USP Name</th>' +
                //            '<th class="text-center" style="width:174px">Summary Billing</th>' +
                //            '<th class="text-center" style="width:88px">E Bill w/ Password</th>' +
                //            '<th class="text-center" style="width:88px">E Bill w/o Password</th>' +
                //            '<th class="text-center" style="width:88px">E-Bill Via Link</th>' +
                //            '<th class="text-center" style="width:88px">EDI 810</th>' +
                //            '<th class="text-center" style="width:88px">Paper Bill</th>' +
                //        '</tr>'
                //    );
                $('.toggle').bootstrapToggle('destroy');
                $('.toggle').bootstrapToggle({
                    on: 'YES',
                    off: 'NO',
                    width: '100px'
                });

                $('.chkSummaryToggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "Summary" + "'}",

                        //async: false,
                        success: function (response) {
                         

                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkPassword1Toggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "Password1" + "'}",

                        //async: false,
                        success: function (response) {
                        
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkPassword2Toggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "Password2" + "'}",

                        //async: false,
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkLinkToggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "Link" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkEDI810Toggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "EDI" + "'}",

                        //async: false,
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkBillToggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "Bill" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });

                //ASPS
                $('.chkSummaryASPSToggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "SummaryASPS" + "'}",

                        //async: false,
                        success: function (response) {


                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkPassword1ASPSToggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "Password1ASPS" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkPassword2ASPSToggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "Password2ASPS" + "'}",

                        //async: false,
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkLinkASPSToggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "LinkASPS" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkEDI810ASPSToggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "EDIASPS" + "'}",

                        //async: false,
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkBillASPSToggle').change(function () {

                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPinvoice',
                        data: "{'val':'" + val
                            + "','USP':'" + "BillASPS" + "'}",

                        //async: false,
                        success: function (response) {

                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {

                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function getBills() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'USPCapabilities.aspx/GetBills',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
           
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                var ach = '';
                var wire = '';
                var credit = '';
                var check = '';
                //ASPS
                var achASPS = '';
                var wireASPS = '';
                var creditASPS = '';
                var checkASPS = '';
                //$('#tblbills thead').empty();
                $('#tblbills tbody').empty();

                $.each(records, function (idx, val) {
                    if (val.ACHUSP == '1') {
                        ach = '<input type="checkbox" value="' + val.USP_Name + ',' + val.ACHUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkACHToggle" checked="checked"/>';
                    }
                    else {
                        ach = '<input type="checkbox" value="' + val.USP_Name + ',' + val.ACHUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkACHToggle"/>';
                    }
                    if (val.wireUSP == '1') {
                        wire = '<input type="checkbox" value="' + val.USP_Name + ',' + val.wireUSP + '" value="' + val.USP_Name + ',' + val.wireUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkwireToggle" checked="checked"/>';
                    }
                    else {
                        wire = '<input type="checkbox" value="' + val.USP_Name + ',' + val.wireUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkwireToggle"/>';
                    }

                    if (val.creditUSP == '1') {
                        credit = '<input type="checkbox" value="' + val.USP_Name + ',' + val.creditUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkcreditToggle" checked="checked"/>';
                    }
                    else {
                        credit = '<input type="checkbox" value="' + val.USP_Name + ',' + val.creditUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkcreditToggle"/>';
                    }
                    if (val.CheckUSP == '1') {
                        check = '<input type="checkbox" value="' + val.USP_Name + ',' + val.CheckUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkcheckToggle" checked="checked"/>';
                    }
                    else {
                        check = '<input type="checkbox" value="' + val.USP_Name + ',' + val.CheckUSP + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkcheckToggle"/>';
                    }

                    //ASPS
                    if (val.ACHASPS == '1') {
                        achASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ','
                            + val.ACHASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkACHASPSToggle" checked="checked"/>';
                    }
                    else {
                        achASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ','
                            + val.ACHASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkACHASPSToggle"/>';
                    }

                    if (val.wireASPS == '1') {
                        wireASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ','
                            + val.wireASPS + '"  data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkwireASPSToggle" checked="checked"/>';
                    }
                    else {
                        wireASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ','
                            + val.wireASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkwireASPSToggle"/>';
                    }

                    if (val.creditASPS == '1') {
                        creditASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ','
                            + val.creditASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkcreditASPSToggle" checked="checked"/>';
                    }
                    else {
                        creditASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ','
                            + val.creditASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkcreditASPSToggle"/>';
                    }
                    if (val.CheckASPS == '1') {
                        checkASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ','
                            + val.CheckASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkcheckASPSToggle" checked="checked"/>';
                    }
                    else {
                        checkASPS = '&nbsp&nbsp<input type="checkbox" value="' + val.USP_Name + ','
                            + val.CheckASPS + '" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle chkcheckASPSToggle"/>';
                    }
                    $('#tblbills tbody').append(
                        '<tr >' +
                            '<td class="text-left" >' + val.USP_Name + '</td>' +
                            '<td class="text-center">' + ach + achASPS + '</td>' +
                            '<td class="text-center" >' + wire + wireASPS + '</td>' +
                            '<td class="text-center">' + credit + creditASPS + '</td>' +
                            '<td class="text-center">' + check + checkASPS + '</td>' +
                        '</tr>'
                    );
                });
                //$('#tblbills thead').append(
                //        '<tr >' +
                //            '<th class="text-center" style="width:122px">USP Name</th>' +
                //            '<th class="text-center" style="width:174px">ACH</th>' +
                //            '<th class="text-center" style="width:88px">Wire Transfer</th>' +
                //            '<th class="text-center" style="width:88px">Credit Card</th>' +
                //            '<th class="text-center" style="width:88px">Check</th>' +
                //        '</tr>'
                //    );
                $('.toggle').bootstrapToggle('destroy');
                $('.toggle').bootstrapToggle({
                    on: 'YES',
                    off: 'NO'
                });

                $('.chkACHToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPBills',
                        data: "{'val':'" + val
                            + "','USP':'" + "ACH" + "'}",
                        success: function (response) {
                            
                          
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {
                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkwireToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPBills',
                        data: "{'val':'" + val
                            + "','USP':'" + "Wire" + "'}",
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {
                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkcreditToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPBills',
                        data: "{'val':'" + val
                            + "','USP':'" + "Credit" + "'}",
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {
                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkcheckToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPBills',
                        data: "{'val':'" + val
                            + "','USP':'" + "Check" + "'}",
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {
                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });

                //ASPS
                $('.chkACHASPSToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPBills',
                        data: "{'val':'" + val
                            + "','USP':'" + "ACHASPS" + "'}",
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {
                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkwireASPSToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPBills',
                        data: "{'val':'" + val
                            + "','USP':'" + "WireASPS" + "'}",
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {
                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkcreditASPSToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPBills',
                        data: "{'val':'" + val
                            + "','USP':'" + "CreditASPS" + "'}",
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {
                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('.chkcheckASPSToggle').change(function () {
                    var val = $(this).attr('value');
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'USPCapabilities.aspx/UPdateUSPBills',
                        data: "{'val':'" + val
                            + "','USP':'" + "CheckASPS" + "'}",
                        success: function (response) {
                            $('#modalLoading').modal('hide');
                        },
                        error: function (response) {
                            console.log(response.responseText);
                            $('#modalLoading').modal('hide');
                        }
                    });
                });
                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
$('#Tinvoice').click(function () {
   // $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    getInvoice();
   // $('#modalLoading').modal('hide');
});
$('#Tstop').click(function () {
   // $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    getStop();
   //$('#modalLoading').modal('hide');
});
$('#TBills').click(function () {
  //  $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    getBills();
  //  $('#modalLoading').modal('hide');
});


$('#Tprio').click(function () {
  //  $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    getRuleInfoDisplay();
    getRuleStop();
    getRuleInvoice();
    getRuleBills();
    
});
$('#Tusp').click(function () {
   
    getStart();
   
});
$('#Tother').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    Task();
    Status();
    StopStatus();
    UtilityType();
    $('#modalLoading').modal('hide');
});
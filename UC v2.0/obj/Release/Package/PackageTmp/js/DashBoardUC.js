﻿$(document).ready(function () {


    $(document).on('click', ".table .btn", function (e) {
        e.preventDefault();
        process = $(this).attr('value')
        ShowVal(process);
    });
    var $container = $("#content");
    getDashBoard();
    var refreshId = setInterval(function () {
        getDashBoard();
    }, 600000);
});




function ShowVal(process) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('#tblBulk').bootstrapTable('destroy');
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"process" : "' + process + '"}',
        url: 'DashBoardUC.aspx/GetProcess',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblBulk').bootstrapTable({
                    data: records,
                    height: 560,
                    width: 1000,
                    pagination: true
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnSearch').click(function () {
    var Date = document.getElementById('txtDate').value
    var Process = document.getElementById('ddlProcess').value;
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('#tblHistorical').bootstrapTable('destroy');
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: "{'Date' : '" + Date +
              "','Process' : '" + Process + "'}",
        url: 'DashBoardUC.aspx/GetHistorical',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblHistorical').bootstrapTable({
                    data: records,
                    pagination: true,
                    width: 1000
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });

});


function Show(value) {
    return '<button type="button" data-id="' + value + '" value="' + value
        + '" class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#myModalEdit" >View</button>'
}


function bar(bulkEmail, OnlineRequest, Call, StopbulkEmail, StopOnlineRequest, StopCall) {
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        title: {
            text: "Workable Left"
        },
        axisY: {
            title: "Start Service Workable Left",
            titleFontColor: "#4F81BC",
            lineColor: "#4F81BC",
            labelFontColor: "#4F81BC",
            tickColor: "#4F81BC"
        },
        axisY2: {
            title: "Stop Service Workable Left",
            titleFontColor: "#C0504E",
            lineColor: "#C0504E",
            labelFontColor: "#C0504E",
            tickColor: "#C0504E"
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        data: [{
            type: "column",
            name: "Start Service",
            legendText: "Start Service",
            showInLegend: true,
            dataPoints: [
                { label: "Bulk Email", y: bulkEmail },
                { label: "Online Request", y: OnlineRequest },
                { label: "Callout", y: Call },
            ]
        },
	{
	    type: "column",
	    name: "Stop Service",
	    legendText: "Stop Service",
	    axisYType: "secondary",
	    showInLegend: true,
	    dataPoints: [
			{ label: "Bulk Email", y: StopbulkEmail },
            { label: "Online Request", y: StopOnlineRequest },
            { label: "Callout", y: StopCall },
	    ]
	}]
    });
    chart.render();

    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chart.render();
    }
}



function getDashBoard() {
    var bulkemail = "";
    var onlinerequest = "";
    var call = "";
    var Stopbulkemail = "";
    var Stoponlinerequest = "";
    var Stopcall = "";
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DashBoardUC.aspx/GetDashBoard',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#tblDashBoard,#tblTotal').bootstrapTable('destroy');

                var records = d.data.asd;
                var records2 = d.data.asd2;


                $.each(records, function (idx, val) {
                    if (val.Solution == 'Bulk Email') {
                        document.getElementById('txtWCount').innerHTML = val.WorkableCount;
                        document.getElementById('txtCountLeft').innerHTML = val.WorkableCountLeft + " Workable Count Left";
                        sessionStorage.setItem('BulkEmail', val.StartService);
                        bulkemail = val.StartService;
                        Stopbulkemail = val.StopService;
                        document.getElementById('txtBulkEmail').innerHTML = val.StartService;
                        document.getElementById('txtBulkEmail2').innerHTML = val.StopService;
                    }

                    if (val.Solution == 'Online Request') {
                        document.getElementById('txtWCount2').innerHTML = val.WorkableCount;
                        document.getElementById('txtCountLeft2').innerHTML = val.WorkableCountLeft + " Workable Count Left";
                        sessionStorage.setItem('OnlineRequest', val.StartService);
                        onlinerequest = val.StartService;
                        Stoponlinerequest = val.StopService;
                        document.getElementById('txtOnline').innerHTML = val.StartService;
                        document.getElementById('txtOnline2').innerHTML = val.StopService;
                    }

                    if (val.Solution == 'Callout') {
                        document.getElementById('txtWCount3').innerHTML = val.WorkableCount;
                        document.getElementById('txtCountLeft3').innerHTML = val.WorkableCountLeft + " Workable Count Left";
                        sessionStorage.setItem('Callout', val.StartService);
                        call = val.StartService;
                        Stopcall = val.StopService;
                        document.getElementById('txtCall').innerHTML = val.StartService;
                        document.getElementById('txtCall2').innerHTML = val.StopService;
                    }

                    bar(bulkemail, onlinerequest, call,Stopbulkemail,Stoponlinerequest,Stopcall);

                });


                $.each(records2, function (idx, val) {
                    document.getElementById('txtCount').innerHTML = val.sum;
                    document.getElementById('txtDatetoday').innerHTML = val.DateNow;
                });
                $('#tblDashBoard').bootstrapTable({
                    data: records,

                });
                $('#tblTotal').bootstrapTable({
                    data: records2,
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}



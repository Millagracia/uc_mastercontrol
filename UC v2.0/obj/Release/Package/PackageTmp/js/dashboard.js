﻿$(document).ready(function () {
    getDisplay();
    getStartService();
    getStopService();
    $(".main-container.collapse").not($(this)).collapse('hide');
   // document.getElementById("btnLoading").disabled = true;
   // document.getElementById("btnLoading").show = false;
   // $('#btnLoading').hide;
});


function getDisplay() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DashBoard.aspx/GetList',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#tblUSPprocess').bootstrapTable('destroy');
                var records = d.data.asd;
                $('#tblUSPprocess').bootstrapTable({
                    data: records,
           
                    width: 1000,
                    pagination: true
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function getStartService() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DashBoard.aspx/GetStart',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#tblStartService').bootstrapTable('destroy');
                var records = d.data.asd;
                $('#tblStartService').bootstrapTable({
                    data: records,
                    
                    width: 1000,
                    pagination: true
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function getStopService() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DashBoard.aspx/GetStop',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#tblStopService').bootstrapTable('destroy');
                var records = d.data.asd;
                $('#tblStopService').bootstrapTable({
                    data: records,

                    width: 1000,
                    pagination: true
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
$('#btnRUN').click(function () {
    $(".main-container.collapse").not($(this)).collapse('show');
});

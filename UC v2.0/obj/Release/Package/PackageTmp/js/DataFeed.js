﻿$(document).ready(function () {
  
});

window.onload = function ()
{
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('#tblDataFeed').bootstrapTable('destroy');
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'DataFeed.aspx/BindData',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblDataFeed').bootstrapTable({
                    data: records,
                    height: 560,
                    width: 1000,
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnApply').click(function () {

    //Get Title
    var GetTitle = [];
    var Days = '';
    if ($('#weekday-mon').is(':checked') == true)
    {
        Days += 'Monday'
    }

    if ($('#weekday-tue').is(':checked') == true) {
        if (Days != '')
        {
            Days += ',Tuesday'
        }
        else
        {
            Days += 'Tuesday'
        }
        
    }

    if ($('#weekday-wed').is(':checked') == true) {

        if (Days != '') {
            Days += ',Wednesday'
        }
        else {
            Days += 'Wednesday'
        }
    }

    if ($('#weekday-thu').is(':checked') == true) {
        if (Days != '') {
            Days += ',Thursday'
        }
        else {
            Days += 'Thursday'
        }
    }

    if ($('#weekday-fri').is(':checked') == true) {
        if (Days != '') {
            Days += ',Friday'
        }
        else {
            Days += 'Friday'
        }
    }

    if ($('#weekday-sat').is(':checked') == true) {
        if (Days != '') {
            Days += ',Saturday'
        }
        else {
            Days += 'Saturday'
        }
    }

    if ($('#weekday-sun').is(':checked') == true) {
        if (Days != '') {
            Days += ',Sunday'
        }
        else {
            Days += 'Sunday'
        }
    
    }
    GetTitle.push($('#txtTitle').val());
    GetTitle.push($('#txtDesc').val());
    GetTitle.push($('#filePathLoc').val());
    GetTitle.push(Days);
    GetTitle.push($('#txtTime').val());
    GetTitle.push($('#txtTimeTo').val());
    GetTitle.push($('#ddlTimezone :selected').text());

    //Get Alert Message
    //Success Message
    var GetMessage = [];
    GetMessage.push($('#txtSuccAlert').val());
    GetMessage.push($('#txtSuccAreaMessage').val());
    //Fail Message
    GetMessage.push($('#txtFailAlert').val());
    GetMessage.push($('#txtFailAreaMessage').val());
    
    

    //Get Condition
    var NoOfCondition = 1;
    var GetCondition1 = [];
    var GetCondition2 = [];
    var GetCondition3 = [];
    var GetCondition4 = [];
    var GetCondition5 = [];


    if ($("#dvCondition1:visible").length == 1) {
        GetCondition1.push($('#cbIsActive1').is(':checked'));
        GetCondition1.push($('#ddlAlertRecei1').val());
        GetCondition1.push($('#ddAlertGLE1').val());
        GetCondition1.push($('#txtHour1').val());
        GetCondition1.push($('#ddlHMS1').val());
        GetCondition1.push($('#ddlAlertSuc1').val());
        GetCondition1.push($('#txtTo1').val());
    }


    if ($("#dvCondition2:visible").length == 1)
    {
        NoOfCondition = 2;
        GetCondition2.push($('#cbIsActive2').is(':checked'));
        GetCondition2.push($('#ddlRecei2').val());
        GetCondition2.push($('#ddlGLE2').val());
        GetCondition2.push($('#txtAlertHour2').val());
        GetCondition2.push($('#ddlHMS2').val());
        GetCondition2.push($('#ddlSucc2').val());
        GetCondition2.push($('#txtTo2').val());
        
    }

    if ($("#dvCondition3:visible").length == 1) {
        NoOfCondition = 3;
        GetCondition3.push($('#cbIsActive3').is(':checked'));
        GetCondition3.push($('#ddlRecei3').val());
        GetCondition3.push($('#ddlGLE3').val());
        GetCondition3.push($('#txtAlertHour3').val());
        GetCondition3.push($('#ddlHMS3').val());
        GetCondition3.push($('#ddlSucc3').val());
        GetCondition3.push($('#txtTo3').val());
    }

    if ($("#dvCondition4:visible").length == 1) {
        NoOfCondition = 4;
        GetCondition4.push($('#cbIsActive4').is(':checked'));
        GetCondition4.push($('#ddlRecei4').val());
        GetCondition4.push($('#ddlGLE4').val());
        GetCondition4.push($('#txtAlertHour4').val());
        GetCondition4.push($('#ddlHMS4').val());
        GetCondition4.push($('#ddlSucc4').val());
        GetCondition4.push($('#txtTo4').val());
    }

    if ($("#dvCondition5:visible").length == 1) {
        NoOfCondition = 5;
        GetCondition5.push($('#cbIsActive5').is(':checked'));
        GetCondition5.push($('#ddlRecei5').val());
        GetCondition5.push($('#ddlGLE5').val());
        GetCondition5.push($('#txtAlertHour5').val());
        GetCondition5.push($('#ddlHMS5').val());
        GetCondition5.push($('#ddlSucc5').val());
        GetCondition5.push($('#txtTo5').val());
    }

    
  
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        data: '{GetTitle : ' + JSON.stringify(GetTitle) +
            ',GetMessage : ' + JSON.stringify(GetMessage)  +
            ',GetCondition1 : ' + JSON.stringify(GetCondition1)  +
            ',GetCondition2 : ' + JSON.stringify(GetCondition2)  +
            ',GetCondition3 : ' + JSON.stringify(GetCondition3)  +
            ',GetCondition4 : ' + JSON.stringify(GetCondition4)  +
            ',GetCondition5 : ' + JSON.stringify(GetCondition5) + '}',
        contentType: 'application/json; charset=utf-8',
        url: 'DataFeed.aspx/InsertData',
        success: function (data) {
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
    });



    dvAddNew.style.display = 'none';
    if ($("#dvTable:visible").length == 0) {
        $("#dvTable").show();

    }
});

$('#btnAddNew1').click(function () {
    dvAddNew1.style.display = 'none';
    if ($("#dvCondition2:visible").length == 0) {
        $("#dvCondition2").show();
        $("#dvRemove1").show();
    }
});
$('#btnAddNew2').click(function () {
    dvAddNew2.style.display = 'none';
    if ($("#dvCondition3:visible").length == 0) {
        $("#dvCondition3").show();
        $("#dvRemove2").show();
    }
});
$('#btnAddNew3').click(function () {
    dvAddNew3.style.display = 'none';
    if ($("#dvCondition4:visible").length == 0) {
        $("#dvCondition4").show();
        $("#dvRemove3").show();
    }
});
$('#btnAddNew4').click(function () {
    dvAddNew4.style.display = 'none';
    if ($("#dvCondition5:visible").length == 0) {
        $("#dvCondition5").show();
        $("#dvRemove4").show();
    }
});
$('#btnAddNew5').click(function () {
    if ($("#dvCondition1:visible").length == 0)
    {
        $("#dvCondition1").show();
        $("#dvRemove1").show();
        dvAddNew1.style.display = 'none';
    }
    else if($("#dvCondition2:visible").length == 0)
    {
        $("#dvCondition2").show();
        $("#dvRemove2").show();
        dvAddNew2.style.display = 'none';
    }
    else if ($("#dvCondition3:visible").length == 0) {
        $("#dvCondition3").show();
        $("#dvRemove3").show();
        dvAddNew3.style.display = 'none';
    }
    else{
        $("#dvCondition4").show();
        $("#dvRemove4").show();
        dvAddNew4.style.display = 'none';
    }
   
});
$('#btnRemove1').click(function () {
    dvCondition1.style.display = 'none';
});
$('#btnRemove2').click(function () {
    dvCondition2.style.display = 'none';
    dvRemove1.style.display = 'none';
        $("#dvAddNew1").show();
 
    
});
$('#btnRemove3').click(function () {
    dvCondition3.style.display = 'none';
    $("#dvAddNew2").show();
});
$('#btnRemove4').click(function () {
    dvCondition4.style.display = 'none';
    $("#dvAddNew3").show();
});
$('#btnRemove5').click(function () {
    dvCondition5.style.display = 'none';
    $("#dvAddNew4").show();
});
$('#btnAddnew').click(function () {
    dvTable.style.display = 'none';
    if ($("#dvAddNew:visible").length == 0) {
        $("#dvAddNew").show();
    }
});
$('#btnReset').click(function () {

    dvAddNew.style.display = 'none';
    if ($("#dvTable:visible").length == 0) {
        $("#dvTable").show();
    }
});
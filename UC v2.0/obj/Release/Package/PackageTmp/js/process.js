﻿$(document).ready(function () {
    getStart();
   
});

function getStart()
{
    $.ajax({
        type: 'POST',
        url: 'Process.aspx/GetStart',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            var d = $.parseJSON(data.d);
            if (d.Success) {
                $('#tblStartProcess').bootstrapTable('destroy');
                var records = d.data.record;
                $('#tblStartProcess').bootstrapTable({
                    data: records,
                    height: 560
                });
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
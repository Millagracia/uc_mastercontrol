﻿$(document).ready(function () {
	//getPrioritization();
	loadPrio('');
});

function sortBy(val) {
	var sort = $(val).val();
	var order = $(val).closest('tr').find('td:eq(4) select');

	var intArr = ['Property ID', 'propNum', 'sla', 'vprStatAge', 'Aging', 'Zip code', 'Vendor Info', 'daysleft'];
	var strArr = ['City', 'Clients', 'Occupancy Status', 'Pod Name', 'Property ID', 'REOStatus', 'SLA', 'State',
						'username', 'Utility Service Provider', 'Utility Status', 'Vendor Info', 'WSTRAT Date'];
	var dateArr = ['Boarded Date', 'Due Date'];
	var customArr = ['client', 'occStat', 'sla', 'State', 'City', 'Zip code', 'Aging', 'Utility Service Provider'];
	if ($(val).val() == 'daysleft') {
	    order.empty();
	    order.append(
                 '<option value="">--Select One--</option>' +
				 '<option value="cust" >Custom</option>'
        );
	    return;
	}

	if ($.inArray(sort, intArr) !== -1) {
		order.empty();
		if ($.inArray(sort, customArr) !== -1) {
			order.append(
				'<option value="">--Select One--</option>' +
				'<option value="asc">Lowest to Highest</option>' +
				'<option value="desc">Highest to Lowest</option>' +
				'<option value="cust" >Custom</option>'
			);
		} else {
			order.append(
				'<option value="">--Select One--</option>' +
				'<option value="asc">Lowest to Highest</option>' +
				'<option value="desc">Highest to Lowest</option>'
			);
		}
	} else if ($.inArray(sort, strArr) !== -1) {
		order.empty();
		if ($.inArray(sort, customArr) !== -1) {
			order.append(
				'<option value="">--Select One--</option>' +
				'<option value="asc">A - Z</option>' +
				'<option value="desc">Z - A</option>' +
				'<option value="cust" >Custom</option>'
			);
		} else {
			order.append(
				'<option value="">--Select One--</option>' +
				'<option value="asc">A - Z</option>' +
				'<option value="desc">Z - A</option>'
			);
		}
	} else if ($.inArray(sort, dateArr) !== -1) {
		order.empty();
		if ($.inArray(sort, customArr) !== -1) {
			if (sort != 'daysleft') {
				order.append(
					'<option value="">--Select One--</option>' +
					'<option value="asc">Oldest to Newest</option>' +
					'<option value="desc">Newest to Oldest</option>' +
					'<option value="cust" >Custom</option>'
				);
			} else {
				order.append(
					'<option value="">--Select One--</option>' +
					'<option value="cust" >Custom</option>'
				);
			}
		} else {
			order.append(
				'<option value="">--Select One--</option>' +
				'<option value="asc">Oldest to Newest</option>' +
				'<option value="desc">Newest to Oldest</option>'
			);
		}
	} else {
		order.empty();
		order.append(
			'<option value="">--Select One--</option>' +
			'<option value="asc">A - Z</option>' +
			'<option value="desc">Z - A</option>' +
			'<option value="asc">Lowest to Highest</option>' +
			'<option value="desc">Highest to Lowest</option>' +
			'<option value="asc">Oldest to Newest</option>' +
			'<option value="desc">Newest to Oldest</option>' +
			'<option value="cust">Custom</option>'
		);
	}
}

function sortb(sort, indx) {

	var order = $('#tblPrioList tbody tr:eq(' + indx + ') td:eq(4) select');

	var intArr = ['propID', 'propNum', 'sla', 'vprStatAge', 'totalAge', 'zip', 'vendor', 'daysleft'];
	var strArr = ['prod', 'servType', 'propStat', 'vprStat', 'clientStat', 'occStat', 'assignedAssoc', 'propAdd',
					'city', 'state', 'municipalName', 'pod', 'client'];
	var dateArr = ['crtDate', 'followUp'];
	var customArr = ['client', 'occStat', 'sla', 'state', 'city', 'zip', 'totalAge', 'daysleft'];

	if ($.inArray(sort, intArr) !== -1) {
		order.empty();
		if ($.inArray(sort, customArr) !== -1) {
			order.append(
				'<option value="">--Select One--</option>' +
				'<option value="asc">Lowest to Highest</option>' +
				'<option value="desc">Highest to Lowest</option>' +
				'<option value="cust" >Custom</option>'
			);
		} else {
			order.append(
				'<option value="">--Select One--</option>' +
				'<option value="asc">Lowest to Highest</option>' +
				'<option value="desc">Highest to Lowest</option>'
			);
		}
	} else if ($.inArray(sort, strArr) !== -1) {
		order.empty();
		if ($.inArray(sort, customArr) !== -1) {
			order.append(
				'<option value="">--Select One--</option>' +
				'<option value="asc">A - Z</option>' +
				'<option value="desc">Z - A</option>' +
				'<option value="cust" >Custom</option>'
			);
		} else {
			order.append(
				'<option value="">--Select One--</option>' +
				'<option value="asc">A - Z</option>' +
				'<option value="desc">Z - A</option>'
			);
		}
	} else if ($.inArray(sort, dateArr) !== -1) {
		order.empty();
		if ($.inArray(sort, customArr) !== -1) {
			if (sort != 'daysleft') {
				order.append(
					'<option value="">--Select One--</option>' +
					'<option value="asc">Oldest to Newest</option>' +
					'<option value="desc">Newest to Oldest</option>' +
					'<option value="cust" >Custom</option>'
				);
			} else {
				order.append(
					'<option value="">--Select One--</option>' +
					'<option value="cust" >Custom</option>'
				);
			}
		} else {
			order.append(
				'<option value="">--Select One--</option>' +
				'<option value="asc">Oldest to Newest</option>' +
				'<option value="desc">Newest to Oldest</option>'
			);
		}
	} else {
		order.empty();
		order.append(
			'<option value="">--Select One--</option>' +
			'<option value="asc">A - Z</option>' +
			'<option value="desc">Z - A</option>' +
			'<option value="asc">Lowest to Highest</option>' +
			'<option value="desc">Highest to Lowest</option>' +
			'<option value="asc">Oldest to Newest</option>' +
			'<option value="desc">Newest to Oldest</option>' +
			'<option value="cust" >Custom</option>'
		);
	}
}

function orderBy(val) {
	var sort = $(val).closest('tr').find('td:eq(2) select').val();
	var order = $(val).val();
	var prioId = $('#prioId').text();

	orderb(sort, order, prioId, 'new');

}

function orderb(sort, order, prioId, tag) {
	console.log(sort, order, prioId, tag);
	if (order == 'cust' && sort != '' && tag != '') {
		$('#mainModal').hide();
		$('#modalNewAccess .modal-dialog').removeAttr('style');
	}

	if (order == 'cust' && sort != 'daysleft') {
		$('#custom1').show();
		$('#custom2').hide();

		if (sort == 'client') {
			$('#tblCustom1 tbody').empty();
			$('#tblCustom2 tbody').empty();
			$('#tblCustom1 tbody').append(
				'<tr onclick="clickRow(this);"><td>Ocwen</td></tr>' +
				'<tr onclick="clickRow(this);"><td>RESI</td></tr>' +
				'<tr onclick="clickRow(this);"><td>Capital One</td></tr>' +
				'<tr onclick="clickRow(this);"><td>Wells Fargo</td></tr>'
			);

			if (prioId != '') {
				$.ajax({
					type: 'POST',
					url: 'Prioritization.aspx/GetCustom',
					data: '{sort: "' + sort + '", prioId: "' + prioId + '"}',
					contentType: 'application/json; charset=utf-8',
					success: function (data) {
						//console.log(data);

						$.each(data.d, function (idx, val) {
							$('#tblCustom1 tbody tr').each(function (tblIdx, tblVal) {
								if ($(this).find('td').text() == val) {
									$('#tblCustom1 tbody tr:eq(' + tblIdx + ')').remove();

									$('#tblCustom2 tbody').append(
										'<tr onclick="clickRow(this);"><td>' + val + '</td></tr>'
									);
								}
							});
						});

					}, error: function (response) {
						console.log(response.responseText);
					}
				});
			}
		} else if (sort == 'occStat') {
			$('#tblCustom1 tbody').empty();
			$('#tblCustom2 tbody').empty();
			$('#tblCustom1 tbody').append(
				'<tr onclick="clickRow(this);"><td>Vacant</td></tr>' +
				'<tr onclick="clickRow(this);"><td>Occupied</td></tr>'
			);

			if (prioId != '') {
				$.ajax({
					type: 'POST',
					url: 'Prioritization.aspx/GetCustom',
					data: '{sort: "' + sort + '", prioId: "' + prioId + '"}',
					contentType: 'application/json; charset=utf-8',
					success: function (data) {
						//console.log(data);

						$.each(data.d, function (idx, val) {
							$('#tblCustom1 tbody tr').each(function (tblIdx, tblVal) {
								if ($(this).find('td').text() == val) {
									$('#tblCustom1 tbody tr:eq(' + tblIdx + ')').remove();

									$('#tblCustom2 tbody').append(
										'<tr onclick="clickRow(this);"><td>' + val + '</td></tr>'
									);
								}
							});
						});

					}, error: function (response) {
						console.log(response.responseText);
					}
				});
			}
		} else if (sort == 'sla') {
			$('#tblCustom1 tbody').empty();
			$('#tblCustom2 tbody').empty();
			$('#tblCustom1 tbody').append(
				'<tr onclick="clickRow(this);"><td>10 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>15 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>20 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>25 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>30 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>35 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>40 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>45 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>50 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>55 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>60 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>65 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>70 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>75 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>80 Days</td></tr>'
			);

			if (prioId != '') {
				$.ajax({
					type: 'POST',
					url: 'Prioritization.aspx/GetCustom',
					data: '{sort: "' + sort + '", prioId: "' + prioId + '"}',
					contentType: 'application/json; charset=utf-8',
					success: function (data) {
						//console.log(data);

						$.each(data.d, function (idx, val) {
							$('#tblCustom1 tbody tr').each(function (tblIdx, tblVal) {
								if ($(this).find('td').text() == val) {
									$('#tblCustom1 tbody tr:eq(' + tblIdx + ')').remove();

									$('#tblCustom2 tbody').append(
										'<tr onclick="clickRow(this);"><td>' + val + '</td></tr>'
									);
								}
							});
						});

					}, error: function (response) {
						console.log(response.responseText);
					}
				});
			}
		} else if (sort == 'State') {
			$('#tblCustom1 tbody').empty();
			$('#tblCustom2 tbody').empty();

			getState();

			if (prioId != '') {
				$.ajax({
					type: 'POST',
					url: 'Prioritization.aspx/GetCustom',
					data: '{sort: "' + sort + '", prioId: "' + prioId + '"}',
					contentType: 'application/json; charset=utf-8',
					success: function (data) {
						//console.log(data);

						$.each(data.d, function (idx, val) {
							$('#tblCustom1 tbody tr').each(function (tblIdx, tblVal) {
								if ($(this).find('td').text() == val) {
									$('#tblCustom1 tbody tr:eq(' + tblIdx + ')').remove();

									$('#tblCustom2 tbody').append(
										'<tr onclick="clickRow(this);"><td>' + val + '</td></tr>'
									);
								}
							});
						});

					}, error: function (response) {
						console.log(response.responseText);
					}
				});
			}
		} else if (sort == 'city') {
			$('#tblCustom1 tbody').empty();
			$('#tblCustom2 tbody').empty();

			getCity();

			if (prioId != '') {
				$.ajax({
					type: 'POST',
					url: 'Prioritization.aspx/GetCustom',
					data: '{sort: "' + sort + '", prioId: "' + prioId + '"}',
					contentType: 'application/json; charset=utf-8',
					success: function (data) {
						//console.log(data);

						$.each(data.d, function (idx, val) {
							$('#tblCustom1 tbody tr').each(function (tblIdx, tblVal) {
								if ($(this).find('td').text() == val) {
									$('#tblCustom1 tbody tr:eq(' + tblIdx + ')').remove();

									$('#tblCustom2 tbody').append(
										'<tr onclick="clickRow(this);"><td>' + val + '</td></tr>'
									);
								}
							});
						});

					}, error: function (response) {
						console.log(response.responseText);
					}
				});
			}
		} else if (sort == 'Zip code') {
			$('#tblCustom1 tbody').empty();
			$('#tblCustom2 tbody').empty();

			getZip();

			if (prioId != '') {
				$.ajax({
					type: 'POST',
					url: 'Prioritization.aspx/GetCustom',
					data: '{sort: "' + sort + '", prioId: "' + prioId + '"}',
					contentType: 'application/json; charset=utf-8',
					success: function (data) {
						//console.log(data);

						$.each(data.d, function (idx, val) {
							$('#tblCustom1 tbody tr').each(function (tblIdx, tblVal) {
								if ($(this).find('td').text() == val) {
									$('#tblCustom1 tbody tr:eq(' + tblIdx + ')').remove();

									$('#tblCustom2 tbody').append(
										'<tr onclick="clickRow(this);"><td>' + val + '</td></tr>'
									);
								}
							});
						});

					}, error: function (response) {
						console.log(response.responseText);
					}
				});
			}
		} else if (sort == 'Aging') {
			$('#tblCustom1 tbody').empty();
			$('#tblCustom2 tbody').empty();
			$('#tblCustom1 tbody').append(
				'<tr onclick="clickRow(this);"><td>1 Day</td></tr>' +
				'<tr onclick="clickRow(this);"><td>2 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>3 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>4 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>5 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>6 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>7 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>8 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>9 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>10 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>11 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>12 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>13 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>14 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>15 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>16 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>17 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>18 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>19 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>20 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>21 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>22 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>23 Days</td></tr>' +
				'<tr onclick="clickRow(this);"><td>24 Days</td></tr>'
			);

			if (prioId != '') {
				$.ajax({
					type: 'POST',
					url: 'Prioritization.aspx/GetCustom',
					data: '{sort: "' + sort + '", prioId: "' + prioId + '"}',
					contentType: 'application/json; charset=utf-8',
					success: function (data) {
						//console.log(data);

						$.each(data.d, function (idx, val) {
							$('#tblCustom1 tbody tr').each(function (tblIdx, tblVal) {
								if ($(this).find('td').text() == val) {
									$('#tblCustom1 tbody tr:eq(' + tblIdx + ')').remove();

									$('#tblCustom2 tbody').append(
										'<tr onclick="clickRow(this);"><td>' + val + '</td></tr>'
									);
								}
							});
						});

					}, error: function (response) {
						console.log(response.responseText);
					}
				});
			}
		}

	} else if (order == 'cust' && sort == 'daysleft') {
		$('#custom1').hide();
		$('#custom2').show();

	}
	if (sort == 'Utility Service Provider') {
	  
	    $('#tblCustom1 tbody').empty();
	    $('#tblCustom2 tbody').empty();
	    $('#tblCustom1 tbody').append(
            '<tr onclick="clickRow(this);" ><td>Duke Energy</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Florida Power and Light</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>ComEd</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Baltimore Gas and Electric</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>National Grid</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>PSE and G</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Nicor Gas</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>PG and E</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>DTE Energy</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Eversource</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Consumers Energy</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Columbia Gas</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>TXU Energy</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Puget Sound Energy</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Centerpoint Energy</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Dominion Virginia Power</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Xcel Energy</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>TECO Peoples Gas </td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Atmos Energy Corp</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Southern California Gas</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>PECO</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>AEP</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Southwest Gas</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Ameren Illinois</td> </tr>' +
        '<tr onclick="clickRow(this);"><td>Georgia Power</td> </tr>'
        );
	    var asc = 'asc';
	    tbody = $("#tblCustom1").find('tbody');

	    tbody.find('tr').sort(function (a, b) {
	        if (asc) {
	            return $('td:first', a).text().localeCompare($('td:first', b).text());
	        } else {
	            return $('td:first', b).text().localeCompare($('td:first', a).text());
	        }
	    }).appendTo(tbody);
	}
}

function getState() {
	//$('#modalLoading').modal('show');
	$.ajax({
		type: 'POST',
		url: 'Workable.aspx/GetState',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var d = $.parseJSON(data.d);

			if (d.Success) {
				var records = d.data.record;
				//console.log(records);
				$('#tblCustom1 tbody').empty();
				$.each(records, function (idx, val) {
					$('#tblCustom1 tbody').append(
						'<tr onclick="clickRow(this);">' +
							'<td>' + val.State + '</td>' +
						'</tr>'
					);
				});
			}
			//$('#modalLoading').modal('hide');
		}, error: function (response) {
			console.log(response.responseText);
		}, failure: function (response) {
			console.log(response.responseText);
		}
	});
}

function getCity() {
	//$('#modalLoading').modal('show');
	$.ajax({
		type: 'POST',
		url: 'Workable.aspx/GetCity',
		data: '{state: ""}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var d = $.parseJSON(data.d);

			if (d.Success) {
				var records = d.data.record;
				$('#tblCustom1 tbody').empty();
				$.each(records, function (idx, val) {
					$('#tblCustom1 tbody').append(
						'<tr onclick="clickRow(this);">' +
							'<td>' + val.City + '</td>' +
						'</tr>'
					);
				});
			}

			//$('#modalLoading').modal('hide');
		}, error: function (response) {
			console.log(response.responseText);
		}, failure: function (response) {
			console.log(response.responseText);
		}
	});
}

function getZip() {
	//$('#modalLoading').modal('show');
	$.ajax({
		type: 'POST',
		url: 'Workable.aspx/GetZip',
		data: '{city: ""}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var d = $.parseJSON(data.d);

			if (d.Success) {
				var records = d.data.record;
				//console.log(records);
				$('#tblCustom1 tbody').empty();
				$.each(records, function (idx, val) {
					$('#tblCustom1 tbody').append(
						'<tr onclick="clickRow(this);">' +
							'<td>' + val.ZipCode + '</td>' +
						'</tr>'
					);
				});
			}

			//$('#modalLoading').modal('hide');
		}, error: function (response) {
			console.log(response.responseText);
		}, failure: function (response) {
			console.log(response.responseText);
		}
	});
}

$('.btnSave').click(function () {
	$('#mainModal').show();
	$('#modalNewAccess .modal-dialog').attr('style', 'width: 80%; height: 900px');
	$('#custom1').hide();
	$('#custom2').hide();
});

function clickRow(val) {
	if ($(val).hasClass('activeRow')) {
		$(val).removeClass('activeRow');
	} else {
		$(val).addClass('activeRow');
	}
}

function leftArrow() {
	$.each($('#tblCustom2 tbody tr.activeRow'), function (idx, val) {
		$(this).removeClass('activeRow')
		$('#tblCustom1 tbody').append(val);
	});
}

function rightArrow() {
	$.each($('#tblCustom1 tbody tr.activeRow'), function (idx, val) {
		$(this).removeClass('activeRow')
		$('#tblCustom2 tbody').append(val);


	});
}

function allActive() {
	$('#tblCustom1 tbody tr').addClass('activeRow');
}

function clearActive() {
	$('#tblCustom1 tbody tr').removeClass('activeRow');
}

$('#lnkAddNew').click(function () {
	loadPrioList();
});

function loadPrioList() {
	$('#tblPrioList tbody').append(
		'<tr>' +
			'<td><input data-id="" class="chkBoxPrio iCheck" type="checkbox" /></td>' +
			'<td>Sort By</td>' +
			'<td>' +
				'<select class="form-control input-sm slctSort" onchange="sortBy(this);">' +
					'<option value="">--Select One--</option>' +
					'<option value="Aging">Aging</option>' +
					'<option value="Boarded Date">Boarded Date</option>' +
					'<option value="City">City</option>' +
					'<option value="Clients">Clients</option>' +
					'<option value="Due Date">Due Date</option>' +
					'<option value="Occupancy Status">Occupancy Status</option>' +
					'<option value="Pod Name">Pod Name</option>' +
					'<option value="Property ID">Property ID</option>' +
					'<option value="REOStatus">REOStatus</option>' +
					'<option value="SLA">SLA</option>' +
					'<option value="State">State</option>' +
					'<option value="username">username</option>' +
                    '<option value="Utility Service Provider">Utility Service Provider</option>' +
					'<option value="Utility Status">Utility Status</option>' +
					'<option value="Vendor Info">Vendor Info</option>' +
					'<option value="WSTRAT Date">WSTRAT Date</option>' +
					'<option value="Zip code">Zip code</option>' +
				'</select>' +
			'</td>' +
			'<td>Order by</td>' +
			'<td>' +
				'<select class="form-control input-sm slctOrder" onchange="orderBy(this);">' +
					'<option value="">--Select One--</option>' +
					'<option value="asc">A - Z</option>' +
					'<option value="desc">Z - A</option>' +
					'<option value="asc">Lowest to Highest</option>' +
					'<option value="desc">Highest to Lowest</option>' +
					'<option value="asc">Oldest to Newest</option>' +
					'<option value="desc">Newest to Oldest</option>' +
					'<option value="cust">Custom</option>' +
				'</select>' +
			'</td>' +
			//'<td>Applied To</td>' +
			//'<td>' +
			//	'<select class="form-control input-sm slctApplied">' +
			//		'<option value="">--Select One--</option>' +
			//		'<option value="pdf">PDF</option>' +
			//		'<option value="website">Website (Manual)</option>' +
			//		'<option value="prochamp">Prochamp</option>' +
			//		'<option value="email">Email</option>' +
			//	'</select>' +
			//'</td>' +
			'<td>' +
				'<input type="checkbox" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />' +
			'</td>' +
		'</tr>'
	);

	$('.toggle').bootstrapToggle('destroy');
	$('.toggle').bootstrapToggle({
		on: 'Active',
		off: 'Inactive'
	});

	//iCheck for checkbox and radio inputs
	$('.iCheck').iCheck({
		checkboxClass: 'icheckbox_flat-blue',
		radioClass: 'iradio_flat-blue'
	});
}

$('#btnNew_Type').click(function () {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });

	var prioId = $('#prioId').text();
	var title = $('#titleId').val();
	var desc = $('#descId').val();
	var loca = $('#locationId').closest('span').find('div button').attr('title');
	var bunit = $('#buId').closest('span').find('div button').attr('title');
	var level = $('#levelId').closest('span').find('div button').attr('title');
	var bseg = $('#bsId').closest('span').find('div button').attr('title');
	var acton = $('#actonId').val();
	var actil = $('#actuntilId').val();
	var keepact = $('#keepactId option:selected').text();
	var userid = $('#usersId').closest('span').find('div button').attr('title');
	var dne = $('#dneChk').is(':checked') ? "1" : "0";
	var cby = $('#user').val();
	var prioMain = {
		'id': prioId,
		'title': title,
		'description': desc,
		'location': loca,
		'level': level,
		'bunit': bunit,
		'bsegment': bseg,
		'actOn': acton,
		'actUntil': actil,
		'keepAct': keepact,
		'users': userid,
		'do_not_end': dne,
		'created_by': cby
	}
	console.log(prioMain);

	$.ajax({
		type: 'POST',
		url: 'Prioritization.aspx/SaveMainPrio',
		data: '{priomain:' + JSON.stringify(prioMain) + '}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			$.each($('#tblPrioList tbody tr'), function (idx, val) {

				var prioID = $(this).find('.chkBoxPrio').attr('data-id');
				var active;
				if ($(this).find('.chkBoxActive').is(':checked')) {
					active = "True";
				} else {
					active = "False";
				}

				var sort = $(this).find('.slctSort option:selected').val();
				var order = $(this).find('.slctOrder option:selected').val();
				var orderArr = "";
				if (order != 'cust') {
					orderArr = order;
				} else {
					if (sort != 'daysleft') {
						$.each($('#tblCustom2 tbody tr'), function () {
							orderArr += $(this).find('td').text() + ',';
						});

						orderArr = orderArr.slice(0, -1);
					} else {
						orderArr += $('#tblCustDays tbody tr td input').text() + ',';
						$.each($('#tblCustDays tbody tr td select'), function () {
							orderArr += $(this).val() + ',';
						});

						orderArr = orderArr.slice(0, -1);
					}
				}
				//var applied = $(this).find('.slctApplied option:selected').val();

				$.ajax({
					type: 'POST',
					url: 'Prioritization.aspx/SavePrio',
					data: '{prioID: "' + prioID + '", active: "' + active + '", sort: "' + sort + '", order: "' + orderArr + '"}',
					contentType: 'application/json; charset=utf-8',
					success: function (data) {
						loadPrio('');
						
					}, error: function (response) {
						console.log(response.responseText);
					}, failure: function (response) {
						console.log(response.responseText);
					}
				});
			});
			$('#modalLoading').modal('hide');
			$('#modalNewAccess').modal('hide');

		}, error: function (response) {
		    console.log(response.responseText);
		    
		}, failure: function (response) {
			console.log(response.responseText);
		}
	});
});

function loadPrio(prioId) {

	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	$('#prioId').text(prioId);

	$.ajax({
		type: 'POST',
		url: 'Prioritization.aspx/loadPrio',
		data: '{prioId: "' + prioId + '"}',
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var obj1 = data.d.tblPrioMain;
			var obj2 = data.d.tblPrio;
			console.log(data);

			if (prioId == '') {
				var toAppend = "";
				$('#bdtblPrio').empty();
				for (var i = 0; i < obj1.length; i++) {
					toAppend = "<tr>";
					toAppend += "<td><input type='checkbox' id='chk_" + obj1[i].id + "' class='chkRow1 iCheck'/></td>";
					toAppend += "<td>" + obj1[i].title + "</td>";
					toAppend += "<td>" + obj1[i].description + "</td>";
					toAppend += "<td>" + obj1[i].created_by + "</td>";
					toAppend += "<td>" + obj1[i].created_date + "</td>";
					toAppend += "<td><button onclick='updatePrio(this);' type='button' data-id='" + obj1[i].id + "' class='btn btn-success'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button><button type='button' data-id='" + obj1[i].id + "' onclick='deletePrio(this);' class='btn btn-danger delBtn' style='margin-left: 10%;'><i class='fa fa-trash-o'  aria-hidden='true'></i></button></td></tr>";

					$('#bdtblPrio').append(toAppend);
					//console.log(toAppend);
				}
			} else {
				$('#titleId').val(obj1[0].title);
				$('#descId').text(obj1[0].description);

				if (obj1[0].location.length > 0) {
					var locArr = obj1[0].location.split(',');

					$.each(locArr, function (idx, val) {
						$.each($('#locationId option'), function () {
							if ($(this).text() == $.trim(val)) {
								$(this).prop('selected', true);
							}
						});
					});
				}

				if (obj1[0].bunit.length > 0) {
					var buArr = obj1[0].bunit.split(',');

					$.each(buArr, function (idx, val) {
						$.each($('#buId option'), function () {
							if ($.trim($(this).text()) == $.trim(val)) {
								$(this).prop('selected', true);
							}
						});
					});
				}

				if (obj1[0].level.length > 0) {
					var levelArr = obj1[0].level.split(',');

					$.each(levelArr, function (idx, val) {
						$.each($('#levelId option'), function () {
							if ($.trim($(this).text()) == $.trim(val)) {
								$(this).prop('selected', true);
							}
						});
					});
				}

				if (obj1[0].bsegment.length > 0) {
					var bsArr = obj1[0].bsegment.split(',');

					$.each(bsArr, function (idx, val) {
						$.each($('#bsId option'), function () {
							if ($.trim($(this).text()) == $.trim(val)) {
								$(this).prop('selected', true);
							}
						});
					});
				}

				$('#locationId, #levelId, #usersId, #keepactId').multiselect('destroy');

				$('#locationId, #levelId, #usersId, #keepactId').multiselect({
				    numberDisplayed: 0,
				    includeSelectAllOption: true,
				    enableFiltering: true,
				    maxHeight: 200
				});

				$('#buId').multiselect();

				$('#bsId').multiselect({
					numberDisplayed: 1,
					includeSelectAllOption: true,
					maxHeight: 150,
					onChange: function (element, checked) {
						var ct = $('#bsId option').length;
						var segment = $('#bsId option:selected');
						console.log();
						if (segment.length < ct) {
							if (segment.length > 1) {
								var selected = [];
								$(segment).each(function (index, bs) {
									selected.push([$(this).val()]);
								});
								getBU(selected);
							} else if (segment.length == 0) {
								getBU('');
							} else {
								var selected = [];
								$(segment).each(function (index, bs) {
									selected.push([$(this).val()]);
								});
								getBU(selected);
							}
						} else {
							var selected = [];
							$(segment).each(function (index, bs) {
								selected.push([$(this).val()]);
							});
							getBU(selected);
						}
					},
					onSelectAll: function () {
						var segment = $('#bsId option:selected');
						var selected = [];
						$(segment).each(function (index, bs) {
							selected.push([$(this).val()]);
						});
						getBU(selected);
					},
					onDeselectAll: function () {
						getBU('');
					},
					onDropdownShow: function () {

					}
				});

				$('#actonId').val(obj1[0].actOn);
				$('#actuntilId').val(obj1[0].actUntil);
				$('#keepactId').val(obj1[0].keepAct);

				if (obj1[0].users.length > 0) {
					var usersArr = obj1[0].users.split(',');

					$.each(usersArr, function (idx, val) {
						$.each($('#usersId option'), function () {
							if ($.trim($(this).text()) == $.trim(val)) {
								$(this).prop('selected', true);
							}
						});
					});
				}

				var dne = obj1[0].do_not_end;
				if (dne == 'true') {
					$('#dneChk').prop('checked', true);
				} else {
					$('#dneChk').prop('checked', false);
				}

				$('#tblPrioList tbody').empty();
				for (var i = 0; i < obj1.length; i++) {
					loadPrioList();
					$('.slctSort:eq(' + i + ')').val(obj2[i].sortBy);

					sortb(obj2[i].sortBy, i)

					var orderArr = ['client', 'occStat', 'sla', 'state', 'city', 'zip', 'totalAge', 'daysleft'];
					if ($.inArray(obj2[i].sortBy, orderArr) !== -1) {
						$('.slctOrder:eq(' + i + ')').val('cust');
						//orderb(obj2[i].sortBy, 'cust', '', '');
					} else {
						$('.slctOrder:eq(' + i + ')').val(obj2[i].orderBy);
						//orderb(obj2[i].sortBy, obj2[i].orderBy, '', '');
					}
					//alert(obj2[i].isActive);
					if (obj2[i].isActive == 'True') {
						//alert(i);
						$('#tblPrioList tbody tr:eq(' + i + ') td:eq(5) input.toggle').bootstrapToggle('toggle');
					} else {

					}
				}

			}

			$('.iCheck').iCheck({
				checkboxClass: 'icheckbox_flat-blue',
				radioClass: 'iradio_flat-blue'
			});

			$('#modalLoading').modal('hide');

		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});
}

function updatePrio(val) {
	var prioId = $(val).attr('data-id');

	$('#modalNewAccess').modal('show');
	$('#modalNewAccess .modal-dialog').attr('style', 'width: 80%; height: 900px');
	$('#mainModal').show();
	$('#custom1').hide();
	$('#custom2').hide();

	$('.datepicker').datepicker({
		autoclose: true
	});

	$('.iCheck').iCheck({
		checkboxClass: 'icheckbox_flat-blue',
		radioClass: 'iradio_flat-blue'
	});

	loadPrio(prioId);

}

function deletePrio(val) {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	var prioId = $(val).attr('data-id');

	$.ajax({
		type: 'POST',
		url: 'Prioritization.aspx/DeletePrio',
		data: '{prioId: "' + prioId + '"}',
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			console.log(data);

			loadPrio('');

			$('#modalLoading').modal('hide');
		}, error: function (response) {
			console.log(response.responseText);
		}
	});
}

$('#newPrioSet').click(function () {
	$('#modalNewAccess').modal('show');
	$('#modalNewAccess .modal-dialog').attr('style', 'width: 80%; height: 900px');


	$('.datepicker').datepicker({
		autoclose: true
	});

	$('.iCheck').iCheck({
		checkboxClass: 'icheckbox_flat-blue',
		radioClass: 'iradio_flat-blue'
	});

	$('#locationId, #levelId, #usersId, #keepactId').multiselect('destroy');
	//$('#buId').multiselect();
	//$('#locationId, #buId, #levelId, #bsId').multiselect({
	//	numberDisplayed: 1,
	//	includeSelectAllOption: true
	//});

	$('#locationId, #levelId, #usersId, #keepactId').multiselect({
		numberDisplayed: 1,
		includeSelectAllOption: true,
	});

	$('#buId').multiselect();

	$('#bsId').multiselect({
		numberDisplayed: 1,
		includeSelectAllOption: true,
		maxHeight: 150,
		onChange: function (element, checked) {
			var ct = $('#bsId option').length;
			var segment = $('#bsId option:selected');
			console.log();
			if (segment.length < ct) {
				if (segment.length > 1) {
					var selected = [];
					$(segment).each(function (index, bs) {
						selected.push([$(this).val()]);
					});
					getBU(selected);
				} else if (segment.length == 0) {
					getBU('');
				} else {
					var selected = [];
					$(segment).each(function (index, bs) {
						selected.push([$(this).val()]);
					});
					getBU(selected);
				}
			} else {
				var selected = [];
				$(segment).each(function (index, bs) {
					selected.push([$(this).val()]);
				});
				getBU(selected);
			}
		},
		onSelectAll: function () {
			var segment = $('#bsId option:selected');
			var selected = [];
			$(segment).each(function (index, bs) {
				selected.push([$(this).val()]);
			});
			getBU(selected);
		},
		onDeselectAll: function () {
			getBU('');
		},
		onDropdownShow: function () {

		}
	});

	$('#mainModal input[type="text"], #mainModal textarea').val('');

	$('#mainModal select').each(function () {
		$(this).find('option:first-child').prop('selected', true);
	});

	$('#tblPrioList tbody').empty();

	$('#prioId').text('');

	$('#mainModal').show();
	$('#custom1').hide();
	$('#custom2').hide();

});

function getBU(segment) {
	$.ajax({
		type: 'POST',
		url: 'Prioritization.aspx/GetBU',
		data: '{segment: "' + segment + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {

			$('#buId').multiselect('destroy');
			$('#buId').empty();

			var d = $.parseJSON(data.d);

			if (d.Success) {
				var records = d.data.record;
				$.each(records, function (idx, val) {
					$('#buId').append(
						'<option>' + val.bu + '</option>'
					);
				});
			}

			$('#buId').multiselect();

		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});
}
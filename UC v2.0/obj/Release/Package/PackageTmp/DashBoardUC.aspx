﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DashBoardUC.aspx.cs" Inherits="UC_v2._0.DashBoardUC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <br />
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Queue Monitoring</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Historical</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="panel panel-primary" style="background-color: #ecf0f5">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="info-box">
                                                <span class="info-box-icon bg-aqua"><i class="fa fa-line-chart"></i></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">Total Workable Count </span>
                                                    <span class="info-box-number" id="txtCount"></span>
                                                    <span class="info-box-number" id="txtDatetoday"></span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>




                                            <table id="tblTotal" class="table" style="margin-bottom: 5px; display: none">
                                                <thead>
                                                    <tr>
                                                        <th data-field="DateNow">Date</th>
                                                        <th data-field="sum">Total Workable Count</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="info-box bg-green">
                                                <span class="info-box-icon"><i class="fa fa-envelope-o"></i></span>

                                                <div class="info-box-content">

                                                    <span class="info-box-text">Bulk Email | Workable Count</span>
                                                    <span class="info-box-number" id="txtWCount"></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 100%"></div>
                                                    </div>
                                                    <span class="progress-description" id="txtCountLeft"></span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>

                                            <div class="info-box bg-green">
                                                <span class="info-box-icon"><i class="fa fa-rss"></i></span>

                                                <div class="info-box-content">

                                                    <span class="info-box-text">Online Request | Workable Count</span>
                                                    <span class="info-box-number" id="txtWCount2"></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 100%"></div>
                                                    </div>
                                                    <span class="progress-description" id="txtCountLeft2"></span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                            <div class="info-box bg-green">
                                                <span class="info-box-icon"><i class="fa fa-phone"></i></span>

                                                <div class="info-box-content">

                                                    <span class="info-box-text">Callout | Workable Count</span>
                                                    <span class="info-box-number" id="txtWCount3"></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 100%"></div>
                                                    </div>
                                                    <span class="progress-description" id="txtCountLeft3"></span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div id="chartContainer" style="height: 370px; width: 100%; "></div>
                                        </div>


                                        <div class="col-sm-3" style="display:none">
                                            <div class="box box-widget widget-user-2">
                                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                                <div class="widget-user-header bg-aqua">

                                                    <!-- /.widget-user-image -->
                                                    <h3 class="widget-user-username" style="margin-left: 1px">Start Service</h3>

                                                </div>
                                                <div class="box-footer no-padding">
                                                    <ul class="nav nav-stacked">
                                                        <li><a href="#">Bulk Email <span class="pull-right badge bg-blue" id="txtBulkEmail"></span>
                                                        </a></li>
                                                        <li><a href="#">Online Request <span class="pull-right badge bg-aqua" id="txtOnline"></span></a></li>
                                                        <li><a href="#">Callout <span class="pull-right badge bg-green" id="txtCall"></span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3" style="display:none">
                                            <div class="box box-widget widget-user-2">
                                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                                <div class="widget-user-header bg-aqua">
                                                    <!-- /.widget-user-image -->
                                                    <h3 class="widget-user-username" style="margin-left: 1px">Stop Service</h3>
                                                </div>
                                                <div class="box-footer no-padding">
                                                    <ul class="nav nav-stacked">
                                                        <li><a href="#">Bulk Email <span class="pull-right badge bg-blue" id="txtBulkEmail2"></span></a></li>
                                                        <li><a href="#">Online Request <span class="pull-right badge bg-aqua" id="txtOnline2"></span></a></li>
                                                        <li><a href="#">Callout <span class="pull-right badge bg-green" id="txtCall2"></span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <br />
                                    <br />


                                    <%-- <div class="progress" width="20">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                            <h4 class="modal-title" id="myModalLabel">
                                                <p id="demo"></p>
                                                %</h4>
                                        </div>
                                    </div>--%>
                                    <table id="tblDashBoard" class="table" style="margin-bottom: 5px; background-color: white">
                                        <thead>
                                            <tr>
                                                <th data-field="Solution">Solution</th>
                                                <th data-field="WorkableCount">Workable Count</th>
                                                <th data-field="WorkableCountLeft">Workable Count Left</th>
                                                <th data-field="StartService">Start Service</th>
                                                <th data-field="StartServiceAge">Start Service Age</th>
                                                <th data-field="StopService">Stop Service</th>
                                                <th data-field="StopServiceAge">Stop Service Age</th>
                                                <th data-field="Solution" class="text-center" data-formatter="Show"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>

                        <div class="tab-pane " id="tab_2">
                            <div class="panel panel-primary" style="background-color: #ecf0f5">
                                <div class="panel-body">
                                    <div class="row">
                                        <br />
                                        <div class="row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-1 text-right">
                                            </div>
                                            <div class="col-lg-2">
                                                Date :
                                                <asp:TextBox ID="txtDate" class="form-control" runat="server" ClientIDMode="Static" TextMode="Date"></asp:TextBox>
                                            </div>

                                            <div class="col-lg-2">
                                                Process :
                                                <asp:DropDownList ID="ddlProcess" ClientIDMode="Static" class="form-control" runat="server">
                                                    <asp:ListItem>All</asp:ListItem>
                                                    <asp:ListItem>Bulk Email</asp:ListItem>
                                                    <asp:ListItem>Online Request</asp:ListItem>
                                                    <asp:ListItem>Callout</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-lg-2">
                                                <br />
                                                <button type="button" class="btn btn-success form-control" id="btnSearch">Search</button>
                                            </div>
                                        </div>


                                        <br />
                                        <table id="tblHistorical" class="table" style="margin-bottom: 5px" data-show-export="true" data-page-list="[10, 25, 50, 100, ALL]">
                                            <thead>
                                                <tr>
                                                    <th data-field="ProcessType" data-sortable="true"></th>
                                                    <th data-field="Client Status" data-sortable="true">Client Status</th>
                                                    <th data-field="VendorId" data-sortable="true">VendorId</th>
                                                    <th data-field="USP" data-sortable="true">USP</th>
                                                    <th data-field="Address" data-sortable="true">Address</th>
                                                    <th data-field="Property ID" data-sortable="true">Property ID</th>
                                                    <th data-field="Utility" data-sortable="true">Utility</th>
                                                    <th data-field="InflowDate" data-sortable="true">InflowDate</th>
                                                    <th data-field="Status" data-sortable="true">Status</th>
                                                    <th data-field="PreviousStatus" data-sortable="true">PreviousStatus</th>
                                                    <th data-field="REOStatus" data-sortable="true">REOStatus</th>
                                                    <th data-field="PostedBy" data-sortable="true">PostedBy</th>
                                                    <th data-field="PostedDate" data-sortable="true">PostedDate</th>
                                                    <th data-field="Aging" data-sortable="true">Aging</th>
                                                    <th data-field="Skip" data-sortable="true">Skip</th>
                                                    <th data-field="SkipReason" data-sortable="true">SkipReason</th>
                                                    <th data-field="UpdateFlag" data-sortable="true">UpdateFlag</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal fade" id="myModalEdit" role="dialog">
                    <div class="modal-dialog modal-lg" style="margin-left: 140px">
                        <div class="modal-content" style="width: 1400px">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title" style="text-align: center">Workable USP</h4>
                            </div>
                            <div class="modal-body">
                                <table id="tblBulk" class="table" style="margin-bottom: 5px" data-show-export="true" data-page-list="[10, 25, 50, 100, ALL]">
                                    <thead>
                                        <tr>
                                            <th data-field="ProcessType" data-sortable="true"></th>
                                            <th data-field="Process" data-sortable="true">Process</th>
                                            <th data-field="VendorId" data-sortable="true">VendorId</th>
                                            <th data-field="USP" data-sortable="true">USP</th>
                                            <th data-field="Address" data-sortable="true">Address</th>
                                            <th data-field="Property ID" data-sortable="true">Property ID</th>
                                            <th data-field="Utility" data-sortable="true">Utility</th>
                                            <th data-field="Aging" data-sortable="true">Aging</th>
                                            <th data-field="InflowDate" data-sortable="true">InflowDate</th>
                                            <th data-field="Status" data-sortable="true">Status</th>
                                            <th data-field="Workable" data-sortable="true">Workable</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer" style="text-align: center">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script src="js/DashBoardUC.js"></script>
    
</asp:Content>

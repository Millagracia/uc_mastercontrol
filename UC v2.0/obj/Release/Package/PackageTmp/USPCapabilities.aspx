﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="USPCapabilities.aspx.cs" Inherits="UC_v2._0.USPCapabilities" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<style>
        /* 
Inspired by http://dribbble.com/shots/890759-Ui-Kit-Metro/attachments/97174
*/
        *, *:before, *:after {
            /* Chrome 9-, Safari 5-, iOS 4.2-, Android 3-, Blackberry 7- */
            -webkit-box-sizing: border-box;
            /* Firefox (desktop or Android) 28- */
            -moz-box-sizing: border-box;
            /* Firefox 29+, IE 8+, Chrome 10+, Safari 5.1+, Opera 9.5+, iOS 5+, Opera Mini Anything, Blackberry 10+, Android 4+ */
            box-sizing: border-box;
        }

        body {
            background: url(http://habrastorage.org/files/90a/010/3e8/90a0103e8ec749c4843ffdd8697b10e2.jpg);
            text-align: center;
            padding-top: 40px;
        }

        .btn-nav {
            background-color: #fff;
            border: 1px solid #e0e1db;
            -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
            -moz-box-sizing: border-box; /* Firefox, other Gecko */
            box-sizing: border-box; /* Opera/IE 8+ */
        }

            .btn-nav:hover {
                color: #016023;
                cursor: pointer;
                -webkit-transition: color 1s; /* For Safari 3.1 to 6.0 */
                transition: color 1s;
            }

            .btn-nav.active {
                color: #016023;
                padding: 2px;
                border-top: 6px solid #016023;
                border-bottom: 6px solid #016023;
                border-left: 0;
                border-right: 0;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                -webkit-transition: border 0.3s ease-out, color 0.3s ease 0.5s;
                -moz-transition: border 0.3s ease-out, color 0.3s ease 0.5s;
                -ms-transition: border 0.3s ease-out, color 0.3s ease 0.5s; /* IE10 is actually unprefixed */
                -o-transition: border 0.3s ease-out, color 0.3s ease 0.5s;
                transition: border 0.3s ease-out, color 0.3s ease 0.5s;
                -webkit-animation: pulsate 1.2s linear infinite;
                animation: pulsate 1.2s linear infinite;
            }

                .btn-nav.active:before {
                    content: '';
                    position: absolute;
                    border-style: solid;
                    border-width: 6px 6px 0;
                    border-color: #016023 transparent;
                    display: block;
                    width: 0;
                    z-index: 1;
                    margin-left: -6px;
                    top: 0;
                    left: 50%;
                }

            .btn-nav .glyphicon {
                padding-top: 16px;
                font-size: 27px;
            }

            .btn-nav.active p {
                margin-bottom: 8px;
            }

        @-webkit-keyframes pulsate {
            50% {
                color: #000;
            }
        }

        @keyframes pulsate {
            50% {
                color: #000;
            }
        }

        @media (max-width: 480px) {
            .btn-group {
                display: block !important;
                float: none !important;
                width: 100% !important;
                max-width: 100% !important;
            }
        }

        @media (max-width: 600px) {
            .btn-nav .glyphicon {
                padding-top: 12px;
                font-size: 26px;
            }
        }
    </style>--%>

    <style>
        tbody {
            display: block;
            height: 350px;
            overflow: auto;
        }

            thead, tbody tr {
                display: table;
                width: 100%;
                table-layout: fixed;
            }

        thead {
            width: calc( 100% - 1em );
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="content-wrapper" style="min-height: 733px">
        <div class="content-header">
            <h1>General Workflow Settings</h1>
            <ol class="breadcrumb">
                <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">
                    <span><i class="fa fa-sitemap"></i>&nbsp;USP Capabilities</span>
                </li>
            </ol>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_3" data-toggle="tab" aria-expanded="true">Eligible Client Statuses</a></li>
                        <li class=""><a href="#tab_2" id="Tusp" data-toggle="tab" aria-expanded="false">USP Capabilities</a></li>
                        <li class=""><a href="#tab_1" id="Tprio" data-toggle="tab" aria-expanded="true">Prioritization</a></li>
                        <li class=""><a href="#tab_4" id="Tother" data-toggle="tab" aria-expanded="true">Other Settings</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_3">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <div class="row form-group">
                             
                                        


                                        <div class="col-md-12">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a href="#reo" data-toggle="tab" aria-expanded="false">REO</a></li>
                                                <li class=""><a href="#resi" id="xTstop" data-toggle="tab" aria-expanded="true">RESI</a></li>
                                                <li class=""><a href="#pfc" id="xTinvoice" data-toggle="tab" aria-expanded="true">PFC</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <br />
                                                <div class="tab-pane active" id="reo">
                                                    <table id="tblREO" class="table" style="margin-bottom: 5px" data-show-footer="false">
                                                        <thead>
                                                            <tr>
                                                                <th data-field="Client_Status">Client Status</th>
                                                                <th data-field="Start_Service" class="text-center" data-formatter="chkStart">Start Service</th>
                                                                <th data-field="Stop_Service" class="text-center" data-formatter="chkStop">Stop Service</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="resi">
                                                    <table id="tblResi" class="table" style="margin-bottom: 5px" data-show-footer="false">
                                                        <thead>
                                                            <tr>
                                                                <th data-field="Client_Status">Client </th>
                                                                <th data-field="Start_Service" class="text-center" data-formatter="chkStart">Start Service</th>
                                                                <th data-field="Stop_Service" class="text-center" data-formatter="chkStop">Stop Service</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="tab-pane " id="pfc">
                                                    <table id="tblpfc" class="table" style="margin-bottom: 5px" data-show-footer="false">
                                                        <thead>
                                                            <tr>
                                                                <th data-field="Client_Status">Client </th>
                                                                <th data-field="Start_Service" class="text-center" data-formatter="chkStartPFC">Start Service</th>
                                                                <th data-field="Stop_Service" class="text-center" data-formatter="chkStopPFC">Stop Service</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="col-md-12">

                                            <%--<div class="row">
                                                        <div class="btn-group btn-group-justified">
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-nav active">
                                                                    <span class="glyphicon glyphicon-folder-close"></span>
                                                                    <p>Start Service</p>
                                                                </button>
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-nav">
                                                                     <span class="glyphicon glyphicon-folder-close"></span>
                                                                    <p>Stop Service</p>
                                                                </button>
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-nav active">
                                                                    <span class="glyphicon glyphicon-folder-close"></span>
                                                                    <p>Invoicing</p>
                                                                </button> 
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-nav">
                                                                    <span class="glyphicon glyphicon-folder-close"></span>
                                                                    <p>Bills Payment</p>
                                                                </button>
                                                            </div>
                                                           
                                                        </div>
                                                    </div>--%>
                                            <div class="text-right">
                                                   <button class="btn btn-primary btn-sm" onclick="tablesToExcel(['tblStartExcel','tblStopExcel','tblInvoiceExcel','tblBillsExcel'], ['Start Service','Stop Service','Invoicing','Bills Payment'], 'USP_Capabilities.xls', 'Excel')">Export All to Excel</button>
                                            </div>
                                         
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a href="#start" data-toggle="tab" aria-expanded="false">Start Service</a></li>
                                                <li class=""><a href="#stop" id="Tstop" data-toggle="tab" aria-expanded="true">Stop Service</a></li>
                                                <li class=""><a href="#invoicing" id="Tinvoice" data-toggle="tab" aria-expanded="true">Invoicing</a></li>
                                                <li class=""><a href="#bills" id="TBills" data-toggle="tab" aria-expanded="true">Bills Payment</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <div style="text-align: right">
                                                </div>
                                                <br />
                                                <div class="tab-pane active" id="start">

                                                    <table class="table table-bordered " id="tblStartService" data-search="true" data-height="499">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">USP Name </th>
                                                                <th class="text-center">Bulk Email </th>
                                                                <th class="text-center">Online Request</th>
                                                                <th class="text-center">EDI 814</th>
                                                                <th class="text-center">Call USP</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center"></th>
                                                                <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="stop">

                                                    <table class="table table-bordered " id="tblStop" data-height="499">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">USP Name </th>
                                                                <th class="text-center">Bulk Email </th>
                                                                <th class="text-center">Online Request</th>
                                                                <th class="text-center">EDI 814</th>
                                                                <th class="text-center">Call USP</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center"></th>
                                                                <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>

                                                            </tr>

                                                        </thead>

                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="tab-pane " id="invoicing">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <table class="table table-bordered" id="tblInvoicing" data-height="499">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">USP Name</th>
                                                                        <th class="text-center">Summary Billing</th>
                                                                        <th class="text-center">E Bill w/ Password</th>
                                                                        <th class="text-center">E Bill w/o Password</th>
                                                                        <th class="text-center">E-Bill Via Link</th>
                                                                        <th class="text-center">EDI 810</th>
                                                                        <th class="text-center">Paper Bill</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="text-center"></th>
                                                                        <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                        <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                        <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                        <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                        <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                        <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>

                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="tab-pane " id="bills">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <table class="table table-bordered" id="tblbills" data-height="499">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">USP Name</th>
                                                                        <th class="text-center">ACH</th>
                                                                        <th class="text-center">Wire Transfer</th>
                                                                        <th class="text-center">Credit Card</th>
                                                                        <th class="text-center">Check</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="text-center"></th>
                                                                        <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                        <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                        <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                        <th class="text-center">USP &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ASPS</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1">
                            <%--  <div class="nav-tabs-custom">--%>
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_2_1" data-toggle="tab" aria-expanded="false">Start Service</a></li>
                                <li><a href="#tab_2_2" data-toggle="tab" aria-expanded="false">Stop service</a></li>
                                <li><a href="#tab_2_3" data-toggle="tab" aria-expanded="false">Invoice</a></li>
                                <li><a href="#tab_2_4" data-toggle="tab" aria-expanded="false">Payments</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_2_1">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-8">
                                                            <table id="tblStartPRIO" class="table table-no-bordered" style="margin-bottom: 5px" data-show-footer="false">
                                                                <thead>
                                                                    <tr>
                                                                        <th data-field="Prio" data-width="150px">PRIORITIZATION </th>
                                                                        <th data-field="Method" data-width="150px">METHOD</th>
                                                                        <th data-field="Prio_ID" class="text-center" data-formatter="ddlPrio" data-width="300px">Qualified Utility Status</th>
                                                                        <th data-field="Prio_ID" data-formatter="btnSave"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <br />
                                                            <button type="button" id="AAA" onclick="Change(this)" class="btn btn-primary btn-sm">Set Priority</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 divADD">
                                                    <a class="lnkADDNEW" style="cursor: pointer; font-style: italic;"><i class="fa fa-plus"></i>&nbsp;Add New</a>
                                                    <a class="lnkMODIFY" style="cursor: pointer; font-style: italic;" class="modify"><i class="fa fa-edit"></i>&nbsp;Modify</a>
                                                </div>
                                                <div class="col-md-12 divSAVE" style="display: none;">
                                                    <a class="lnkSAVE" style="cursor: pointer; font-style: italic;"><i class="fa fa-save"></i>&nbsp;Save</a>
                                                    <a class="lnkCANCEL" style="cursor: pointer; font-style: italic;"><i class="fa fa-close"></i>&nbsp;Cancel</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_2_2">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-8">
                                                            <table id="tblStopPRIO" class="table table-no-bordered" style="margin-bottom: 5px" data-show-footer="false">
                                                                <thead>
                                                                    <tr>
                                                                        <th data-field="Prio" data-width="150px">PRIORITIZATION </th>
                                                                        <th data-field="Method" data-width="150px">METHOD</th>
                                                                        <th data-field="Prio_ID" class="text-center" data-formatter="ddlPrioStop" data-width="300px">Qualified Utility Status</th>
                                                                        <th data-field="Prio_ID" data-formatter="btnSaveStop"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <br />
                                                            <button type="button" id="btnStop" onclick="Setprio2(this)" class="btn btn-primary btn-sm">Set Priority</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 divADD">
                                                    <a class="lnkADDNEW" style="cursor: pointer; font-style: italic;"><i class="fa fa-plus"></i>&nbsp;Add New</a>
                                                    <a class="lnkMODIFY" style="cursor: pointer; font-style: italic;" class="modify"><i class="fa fa-edit"></i>&nbsp;Modify</a>
                                                </div>
                                                <div class="col-md-12 divSAVE" style="display: none;">
                                                    <a class="lnkSAVE" style="cursor: pointer; font-style: italic;"><i class="fa fa-save"></i>&nbsp;Save</a>
                                                    <a class="lnkCANCEL" style="cursor: pointer; font-style: italic;"><i class="fa fa-close"></i>&nbsp;Cancel</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_2_3">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-8">
                                                            <table id="tblInvoicePRIO" class="table table-no-bordered" style="margin-bottom: 5px" data-show-footer="false">
                                                                <thead>
                                                                    <tr>
                                                                        <th data-field="Prio" data-width="150px">PRIORITIZATION </th>
                                                                        <th data-field="Method" data-width="150px">METHOD</th>
                                                                        <th data-field="Prio_ID" class="text-center" data-formatter="ddlPrioInvoice" data-width="300px">Qualified Utility Status</th>
                                                                        <th data-field="Prio_ID" data-formatter="btnSaveInvoice"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <br />
                                                            <button type="button" id="btnInvoice" onclick="Setprio3(this)" class="btn btn-primary btn-sm">Set Priority</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 divADD">
                                                    <a class="lnkADDNEW" style="cursor: pointer; font-style: italic;"><i class="fa fa-plus"></i>&nbsp;Add New</a>
                                                    <a class="lnkMODIFY" style="cursor: pointer; font-style: italic;" class="modify"><i class="fa fa-edit"></i>&nbsp;Modify</a>
                                                </div>
                                                <div class="col-md-12 divSAVE" style="display: none;">
                                                    <a class="lnkSAVE" style="cursor: pointer; font-style: italic;"><i class="fa fa-save"></i>&nbsp;Save</a>
                                                    <a class="lnkCANCEL" style="cursor: pointer; font-style: italic;"><i class="fa fa-close"></i>&nbsp;Cancel</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_2_4">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-8">
                                                            <%--<table id="tblBillsPRIO" class="table" style="margin-bottom: 5px">
                                                                <thead>
                                                                    <tr>
                                                                        <th>PRIORITIZATION </th>
                                                                        <th>METHOD</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>--%>
                                                            <table id="tblBillsPRIO" class="table table-no-bordered" style="margin-bottom: 5px" data-show-footer="false">
                                                                <thead>
                                                                    <tr>
                                                                        <th data-field="Prio" data-width="150px">PRIORITIZATION </th>
                                                                        <th data-field="Method" data-width="150px">METHOD</th>
                                                                        <th data-field="Prio_ID" class="text-center" data-formatter="ddlPrioBills" data-width="300px">Qualified Utility Status</th>
                                                                        <th data-field="Prio_ID" data-formatter="btnSaveBills"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                            <%--<asp:Button ID="Button2" class="btn btn-link btn-sm" runat="server" Text="+Add New" OnClick="Button2_Click" />--%>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <br />
                                                            <button type="button" id="btnBills" onclick="Setprio4(this)" class="btn btn-primary btn-sm">Set Priority</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 divADD">
                                                    <a class="lnkADDNEW" style="cursor: pointer; font-style: italic;"><i class="fa fa-plus"></i>&nbsp;Add New</a>
                                                    <a class="lnkMODIFY" style="cursor: pointer; font-style: italic;" class="modify"><i class="fa fa-edit"></i>&nbsp;Modify</a>
                                                </div>
                                                <div class="col-md-12 divSAVE" style="display: none;">
                                                    <a class="lnkSAVE" style="cursor: pointer; font-style: italic;"><i class="fa fa-save"></i>&nbsp;Save</a>
                                                    <a class="lnkCANCEL" style="cursor: pointer; font-style: italic;"><i class="fa fa-close"></i>&nbsp;Cancel</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--  </div>--%>
                        </div>
                        <div class="tab-pane" id="tab_4">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <br />
                                            <div class="row">
                                                <div class="col-md-2" style="padding: 5px">
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading panel-primary text-center">
                                                            Task
                                                        </div>
                                                        <div class="panel-body">
                                                            <table id="tblTask" class="table table-no-bordered" data-show-header="false">
                                                                <thead>
                                                                    <tr>
                                                                        <th data-field="isCheck" data-width="30px" data-formatter="chkTask"></th>
                                                                        <th data-field="Task_Desc"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="padding: 5px">
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading panel-primary text-center">
                                                            Utility Type
                                                        </div>
                                                        <div class="panel-body">
                                                            <table id="tblUtilityTypeStatus" class="table table-no-bordered" data-show-header="false">
                                                                <thead>
                                                                    <tr>
                                                                        <th data-field="isCheck" data-width="30px" data-formatter="chkUtility"></th>
                                                                        <th data-field="Utility_desc"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8" style="padding: 5px">
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading panel-primary text-center">
                                                            Utility Type
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <table id="tblUtilityType" class="table table-no-bordered">
                                                                        <thead>
                                                                            <tr>
                                                                                <th data-field="isCheck" data-width="30px" data-formatter="chkStatus"></th>
                                                                                <th data-field="Start_Desc">Start Service Status</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <table id="tblUtilityTypeStop" class="table table-no-bordered">
                                                                        <thead>
                                                                            <tr>
                                                                                <th data-field="isCheck" data-width="30px" data-formatter="chkStatusStop"></th>
                                                                                <th data-field="Stop_Desc">Stop Service Status</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: right">
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div style="display: none">
            <table id="tblStartExcel" data-search="true" class="table" >
                <thead>
                    <tr>
                        <th data-field="USP_Name">USP Name</th>
                        <th data-field="Bulk Email USP">Bulk Email USP</th>
                        <th data-field="Bulk Email ASPS">Bulk Email ASPS</th>
                        <th data-field="Online Request USP">Online Request USP</th>
                        <th data-field="Online Request ASPS">Online Request ASPS</th>
                        <th data-field="Call USP">Call USP</th>
                        <th data-field="Call ASPS">Call ASPS</th>
                        <th data-field="EDI 814 USP">EDI 814 USP</th>
                        <th data-field="EDI 814 ASPS">EDI 814 ASPS</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <table id="tblStopExcel" data-search="true" class="table">
                <thead>
                    <tr>
                        <th data-field="USP_Name">USP Name</th>
                        <th data-field="Bulk Email USP">Bulk Email USP</th>
                        <th data-field="Bulk Email ASPS">Bulk Email ASPS</th>
                        <th data-field="Online Request USP">Online Request USP</th>
                        <th data-field="Online Request ASPS">Online Request ASPS</th>
                        <th data-field="Call USP">Call USP</th>
                        <th data-field="Call ASPS">Call ASPS</th>
                        <th data-field="EDI 814 USP">EDI 814 USP</th>
                        <th data-field="EDI 814 ASPS">EDI 814 ASPS</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
             <table id="tblInvoiceExcel" data-search="true" class="table">
                <thead>
                    <tr>
                        <th data-field="USP_Name">USP Name</th>
                        <th data-field="Summary Billing USP">Summary Billing USP</th>
                        <th data-field="Summary Billing ASPS">Summary Billing ASPS</th>
                        <th data-field="E-Bill w/ Password USP">E-Bill w/ Password USP</th>
                        <th data-field="E-Bill w/ Password ASPS">E-Bill w/ Password ASPS</th>
                        <th data-field="E-Bill w/o Password USP">E-Bill w/o Password USP</th>
                        <th data-field="E-Bill w/o Password ASPS">E-Bill w/o Password ASPS</th>
                        <th data-field="E-Bill Via Link USP">E-Bill Via Link USP</th>
                        <th data-field="E-Bill Via Link ASPS">E-Bill Via Link ASPS</th>
                        <th data-field="EDI 810 USP">EDI 810 USP</th>
                        <th data-field="EDI 810 ASPS">EDI 810 ASPS</th>
                        <th data-field="Paper Bill USP">Paper Bill USP</th>
                        <th data-field="Paper Bill ASPS">Paper Bill ASPS</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <table id="tblBillsExcel" data-search="true" class="table">
                <thead>
                    <tr>
                        <th data-field="USP_Name">USP Name</th>
                        <th data-field="ACH USP">ACH USP</th>
                        <th data-field="ACH ASPS">ACH ASPS</th>
                        <th data-field="Wire Transfer USP">Wire Transfer USP</th>
                        <th data-field="Wire Transfer ASPS">Wire Transfer ASPS</th>
                        <th data-field="Credit Card USP">Credit Card USP</th>
                        <th data-field="Credit Card ASPS">Credit Card ASPS</th>
                        <th data-field="Check USP">Check USP</th>
                        <th data-field="Check ASPS">Check ASPS</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>


        <%--AAAA--%>
    </div>


    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background: #3c8dbc; color: white; text-align: center">
                    <button type="button" style="color: white" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Capabilities</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <table class="table table-borderless" id="tblStart">
                                <thead>
                                    <tr style="border-top: none">
                                        <th>Start Service
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr style="border-top: none">
                                        <td></td>
                                        <td>Bulk Email </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr style="border-top: none">
                                        <td></td>
                                        <td>Online Request </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr style="border-top: none">
                                        <td></td>
                                        <td>EDI 814 </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr style="border-top: none">
                                        <td></td>
                                        <td>Call </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr style="border-top: none">
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button type="button" class="btn3d btn btn-default btn-sm"><span class="glyphicon glyphicon-download-alt"></span>Add New</button>

                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table" id="tblStopSer">
                                <thead>
                                    <tr>
                                        <th>Stop Service
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Bulk Email </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Online Request </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>EDI 814 </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Call </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button type="button" class="btn3d btn btn-default btn-sm"><span class="glyphicon glyphicon-download-alt"></span>Add New</button>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table" id="tblInvoice">
                                <thead>
                                    <tr>
                                        <th>Invoicing
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Summary Billing </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>E-Bill w/ Pasword </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>E-Bill w/o Pasword </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>E-Bill via Link </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>EDI 810 </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Paper Bill </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button type="button" class="btn3d btn btn-default btn-sm"><span class="glyphicon glyphicon-download-alt"></span>Add New</button>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table" id="tblBill">
                                <thead>
                                    <tr>
                                        <th>Stop Service
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>ACH </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Wire Transfer </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Credit Card </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Check </td>
                                        <td>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Edit</button>|
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Remove</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button type="button" class="btn3d btn btn-default btn-sm"><span class="glyphicon glyphicon-download-alt"></span>Add New</button>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>



                </div>
                <div class="modal-footer" style="text-align: center">
                    <button type="button" class="btn3d btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    <button type="button" class="btn3d btn btn-primary btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="Server">
    <script>
        $(document).ready(function () {
            $('.toggle').bootstrapToggle('destroy');
            $('.toggle').bootstrapToggle({
                on: 'Active',
                off: 'Inactive'
            });
        });
    </script>
    <script type="text/javascript">
        var newcount, newdata;
        $(document).ready(function () {
            $('.toggle').bootstrapToggle('destroy');
            $('.toggle').bootstrapToggle({
                on: 'Active',
                off: 'Inactive'
            });
        });

        var asc = 'asc';
        tbody = $("#tblWF").find('tbody');

        tbody.find('tr').sort(function (a, b) {
            if (asc) {
                return $('td:first', a).text().localeCompare($('td:first', b).text());
            } else {
                return $('td:first', b).text().localeCompare($('td:first', a).text());
            }
        }).appendTo(tbody);
        $("#tblEXCLUDEDSTATES").on("keyup", ".target", function () {

            $(this).closest("tr").find("td label").html($(this).val());
        });

        $(".tblPRIORITY tbody ").sortable({
            items: "tr",
            update: function (event, ui) {

                var counter = 1;
                $.each($(this).find("tr"), function () {

                    $(this).find("td:first").html(counter);
                    counter++;
                });
            }
        });
        $(".tblPRIORITY tbody").disableSelection();
        $(".lnkADDNEW").on("click", function () {
            var count = $(this).closest(".panel-body").find(".tblPRIORITY tbody tr").length + 1;
            newcount = count;
            $(this).closest(".panel-body").find(".tblPRIORITY tbody").append("<tr class='trNEW'><td>" + count + "</td><td><input type='text' class='txtNEW'></input ></td><td style='border-style:none;'></td> <td style='border-style:none;display:none;' class='btnFOREDITING'><button type='button' class='btn btn-btn btn-success' title='Edit'><i class='fa fa-edit'></i></button><button type='button' class='btn btn-btn btn-danger' title='Remove'><i class='fa fa-remove'></i></button></td></tr>");
            $(this).closest(".panel-body").find(".divADD").hide();
            $(this).closest(".panel-body").find(".divSAVE").show();
        });
        $(".lnkCANCEL").on("click", function () {
            $(this).closest(".panel-body").find(".divSAVE").hide();
            $(this).closest(".panel-body").find(".divADD").show();
            $(this).closest(".panel-body").find(".trNEW").remove();
        });
        $(".lnkSAVE").on("click", function () {
            $(this).closest(".panel-body").find(".divSAVE").hide();
            $(this).closest(".panel-body").find(".divADD").show();
            newdata = $.trim($(this).closest(".panel-body").find(".txtNEW").val());
            $(this).closest(".panel-body").find(".tblPRIORITY tbody .trNEW").remove();
            $(this).closest(".panel-body").find(".tblPRIORITY tbody").append("<tr><td>" + newcount + "</td><td>" + newdata + "</td><td style='border-style: none;display:none' class='btnFOREDITING'> <button type='button' class='btn btn-btn btn-success' title='Edit'><i class='fa fa-edit'></i></button> <button type='button' class='btn btn-btn btn-danger' title='Remove'><i class='fa fa-remove'></i></button> </td></tr>");
            // $("#tblWF thead tr").append("<th>" + newdata + "</th>");
            //$("#tblWF tbody tr").append("<td class='text-center'> <input type='checkbox' data-toggle='toggle' data-style='ios' class='chkBoxActive toggle' checked='checked'>  </td>");

        });
        $(".divADD").on("click", ".modify", function () {
            $(this).html('<i class="fa fa-remove"></i>&nbsp;Cancel').removeClass("modify").addClass("cancel");

            $(".btnFOREDITING").show();

        });
        $(".divADD").on("click", ".cancel", function () {
            $(this).html('<i class="fa fa-edit"></i>&nbsp;Modify').removeClass("cancel").addClass("modify");

            $(".btnFOREDITING").hide();

        });
        $("#tblPRIORITY tbody").on("click", "tr .btn-danger", function () {

            var index = $("#tblPRIORITY tbody tr").index($(this).closest("tr")) + 1;
            $(this).closest("tr").remove();

            $("#tblWF thead tr th:eq(" + index + ")").remove();
            $.each($("#tblWF tbody tr"), function () {
                $(this).find("td:eq(" + index + ")").remove();
            });
            var counter = 1;
            $.each($("#tblPRIORITY tbody tr"), function () {
                $(this).find("td:first").html(counter);
                counter++;
            });
        });
    </script>
    <script src="js/USPcapabilities.js"></script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="PaymentSettings.aspx.cs" Inherits="UC_v2._0.PaymentSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=90);
        opacity: 0.3;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 1200px;
        height: 558px;
    }
    .modalPopup2
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 920px;
        height: 430px;
    }
     .navbar {
    position: relative;
    min-height: 26px;
    margin-bottom: 4px;
    border: 1px solid transparent;
}
        .navbar-nav > li > a {
    padding-top: 11px;
    padding-bottom: 11px;
  }
        .navbar-text {
    margin-top: 9px;
    margin-bottom: 11px;
}
        .navbar-inverse {
    background-color: #274761;
    border-color: #2c628a;
}
        .navbar-inverse .navbar-nav > li > a {
    color: #d8d8d8;
}
        .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus {
    color: #fff;
    background-color: #3d4b5d;
}
        table tbody tr:hover {
            background: #8ce196;
            cursor: pointer;
        }

        table tbody tr.selected {
            background: #81c341;
        }

        #ulSETTINGS li input[type='text'] {
            width: 51px;
            padding: 3px 8px;
            height: 28px;
        }

        select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            padding: 3px 10px;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div class="content-wrapper">

           <div class="row">
            <%--------------------------------------------------------------%>
        
            <div class="col-sm-12"">
                <div class="row"><div class="col-sm-12" style="background-color:#4d6578; width:1368px"><h4>Bills Processing</h4></div></div>  

                <div class="row">
                    <div class="col-md-2" style="width:259px">
                      <div class="panel panel-New">
                    <div class="panel-Newheading" style="height:30px; text-align:center; font-size:20px">Rules</div>
                          <div class="panel-body" style="height: 604px; font-size:medium">

                              <table class="table table-striped" style="font-size:12px">
                            <thead>
                                <tr>
                                    <th style="width:10px"></th>
                                    <th >Name</th>
                                    <th>Last Run</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" /></td>
                                    <td>Trailing</td>
                                    <td>12/01/2016</td>
                                    <td>11:00 AM</td>
                                </tr>
                                 
                                 
                            </tbody>
                        </table>


                              </div>
                          <div class="panel-footer" style="background-color:transparent">
                              <center>
                                <asp:Button runat="server"  id="btnAdd" Text="Add New" class="btn btn-primary btn-sm"/>

                            </center>
                          </div>
                          </div>  
                    </div>
                    <div class="col-md-9" style="padding-left:3px">
                      <div class="panel panel-New" style="width:1105px" >
                    <div class="panel-Newheading" style="height:30px; text-align:center; font-size:20px"> Payment Setting    </div>
                          <div class="panel-body" style="height: 604px; font-size:medium">


                                        <div class="row" style="padding-left:10px; padding-top:20px; font-size:13px">
                                        <div class="col-md-2" >
                                            <asp:Label ID="Label3" runat="server" Text="Label">Title
                                    <div style="float:right">:</div>
                                </asp:Label>
                                        </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control form-group-sm" /> 
                                            </div>
                                        </div> 
                                        <div class="row" style="padding-left:10px; padding-top:5px; font-size:13px">
                                        <div class="col-md-2"  >
                                            <asp:Label ID="Label4" runat="server" Text="Label">Description
                                    <div style="float:right">:</div>
                                </asp:Label>
                                        </div>
                                            <div class="col-md-3">
                                                <textarea class="form-control" rows="2" id="comment"></textarea>
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="row" style="padding-left:10px; padding-top:5px; font-size:13px">
                                        <div class="col-md-4">
                                            <asp:Label ID="Label5" runat="server" Text="Label">Which Condition(s) do you want to check :
                                </asp:Label>
                                        </div>
                                            
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px; font-size:13px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">Please select the Client(s)</div>
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList14" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px; font-size:13px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">The Status of the property is</div>
                                        <div class="col-md-2" style="padding-right:0px; width:142px">
                                                <asp:DropDownList ID="DropDownList15" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                        </div>
                                         
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px; font-size:13px">
                                        <div class="col-md-1" style="width:30px; padding-left:86px">
                                            
                                        </div>
                                         
                                         
                                        <div class="col-md-3" style="padding-left:0px; width:174px">and it has been in this status</div>
                                        <div class="col-md-2" style="padding-right:0px; padding-left:0px; width:122px">
                                                <asp:DropDownList ID="DropDownList29" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                        </div>
                                            <div class="col-md-2" style="padding-right:0px; padding-left:0px; width:100px">
                                                 <input type="text" class="form-control" style="height:26px" id="usr">
                                        </div>
                                            <div class="col-md-2" style="padding-right:0px; padding-left:0px; width:122px">
                                                <asp:DropDownList ID="DropDownList30" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                        </div>
                                            <div class="col-md-2" style="padding-right:0px; padding-left:0px; width:122px">
                                                <asp:DropDownList ID="DropDownList31" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                        </div>
                                            <div class="col-md-3" style="padding-left:0px; width:38px">From</div>
                                            <div class="col-md-2" style="padding-right:0px; padding-left:0px; width:122px">
                                                <asp:DropDownList ID="DropDownList32" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                        </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px; font-size:13px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">The Property Status is</div>
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList20" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px; font-size:13px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">The Occupancy Status is</div>
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList21" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px; font-size:13px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">The POD is</div>
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList22" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px; font-size:13px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">The amount is</div>
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList23" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px; font-size:13px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">Apply this rule to the selected USP</div>
                                            <div class="col-md-2" style="width:125px">
                                                <asp:DropDownList ID="DropDownList24" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        <div class="col-md-3" style="width:75px; padding-right:0px">and State</div>
                                        <div class="col-md-2" style="width:125px; padding-left:0px">
                                                <asp:DropDownList ID="DropDownList25" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                            <div class="col-md-3" style="width:63px; padding-right:0px; padding-left:0px">and City</div>
                                        <div class="col-md-2" style="width:125px; padding-left:0px">
                                                <asp:DropDownList ID="DropDownList26" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row" style="padding-left:10px; padding-top:5px; font-size:13px">
                                        <div class="col-md-3" style="width:200px">
                                           If the Condition(s) are met then
                                        </div> 
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList27" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>











                              </div>
                          <div class="panel-footer" style="background-color:transparent">
                              <center>
                                <asp:Button runat="server"  id="btnAddPaySet" Text="Apply" class="btn btn-primary btn-sm"/>
                                  <asp:Button runat="server"  id="Button2" Text="Reset" class="btn btn-primary btn-sm"/>


                                  <!-- ModalPopupExtender -->
                            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel3" TargetControlID="btnAddPaySet"
                                     BackgroundCssClass="modalBackground">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel3" runat="server" CssClass="modalPopup" align="left" style = "display:none; padding-left:0px; padding-top:0px" >
                                
                                <div class="panel">
                                    <div class="panel-heading" style="background-color:#4d6578; color:#fff">
                                        Add New Payment Settings
                                    </div>
                                    <div class="panel-body">

                                        <div class="row" style="padding-left:10px; padding-top:20px">
                                        <div class="col-md-2">
                                            <asp:Label ID="Label10" runat="server" Text="Label">Title
                                    <div style="float:right">:</div>
                                </asp:Label>
                                        </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control form-group-sm" /> 
                                            </div>
                                        </div> 
                                        <div class="row" style="padding-left:10px; padding-top:5px">
                                        <div class="col-md-2">
                                            <asp:Label ID="Label1" runat="server" Text="Label">Description
                                    <div style="float:right">:</div>
                                </asp:Label>
                                        </div>
                                            <div class="col-md-3">
                                                <textarea class="form-control" rows="2" id="comment"></textarea>
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="row" style="padding-left:10px; padding-top:5px">
                                        <div class="col-md-4">
                                            <asp:Label ID="Label2" runat="server" Text="Label">Which Condition(s) do you want to check :
                                </asp:Label>
                                        </div>
                                            
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">Please select the Client(s)</div>
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList64" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">The Status of the property is</div>
                                        <div class="col-md-2" style="padding-right:0px; width:142px">
                                                <asp:DropDownList ID="DropDownList1" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                        </div>
                                        <div class="col-md-3" style="padding-left:0px; width:174px">and it has been in this status</div>
                                        <div class="col-md-2" style="padding-right:0px; padding-left:0px; width:122px">
                                                <asp:DropDownList ID="DropDownList8" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                        </div>
                                            <div class="col-md-2" style="padding-right:0px; padding-left:0px; width:100px">
                                                 <input type="text" class="form-control" style="height:29px" id="usr">
                                        </div>
                                            <div class="col-md-2" style="padding-right:0px; padding-left:0px; width:122px">
                                                <asp:DropDownList ID="DropDownList9" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                        </div>
                                            <div class="col-md-2" style="padding-right:0px; padding-left:0px; width:122px">
                                                <asp:DropDownList ID="DropDownList10" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                        </div>
                                            <div class="col-md-3" style="padding-left:0px; width:38px">From</div>
                                            <div class="col-md-2" style="padding-right:0px; padding-left:0px; width:122px">
                                                <asp:DropDownList ID="DropDownList11" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                        </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">The Property Status is</div>
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList2" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">The Occupancy Status is</div>
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList3" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">The POD is</div>
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList4" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">The amount is</div>
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList5" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="row" style="padding-left:10px; padding-top:5px">
                                        <div class="col-md-1" style="width:30px">
                                            <input type="checkbox" id="chkRow1" class="chk1 iCheck" data-id="11" />
                                        </div>
                                        <div class="col-md-3" style="width:215px; padding-right:0px">Apply this rule to the selected USP</div>
                                            <div class="col-md-2" style="width:125px">
                                                <asp:DropDownList ID="DropDownList6" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        <div class="col-md-3" style="width:75px; padding-right:0px">and State</div>
                                        <div class="col-md-2" style="width:125px; padding-left:0px">
                                                <asp:DropDownList ID="DropDownList12" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                            <div class="col-md-3" style="width:63px; padding-right:0px; padding-left:0px">and City</div>
                                        <div class="col-md-2" style="width:125px; padding-left:0px">
                                                <asp:DropDownList ID="DropDownList13" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:10px; padding-top:5px">
                                        <div class="col-md-3" style="width:200px">
                                           If the Condition(s) are met then
                                        </div> 
                                            <div class="col-md-2">
                                                <asp:DropDownList ID="DropDownList7" Width="120px"  runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                
                                 
                                <div class="panel">
                                    <div class="panel-footer" style="float:right; height:40px; background-color:transparent">
                                          <asp:Button ID="Button24" class="btn btn-primary" runat="server" Text="Save" /> 
                                    </div>
                                </div>
                            </asp:Panel> 
                                   
                            </center>
                          </div>
                          </div>  
                    </div>

                </div>


               </div>

        </div>

        </div>
</asp:Content>


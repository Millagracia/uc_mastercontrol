﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Reporting.aspx.cs" Inherits="UC_v2._0.Reporting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Summary</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Raw Data</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="panel panel-primary" style="background-color: #ecf0f5">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <div class="row">
                                                <div class="col-lg-3 text-right" style="padding-top: 5px; padding-left: 0px; padding-right: 0px">
                                                    Date :
                                                </div>
                                                <div class="col-lg-7">
                                                    <asp:DropDownList ID="ddlDate" ClientIDMode="Static" class="form-control" runat="server">
                                                        <asp:ListItem>All</asp:ListItem>
                                                        <asp:ListItem>This Week</asp:ListItem>
                                                        <asp:ListItem>This Month</asp:ListItem>
                                                        <asp:ListItem>This Year</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-lg-2" style="padding-top: 5px;">
                                                    or
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-2">
                                            <div class="row" style="padding-left: 0px">
                                                <div class="col-lg-9 text-left">
                                                    <asp:TextBox ID="txtDate" class="form-control" runat="server" ClientIDMode="Static" TextMode="Date"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-1" style="padding-top: 5px;">
                                                    to
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2" style="padding-left: 0px">
                                            <div class="col-lg-9">
                                                <asp:TextBox ID="txtDate2" class="form-control" runat="server" ClientIDMode="Static" TextMode="Date"></asp:TextBox>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-3" style="padding: 2px">
                                            <br />
                                            <div class="panel-body" style="background-color: white; box-shadow: 2px 2px 5px #888888; height: 390px">
                                                <div class="col-xs-12 text-center text-bold">
                                                    CLOSED
                                                </div>

                                                <div class="col-xs-12" style="padding-top: 10px">
                                                    <div class="col-xs-4 text-right">
                                                        Show By :
                                                    </div>
                                                    <div class="col-xs-7">
                                                        <asp:DropDownList ID="ddlShowBy" ClientIDMode="Static" class="form-control" runat="server">
                                                            <asp:ListItem>All</asp:ListItem>
                                                            <asp:ListItem>Solution</asp:ListItem>
                                                            <asp:ListItem>Utility</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="padding-top: 10px">
                                                    <table id="tblClosed" class="table" style="margin-bottom: 5px">
                                                        <thead>
                                                            <tr>
                                                                <th data-field="Solution" data-sortable="true">Solution</th>
                                                                <th data-field="Utility_On" data-sortable="true">Utility On</th>
                                                                <th data-field="Utility_Off" data-sortable="true">Utility Off</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>



                                                <br />
                                            </div>
                                        </div>
                                        <div class="col-xs-5" style="padding: 2px">
                                            <br />
                                            <div class="panel-body" style="background-color: white; box-shadow: 2px 2px 5px #888888; height: 390px">
                                                <div class="col-xs-12" style="padding-top: 2px">
                                                    <div class="col-xs-2 text-right">
                                                        Show By :
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <asp:DropDownList ID="ddlShowByGraph" onchange="changeShow()" ClientIDMode="Static" class="form-control" runat="server">
                                                            <asp:ListItem  >Select</asp:ListItem>
                                                            <asp:ListItem >Bulk Email</asp:ListItem>
                                                            <asp:ListItem >Online Request</asp:ListItem>
                                                            <asp:ListItem >CallOut</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="padding-top: 2px">
                                                    <div id="chartContainer" style="height: 320px; width: 100%;"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4" style="padding: 2px">
                                            <br />
                                            <div class="panel-body" style="background-color: white; box-shadow: 2px 2px 5px #888888; height: 390px">
                                                <div class="col-xs-12 text-center text-bold">
                                                    Status Movement
                                                </div>
                                                <div class="col-xs-12" style="padding-top: 2px; padding-left: 0px; padding-right: 0px">

                                                    <div class="col-xs-4">
                                                        Task
                                                        <asp:DropDownList ID="DropDownList1" ClientIDMode="Static" class="form-control" runat="server">
                                                            <asp:ListItem>All</asp:ListItem>
                                                            <asp:ListItem>Start Service</asp:ListItem>
                                                            <asp:ListItem>Stop Service</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="col-xs-4">
                                                        Solutions
                                                        <asp:DropDownList ID="DropDownList2" ClientIDMode="Static" class="form-control" runat="server">
                                                            <asp:ListItem>Bulk Email</asp:ListItem>
                                                            <asp:ListItem>Online SBS</asp:ListItem>
                                                            <asp:ListItem>Callout</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        USP
                                                        <asp:DropDownList ID="DropDownList3" ClientIDMode="Static" class="form-control" runat="server">
                                                            <asp:ListItem>Bulk Email</asp:ListItem>
                                                            <asp:ListItem>Online SBS</asp:ListItem>
                                                            <asp:ListItem>Callout</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="padding-top: 2px; padding-left: 0px; padding-right: 0px">
                                                    <table id="tbltblStatus" class="table" style="margin-bottom: 5px" data-show-export="true" data-page-list="[10, 25, 50, 100, ALL]">
                                                        <thead>
                                                            <tr>
                                                                <th data-field="ProcessType" data-sortable="true">Status</th>
                                                                <th data-field="Client Status" data-sortable="true">Address Verification Required by Vendor</th>
                                                                <th data-field="VendorId" data-sortable="true">Certified Inspection</th>
                                                                <th data-field="VendorId" data-sortable="true">City Inspection</th>
                                                                <th data-field="VendorId" data-sortable="true">Documents Required by USP</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - All Electric</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - Propane Fuel Oil</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception – Deeds Required</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - HOA Managed</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - City of Chicago Water (Deactivation Only)</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - Well Water</th>
                                                                <th data-field="VendorId" data-sortable="true">Payment Pending Client Approval</th>
                                                                <th data-field="VendorId" data-sortable="true">Payment Requested - CC</th>
                                                                <th data-field="VendorId" data-sortable="true">Payment Requested - Check</th>
                                                                <th data-field="VendorId" data-sortable="true">Payment Requested (to be used for multiple past dues)</th>
                                                                <th data-field="VendorId" data-sortable="true">Pending Another Utility Activation</th>
                                                                <th data-field="VendorId" data-sortable="true">RIP</th>
                                                                <th data-field="VendorId" data-sortable="true">Repair - Underway</th>
                                                                <th data-field="VendorId" data-sortable="true">Repairs Complete - Move to Renovate</th>
                                                                <th data-field="VendorId" data-sortable="true">Repair -  Pending Client Approval</th>
                                                                <th data-field="VendorId" data-sortable="true">Required Documents Sent to USP</th>
                                                                <th data-field="VendorId" data-sortable="true">Utility Off</th>
                                                                <th data-field="VendorId" data-sortable="true">Utility Off-Sale As Is</th>
                                                                <th data-field="VendorId" data-sortable="true">Utility On</th>
                                                                <th data-field="VendorId" data-sortable="true">Utility Ordered</th>
                                                                <th data-field="VendorId" data-sortable="true">Vendor Presence Required</th>
                                                                <th data-field="VendorId" data-sortable="true">Out of REO</th>
                                                                <th data-field="VendorId" data-sortable="true">Null</th>
                                                                <th data-field="VendorId" data-sortable="true">Utility Off</th>
                                                                <th data-field="VendorId" data-sortable="true">HUD\Proof of Sale</th>
                                                                <th data-field="VendorId" data-sortable="true">Letter of request required</th>
                                                                <th data-field="VendorId" data-sortable="true">Settlement of Balance</th>
                                                                <th data-field="VendorId" data-sortable="true">Turn Off form Required</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - City of Chicago Water (Deactivation Only)</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12" style="padding-left: 2px; padding-right: 2px; padding-top: 10px;">
                                            <div class="panel-body" style="background-color: white; box-shadow: 2px 2px 5px #888888; height: 390px">
                                                <div class="col-xs-12" style="padding-top: 2px">
                                                    <div class="col-xs-1 text-right">
                                                        Show By :
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <asp:DropDownList ID="DropDownList4" ClientIDMode="Static" class="form-control" runat="server">
                                                            <asp:ListItem>All</asp:ListItem>
                                                            <asp:ListItem>Start Service</asp:ListItem>
                                                            <asp:ListItem>Stop Service</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-xs-6 text-bold text-center">
                                                        USP List
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="padding-top: 2px">
                                                    <table id="tblUSPlist" class="table" style="margin-bottom: 5px" data-show-export="true" data-page-list="[10, 25, 50, 100, ALL]">
                                                        <thead>
                                                            <tr>
                                                                <th data-field="ProcessType" data-sortable="true">USP Name</th>
                                                                <th data-field="Client Status" data-sortable="true">Address Verification Required by Vendor</th>
                                                                <th data-field="VendorId" data-sortable="true">Certified Inspection</th>
                                                                <th data-field="VendorId" data-sortable="true">City Inspection</th>
                                                                <th data-field="VendorId" data-sortable="true">Documents Required by USP</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - All Electric</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - Propane Fuel Oil</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception – Deeds Required</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - HOA Managed</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - City of Chicago Water (Deactivation Only)</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - Well Water</th>
                                                                <th data-field="VendorId" data-sortable="true">Payment Pending Client Approval</th>
                                                                <th data-field="VendorId" data-sortable="true">Payment Requested - CC</th>
                                                                <th data-field="VendorId" data-sortable="true">Payment Requested - Check</th>
                                                                <th data-field="VendorId" data-sortable="true">Payment Requested (to be used for multiple past dues)</th>
                                                                <th data-field="VendorId" data-sortable="true">Pending Another Utility Activation</th>
                                                                <th data-field="VendorId" data-sortable="true">RIP</th>
                                                                <th data-field="VendorId" data-sortable="true">Repair - Underway</th>
                                                                <th data-field="VendorId" data-sortable="true">Repairs Complete - Move to Renovate</th>
                                                                <th data-field="VendorId" data-sortable="true">Repair -  Pending Client Approval</th>
                                                                <th data-field="VendorId" data-sortable="true">Required Documents Sent to USP</th>
                                                                <th data-field="VendorId" data-sortable="true">Utility Off</th>
                                                                <th data-field="VendorId" data-sortable="true">Utility Off-Sale As Is</th>
                                                                <th data-field="VendorId" data-sortable="true">Utility On</th>
                                                                <th data-field="VendorId" data-sortable="true">Utility Ordered</th>
                                                                <th data-field="VendorId" data-sortable="true">Vendor Presence Required</th>
                                                                <th data-field="VendorId" data-sortable="true">Out of REO</th>
                                                                <th data-field="VendorId" data-sortable="true">Null</th>
                                                                <th data-field="VendorId" data-sortable="true">Utility Off</th>
                                                                <th data-field="VendorId" data-sortable="true">HUD\Proof of Sale</th>
                                                                <th data-field="VendorId" data-sortable="true">Letter of request required</th>
                                                                <th data-field="VendorId" data-sortable="true">Settlement of Balance</th>
                                                                <th data-field="VendorId" data-sortable="true">Turn Off form Required</th>
                                                                <th data-field="VendorId" data-sortable="true">Exception - City of Chicago Water (Deactivation Only)</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>



                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane " id="tab_2">
                            <div class="panel panel-primary" style="background-color: #ecf0f5">
                                <div class="panel-body">
                                    <div class="row">
                                        <br />
                                        <br />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <%--<script src="js/Reporting.js"></script>--%>
    <script src="js/Reporting2.js"></script>
    <script>
       
    </script>
</asp:Content>

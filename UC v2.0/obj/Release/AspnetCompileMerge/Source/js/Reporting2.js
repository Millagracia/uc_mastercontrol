﻿$(document).ready(function () {
   
});




window.onload = function () {
    GetGraph();


    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('#tblClosed').bootstrapTable('destroy');
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'Reporting.aspx/GetClosed',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblClosed').bootstrapTable({
                    data: records,
                    width: 1000,
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });




    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'Reporting.aspx/GetMovement',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });

}


function changeShow() {

    var selectedShow = document.getElementById('ddlShowByGraph').value;

    var DataPointsValOn = [];
    var DataPointsValOff = [];
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'Reporting.aspx/GraphChange',
        data: '{"selectedShow" : "' + selectedShow + '"}',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $.each(records, function (idx, val) {

                    DataPointsValOn.push({ x: new Date(val.Date.replace(/["]/g, '')), y: val.Utility_On })
                    DataPointsValOff.push({ x: new Date(val.Date.replace(/["]/g, '')), y: val.Utility_Off })
                });

            }
            Graph(DataPointsValOn, DataPointsValOff)
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
    $('#modalLoading').modal('hide');
   
}

function GetGraph() {
    var DataPointsValOn = [];
    var DataPointsValOff = [];
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'Reporting.aspx/Graph',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $.each(records, function (idx, val) {
                    
                    DataPointsValOn.push({ x: new Date(val.Date.replace(/["]/g, '')), y: val.Utility_On })
                    DataPointsValOff.push({ x: new Date(val.Date.replace(/["]/g, '')), y: val.Utility_Off })
                });
               
            }
            Graph(DataPointsValOn, DataPointsValOff)
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}


function Graph(DataPointsValOn, DataPointsValOff) {
    
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: ""
        },
        axisY: {
            includeZero: false
        },
        data: [{
            type: "line",
            lineColor: "#3399ff",
            color: "#3399ff",
            legendText: "Utility On",
            showInLegend: true,
            dataPoints: DataPointsValOn

        },
               {
                   type: "line",
                   lineColor: "#C0504E",
                   color: "#C0504E",
                   legendText: "Utility Off",
                   showInLegend: true,
                   dataPoints: DataPointsValOff
               }
        ]
    });
    chart.destroy();
    chart.render();



}

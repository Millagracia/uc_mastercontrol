﻿$(document).ready(function () {
    //getEmp();
    //$('#myTable').pageMe({ pagerSelector: '#myPager', showPrevNext: true, hidePageNumbers: false, perPage: 4 });
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    loadUserTable();
    loadrole_accesstype("Role");

    $('#roleBtn').click(function () {
        $('#roleDiv').hide();
        $('#roleDiv').fadeIn();
        $('#headType').text("Role");
        loadrole_accesstype("Role");
    });

    $('#skillBtn').click(function () {
        $('#roleDiv').hide();
        $('#roleDiv').fadeIn();
        $('#headType').text("Skill Level");
        loadrole_accesstype("Skill Level");
    });
   
});
$('#btnNew_Type').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var type = $('#headType').text();
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/addAccess',
        data: '{"name" : "' + $('#accessTypeName').val() + '","desc" : "' + $('#accessTypeDesc').val() + '","type" : "' + type + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#accessTypeName').val("");
            $('#accessTypeDesc').val("")

            loadrole_accesstype(type);
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });

});

$('#btnEdit_Type').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var type = $('#headType').text();
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/editAccess',
        data: '{"id" : "' + $('#lineId').val() + '","name" : "' + $('#eaccessTypeName').val() + '","desc" : "' + $('#eaccessTypeDesc').val() + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#eaccessTypeName').val("");
            $('#eaccessTypeDesc').val("")
            loadrole_accesstype(data.d);
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });

});

$("#btnSEARCH").click(function () {
    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var type = $('#headType').text();
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/filterLIST',
        data: '{"immediate" : "' + $.trim($('#immSlct option:selected').text()) + '","userid" : "' + $.trim($('#userSelect option:selected').val()) + '","username" : "' + $.trim($('#userFltr').val()) + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#tblUser').bootstrapTable('destroy');
            var obj1 = [];
            var category = $.trim($('#posSlct option:selected').val());

          


            if ($.trim($('#posSlct option:selected').text()) != "Select All") {
                $.each(data.d.tblUserList, function (i, el) {
                    if ($.trim(el.emplevel) == category) {
                        obj1.push(el);
                    }
                });
            }

             
            else {
                obj1 = data.d.tblUserList;
            }
            $('#tblUser').bootstrapTable({
                data: obj1,
                height: 500,
                width: 1000,
                pagination: true
            });
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});

function editLine(id) {
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/loadLineAccess',
        data: '{"id" : "' + id + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var obj = data.d;
            console.log(obj);
            $('#eaccessTypeName').val(obj.name);
            $('#eaccessTypeDesc').val(obj.desc);
            $('#lineId').val(obj.id);

        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function deleteLine(id) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/deleteLine',
        data: '{"id" : "' + id + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            loadrole_accesstype(data.d);
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function loadrole_accesstype(type) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('#tbdy').empty();
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/loadAccess',
        data: '{"type":"' + type + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var obj = data.d;
            console.log(obj);
            for (var i = 0; i < obj.length; i++) {
                var ta = " <tr>";
                ta += "<td style='width:70%'><a type='button' style='cursor: pointer;' id='role_" + obj[i].id + "' onclick='loadrole_accessfunc(" + obj[i].id + ")'>" + obj[i].name + "</a></td>";
                ta += "<td style='width:15%'><button type='button' class='btn btn-success btn-sm' onclick='editLine(" + obj[i].id + ")' style='width:32px;'data-toggle='modal' data-target='#modalEditAccess'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></td>"
                ta += "<td style='width:15%'><button type='button' class='btn btn-danger btn-sm' onclick='deleteLine(" + obj[i].id + ")' style='width:32px;'><i class='fa fa-trash-o' style='color:white' aria-hidden='true'></i></button></td>"
                $('#tbdy').append(ta);
            }
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function loadrole_accessfunc(id) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('#tFunc').empty();
    $('#tison').empty();
    var type = $('#headType').text();
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/loadFunction',
        data: '{"id" : "' + id + '","type" : "' + type + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var obj = data.d;
            console.log(obj);
            $('#conId').val(id);
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].isOn == "True") {
                    var ta = " <tr>";
                    ta += "<td style='width:15%'><input type='checkbox' id='" + obj[i].id + "' class='fncChkA'></td>";
                    ta += "<td style='width:85%' >" + obj[i].name + "</td><tr>";
                    $('#tison').append(ta);
                }
                else {
                    var ta = " <tr>";
                    ta += "<td style='width:15%'><input type='checkbox' id='" + obj[i].id + "' class='fncChkR'></td>";
                    ta += "<td style='width:85%'>" + obj[i].name + "</td><tr>";
                    $('#tFunc').append(ta);
                }
            }
            $('#modalLoading').modal('hide');

        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#appFunc').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var sList = "";
    $('.fncChkR').each(function () {
        var sThisVal = (this.checked ? $(this).attr('id') : "0");
        sList += (sList == "" ? sThisVal : "," + sThisVal);
    });
    console.log(sList);
    $('.fncChkA').each(function () {
        var sThisVal = $(this).attr('id');
        sList += (sList == "" ? sThisVal : "," + sThisVal);
    });
    console.log(sList);
    var id = $('#conId').val();
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/updateFunction',
        data: '{"id" : "' + id + '","newStr" : "' + sList + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            loadrole_accessfunc(id);
            $('#modalLoading').modal('hide');


        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});


$('#remFunc').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var sList = "";
    $('.fncChkA').each(function () {
        var sThisVal = (this.checked ? "0" : $(this).attr('id'));
        sList += (sList == "" ? sThisVal : "," + sThisVal);
    });

    console.log(sList);
    var id = $('#conId').val();
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/updateFunction',
        data: '{"id" : "' + id + '","newStr" : "' + sList + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            loadrole_accessfunc(id);
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});


$("#immSlct").on("change", function () {

    $('#userSelect option').hide();
    var immediate = $.trim($(this).find('option:selected').text());
    $("#userSelect option[manager='" + immediate + "']").show();
    $("#userSelect option[manager='']").show();
    $("#userSelect").val($("#userSelect option:first").val());
});
$('#userTab').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    loadUserTable();
    popActions();
     
    
});
function loadUserTable() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/userList',
        data: '{}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            $('#tblUser').bootstrapTable('destroy');

            $('#tblUser').bootstrapTable({
                data: data.d.tblUserList,
                height: 500,
                width: 1000,
                pagination: true
            });
            var obj = data.d.tblUserList;
            $('#userSelect').empty();
            $('#userSelect').append("<option value='0' manager=''>Select All</option>");
            for (var i = 0; i < obj.length; i++) {
                var ta;
                ta1 = "<option value='" + obj[i].id + "' manager='" + obj[i].immsupp + "'>" + obj[i].empname + "</option>";
                $('#userSelect').append(ta1);


            }



            //$('.iCheck').iCheck({
            //	checkboxClass: 'icheckbox_flat-blue',
            //	radioClass: 'iradio_flat-blue'
            //});

            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function idFormatter1(value) {
    return "<input type='checkbox' id='chkRow1' class='chkRow1 iCheck' data-id='" + value + "' />";
}

function popActions() {
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/popRS',
        data: '{}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var obj = data.d;
            console.log(obj);
            $('#roleSlct').empty();
            $('#roleSlctfilter').empty();
            $('#skillSlct').empty();
            var ta1 = "";
            var ta2 = "";
            $('#roleSlctfilter').append("<option value='0'>Select All</option>");
            for (var i = 0; i < obj.length; i++) {

                if (obj[i].rstype == "Role") {
                    ta1 = "<option value='" + obj[i].id + "'>" + obj[i].rsname + "</option>";
                    $('#roleSlct').append(ta1);
                    $('#roleSlctfilter').append(ta1);
                }
                else {
                    ta2 = "<option value='" + obj[i].id + "'>" + obj[i].rsname + "</option>";
                    $('#skillSlct').append(ta2);
                }
            }

        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/immediateSuperior',
        data: '{}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#immSlct').empty();
            var obj = data.d.tblUserList;
            $('#immSlct').append("<option value='0'>Select All</option>");
            for (var i = 0; i < obj.length; i++) {
                ta1 = "<option value='" + obj[i].id + "'>" + obj[i].empname + "</option>";
                $('#immSlct').append(ta1);
            }

            //$('.iCheck').iCheck({
            //	checkboxClass: 'icheckbox_flat-blue',
            //	radioClass: 'iradio_flat-blue'
            //});


        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#applyBtn').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var role_id = $('#roleSlct option:selected').val();
    var skill_id = $('#skillSlct option:selected').val();
    var emp_id = "";
    $('.chkRow1').each(function () {
        var sThisVal = (this.checked ? $(this).attr('data-id') : "0");
        emp_id += (emp_id == "" ? sThisVal : "," + sThisVal);
    });
    emp_id = emp_id.replace(/\,0/g, "");;
    $.ajax({
        type: 'POST',
        url: 'UserSettings.aspx/applyAction',
        data: '{"role_id" : "' + role_id + '","skill_id" : "' + skill_id + '","emp_id" : "' + emp_id + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            loadUserTable();
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });


});
//function getEmp() {
//    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
//    $.ajax({
//        type: 'POST',
//        contentType: "application/json; charset=utf-8",
//        url: 'UserSettings.aspx/GetEmp',
        
//        success: function (data) {
//            var d = $.parseJSON(data.d);
//            if (d.Success) {
               

               
//                var records = d.data.asd;

//                $('#tblUser tbody').empty();

//                $.each(records, function (idx, val) {
//                    $('#tblUser tbody').append(
//                             '<tr >' +
//                                 '<td>' + '<td>' + val.empname +
//                                 '<td>' + val.emplevel +
//                                 '<td>' + val.immsupp +
//                                 '<td>' + val.role_id +
//                                 '<td>' + val.skill_id +
//                             '</tr>'
//                    );
//                });
//            }
//            $('#modalLoading').modal('hide');
//        },

//        error: function () {
//            console.log('there is some error');
//        }
//    });

//}


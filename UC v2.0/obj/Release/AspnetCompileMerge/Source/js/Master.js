﻿$(document).ready(function () {
    var nt = $('#lblNTID').text();
    getEmpInfo(nt);

    var d = new Date();

    $('#lblToday').text('Today is: ' + getDate(d));

});

function getEmpInfo(nt) {
    $.ajax({
        type: 'POST',
        url: 'ApplyLeave.aspx/GetEmpInfo',
        data: '{nt: "' + nt + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                if (records.length > 0) {
                    if (records[0].AccessLevel != '1') {
                        $('#btnStat').hide();
                        $('#divBtnGrp').show();
                        $('#lblFullName').attr('data-id', records[0].AccessLevel);
                        //localStorage.setItem('Error', 'Access Level');
                        //location.replace('Error.aspx');
                    } else {
                        $('#btnStat').show();
                        $('#divBtnGrp').hide();
                        $('#lblFullName').attr('data-id', records[0].AccessLevel);
                    }

                    getLastActivity();

                    $('#lblFullName').text(records[0].EmpName);
                } else {
                    localStorage.setItem('Error', 'No Record');
                    location.replace('Error.aspx');
                }
            }
        },
        error: function (response) {
            console.log(response.responseText);
            localStorage['error'] = response.responseText;
            location.replace('Error.aspx');
        }
    });
}

function getLastActivity() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var nt = $('#lblNTID').text();
    var access = $('#lblFullName').attr('data-id');

    var myData = [nt]

    $.ajax({
        type: 'POST',
        url: 'Index.aspx/GetLastAct',
        data: '{myData: ' + JSON.stringify(myData) + '}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                if (records.length > 0) {
                    if (access == '1') {
                        $('#btnStat').show();
                        $('#divBtnGrp').hide();
                        btnCurrStat(records[0].ReasonID);
                        $('#btnStat').attr('data-id', records[0].ReasonID);
                    } else {
                        $('#btnStat').hide();
                        $('#divBtnGrp').show();

                        if (records[0].ReasonID == '2') {
                            $('#btnTitle').text('Logged In');
                            $('#btnLogin').css('cursor', '');
                            $('#btnLogin').text('Logged In');
                            $('#btnLogout').text('Log Out');
                            $('#btnLogout').css('cursor', 'pointer');
                            $('#btnLogin').closest('li').addClass('disabled');
                            $('#btnLogout').closest('li').removeClass('disabled');
                            location.href = 'ApplyLeave.aspx';

                        } else if (records[0].ReasonID == '14' || records[0].ReasonID == '16') {
                            $('#btnTitle').text('Logged Out');
                            $('#btnLogout').css('cursor', '');
                            $('#btnLogout').text('Logged Out');
                            $('#btnLogin').text('Log In');
                            $('#btnLogin').css('cursor', 'pointer');
                            $('#btnLogout').closest('li').addClass('disabled');
                            $('#btnLogin').closest('li').removeClass('disabled');
                        }
                    }
                } else {
                    if (access == '1') {
                        $('#btnStat').show();
                        $('#divBtnGrp').hide();
                        $('#btnStat').html('');
                        $('#btnStat').append('<i class="fa fa-sign-in"></i>&nbsp;Log In');
                        $('#btnStat').removeClass();
                        $('#btnStat').addClass('btn btn-danger btn-sm');
                    } else {
                        $('#btnStat').hide();
                        $('#divBtnGrp').show();

                        $('#btnTitle').text('Logged Out');
                        $('#btnLogout').css('cursor', '');
                        $('#btnLogout').text('Logged Out');
                        $('#btnLogin').text('Log In');
                        $('#btnLogin').css('cursor', 'pointer');
                        $('#btnLogout').closest('li').addClass('disabled');
                        $('#btnLogin').closest('li').removeClass('disabled');
                    }
                }

                var lastLogs = d.data.lastLog;

                if (lastLogs.length > 0) {
                    $('#lblLastLog').text('Last login is: ' + getDate(lastLogs[0].Start_Time.substr(0, 10)));
                }

                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
            localStorage['error'] = response.responseText;
            location.replace('Error.aspx');
        }
    });
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
// Associates Codes
//-----------------------------------------------------------------------------------------------------------------------------------------------------

$('#btnStat').click(function () {
    var btnStat = $(this).text().trim();

    if (btnStat == 'Log In') {
        login('');
    } else {
        $('#modalBtnStatus').modal({ backdrop: 'static', keyboard: false });
        btnCurrStat($(this).attr('data-id'));

        var rd = "";
        $.each($('input[name=rdStat]'), function (idx, val) {
            if ($(this).is(':checked')) {
                rd = $(this).attr('data-id');
            }
        });

        getSegments(rd);
    }
});

$('input[name=rdStat]').on('change', function () {
    var rd = $(this).attr('data-id');
    var btnData = $('#btnStat').attr('data-id');

    if (rd == '1') {
        $('#divLogout').hide();
        $('#divSlctSegment').hide();
        if (btnData == '2') {
            $('#divBtnOk').hide();
        } else {
            $('#divBtnOk').show();
        }
    } else if (rd == '2') {

        getSegments(rd);

        $('#divLogout').hide();
        $('#divSlctSegment').show();
        $('#divBtnOk').show();
    } else if (rd == '3') {

        getSegments(rd);

        $('#divLogout').hide();
        $('#divSlctSegment').show();
        $('#divBtnOk').show();
    } else if (rd == '4') {
        $('#divLogout').show();
        $('#divSlctSegment').hide();
        $('#divBtnOk').hide();
    }
});

$('#btnOK').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var rd = "";
    $.each($('input[name=rdStat]'), function (idx, val) {
        if ($(this).is(':checked')) {
            rd = $(this).attr('data-id');
        }
    });

    if (rd == '1') {
        var segment = '2';
        changeSegment(segment);
    } else if (rd == '2') {
        var segment = $('#slctSegment').val();
        changeSegment(segment);
    } else if (rd == '3') {
        var segment = $('#slctSegment').val();
        changeSegment(segment);
    }
});

$('#btnYes').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });

    Logout('');
});

function Logout(val) {
    var nt = $('#lblNTID').text();

    var myData = [nt, val];

    $.ajax({
        type: 'POST',
        url: 'Index.aspx/Logout',
        data: '{myData: ' + JSON.stringify(myData) + '}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var tag = data.d.split('_');

            if (tag[0] == 'tag') {
                if (tag[1] == 'default') {
                    location.replace('HoursWorked.aspx');
                } else if (tag[1] == 'late' || tag[1] == 'early') {
                    location.replace('Index.aspx');
                }
            } else {
                if (tag[0] == 'default') {
                    Logout("NULL");
                } else if (tag[0] == 'late') {
                    var mins = tag[1];
                    alertify.confirm('You are logging out ' + mins + ' minutes late, apply for overtime approval?',
                        function () {
                            //yes
                            Logout('Late');
                        },
                        function () {
                            //no
                            Logout('Unpaid');
                        });
                } else if (tag[0] == 'early') {
                    var mins = tag[1];
                    alertify.confirm('Are you sure to log out ' + mins + ' minutes early?',
                        function () {
                            //yes
                            Logout('Early');
                        },
                        function () {
                            //no
                            
                        });
                } else if (tag[0] == '0') {
                    //Catch Exception
                }
            }

            $('#modalLoading').modal('hide');
            $('#modalBtnStatus').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
            localStorage['error'] = response.responseText;
            location.replace('Error.aspx');
        }
    });
}

$('#btnNo').click(function () {
    $('#modalBtnStatus').modal('hide');
});

$('#btnClose').click(function () {
    $('#modalBtnStatus').modal('hide');
});

function login(def) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var nt = $('#lblNTID').text();

    var myData = [nt, def];

    $.ajax({
        type: 'POST',
        url: 'Index.aspx/Login',
        data: '{myData: ' + JSON.stringify(myData) + '}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            var mins, val;

            if (data.d.length > 1) {
                mins = data.d.substr(data.d.indexOf('_') + 1, data.d.length - 1);
                val = data.d.substring(0, data.d.indexOf('_'));
            } else {
                val = data.d;
            }

            if (val == 'early') {
                alertify.confirm('You are logging in ' + mins + ' minutes early, apply for overtime approval?',
                    function () {
                        login('Pending');
                    },
                    function () {
                        login('unpaid');
                    });
            } else if (val == '0') {
                login('NULL');
            } else if (val == '1') {
                btnCurrStat('2');
                $('#btnStat').attr('data-id', '2');

                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
            localStorage['error'] = response.responseText;
            location.replace('Error.aspx');
        }
    });
}

function changeSegment(val) {
    var nt = $('#lblNTID').text();

    var myData = [nt, val]

    $.ajax({
        type: 'POST',
        url: 'Index.aspx/ChangeSegment',
        data: '{myData: ' + JSON.stringify(myData) + '}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            btnCurrStat(val);
            $('#modalBtnStatus').modal('hide');
            $('#modalLoading').modal('hide');
            $('div.modal-backdrop').hide();
        },
        error: function (response) {
            console.log(response.responseText);
            localStorage['error'] = response.responseText;
            location.replace('Error.aspx');
        }
    });
}

function btnCurrStat(val) {
    var btnStat = '';

    var notready = [9, 10, 11, 12, 13];
    var park = [5, 6, 7, 8];

    if (val == '1' || val == '14') {
        btnStat = 'Log In';

        $('#btnStat').html('');
        $('#btnStat').append('<i class="fa fa-sign-in"></i>&nbsp;' + btnStat);

        $('#btnStat').removeClass();
        $('#btnStat').addClass('btn btn-danger btn-sm');
    } else if (val == '2') {
        btnStat = 'Ready';

        $('#btnStat').html('');
        $('#btnStat').append('<i class="fa fa-circle" style="color: green;"></i>&nbsp;' + btnStat);

        $('#btnStat').removeClass();
        $('#btnStat').addClass('btn btn-primary btn-sm');

        $.each($('input[name=rdStat]'), function (idx, val) {
            $(this).prop('checked', false);
        });

        $('input[name=rdStat]:eq(0)').prop('checked', true);

        $('#divLogout').hide();
        $('#divSlctSegment').hide();
    } else if ($.inArray(parseInt(val), notready) !== -1) {
        btnStat = 'Not Ready - ';
        if (val == '9') {
            btnStat += 'First Break';
        } else if (val == '10') {
            btnStat += 'Lunch Break';
        } else if (val == '11') {
            btnStat += 'Last Break';
        } else if (val == '12') {
            btnStat += 'Clinic';
        } else if (val == '13') {
            btnStat += 'Bio Break';
        }

        $('#btnStat').html('');
        $('#btnStat').append('<i class="fa fa-circle" style="color: red;"></i>&nbsp;' + btnStat);

        $('#btnStat').removeClass();
        $('#btnStat').addClass('btn btn-warning btn-sm');

        $.each($('input[name=rdStat]'), function (idx, val) {
            $(this).prop('checked', false);
        });

        $('input[name=rdStat]:eq(1)').prop('checked', true);

        $('#divLogout').hide();
        $('#divSlctSegment').show();
        $('#divBtnOk').hide();
    } else if ($.inArray(parseInt(val), park) !== -1) {
        btnStat = 'Park - ';
        if (val == '5') {
            btnStat += 'Meeting';
        } else if (val == '6') {
            btnStat += 'Coaching';
        } else if (val == '7') {
            btnStat += 'Training';
        } else if (val == '8') {
            btnStat += 'System Issue';
        }

        $('#btnStat').html('');
        $('#btnStat').append('<i class="fa fa-pause"></i>&nbsp;' + btnStat);

        $('#btnStat').removeClass();
        $('#btnStat').addClass('btn btn-success btn-sm');

        $.each($('input[name=rdStat]'), function (idx, val) {
            $(this).prop('checked', false);
        });

        $('input[name=rdStat]:eq(2)').prop('checked', true);

        $('#divLogout').hide();
        $('#divSlctSegment').show();
        $('#divBtnOk').hide();
    }
}

function getSegments(rd) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'Index.aspx/GetSegments',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                if (rd == '2') {
                    var breaks = d.data.brk;
                    $('#slctSegment').empty();
                    $.each(breaks, function (idx, val) {
                        $('#slctSegment').append(
							'<option value="' + val.ReasonID + '">' + val.ReasonDesc + '</option>'
						);
                    });

                    $('#modalLoading').modal('hide');
                } else {
                    var parks = d.data.prk;
                    $('#slctSegment').empty();
                    $.each(parks, function (idx, val) {
                        $('#slctSegment').append(
							'<option value="' + val.ReasonID + '">' + val.ReasonDesc + '</option>'
						);
                    });

                    $('#modalLoading').modal('hide');
                }
            }
        },
        error: function (response) {
            console.log(response.responseText);
            localStorage['error'] = response.responseText;
            location.replace('Error.aspx');
        }
    });
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
// Supervisor CODES
//-----------------------------------------------------------------------------------------------------------------------------------------------------

$('#btnLogin').click(function () {
    if ($(this).closest('li').hasClass('disabled')) {
        return false;
    } else {
        login2();
    }
});

$('#btnLogout').click(function () {
    if ($(this).closest('li').hasClass('disabled')) {
        return false;
    } else {
        $(this).text('Logged Out');
        $(this).css('cursor', '');
        $('#btnLogin').text('Log In');
        $('#btnLogin').css('cursor', 'pointer');
        $(this).closest('li').addClass('disabled');
        $('#btnLogin').closest('li').removeClass('disabled');

        $('#btnTitle').text('Log In');
    }
});


function login2() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var nt = $('#lblNTID').text();

    var myData = [nt];

    $.ajax({
        type: 'POST',
        url: 'TLCodes.aspx/Login',
        data: '{myData: ' + JSON.stringify(myData) + '}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            if (data.d == '1') {
                $('#btnTitle').text('Logged In');
                $('#btnLogin').text('Logged In');
                $('#btnLogin').css('cursor', '');
                $('#btnLogout').text('Log Out');
                $('#btnLogout').css('cursor', 'pointer');

                $('#btnLogin').closest('li').addClass('disabled');
                $('#btnLogout').closest('li').removeClass('disabled');

                location.replace('ApplyLeave.aspx');
            } else {
                localStorage['error'] = data.d;
                location.replace('Error.aspx');
            }

            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
            localStorage['error'] = response.responseText;
            location.replace('Error.aspx');
        }
    });
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------

function detectBrowser() {
    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;
}

function KeyPress(e) {
    var evtobj = window.event ? event : e

    var nt = $('#lblNTID').text();
    var access = $('#lblFullName').attr('data-id');

    if (access == '1') {
        if ((evtobj.keyCode == 123) ||
        (evtobj.ctrlKey && evtobj.shiftKey && evtobj.keyCode == 73) ||
        (evtobj.ctrlKey && evtobj.shiftKey && evtobj.keyCode == 74) ||
        (evtobj.ctrlKey && evtobj.shiftKey && evtobj.keyCode == 67)) {
            return false;
        }
    }
}

document.onkeydown = KeyPress;

function getDate(date) {
    var StrMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var d = new Date(date);
    var month = d.getMonth();
    var day = d.getDate();
    day = (d.toString().length == 1) ? '0' + day : day;
    var year = d.getFullYear();

    return StrMonths[month] + ' ' + day + ', ' + year;
}

function getDateTime(date) {
    if (date != null) {
        var strDate = date.replace('T', ' ');

        var StrMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var d = new Date(strDate);
        var month = d.getMonth();
        var day = d.getDate();
        day = (day.toString().length == 1) ? '0' + day : day;
        var year = d.getFullYear();


        var hours = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
        var minutes = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
        var seconds = d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds();

        return StrMonths[month] + ' ' + day + ', ' + year + ' ' + hours + ':' + minutes + ':' + seconds;
    } else {
        return '-';
    }
}

function inGetDate(date) {
    var StrMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var d = new Date(date);
    var month = d.getMonth();
    var day = d.getDate();
    day = (d.toString().length == 1) ? '0' + day : day;
    var year = d.getFullYear();

    var val = StrMonths[month] + ' ' + day + ', ' + year;

    return '<input type="text" class="form-control input-sm" value="' + val + '" />';
}

function inGetDateTime(date) {
    if (date != null) {
        var strDate = date.replace('T', ' ');

        var StrMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var d = new Date(strDate);
        var month = d.getMonth();
        var day = d.getDate();
        day = (day.toString().length == 1) ? '0' + day : day;
        var year = d.getFullYear();


        var hours = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
        var minutes = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
        var seconds = d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds();

        var val = StrMonths[month] + ' ' + day + ', ' + year + ' ' + hours + ':' + minutes + ':' + seconds;

        return '<input type="text" class="form-control input-sm" value="' + val + '" />';
    } else {
        return '';
    }
}

function getMonths() {
    var StrMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    return StrMonths;
}

(function ($) {
    $.fn.isBgColor = function (color) {
        var thisBgColor = this.eq(0).css('backgroundColor');
        var computedColor = $('<div/>').css({
            backgroundColor: color
        }).css('backgroundColor');
        return thisBgColor === computedColor;
    }
})(jQuery);


//-----------------------------------------------------------------------------------------------------------------------------------------------------
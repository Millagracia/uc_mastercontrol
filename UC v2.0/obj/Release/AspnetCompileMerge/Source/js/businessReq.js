﻿$(document).ready(function () {
    getUSP();
    getReq();
    document.getElementById("inactive").checked = true;


    $('#lbZip').multiselect({
        numberDisplayed: 0,
        includeSelectAllOption: true,
        maxWidth: 100,
        maxHeight: 200
    });
});
function getReq() {
   
}
$('#chkDeposit').click(function () {
    if(document.getElementById("chkDeposit").checked == true)
    {
        document.getElementById("txtStandard1").disabled = false;
    }
    else
    {
        document.getElementById("txtStandard1").disabled = true;
    }
});
$('#chkactivation').click(function () {
    if (document.getElementById("chkactivation").checked == true) {
        document.getElementById("txtStandard2").disabled = false;
    }
    else {
        document.getElementById("txtStandard2").disabled = true;
    }
});
$('#chkLien').click(function () {
    if (document.getElementById("chkLien").checked == true) {
        document.getElementById("txtStandard3").disabled = false;
    }
    else {
        document.getElementById("txtStandard3").disabled = true;
    }
});
$('#inactive').click(function () {
    document.getElementById("active").checked = false;
    document.getElementById("txtStandard1").disabled = true;
    document.getElementById("txtStandard2").disabled = true;
    document.getElementById("txtStandard3").disabled = true;
    document.getElementById("chkDeposit").disabled = true;
    document.getElementById("chkactivation").disabled = true;
    document.getElementById("chkLien").disabled = true;
    document.getElementById("chkDeposit").checked = false;
    document.getElementById("chkactivation").checked = false;
    document.getElementById("chkLien").checked = false;

    document.getElementById("txtStandard1").value = '';
    document.getElementById("txtStandard2").value = '';
    document.getElementById("txtStandard3").value = '';

    //document.getElementById("txtComplete").disabled = false;
    //document.getElementById("ddlComplete_BC").disabled = false;
    //document.getElementById("ddlComplete_DMY").disabled = false;
});
$('#chkInactive').click(function () {
    document.getElementById("chkActive").checked = false;
    document.getElementById("txtEmailAdd").disabled = true;
    document.getElementById("txtAttention").disabled = true;
    document.getElementById("txtFax").disabled = true;


    document.getElementById("txtEmailAdd").value = '';
    document.getElementById("txtAttention").value = '';
    document.getElementById("txtFax").value = '';

    //CheckBox
    document.getElementById("chkDeed").disabled = true;
    document.getElementById("chkRequest").disabled = true;
    document.getElementById("chkPOA").disabled = true;
    document.getElementById("chkAuthorization").disabled = true;
    document.getElementById("chkOcwenPOA").disabled = true;
    document.getElementById("chkASFI").disabled = true;
    document.getElementById("chkForm").disabled = true;
    document.getElementById("chkListing").disabled = true;

    document.getElementById("chkDeed").checked = false;
    document.getElementById("chkRequest").checked = false;
    document.getElementById("chkPOA").checked = false;
    document.getElementById("chkAuthorization").checked = false;
    document.getElementById("chkOcwenPOA").checked = false;
    document.getElementById("chkASFI").checked = false;
    document.getElementById("chkForm").checked = false;
    document.getElementById("chkListing").checked = false;
});
$('#chkActive').click(function () {
    document.getElementById("chkInactive").checked = false;
    document.getElementById("txtEmailAdd").disabled = false;
    document.getElementById("txtAttention").disabled = false;
    document.getElementById("txtFax").disabled = false;

    //CheckBox
    document.getElementById("chkDeed").disabled = false;
    document.getElementById("chkRequest").disabled = false;
    document.getElementById("chkPOA").disabled = false;
    document.getElementById("chkAuthorization").disabled = false;
    document.getElementById("chkOcwenPOA").disabled = false;
    document.getElementById("chkASFI").disabled = false;
    document.getElementById("chkForm").disabled = false;
    document.getElementById("chkListing").disabled = false;
});
$('#active').click(function () {
    document.getElementById("inactive").checked = false;
    document.getElementById("txtStandard1").disabled = false;
    document.getElementById("txtStandard2").disabled = false;
    document.getElementById("txtStandard3").disabled = false;
    document.getElementById("chkDeposit").disabled = false;
    document.getElementById("chkactivation").disabled = false;
    document.getElementById("chkLien").disabled = false;
    document.getElementById("chkDeposit").checked = true;
    document.getElementById("chkactivation").checked = true;
    document.getElementById("chkLien").checked = true;
    //document.getElementById("txtComplete").disabled = false;
    //document.getElementById("ddlComplete_BC").disabled = false;
    //document.getElementById("ddlComplete_DMY").disabled = false;
});
$('#chkInactive1').click(function () {
    document.getElementById("chkActive1").checked = false;
    document.getElementById("txtReport").disabled = true;
    document.getElementById("txtCityEmail").disabled = true;
    document.getElementById("txtAtten").disabled = true;
    document.getElementById("txtCityFax").disabled = true;

    document.getElementById("txtReport").value = '';
    document.getElementById("txtCityEmail").value = '';
    document.getElementById("txtAtten").value = '';
    document.getElementById("txtCityFax").value = '';
});
$('#chkActive1').click(function () {
    document.getElementById("chkInactive1").checked = false;
    document.getElementById("txtReport").disabled = false;
    document.getElementById("txtCityEmail").disabled = false;
    document.getElementById("txtAtten").disabled = false;
    document.getElementById("txtCityFax").disabled = false;
});
$('#chkInactive2').click(function () {
    document.getElementById("chkActive2").checked = false;

    document.getElementById("chkElec").checked = false;
    document.getElementById("chkGas").checked = false;
    document.getElementById("chkWater").checked = false;

    document.getElementById("chkElec").disabled = true;
    document.getElementById("chkGas").disabled = true;
    document.getElementById("chkWater").disabled = true;

    document.getElementById("togWater1").disabled = true;
    document.getElementById("togWater2").disabled = true;
    document.getElementById("togGas1").disabled = true;
    document.getElementById("togGas2").disabled = true;
    document.getElementById("togElec1").disabled = true;
    document.getElementById("togElec2").disabled = true;
});
$('#chkActive2').click(function () {
    document.getElementById("chkInactive2").checked = false;
    document.getElementById("chkElec").checked = true;
    document.getElementById("chkGas").checked = true;
    document.getElementById("chkWater").checked = true;

    document.getElementById("chkElec").disabled = false;
    document.getElementById("chkGas").disabled = false;
    document.getElementById("chkWater").disabled = false;

    document.getElementById("togWater1").disabled = false;
    document.getElementById("togWater2").disabled = false;
    document.getElementById("togGas1").disabled = false;
    document.getElementById("togGas2").disabled = false;
    document.getElementById("togElec1").disabled = false;
    document.getElementById("togElec2").disabled = false;
});
$('#chkElec').click(function () {
    if (document.getElementById("chkElec").checked == false)
    {
        document.getElementById("togWater1").disabled = true;
        document.getElementById("togGas1").disabled = true;
    }
    else
    {
        document.getElementById("togWater1").disabled = false;
        document.getElementById("togGas1").disabled = false;
    }
});
$('#chkGas').click(function () {
    if (document.getElementById("chkGas").checked == false) {
        document.getElementById("togWater2").disabled = true;
        document.getElementById("togElec1").disabled = true;
    }
    else {
        document.getElementById("togWater2").disabled = false;
        document.getElementById("togElec1").disabled = false;
    }
});
$('#chkWater').click(function () {
    if (document.getElementById("chkWater").checked == false) {
        document.getElementById("togElec2").disabled = true;
        document.getElementById("togGas2").disabled = true;
    }
    else {
        document.getElementById("togElec2").disabled = false;
        document.getElementById("togGas2").disabled = false;
    }
});
function getUSP() {
    $.ajax({
        type: 'POST',
        url: 'BusinessRequirements.aspx/GetUSP',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblUTILITY tbody').empty();
                $.each(records, function (idx, val) {
                    $('#tblUTILITY tbody').append(
                        '<tr  class="rowHover" onclick="ChangeUtil(this)">' +
                            '<td>' + val.USP_Name + '</td>' +
                        '</tr>'
                    );
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function getState() {
    $.ajax({
        type: 'POST',
        url: 'BusinessRequirements.aspx/GetState',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblSTATE tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblSTATE tbody').append(
                        '<tr  class="rowHover" onclick="ChangeUtil1(this)">' +
                            '<td>' + val.State + '</td>' +
                        '</tr>'
                    );
                });
            }


        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function getCity() {
    $.ajax({
        type: 'POST',
        url: 'BusinessRequirements.aspx/GetCity',
        contentType: 'application/json; charset=utf-8',
        data: "{'State':'" + sessionStorage.getItem("State") + "'}",
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblCITIES tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblCITIES tbody').append(
                        '<tr  class="rowHover" onclick="ChangeUtil2(this)">' +
                            '<td>' + val.City + '</td>' +
                        '</tr>'
                    );
                });
            }


        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
function getInspectionRequirements()
{
    if (document.getElementById("chkRequire").checked == true)
    {
        sessionStorage.setItem("Requirements", document.getElementById("ddlGLE").value +
            "|" + document.getElementById("txtVacancy").value + "|" + document.getElementById("ddlDWM").value)
    }
    else
    {
        sessionStorage.setItem("Requirements", "NONE");
    }
}
function getUSPsetting() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    document.getElementById("inactive").checked = true;
    document.getElementById("active").checked = false;
    document.getElementById("txtStandard1").disabled = true;
    document.getElementById("txtStandard2").disabled = true;
    document.getElementById("txtStandard3").disabled = true;

    document.getElementById("txtStandard1").value = '';
    document.getElementById("txtStandard2").value = '';
    document.getElementById("txtStandard3").value = '';


    document.getElementById("chkDeposit").disabled = true;
    document.getElementById("chkactivation").disabled = true;
    document.getElementById("chkLien").disabled = true;
    document.getElementById("chkDeposit").checked = false;
    document.getElementById("chkactivation").checked = false;
    document.getElementById("chkLien").checked = false;

    document.getElementById("chkInactive").checked = true;
    document.getElementById("chkActive").checked = false;

    document.getElementById("txtEmailAdd").disabled = true;
    document.getElementById("txtAttention").disabled = true;
    document.getElementById("txtFax").disabled = true;

    document.getElementById("txtEmailAdd").value = '';
    document.getElementById("txtAttention").value = '';
    document.getElementById("txtFax").value = '';


    document.getElementById("chkDeed").checked = false;
    document.getElementById("chkDeed").disabled = true;

    document.getElementById("chkPOA").checked = false;
    document.getElementById("chkPOA").disabled = true;

    document.getElementById("chkOcwenPOA").checked = false;
    document.getElementById("chkOcwenPOA").disabled = true;

    document.getElementById("chkForm").checked = false;
    document.getElementById("chkForm").disabled = true;

    document.getElementById("chkRequest").checked = false;
    document.getElementById("chkRequest").disabled = true;

    document.getElementById("chkAuthorization").checked = false;
    document.getElementById("chkAuthorization").disabled = true;

    document.getElementById("chkASFI").checked = false;
    document.getElementById("chkASFI").disabled = true;

    document.getElementById("chkListing").checked = false;
    document.getElementById("chkListing").disabled = true;


    ////////////////////////////////////////
    document.getElementById("chkInactive1").checked = true;
    document.getElementById("chkActive1").checked = false;

    document.getElementById("txtReport").disabled = true;
    document.getElementById("txtCityEmail").disabled = true;
    document.getElementById("txtAtten").disabled = true;
    document.getElementById("txtCityFax").disabled = true;

    document.getElementById("txtReport").value = '';
    document.getElementById("txtCityEmail").value = '';
    document.getElementById("txtAtten").value = '';
    document.getElementById("txtCityFax").value = '';

    document.getElementById("chkInactive2").checked = true;
    document.getElementById("chkActive2").checked = false;

    document.getElementById("chkElec").checked = false;
    document.getElementById("chkGas").checked = false;
    document.getElementById("chkWater").checked = false;

    document.getElementById("chkElec").disabled = true;
    document.getElementById("chkGas").disabled = true;
    document.getElementById("chkWater").disabled = true;

    $('#togWater1').bootstrapToggle('destroy');
    document.getElementById("togWater1").checked = false;
    document.getElementById("togWater1").disabled = true;
    $('#togWater1').bootstrapToggle({});

    $('#togGas1').bootstrapToggle('destroy');
    document.getElementById("togGas1").checked = false;
    document.getElementById("togGas1").disabled = true;
    $('#togGas1').bootstrapToggle({});

    $('#togWater2').bootstrapToggle('destroy');
    document.getElementById("togWater2").checked = false;
    document.getElementById("togWater2").disabled = true;
    $('#togWater2').bootstrapToggle({});

    $('#togElec1').bootstrapToggle('destroy');
    document.getElementById("togElec1").checked = false;
    document.getElementById("togElec1").disabled = true;
    $('#togElec1').bootstrapToggle({});

    $('#togElec2').bootstrapToggle('destroy');
    document.getElementById("togElec2").checked = false;
    document.getElementById("togElec2").disabled = true;
    $('#togElec2').bootstrapToggle({});

    $('#togGas2').bootstrapToggle('destroy');
    document.getElementById("togGas2").checked = false;
    document.getElementById("togGas2").disabled = true;
    $('#togGas2').bootstrapToggle({});

    document.getElementById("chkRequire").checked = false;
    var selected = 'selected="selected"';
    var processAs = ['<', '>', '='];
    var processAs1 = ['Days', 'Weeks', 'Months'];
    if ('AAA' == 'NONE') {
        $('#ddlGLE').empty()
       
        for (var i = 0; i < 3; i++) {
            if ($.inArray(processAs[i], '') !== -1) {
                ta2 = "<option value='" + processAs[i] + "' " + selected + ">" + processAs[i] + "</option>";
            } else {
                ta2 = "<option value='" + processAs[i] + "' manager='" + processAs[i] + "'>" + processAs[i] + "</option>";
            }
            $('#ddlGLE').append(ta2);

        }
        document.getElementById("txtVacancy").value = '1';
        $('#ddlDWM').empty()
        for (var i = 0; i < 3; i++) {
            if (processAs1[i] == '') {
                ta2 = "<option value='" + processAs1[i] + "' " + selected + ">" + processAs1[i] + "</option>";
            } else {
                ta2 = "<option value='" + processAs1[i] + "' manager='" + processAs1[i] + "'>" + processAs1[i] + "</option>";
            }
            $('#ddlDWM').append(ta2);
        }
    }
    else {
        $('#ddlGLE').empty()
        for (var i = 0; i < 3; i++) {
            if ($.inArray(processAs[i], '') !== -1) {
                ta2 = "<option value='" + processAs[i] + "' " + selected + ">" + processAs[i] + "</option>";
            } else {
                ta2 = "<option value='" + processAs[i] + "' manager='" + processAs[i] + "'>" + processAs[i] + "</option>";
            }
            $('#ddlGLE').append(ta2);

        }
        document.getElementById("txtVacancy").value = '1';
        $('#ddlDWM').empty()
        for (var i = 0; i < 3; i++) {
            if (processAs1[i] == '') {
                ta2 = "<option value='" + processAs1[i] + "' " + selected + ">" + processAs1[i] + "</option>";
            } else {
                ta2 = "<option value='" + processAs1[i] + "' manager='" + processAs1[i] + "'>" + processAs1[i] + "</option>";
            }
            $('#ddlDWM').append(ta2);
        }
    }

    document.getElementById("PhStreet").value = '';
    document.getElementById("PhCity").value = '';
    document.getElementById("PhState").value = '';
    document.getElementById("PhZipcode").value = '';

    document.getElementById("Pstreet").value = '';
    document.getElementById("Pcity").value = '';
    document.getElementById("Pstate").value = '';
    document.getElementById("Pzipcode").value = '';

    document.getElementById("POstreet").value = '';
    document.getElementById("POcity").value = '';
    document.getElementById("POstate").value = '';
    document.getElementById("POzipcode").value = '';

    var ddlArray = new Array();
    var ddl = document.getElementById('lbZip');
    for (i = 0; i < ddl.options.length; i++) {
        ddlArray[i] = ddl.options[i].value;
    }

    $('#lbZip').empty();
    $('#lbZip').multiselect('destroy');
   
   
    var selected1 = 'selected="selected"';
    for (var i = 0; i < ddlArray.length; i++) {
        if (ddlArray[i] == '') {
            ta2 = "<option value='" + ddlArray[i] + "' " + selected1 + " manager='" + ddlArray[i] + "'>" + ddlArray[i] + "</option>";
        } else {
            ta2 = "<option value='" + ddlArray[i] + "' manager='" + ddlArray[i] + "'>" + ddlArray[i] + "</option>";
        }
        $('#lbZip').append(ta2);
    }
    $('#lbZip').multiselect({
        numberDisplayed: 0,
        maxHeight: 200
    });


    $.ajax({
        type: 'POST',
        url: 'BusinessRequirements.aspx/GetUSPsettings',
        contentType: 'application/json; charset=utf-8',
        data: "{'USP':'" + sessionStorage.getItem("USP") + "'}",
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $.each(records, function (idx, val) {

                    if (val.Business_ID != '')
                    {
                        document.getElementById("inactive").checked = false;
                        document.getElementById("active").checked = true;
                        document.getElementById("txtStandard1").disabled = false;
                        document.getElementById("txtStandard2").disabled = false;
                        document.getElementById("txtStandard3").disabled = false;

                        document.getElementById("txtStandard1").value = val.Deposit;
                        document.getElementById("txtStandard2").value = val.Activation;
                        document.getElementById("txtStandard3").value = val.lien;

                        document.getElementById("chkDeposit").disabled = false;
                        document.getElementById("chkactivation").disabled = false;
                        document.getElementById("chkLien").disabled = false;
                        document.getElementById("chkDeposit").checked = true;
                        document.getElementById("chkactivation").checked = true;
                        document.getElementById("chkLien").checked = true;

                        document.getElementById("chkInactive").checked = false;
                        document.getElementById("chkActive").checked = true;

                        document.getElementById("txtEmailAdd").disabled = false;
                        document.getElementById("txtAttention").disabled = false;
                        document.getElementById("txtFax").disabled = false;

                        document.getElementById("txtEmailAdd").value = val.email;
                        document.getElementById("txtAttention").value = val.attention;
                        document.getElementById("txtFax").value = val.fax;

                        if(val.foreclosure == 'Required')
                        {
                            document.getElementById("chkDeed").disabled = false;
                            document.getElementById("chkDeed").checked = true;
                            
                        }
                        else
                        {
                            document.getElementById("chkDeed").checked = false;
                            document.getElementById("chkDeed").disabled = false;
                        }

                        if (val.investor == 'Required') {
                            document.getElementById("chkPOA").disabled = false;
                            document.getElementById("chkPOA").checked = true;

                        }
                        else {
                            document.getElementById("chkPOA").checked = false;
                            document.getElementById("chkPOA").disabled = false;
                        }

                        if (val.ocwen == 'Required') {
                            document.getElementById("chkOcwenPOA").disabled = false;
                            document.getElementById("chkOcwenPOA").checked = true;

                        }
                        else {
                            document.getElementById("chkOcwenPOA").checked = false;
                            document.getElementById("chkOcwenPOA").disabled = false;
                        }

                        if (val.application == 'Required') {
                            document.getElementById("chkForm").disabled = false;
                            document.getElementById("chkForm").checked = true;

                        }
                        else {
                            document.getElementById("chkForm").checked = false;
                            document.getElementById("chkForm").disabled = false;
                        }

                        if (val.request == 'Required') {
                            document.getElementById("chkRequest").disabled = false;
                            document.getElementById("chkRequest").checked = true;

                        }
                        else {
                            document.getElementById("chkRequest").checked = false;
                            document.getElementById("chkRequest").disabled = false;
                        }

                        if (val.letter == 'Required') {
                            document.getElementById("chkAuthorization").disabled = false;
                            document.getElementById("chkAuthorization").checked = true;

                        }
                        else {
                            document.getElementById("chkAuthorization").checked = false;
                            document.getElementById("chkAuthorization").disabled = false;
                        }

                        if (val.asfi == 'Required') {
                            document.getElementById("chkASFI").disabled = false;
                            document.getElementById("chkASFI").checked = true;

                        }
                        else {
                            document.getElementById("chkASFI").checked = false;
                            document.getElementById("chkASFI").disabled = false;
                        }

                        if (val.listing == 'Required') {
                            document.getElementById("chkListing").disabled = false;
                            document.getElementById("chkListing").checked = true;

                        }
                        else {

                            document.getElementById("chkListing").checked = false;
                            document.getElementById("chkListing").disabled = false;
                        }

                        document.getElementById("chkInactive1").checked = false;
                        document.getElementById("chkActive1").checked = true;

                        document.getElementById("txtReport").disabled = false;
                        document.getElementById("txtCityEmail").disabled = false;
                        document.getElementById("txtAtten").disabled = false;
                        document.getElementById("txtCityFax").disabled = false;

                        document.getElementById("txtReport").value = val.report;
                        document.getElementById("txtCityEmail").value = val.cityE;
                        document.getElementById("txtAtten").value = val.cityA;
                        document.getElementById("txtCityFax").value = val.cityF;

                        document.getElementById("chkInactive2").checked = false;
                        document.getElementById("chkActive2").checked = true;

                        document.getElementById("chkElec").checked = true;
                        document.getElementById("chkGas").checked = true;
                        document.getElementById("chkWater").checked = true;

                        document.getElementById("chkElec").disabled = false;
                        document.getElementById("chkGas").disabled = false;
                        document.getElementById("chkWater").disabled = false;


                        if (val.Activate_Elec_Water == 'ON')
                        {
                            $('#togWater1').bootstrapToggle('destroy');
                            document.getElementById("togWater1").checked = true;
                            document.getElementById("togWater1").disabled = false;
                            $('#togWater1').bootstrapToggle({
                              
                            });
                        }
                        else
                        {
                            $('#togWater1').bootstrapToggle('destroy');
                            document.getElementById("togWater1").checked = false;
                            document.getElementById("togWater1").disabled = false;
                            $('#togWater1').bootstrapToggle({ });
                        }

                        if (val.Activate_Elec_Gas == 'ON') {
                            $('#togGas1').bootstrapToggle('destroy');
                            document.getElementById("togGas1").checked = true;
                            document.getElementById("togGas1").disabled = false;
                            $('#togGas1').bootstrapToggle({

                            });
                        }
                        else {

                            $('#togGas1').bootstrapToggle('destroy');
                            document.getElementById("togGas1").checked = false;
                            document.getElementById("togGas1").disabled = false;
                            $('#togGas1').bootstrapToggle({});
                        }

                        if (val.Activate_Gas_Water == 'ON') {
                            $('#togWater2').bootstrapToggle('destroy');
                            document.getElementById("togWater2").checked = true;
                            document.getElementById("togWater2").disabled = false;
                            $('#togWater2').bootstrapToggle({

                            });
                        }
                        else {

                            $('#togWater2').bootstrapToggle('destroy');
                            document.getElementById("togWater2").checked = false;
                            document.getElementById("togWater2").disabled = false;
                            $('#togWater2').bootstrapToggle({});
                        }
                        
                        if (val.Activate_Gas_Elec == 'ON') {
                            $('#togElec1').bootstrapToggle('destroy');
                            document.getElementById("togElec1").checked = true;
                            document.getElementById("togElec1").disabled = false;
                            $('#togElec1').bootstrapToggle({

                            });
                        }
                        else {

                            $('#togElec1').bootstrapToggle('destroy');
                            document.getElementById("togElec1").checked = false;
                            document.getElementById("togElec1").disabled = false;
                            $('#togElec1').bootstrapToggle({});
                        }

                        if (val.Activate_Water_Elec == 'ON') {
                            $('#togElec2').bootstrapToggle('destroy');
                            document.getElementById("togElec2").checked = true;
                            document.getElementById("togElec2").disabled = false;
                            $('#togElec2').bootstrapToggle({

                            });
                        }
                        else {

                            $('#togElec2').bootstrapToggle('destroy');
                            document.getElementById("togElec2").checked = false;
                            document.getElementById("togElec2").disabled = false;
                            $('#togElec2').bootstrapToggle({});
                        }

                        if (val.Activate_Water_Gas == 'ON') {
                            $('#togGas2').bootstrapToggle('destroy');
                            document.getElementById("togGas2").checked = true;
                            document.getElementById("togGas2").disabled = false;
                            $('#togGas2').bootstrapToggle({

                            });
                        }
                        else {

                            $('#togGas2').bootstrapToggle('destroy');
                            document.getElementById("togGas2").checked = false;
                            document.getElementById("togGas2").disabled = false;
                            $('#togGas2').bootstrapToggle({});
                        }
                        
                        
                        
                        
                        var selected = 'selected="selected"';
                        var processAs = ['<', '>', '='];
                        var processAs1 = ['Days', 'Weeks', 'Months'];
                        var splitVal = val.inspection;
                        var splitThis = splitVal.split('|');
                        if (val.inspection != 'NONE')
                        {
                            $('#ddlGLE').empty()
                            document.getElementById("chkRequire").checked = true;
                            for (var i = 0; i < 3; i++) {
                                if ($.inArray(processAs[i], splitThis[0]) !== -1) {
                                    ta2 = "<option value='" + processAs[i] + "' " + selected + ">" + processAs[i] + "</option>";
                                } else {
                                    ta2 = "<option value='" + processAs[i] + "' manager='" + processAs[i] + "'>" + processAs[i] + "</option>";
                                }
                                $('#ddlGLE').append(ta2);
                                
                            }
                            document.getElementById("txtVacancy").value = splitThis[1];
                            $('#ddlDWM').empty()
                            for (var i = 0; i < 3; i++) {
                                if (processAs1[i] == splitThis[2]) {
                                    ta2 = "<option value='" + processAs1[i] + "' " + selected + ">" + processAs1[i] + "</option>";
                                } else {
                                    ta2 = "<option value='" + processAs1[i] + "' manager='" + processAs1[i] + "'>" + processAs1[i] + "</option>";
                                }
                                $('#ddlDWM').append(ta2);
                            }
                        }
                        else
                        {
                            $('#ddlGLE').empty()
                            document.getElementById("chkRequire").checked = false;
                            for (var i = 0; i < 3; i++) {
                                if ($.inArray(processAs[i], splitThis[0]) !== -1) {
                                    ta2 = "<option value='" + processAs[i] + "' " + selected + ">" + processAs[i] + "</option>";
                                } else {
                                    ta2 = "<option value='" + processAs[i] + "' manager='" + processAs[i] + "'>" + processAs[i] + "</option>";
                                }
                                $('#ddlGLE').append(ta2);

                            }
                            document.getElementById("txtVacancy").value = '1';
                            $('#ddlDWM').empty()
                            for (var i = 0; i < 3; i++) {
                                if (processAs1[i] == splitThis[2]) {
                                    ta2 = "<option value='" + processAs1[i] + "' " + selected + ">" + processAs1[i] + "</option>";
                                } else {
                                    ta2 = "<option value='" + processAs1[i] + "' manager='" + processAs1[i] + "'>" + processAs1[i] + "</option>";
                                }
                                $('#ddlDWM').append(ta2);
                            }
                        }
                        
                        
                        document.getElementById("PhStreet").value = val.Physical_Street;
                        document.getElementById("PhCity").value = val.Physical_City;
                        document.getElementById("PhState").value = val.Physical_State;
                        document.getElementById("PhZipcode").value = val.Physical_Zipcode;

                        document.getElementById("Pstreet").value = val.Payment_Street;
                        document.getElementById("Pcity").value = val.Payment_City;
                        document.getElementById("Pstate").value = val.Payment_State;
                        document.getElementById("Pzipcode").value = val.Payment_Zipcode;

                        document.getElementById("POstreet").value = val.PO_Street;
                        document.getElementById("POcity").value = val.PO_City;
                        document.getElementById("POstate").value = val.PO_State;
                        document.getElementById("POzipcode").value = val.PO_Zipcode;
                        
                        var ddlArray = new Array();
                        var ddl = document.getElementById('lbZip');
                        for (i = 0; i < ddl.options.length; i++) {
                            ddlArray[i] = ddl.options[i].value;
                        }
                        
                       
                        $('#lbZip').empty();
                        $('#lbZip').multiselect('destroy');
                        var zip = val.ZipCodes;
                        var Splitzip = zip.split(',');
                        var selected1 = 'selected="selected"';
                        for (var i = 0; i < ddlArray.length; i++) {

                            


                            if ($.inArray(ddlArray[i], Splitzip[i]) !== -1) {
                                ta2 = "<option value='" + ddlArray[i] + "' " + selected1 + " manager='" + ddlArray[i] + "'>" + ddlArray[i] + "</option>";
                            } else {
                                ta2 = "<option value='" + ddlArray[i] + "' manager='" + ddlArray[i] + "'>" + ddlArray[i] + "</option>";
                            }
                            $('#lbZip').append(ta2);
                        }
                        $('#lbZip').multiselect({
                            numberDisplayed: 0,
                            maxHeight: 200
                        });
                    }
                    else
                    {
                        


                    }
                   
                    
                    $('#modalLoading').modal('hide');
                });
            }


        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
    $('#modalLoading').modal('hide');
}
function ChangeUtil(Util) { 
    sessionStorage.setItem("USP", $(Util).closest("tr").find("td:lt(1)").text());
    if ($("#divMAINPANEL:visible").length == 0) {
        $("#divMAINPANEL").show();
    }
    if ($("#divBASEPANEL:visible").length == 0) {
        $("#divBASEPANEL").show();
    }
    //getState();
    
    $(Util).closest("table").find("tr").removeClass("selected");

    $(Util).addClass("selected");

    $(Util).addClass("selected");
    document.getElementById("inactive").checked = true;
    document.getElementById("active").checked = false;
    document.getElementById("txtStandard1").disabled = true;
    document.getElementById("txtStandard2").disabled = true;
    document.getElementById("txtStandard3").disabled = true;
    document.getElementById("chkDeposit").disabled = true;
    document.getElementById("chkactivation").disabled = true;
    document.getElementById("chkLien").disabled = true;
    document.getElementById("chkDeposit").checked = false;
    document.getElementById("chkactivation").checked = false;
    document.getElementById("chkLien").checked = false;

    //Documents
    //TextBox
    document.getElementById("txtEmailAdd").disabled = true;
    document.getElementById("txtAttention").disabled = true;
    document.getElementById("txtFax").disabled = true;

    document.getElementById("chkInactive1").checked = true;
    document.getElementById("txtReport").disabled = true;
    document.getElementById("txtCityEmail").disabled = true;
    document.getElementById("txtAtten").disabled = true;
    document.getElementById("txtCityFax").disabled = true;

    document.getElementById("chkInactive2").checked = true;
    document.getElementById("chkElec").disabled = true;
    document.getElementById("chkGas").disabled = true;
    document.getElementById("chkWater").disabled = true;

    document.getElementById("togWater1").disabled = true;
    document.getElementById("togWater2").disabled = true;
    document.getElementById("togGas1").disabled = true;
    document.getElementById("togGas2").disabled = true;
    document.getElementById("togElec1").disabled = true;
    document.getElementById("togElec2").disabled = true;

    getUSPsetting();
}
function ChangeUtil1(Util) {
    sessionStorage.setItem("State", $(Util).closest("tr").find("td:lt(1)").text());
    //if ($("#divMAINPANEL:visible").length == 0) {
    //    $("#divMAINPANEL").show();
    //}

   
   // getCity();
     
    $(Util).closest("table").find("tr").removeClass("selected");

    
}
function ChangeUtil2(Util) {
    sessionStorage.setItem("City", $(Util).closest("tr").find("td:lt(1)").text());
    if ($("#divBASEPANEL:visible").length == 0) {
        $("#divBASEPANEL").show();
    }
   

    $(Util).closest("table").find("tr").removeClass("selected");

    $(Util).addClass("selected");
    document.getElementById("inactive").checked = true;
    document.getElementById("active").checked = false;
    document.getElementById("txtStandard1").disabled = true;
    document.getElementById("txtStandard2").disabled = true;
    document.getElementById("txtStandard3").disabled = true;
    document.getElementById("chkDeposit").disabled = true; 
    document.getElementById("chkactivation").disabled = true;
    document.getElementById("chkLien").disabled = true;
    document.getElementById("chkDeposit").checked = false;
    document.getElementById("chkactivation").checked = false;
    document.getElementById("chkLien").checked = false;

    //Documents
    //TextBox
    document.getElementById("txtEmailAdd").disabled = true;
    document.getElementById("txtAttention").disabled = true;
    document.getElementById("txtFax").disabled = true;
    
    document.getElementById("chkInactive1").checked = true;
    document.getElementById("txtReport").disabled = true;
    document.getElementById("txtCityEmail").disabled = true;
    document.getElementById("txtAtten").disabled = true;
    document.getElementById("txtCityFax").disabled = true;

    document.getElementById("chkInactive2").checked = true;
    document.getElementById("chkElec").disabled = true;
    document.getElementById("chkGas").disabled = true;
    document.getElementById("chkWater").disabled = true;

    document.getElementById("togWater1").disabled = true;
    document.getElementById("togWater2").disabled = true;
    document.getElementById("togGas1").disabled = true;
    document.getElementById("togGas2").disabled = true;
    document.getElementById("togElec1").disabled = true;
    document.getElementById("togElec2").disabled = true;
}
function Document()
{
    if(document.getElementById("chkDeed").checked == true)
    {
        sessionStorage.setItem("Deed", 'Required');
    }
    else
    {
        sessionStorage.setItem("Deed", 'Not Required');
    }

    if (document.getElementById("chkRequest").checked == true) {
        sessionStorage.setItem("Request", 'Required');
    }
    else {
        sessionStorage.setItem("Request", 'Not Required');
    }

    if (document.getElementById("chkPOA").checked == true) {
        sessionStorage.setItem("POA", 'Required');
    }
    else {
        sessionStorage.setItem("POA", 'Not Required');
    }

    if (document.getElementById("chkAuthorization").checked == true) {
        sessionStorage.setItem("Autho", 'Required');
    }
    else {
        sessionStorage.setItem("Autho", 'Not Required');
    }

    if (document.getElementById("chkOcwenPOA").checked == true) {
        sessionStorage.setItem("OcwenPOA", 'Required');
    }
    else {
        sessionStorage.setItem("OcwenPOA", 'Not Required');
    }

    if (document.getElementById("chkASFI").checked == true) {
        sessionStorage.setItem("ASFI", 'Required');
    }
    else {
        sessionStorage.setItem("ASFI", 'Not Required');
    }
    if (document.getElementById("chkForm").checked == true) {
        sessionStorage.setItem("Form", 'Required');
    }
    else {
        sessionStorage.setItem("Form", 'Not Required');
    }
    if (document.getElementById("chkListing").checked == true) {
        sessionStorage.setItem("Listing", 'Required');
    }
    else {
        sessionStorage.setItem("Listing", 'Not Required');
    }

}
function Activation()
{
    if(document.getElementById("chkActive2").checked == true)
    {
        if (document.getElementById("chkElec").checked == true)
        {
            if (document.getElementById("togWater1").checked == true)
            {
                sessionStorage.setItem("Water1", 'ON');
            }
            else
            {
                sessionStorage.setItem("Water1", 'OFF');
            }
            if (document.getElementById("togGas1").checked == true)
            {
                sessionStorage.setItem("Gas1", 'ON');
            }
            else {
                sessionStorage.setItem("Gas1", 'OFF');
            }
        }
        else
        {
            sessionStorage.setItem("Water1", 'OFF');
            sessionStorage.setItem("Gas1", 'OFF');
        }
        ///////////
        if (document.getElementById("chkGas").checked == true)
        {
            if (document.getElementById("togWater2").checked == true)
            {
                sessionStorage.setItem("Water2", 'ON');
            }
            else {
                sessionStorage.setItem("Water2", 'OFF');
            }
            if (document.getElementById("togElec1").checked == true)
            {
                sessionStorage.setItem("Elec1", 'ON');
            }
            else {
                sessionStorage.setItem("Elec1", 'OFF');
            }
        }
        else
        {
            sessionStorage.setItem("Water2", 'OFF');
            sessionStorage.setItem("Elec1", 'OFF');
        }

        /////////////
        if (document.getElementById("chkWater").checked == true)
        {
            if (document.getElementById("togElec2").checked == true)
            {
                sessionStorage.setItem("Elec2", 'ON');
            }
            else {
                sessionStorage.setItem("Elec2", 'OFF');
            }
            if (document.getElementById("togGas2").checked == true)
            {
                sessionStorage.setItem("Gas2", 'ON');
            }
            else {
                sessionStorage.setItem("Gas2", 'OFF');
            }
        }
        else
        {
            essionStorage.setItem("Elec2", 'OFF');
            sessionStorage.setItem("Gas2", 'OFF');
        }
    }
    else
    {
        sessionStorage.setItem("Water1", 'OFF');
        sessionStorage.setItem("Water2", 'OFF');
        sessionStorage.setItem("Gas1", 'OFF');
        sessionStorage.setItem("Gas2", 'OFF');
        sessionStorage.setItem("Elec1", 'OFF');
        sessionStorage.setItem("Elec2", 'OFF');

    }
}
function zipCodes()
{
    var select = document.getElementById("lbZip");
    var SelectItem = "";
    var listLength = select.options.length;
    var countItem = 0;
    for (var i = 0; i < listLength; i++) {
        if (select.options[i].selected)
            if (SelectItem == "") {
                SelectItem = select.options[i].value;
                countItem++
            }
            else {
                SelectItem = SelectItem + ',' + select.options[i].value;
                countItem++
            }
    }
    sessionStorage.setItem("Zip", SelectItem);

}
function Inspection()
{
    
    if(document.getElementById("chkRequire").checked == true)
    {
        
        sessionStorage.setItem("GLE", document.getElementById("ddlGLE").value);
        sessionStorage.setItem("Vacancy", document.getElementById("txtVacancy").value);
        sessionStorage.setItem("DWM", document.getElementById("ddlDWM").value);
    }
    else
    {
        sessionStorage.setItem("GLE", '');
        sessionStorage.setItem("Vacancy", '');
        sessionStorage.setItem("DWM", '');
    }
}
function btnSave() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    Document();
    Activation();
    zipCodes();
    Inspection();
    getInspectionRequirements();
    $.ajax({
        type: 'POST',
        url: 'BusinessRequirements.aspx/SaveRequirements',
        data: "{'Deposit':'" + document.getElementById('txtStandard1').value
            + "','Activation':'" + document.getElementById('txtStandard2').value
            + "','Lien':'" + document.getElementById('txtStandard3').value
            + "','USP':'" + sessionStorage.getItem('USP')
            //+ "','State':'" + sessionStorage.getItem('State')
            //+ "','City':'" + sessionStorage.getItem('City') 
            + "','Email':'" + document.getElementById('txtEmailAdd').value
            + "','Attention':'" + document.getElementById('txtAttention').value
            + "','Fax':'" + document.getElementById('txtFax').value
            + "','Deed':'" + sessionStorage.getItem('Deed')
            + "','Request':'" + sessionStorage.getItem('Request')
            + "','POA':'" + sessionStorage.getItem('POA')
            + "','Autho':'" + sessionStorage.getItem('Autho')
            + "','OcwenPOA':'" + sessionStorage.getItem('OcwenPOA')
            + "','ASFI':'" + sessionStorage.getItem('ASFI')
            + "','Form':'" + sessionStorage.getItem('Form')
            + "','Listing':'" + sessionStorage.getItem('Listing')
            + "','Report':'" + document.getElementById('txtReport').value
            + "','CityEmail':'" + document.getElementById('txtCityEmail').value
            + "','Attention1':'" + document.getElementById('txtAtten').value
            + "','CityFax':'" + document.getElementById('txtCityFax').value
            + "','ElecWater':'" + sessionStorage.getItem('Water1')
            + "','ElecGas':'" + sessionStorage.getItem('Gas1')
            + "','GasWater':'" + sessionStorage.getItem('Water2')
            + "','GasElec':'" + sessionStorage.getItem('Elec1')
            + "','WaterElec':'" + sessionStorage.getItem('Elec2')
            + "','WaterGas':'" + sessionStorage.getItem('Gas2')
            + "','PStreet':'" + document.getElementById('PhStreet').value
            + "','PCity':'" + document.getElementById('PhCity').value
            + "','PState':'" + document.getElementById('PhState').value
            + "','PZip':'" + document.getElementById('PhZipcode').value
            + "','PayStreet':'" + document.getElementById('Pstreet').value
            + "','PayCity':'" + document.getElementById('Pcity').value
            + "','PayState':'" + document.getElementById('Pstate').value
            + "','PayZip':'" + document.getElementById('Pzipcode').value
            + "','POstreet':'" + document.getElementById('POstreet').value
            + "','POcity':'" + document.getElementById('POcity').value
            + "','POstate':'" + document.getElementById('POstate').value
            + "','POzip':'" + document.getElementById('POzipcode').value
            + "','Zip':'" + sessionStorage.getItem("Zip") 
            + "','Require':'" + sessionStorage.getItem("Requirements")
            + "','Check':'" + 'None'
            + "'}",
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                
                
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
    $('#modalLoading').modal('hide');
}
﻿$(document).ready(function () {
    ddlUSP();
    ShowInsert();

    //$('#lbIpages').empty();
    //$('#lbIpages').multiselect('destroy');
    $('#lbIpages').multiselect({
        numberDisplayed: 0,
        includeSelectAllOption: true,
        maxHeight: 200
    });

    $(document).on('click', ".table .btn", function (e) {
        e.preventDefault();
        id = $(this).attr('value')
        EditUSP(id);
        sessionStorage.setItem('USP_ID', id);
    });
});

function EditUSP(id) {

    var USPname = "";
    var Tasks = ["Start Service", "Stop Service"];
    var iPagesVal = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"];
    var iPagesSplit;



    $('#ddlEditUSP,#ddlEditTask,#lbEditIpages').empty();
    $('#lbEditIpages').multiselect('destroy');
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{"USP" : "' + id + '"}',
        url: 'OnlineRequestSetUp.aspx/EditThis',
        dataType: "json",
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                var records2 = d.data.asd2;
                $.each(records, function (idx, val) {

                    USPname = val.USP_Name;

                    for (i = 0; i < Tasks.length; i++) {
                        if (val.Task == Tasks[i]) {
                            ta2 = "<option value='" + Tasks[i] + "' selected='selected'>" + Tasks[i] + "</option>";
                        }
                        else {
                            ta2 = "<option value='" + Tasks[i] + "' >" + Tasks[i] + "</option>";

                        }
                        $('#ddlEditTask').append(ta2);
                    }
                    var srtSplit = val.iPages;
                    iPagesSplit = srtSplit.split(',');
                    var noneVal = "";
                    for (i = 0; i < iPagesVal.length; i++) {

                        for (ii = 0; ii < iPagesSplit.length; ii++) {
                            if (iPagesSplit[ii] == iPagesVal[i]) {
                                noneVal = "Selected"
                                ta2 = "<option value='" + iPagesVal[i] + "' selected='selected'>" + iPagesVal[i] + "</option>";
                            }
                            else if (noneVal == "Selected") {

                            }
                            else {
                                noneVal = "Not Selected"
                            }

                        }

                        if (noneVal == "Selected") {
                            noneVal = ""
                        }
                        else {
                            ta2 = "<option value='" + iPagesVal[i] + "' >" + iPagesVal[i] + "</option>";
                            noneVal = ""
                        }

                        $('#lbEditIpages').append(ta2);

                    }

                    document.getElementById("txtEditNoPages").value = val.NoOfPage;
                });
                $.each(records2, function (idx, val) {
                    if (val.USP_Name == USPname) {
                        ta2 = "<option value='" + val.USP_ID + "' selected='selected'>" + val.USP_Name.toUpperCase() + "</option>";
                    }
                    else {
                        ta2 = "<option value='" + val.USP_ID + "' >" + val.USP_Name.toUpperCase() + "</option>";

                    }
                    $('#ddlEditUSP').append(ta2);
                });
                $('#lbEditIpages').multiselect({
                    numberDisplayed: 0,
                    includeSelectAllOption: true,
                    maxHeight: 200
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}


function ddlUSP() {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'OnlineRequestSetUp.aspx/GetThis',
        dataType: "json",
        success: function (data) {

            var d = $.parseJSON(data.d);
            if (d.Success) {
                $("#ddlUSP").find('option').remove();
                var records = d.data.asd;
                $.each(records, function (idx, val) {
                    ta2 = "<option value='" + val.USP_ID + "' >" + val.USP_Name.toUpperCase() + "</option>";
                    $('#ddlUSP').append(ta2);
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function Edit(value) {
    // return '<button type="button" id="editPanel"  data-id="' + value + '" value="' + value + '" class="btn btn-default btn-sm"><i class="fa fa-edit"></i>&nbsp;Edit</button>'
    return '<p data-placement="top" data-toggle="tooltip" title="Edit"><button type="button" data-id="' + value + '" value="' + value
        + '" class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#myModalEdit" ><span class="glyphicon glyphicon-pencil"></span></button></p>'
}

function Update()
{
    var select = document.getElementById("lbEditIpages");
    var itemSelec = "";
    var listLength = select.options.length;
    for (var i = 0; i < listLength; i++) {
        if (select.options[i].selected)
            if (itemSelec == "") {
                itemSelec = select.options[i].value;
            }
            else {
                itemSelec = itemSelec + ',' + select.options[i].value;
            }
    }

    $.ajax({
        type: 'POST',
        data: '{"USP" : "' + document.getElementById("ddlEditUSP").value +
            '","USP_ID" : "' + sessionStorage.getItem('USP_ID') +
            '","Task" : "' + document.getElementById("ddlEditTask").value +
            '","Pages" : "' + document.getElementById("txtEditNoPages").value +
            '","iPages" : "' + itemSelec + '"}',
        contentType: 'application/json; charset=utf-8',
        url: 'OnlineRequestSetUp.aspx/Update',
        success: function (data) {
            ShowInsert();


            document.getElementById("ddlUSP").value = "";
            document.getElementById("ddlTask").value = "";
            document.getElementById("txtPages").value = "";




        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}


function Save() {
    var select = document.getElementById("lbIpages");
    var itemSelec = "";
    var listLength = select.options.length;
    for (var i = 0; i < listLength; i++) {
        if (select.options[i].selected)
            if (itemSelec == "") {
                itemSelec = select.options[i].value;
            }
            else {
                itemSelec = itemSelec + ',' + select.options[i].value;
            }
    }

    $.ajax({
        type: 'POST',
        data: '{"USP" : "' + document.getElementById("ddlUSP").value +
            '","Task" : "' + document.getElementById("ddlTask").value +
            '","Pages" : "' + document.getElementById("txtPages").value +
            '","iPages" : "' + itemSelec + '"}',
        contentType: 'application/json; charset=utf-8',
        url: 'OnlineRequestSetUp.aspx/Save',
        success: function (data) {
            ShowInsert();


            document.getElementById("ddlUSP").value = "";
            document.getElementById("ddlTask").value = "";
            document.getElementById("txtPages").value = "";




        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}
function ShowInsert(process) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('#tblUSP').bootstrapTable('destroy');
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'OnlineRequestSetUp.aspx/GetUSPs',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;
                $('#tblUSP').bootstrapTable({
                    data: records,
                    height: 560,
                    pagination: true
                });
                $('#modalLoading').modal('hide');
            }
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}
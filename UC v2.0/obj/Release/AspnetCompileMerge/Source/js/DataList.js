﻿$(document).ready(function () {
    getSched();
    
    $('#Button32').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/InsertMethod',
            data: "{'Client':'" + document.getElementById('txtclients').value + "'}",
            //async: false,
            success: function (response) {
                $('#txtclients').val(''); 
                getSched();
            },
            error: function () {
                console.log('there is some error');
            }
        });
        
    }); 
    $('#btnSave').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/SaveService',
            data: "{'Name':'" + document.getElementById('txtName').value
                + "','Description':'" + document.getElementById('txtDesc').value + "'}",
            //async: false,
            success: function (response) {
                $('#txtName').val('');
                $('#txtDesc').val(''); 
                alert("Record Has been Saved in Database");
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                console.log('there is some error');
            }
        });
    }); 
    $('#btnSave2').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/ClientStatus',
            data: "{'Name':'" + document.getElementById('txtName2').value
                + "','Description':'" + document.getElementById('txtDesc2').value + "'}",
            async: false,
            success: function (response) {
                $('#txtName2').val('');
                $('#txtDesc2').val('');

                alert("Record Has been Saved in Database");
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                console.log('there is some error');
            }
        });
    }); 
    $('#btnSave3').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/PODS',
            data: "{'Name':'" + document.getElementById('txtName3').value
                + "','Description':'" + document.getElementById('txtDesc3').value + "'}",
            async: false,
            success: function (response) {
                $('#txtName3').val('');
                $('#txtDesc3').val('');

                alert("Record Has been Saved in Database");
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                console.log('there is some error');
            }
        });
    });
    $('#btnSave4').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/StateCityZip',
            data: "{'State':'" + document.getElementById('txtState').value
                + "','City':'" + document.getElementById('txtCity').value   
                + "','Zip':'" + document.getElementById('txtZip').value + "'}",

            async: false,
            success: function (response) {
                $('#txtState').val('');
                $('#txtCity').val('');
                $('#txtZip').val('');

               
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                alert("Record Has not been Saved in Database");
                console.log('there is some error');
            }
        });
    });
    $('#btnSave5').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/PropertyType',
            data: "{'Name':'" + document.getElementById('txtName5').value
                + "','Description':'" + document.getElementById('txtDesc5').value + "'}",
            async: false,
            success: function (response) {
                $('#txtName5').val('');
                $('#txtDesc5').val('');

                alert("Record Has been Saved in Database");
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                console.log('there is some error');
            }
        });
    });
    $('#btnSave6').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/Occupancy',
            data: "{'Name':'" + document.getElementById('txtName6').value
                + "','Description':'" + document.getElementById('txtDesc6').value + "'}",
            async: false,
            success: function (response) {
                $('#txtName6').val('');
                $('#txtDesc6').val(''); 
                alert("Record Has been Saved in Database");
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                console.log('there is some error');
            }
        });
    }); 
    $('#btnSave7').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/PropertyStatus',
            data: "{'Name':'" + document.getElementById('txtName7').value
                + "','Description':'" + document.getElementById('txtDesc7').value + "'}",
            async: false,
            success: function (response) {
                $('#txtName7').val('');
                $('#txtDesc7').val('');
                alert("Record Has been Saved in Database");
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                console.log('there is some error');
            }
        });
    });
    $('#btnSave8').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/PreservationWork',
            data: "{'Name':'" + document.getElementById('txtName8').value
                + "','Description':'" + document.getElementById('txtDesc8').value + "'}",
            //async: false,
            success: function (response) {
                $('#txtName8').val('');
                $('#txtDesc8').val('');
                alert("Record Has been Saved in Database");
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                console.log('there is some error');
            }
        });
    }); 
    $('#btnSave10').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/USPName',
            data: "{'Name':'" + document.getElementById('txtUSP').value
                + "','Vendor':'" + document.getElementById('txtVendor').value
                + "','Phone':'" + document.getElementById('txtPhone').value
                + "','Email':'" + document.getElementById('txtEmail').value
                + "','address':'" + document.getElementById('txtAddress').value + "'}",
            //async: false,
            success: function (response) {
                $('#txtUSP').val('');
                $('#txtVendor').val('');
                $('#txtPhone').val('');
                $('#txtEmail').val('');
                $('#txtAddress').val('');
                alert("Record Has been Saved in Database");
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                console.log('there is some error');
            }
        });
    });
    $('#btnSave11').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/UtilityStatus',
            data: "{'Name':'" + document.getElementById('txtName9').value
                + "','Description':'" + document.getElementById('txtDesc9').value + "'}",
            //async: false,
            success: function (response) {
                $('#txtName9').val('');
                $('#txtDesc9').val('');
                alert("Record Has been Saved in Database");
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                console.log('there is some error');
            }
        });
    });
    $('#btnSave12').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/WorkOrderType',
            data: "{'Name':'" + document.getElementById('txtName10').value
                + "','Description':'" + document.getElementById('txtDesc10').value + "'}",
            //async: false,
            success: function (response) {
                $('#txtName10').val('');
                $('#txtDesc10').val('');
                alert("Record Has been Saved in Database");
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                console.log('there is some error');
            }
        });
    });
    $('#btnSave13').click(function () {
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'DataList.aspx/WorkOrderStatus',
            data: "{'Name':'" + document.getElementById('txtName11').value
                + "','Description':'" + document.getElementById('txtDesc11').value + "'}",
            //async: false,
            success: function (response) {
                $('#txtName11').val('');
                $('#txtDesc11').val('');
                alert("Record Has been Saved in Database");
                window.location.href = "DataList.aspx";
            },
            error: function (response) {
                console.log('there is some error');
            }
        });
    });
});

$('#serviceloc').click(function () {
    //Service Location
    getServiceLoc(sessionStorage.getItem("getValue"));
});
$('#clientS').click(function () {
    //Client Status 
    getClientStat(sessionStorage.getItem("getValue"));
  
});
$('#pods').click(function () {
    //PODS 
    getPODS(sessionStorage.getItem("getValue"));
});
$('#state').click(function () {
    //State City Zip 
    getStateCityZip(sessionStorage.getItem("getValue"));
});
$('#property').click(function () {
    //Property Type
    getPropertyType(sessionStorage.getItem("getValue")); 
});
$('#occupancy').click(function () {
    //Occupancy
    getOccupancy(sessionStorage.getItem("getValue"));
});
$('#propertyS').click(function () {
    //PropertyStatus
    getPropertyStatus(sessionStorage.getItem("getValue"));
});
$('#preservation').click(function () {
    //Preservation
    getPreservation(sessionStorage.getItem("getValue"));
});

$('#usp').click(function () {
    //USP
    getUSPNameAndVendor(sessionStorage.getItem("getValue")); 
});
$('#utility').click(function () {
    //Utility Status
    getUtilityStat(sessionStorage.getItem("getValue"));
});
$('#workT').click(function () {
    //Work Order Type
    getWorkOrderType(sessionStorage.getItem("getValue"));
});
$('#workS').click(function () {
    //Work Order Status
    getWorkOrderStatus(sessionStorage.getItem("getValue"));
});

function getSched() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'DataList.aspx/GetSched', 
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblClientName tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblClientName tbody').append(
                        '<tr  class="rowHover" onclick="ChangeUtil(this)">' + 
                            '<td>' + val.Client_Name + '<td>' +
                        '</tr>'
                    );
                });  
            }
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}  
function getServiceLoc(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetServiceLoc',
        data: "{'Client':'" + getVal + "'}",
        //async: false,
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblServiceLoc tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblServiceLoc tbody').append(
                        '<tr>' +
                            '<td >' + val.Service_Name +
                            '<td>' + val.Service_Desc +
                        '</tr>'
                    );
                });
            }

            $('#modalLoading').modal('hide');

        },

        error: function () {
            console.log('there is some error');
        }
    });

}  
function getClientStat(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetClientStatus',
        data: "{'Client':'" + getVal + "'}",
        //async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblClientStatus tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblClientStatus tbody').append(
                        '<tr>' +
                            '<td >' + val.Client_Name +
                            '<td>' + val.Client_Desc +
                        '</tr>'
                    );
                });
            }

            
            $('#modalLoading').modal('hide');
        },

        error: function () {
            console.log('there is some error');
        }
    });
}  
function getPODS(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetPODS',
        data: "{'Client':'" + getVal + "'}",
        //async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblPODS tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblPODS tbody').append(
                        '<tr>' +
                            '<td >' + val.POD_Name +
                            '<td>' + val.POD_Desc +
                        '</tr>'
                    );
                });
            }
            $('#modalLoading').modal('hide');
        },

        error: function () {
            console.log('there is some error');
        }
    });
    
}
function getStateCityZip(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetStateCityZip',
        data: "{'Client':'" + getVal + "'}",
        //async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblStateCityZip tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblStateCityZip tbody').append(
                        '<tr>' +
                            '<td >' + val.State +
                            '<td>' + val.City +
                            '<td>' + val.Zip +
                        '</tr>'
                    );
                });
            }

            $('#modalLoading').modal('hide');

        },

        error: function () {
            console.log('there is some error');
        }
    });
}  
function getPropertyType(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetPropertyType',
        data: "{'Client':'" + getVal + "'}",
        //async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblPropertyType tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblPropertyType tbody').append(
                        '<tr>' +
                            '<td >' + val.Pro_Name +
                            '<td>' + val.Pro_Desc + 
                        '</tr>'
                    );
                });
            }

            $('#modalLoading').modal('hide');

        },

        error: function () {
            console.log('there is some error');
        }
    });
}  
function getOccupancy(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetOccupancy',
        data: "{'Client':'" + getVal + "'}",
        //async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblOccupancy tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblOccupancy tbody').append(
                        '<tr>' +
                            '<td >' + val.Occu_Name +
                            '<td>' + val.Occu_Desc +
                        '</tr>'
                    );
                });
            }
            $('#modalLoading').modal('hide');
           

        },

        error: function () {
            console.log('there is some error');
        }
    });
}
function getPropertyStatus(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetPropertyStatus',
        data: "{'Client':'" + getVal + "'}",
        //async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblPropertyStatus tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblPropertyStatus tbody').append(
                        '<tr>' +
                            '<td >' + val.Property_Name +
                            '<td>' + val.Property_Desc +
                        '</tr>'
                    );
                });
            } 
            $('#modalLoading').modal('hide');
        },

        error: function () {
            console.log('there is some error');
        }
    });
}
function getPreservation(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetPreservation',
        data: "{'Client':'" + getVal + "'}",
        //async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblPreservation tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblPreservation tbody').append(
                        '<tr>' +
                            '<td >' + val.Work_Name +
                            '<td>' + val.Work_Desc +
                        '</tr>'
                    );
                });
            }
            $('#modalLoading').modal('hide');
        },

        error: function () {
            console.log('there is some error');
        }
    });
}
function getUSPNameAndVendor(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetUSPNameAndVendor',
        data: "{'Client':'" + getVal + "'}",
        //async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblUSPNameAndVendor tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblUSPNameAndVendor tbody').append(
                        '<tr>' +
                            '<td >' + val.USP_Name +
                            '<td>' + val.USP_VendorID +
                            '<td>' + val.USP_PhoneNo +
                            '<td>' + val.USP_Email +
                            '<td>' + val.USP_Address +
                        '</tr>'
                    );
                });
            }

            $('#modalLoading').modal('hide');

        },

        error: function () {
            console.log('there is some error');
        }
    });
} 
function getUtilityStat(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetUtilityStat',
        data: "{'Client':'" + getVal + "'}",
        //async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblUtilityStatus tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblUtilityStatus tbody').append(
                        '<tr>' +
                            '<td >' + val.Util_Name +
                            '<td>' + val.Util_Desc +
                        '</tr>'
                    );
                });
            }
            $('#modalLoading').modal('hide');
        },

        error: function () {
            console.log('there is some error');
        }
    });

}
function getWorkOrderType(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetWorkOrderType',
        data: "{'Client':'" + getVal + "'}",
        //async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblWorkOrderType tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblWorkOrderType tbody').append(
                        '<tr>' +
                            '<td >' + val.Work_Name +
                            '<td>' + val.Work_Desc +
                        '</tr>'
                    );
                });
            }
            $('#modalLoading').modal('hide');
        },

        error: function () {
            console.log('there is some error');
        }
    });

}
function getWorkOrderStatus(getVal) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'DataList.aspx/GetWorkOrderStatus',
        data: "{'Client':'" + getVal + "'}",
        //async: false,

        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {
                var records = d.data.asd;

                $('#tblWorkOrderStatus tbody').empty();

                $.each(records, function (idx, val) {
                    $('#tblWorkOrderStatus tbody').append(
                        '<tr>' +
                            '<td >' + val.wStatus_Name +
                            '<td>' + val.wStatus_Desc +
                        '</tr>'
                    );
                });
            }
            $('#modalLoading').modal('hide');
        },

        error: function () {
            console.log('there is some error');
        }
    });

}

 
function ChangeUtil(Util) {

    var getVal = $(Util).closest("tr").find("td:lt(2)").text();
    sessionStorage.setItem("getValue", getVal);
    
    
    
   
   
    if ($("#divMAINPANEL:visible").length == 0) {
        $("#divMAINPANEL").show();
    }

    $(Util).closest("table").find("tr").removeClass("selected");

    $(Util).addClass("selected");


    
}


   






 
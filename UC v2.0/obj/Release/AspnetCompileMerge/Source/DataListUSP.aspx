﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DataListUSP.aspx.cs" Inherits="UC_v2._0.DataListUSP" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-body" style="height: 681px">
                        <div class="table-responsive">
                            <iframe id="txtArea1" style="display:none"></iframe>
                           
                            <div style="display:none">
                                   <table id="tblUSPExport"class="table" style="margin-bottom: 5px;">
                                <thead>
                                    <tr>
                                        <th data-field="USP Name">USP Name </th>
                                        <th data-field="Email Address">Email Address</th>
                                        <th data-field="Website">Website</th>
                                        <th data-field="Login Name">Username</th>
                                        <th data-field="Password">Password</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                         
                            <table id="tblUSP" data-search="true" class="table" style="margin-bottom: 5px">
                                <thead>
                                    <tr>
                                        <th data-field="USP_ID">USP ID</th>
                                        <th data-field="USP_Name">USP Name </th>
                                        <th data-field="Email Address">Email Address</th>
                                        <th data-field="Website">Website</th>
                                        <th data-field="Login Name">Username</th>
                                        <th data-field="Password">Password</th>
                                        <th data-field="USP_ID" class="text-center" data-formatter="edit"></th>
                                        <th data-field="USP_ID" class="text-center" data-formatter="btnDelete"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <%--<button type="button" id="btnEdit" onclick="Edit(this)" class="btn btn-default btn-sm"><i class="fa fa-edit"></i>&nbsp;Edit</button>--%>
                        <br />
                        <div style="text-align: right; padding-right: 6px">
                            
                             <button id="btnExport" class="btn btn-primary btn-sm" onclick="fnExcelReport();"> EXPORT </button>
                            <button type="button" id="BBB" data-toggle="modal" data-target="#UploadExcel" class="btn btn-primary btn-sm">Upload</button>

                            <button type="button" id="AAA" onclick="AddNew(this)" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-sm">Add New</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span>Are you sure you want to delete this USP?</div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-success" onclick="DeleteThis(this)"><span class="glyphicon glyphicon-ok-sign"></span>Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="UploadExcel" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align: center">USP Excel File Upload</h4>
                </div>
                <div class="modal-body">

                    <br />
                    <div class="box box-success">
                        <br />
                        <br />
                        <div class="row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-8">
                                <div class="alert alert-info">
                                    <strong>Please!</strong> Get your Excel File into your Desktop.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-2">
                                <asp:FileUpload ID="fileUpload" ClientIDMode="Static" runat="server" />
                            </div>
                        </div>

                        <br />
                        <br />
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center">
                    <button type="button" id="btnUpload" class="btn btn-success btn-sm">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


















    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align: center">Add New USP</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-2"></div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="usr">USP Name:</label>
                                <input type="text" class="form-control" id="txtUSPName">
                            </div>
                        </div>
                        <div class="col-lg-4">

                            <div class="form-group">
                                <label for="usr">Email Address:</label>
                                <input type="text" class="form-control" id="txtEmailAdd">
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="usr">Website:</label>
                                <input type="text" class="form-control" id="txtWebsite">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="usr">UserName:</label>
                                <input type="text" class="form-control" id="txtUserName">
                            </div>
                        </div>
                        <div class="col-lg-4">

                            <div class="form-group">
                                <label for="usr">Password:</label>
                                <input type="text" class="form-control" id="txtPassword">
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2"></div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="usr">USP:</label>
                                <asp:ListBox ID="lbUSP" runat="server" class="form-control" SelectionMode="Multiple" multiple="multiple" ClientIDMode="Static"></asp:ListBox>
                            </div>
                        </div>
                        <div class="col-lg-4">

                           <%-- <div class="form-group">
                                <label for="usr">Vendor ID:</label>
                                <asp:ListBox ID="lbVendorID" runat="server" class="form-control" SelectionMode="Multiple" multiple="multiple" ClientIDMode="Static"></asp:ListBox>
                            </div>--%>

                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center">
                    <button type="button" id="btnSave" onclick="Save(this)" class="btn btn-success btn-sm">Add New</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalEdit" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align: center">Edit USP</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="usr">USP Name:</label>
                                <input type="text" class="form-control" id="txtEditUSPName">
                            </div>
                        </div>
                        <div class="col-lg-4">

                            <div class="form-group">
                                <label for="usr">Email Address:</label>
                                <input type="text" class="form-control" id="txtEditEmailAdd">
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="usr">Website:</label>
                                <input type="text" class="form-control" id="txtEditWebsite">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="usr">UserName:</label>
                                <input type="text" class="form-control" id="txtEditUserName">
                            </div>
                        </div>
                        <div class="col-lg-4">

                            <div class="form-group">
                                <label for="usr">Password:</label>
                                <input type="text" class="form-control" id="txtEditPassword">
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4">
                            <input type="checkbox" id="chkUSPEdit">
                            Do you want Update USP and Vendor ID?
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-2"></div>
                       
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="usr">USP:</label>
                                <asp:ListBox ID="lbEditUSP"  runat="server" class="form-control" SelectionMode="Multiple" multiple="multiple" ClientIDMode="Static"></asp:ListBox>
                            </div>
                        </div>
                       <%-- <div class="col-lg-4">
                            <div class="form-group">
                                <label for="usr">Vendor ID:</label>
                                <asp:ListBox ID="lbEditVendorID"  runat="server" class="form-control" SelectionMode="Multiple" multiple="multiple" ClientIDMode="Static"></asp:ListBox>
                            </div>

                        </div>--%>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center">

                    <button type="button" id="btnUpdate" onclick="Update(this)" class="btn btn-success btn-sm">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="Server">
    <script src="plugins/multiselect/bootstrap-multiselect.js"></script>
    <script src="js/USPdatalist.js"></script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DataList.aspx.cs" Inherits="UC_v2._0.DataList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        

    <style>
        body .divMain {
            height: 100% !important;
            /*background-color: yellow;*/
            overflow: auto;
        }

        .divMain #floatDiv {
            position: absolute;
            left: 14%;
            margin-top: 10px;
            /*margin-left: -200px;*/
            width: 200px;
            height: 110px;
            z-index: 1;
            /*background: rgb(136, 189, 35);*/
            /*box-shadow: black 10px 10px;*/
        }

        h4 {
            text-align: center;
            color: white;
        }

        .modal-header {
            background-color: #4d6578;
        }

        .panel-body {
            padding: 1px;
        }

        .panel-New123 > .panel-heading {
            color: #fff;
            background-color: #4d6578;
            border-color: #4d6578;
        }

        .panel-New > .panel-heading {
            color: #fff;
            background-color: #4d6578;
            border-color: #4d6578;
        }

        .panel-New > .panel-heading1 {
            color: #fff;
            background-color: #4d6578;
            border-color: #4d6578;
        }

        .panel-New > .panel-Newheading {
            color: #fff;
            background-color: #4d6578;
            border-color: #4d6578;
        }

        .panel-Newheading {
            padding: 0px 15px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }

        .col-sm-4 {
            padding-right: -7px;
        }

        .navbar {
            position: relative;
            min-height: 26px;
            margin-bottom: 20px;
            border: 1px solid transparent;
        }

        .form-control {
            display: block;
            width: 100%;
            height: 23px;
            padding: 6px 12px;
            font-size: 16px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }


        .panel {
            margin-bottom: 0px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
        }

        table tbody tr.selected {
            background: #81c341;
        }

        .panel-footer {
            padding: 10px 15px;
            background-color: #b1b1b1;
            border-top: 1px solid #ddd;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
        }

        .content-wrapper {
            min-height: 735px !important;
        }
          table tbody tr:hover {
            background: #8ce196;
            cursor: pointer;
        }

        table tbody tr.selected {
            background: #81c341;
        }

        #ulSETTINGS li input[type='text'] {
            width: 51px;
            padding: 3px 8px;
            height: 28px;
        }

        select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            padding: 3px 10px;
        }

        .navbar-inverse .navbar-nav > li > a {
    color: #e0e0e0;
}
        .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus {
            color: #fff;
            background-color: #3d4b5d;
        }

         .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=90);
        opacity: 0.3;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 480px;
        height: 183px;
    }
    .modalPopup4 {
    background-color: #FFFFFF;
    border-width: 3px;
    border-style: solid;
    border-color: black;
    padding-top: 10px;
    padding-left: 10px;
    width: 480px;
    height: 350px;
}
    .modalPopup3 {
    background-color: #FFFFFF;
    border-width: 3px;
    border-style: solid;
    border-color: black;
    padding-top: 10px;
    padding-left: 10px;
    width: 480px;
    height: 235px;
}
    .modalPopup2
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 920px;
        height: 430px;
    }

    </style>

  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div class="content-wrapper">
        

        <div class="row">
            <%--------------------------------------------------------------%>
        
            <div class="col-sm-12"">

                <div class="panel panel-New">
                    <div class="panel-heading1" style="height: 40px">
                    </div>
                    <div class="panel-body" style="overflow: auto; height: 730px; background-color: #ecf0f5">

                        <div class="row"> 
                            <div class="col-sm-2">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="panel panel-New"  >
                                            <div class="panel-Newheading" style="height: 40px">

                                                <div style="font-size: 27px">
                                                    <b>
                                                        <center>Client</center>
                                                    </b>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="height: 600px; font-size: medium">

                                                <table id="tblClientName" style="text-align: center" class="table no-border" data-util="Gas">
                                                    <tbody></tbody>
                                                </table>

                                            </div>
                                            <div class="panel-footer" style="background-color: #fff">
                                                <center>
                                <asp:Button runat="server"  id="btnshowClient" Text="Add New" class="btn btn-primary btn-sm"/> 
                            </center>


                                                <!-- ModalPopupExtender -->
                                                <cc1:ModalPopupExtender ID="ModalPopupExtender6" runat="server" PopupControlID="Panel7" TargetControlID="btnshowClient"
                                                    BackgroundCssClass="modalBackground">
                                                </cc1:ModalPopupExtender>
                                                <asp:Panel ID="Panel7" runat="server" CssClass="modalPopup" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                    <div class="panel">
                                                        <div class="panel-heading" style="background-color: #4d6578">
                                                            <div style="color: #fff"><b>Add New Client</b></div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-4" style="padding-left: 58px">
                                                            <b>Client Name</b><div style="float: right">:</div>
                                                        </div>
                                                        <div class="col-md-3" style="padding-left: 10px">
                                                            <input type="text" id="txtclients" class="form-control form-group-sm" />
                                                        </div>
                                                    </div>

                                                    <br />
                                                    <br />
                                                    <center>
                                                    <asp:Button ID="Button32" class="btn btn-primary" runat="server" ClientIDMode="Static" Text="Save" />
                                                          &nbsp&nbsp&nbsp&nbsp
                                                    <asp:Button ID="Button33" class="btn btn-primary" runat="server" Text="Cancel" />
                                                    </center>
                                                </asp:Panel>
                                            </div>

                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <div class="col-sm-10" style="padding-left: 3px; width: 1135px" id="divMAINPANEL">
                                <%--    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
         <ContentTemplate>--%>
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab_1" data-toggle="tab">Client and Geography Details</a></li>
                                        <li><a href="#tab_2" data-toggle="tab">Property Preservation Details</a></li>
                                        <li><a href="#tab_3" data-toggle="tab">Utility and Work Item Details</a></li>

                                        <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" style="min-height: 590px" id="tab_1">

                                            <nav class="navbar navbar-inverse" style="background-color: #4d6578">
                                                <div class="row">
                                                    <div class="col-sm-2"></div>
                                                    <div class="col-sm-9">
                                                        <ul class="nav navbar-nav">
                                                            <p class="navbar-text" style="margin: 10px">| &nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li ><a data-toggle="tab" id="serviceloc" href="#Service">Service Location</a></li>
                                                            <p class="navbar-text"  style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li><a data-toggle="tab" id="clientS" href="#Client">Client Status</a></li>
                                                            <p class="navbar-text"  style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li><a data-toggle="tab" id="pods" href="#PODS">PODS</a></li>
                                                            <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li><a data-toggle="tab" id="state" href="#State">State, City and Zip</a></li>
                                                            <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                        </ul>
                                                    </div>

                                                </div>

                                            </nav>

                                            <div class="tab-content">
                                                <div id="Service" class="tab-pane fade">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                        <%--    <Triggers>
                           <asp:PostBackTrigger ControlID="btnSave" />
                       </Triggers>--%>
                                                        <ContentTemplate>
                                                            <div class="panel">
                                                                <div class="panel-body">

                                                                    <table id="tblServiceLoc" class="table table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Name</th>
                                                                                <th>Description</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody></tbody>
                                                                    </table>


                                                                </div>
                                                                <div class="panel-footer" style="float: right; background-color: white">

                                                                    <asp:Button ID="btnServiceLoc" class="btn btn-primary" runat="server" Text="Add New" />
                                                                </div>
                                                            </div>

                                                            <!-- ModalPopupExtender -->
                                                            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="SerLoc" TargetControlID="btnServiceLoc"
                                                                BackgroundCssClass="modalBackground">
                                                            </cc1:ModalPopupExtender>
                                                            <asp:Panel ID="SerLoc" runat="server" CssClass="modalPopup3" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                <div class="panel">
                                                                    <div class="panel-heading" style="background-color: #4d6578">
                                                                        <div style="color: #fff"><b>Add New Service Location</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Name</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtName" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Description</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtDesc" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <br />
                                                                <center>
                                                             <asp:Button ID="btnSave" class="btn btn-primary" runat="server" ClientIDMode="Static"  Text="Save" />
                                                                    &nbsp&nbsp&nbsp&nbsp
                                                                        <asp:Button ID="Button2" class="btn btn-primary" runat="server" Text="Cancel" />
                                                                 </center>
                                                            </asp:Panel>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>


                                                <div id="Client" class="tab-pane fade">
                                                    <div class="panel">
                                                        <div class="panel-body">
                                                            <table id="tblClientStatus" class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 300px">Name</th>
                                                                        <th>Description</th>

                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer" style="float: right; background-color: white">
                                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                <%--    <Triggers>
                           <asp:PostBackTrigger ControlID="btnSave" />
                       </Triggers>--%>
                                                                <ContentTemplate>
                                                                    <asp:Button ID="Addnew2" class="btn btn-primary" runat="server" Text="Add New" />

                                                                    <!-- ModalPopupExtender -->
                                                                    <cc1:ModalPopupExtender ID="ModalPopupExtender3" runat="server" PopupControlID="Panel2" TargetControlID="Addnew2"
                                                                        BackgroundCssClass="modalBackground">
                                                                    </cc1:ModalPopupExtender>
                                                                    <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup3" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                        <div class="panel">
                                                                            <div class="panel-heading" style="background-color: #4d6578">
                                                                                <div style="color: #fff"><b>Add New Client Status</b></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                            <div class="col-md-2"></div>
                                                                            <div class="col-md-4" style="padding-left: 58px">
                                                                                <b>Name</b><div style="float: right">:</div>
                                                                            </div>
                                                                            <div class="col-md-3" style="padding-left: 10px">
                                                                                <input type="text" id="txtName2" class="form-control form-group-sm" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                            <div class="col-md-2"></div>
                                                                            <div class="col-md-4" style="padding-left: 58px">
                                                                                <b>Description</b><div style="float: right">:</div>
                                                                            </div>
                                                                            <div class="col-md-3" style="padding-left: 10px">
                                                                                <input type="text" id="txtDesc2" class="form-control form-group-sm" />
                                                                            </div>
                                                                        </div>
                                                                        <br />
                                                                        <br />
                                                                        <center>
     <asp:Button ID="btnSave2" class="btn btn-primary" runat="server" ClientIDMode="Static"  Text="Save" />
    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button12" class="btn btn-primary" runat="server"  Text="Cancel" />
        </center>
                                                                    </asp:Panel>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="PODS" class="tab-pane fade">
                                                    <div class="panel">
                                                        <div class="panel-body">

                                                            <table id="tblPODS" class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 300px">Name</th>
                                                                        <th>Description</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer" style="float: right; background-color: white">

                                                            <asp:Button ID="Addnew3" class="btn btn-primary" runat="server" Text="Add New" />


                                                            <!-- ModalPopupExtender -->
                                                            <cc1:ModalPopupExtender ID="ModalPopupExtender4" runat="server" PopupControlID="Panel3" TargetControlID="Addnew3"
                                                                BackgroundCssClass="modalBackground">
                                                            </cc1:ModalPopupExtender>
                                                            <asp:Panel ID="Panel3" runat="server" CssClass="modalPopup3" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                <div class="panel">
                                                                    <div class="panel-heading" style="background-color: #4d6578">
                                                                        <div style="color: #fff"><b>Add New PODS</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Name</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtName3" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Description</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtDesc3" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <br />
                                                                <center>
     <asp:Button ID="btnSave3" class="btn btn-primary" runat="server" ClientIDMode="Static"  Text="Save" />
    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button3" class="btn btn-primary" runat="server"  Text="Cancel" />
        </center>
                                                            </asp:Panel>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="State" class="tab-pane fade">
                                                    <div class="panel">
                                                        <div class="panel-body">
                                                            <table id="tblStateCityZip" class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 300px">State</th>
                                                                        <th>City</th>
                                                                        <th>Zip</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer" style="float: right; background-color: white">

                                                            <asp:Button ID="Addnew4" class="btn btn-primary" runat="server" Text="Add New" />

                                                            <!-- ModalPopupExtender -->
                                                            <cc1:ModalPopupExtender ID="ModalPopupExtender5" runat="server" PopupControlID="Panel4" TargetControlID="Addnew4"
                                                                BackgroundCssClass="modalBackground">
                                                            </cc1:ModalPopupExtender>
                                                            <asp:Panel ID="Panel4" runat="server" CssClass="modalPopup3" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                <div class="panel">
                                                                    <div class="panel-heading" style="background-color: #4d6578">
                                                                        <div style="color: #fff"><b>Add New State , City and Zip</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Name</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtState" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 10px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Description</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtCity" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 10px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Description</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtZip" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <center>
     <asp:Button ID="btnSave4" class="btn btn-primary" runat="server" ClientIDMode="Static"  Text="Save" />
    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button4" class="btn btn-primary" runat="server"  Text="Cancel" />
        </center>
                                                            </asp:Panel>

                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" style="min-height: 590px" id="tab_2">


                                            <nav class="navbar navbar-inverse" style="background-color: #4d6578">
                                                <div class="row">
                                                    <div class="col-sm-1"></div>
                                                    <div class="col-sm-10">
                                                        <ul class="nav navbar-nav">
                                                            <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp| &nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li ><a data-toggle="tab" id="property" href="#Type">Property Type</a></li>
                                                            <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li><a data-toggle="tab" href="#Occupancy" id="occupancy">Occupancy Status</a></li>
                                                            <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li><a data-toggle="tab" href="#Status" id="propertyS">Property Status</a></li>
                                                            <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li><a data-toggle="tab" href="#Preservation" id="preservation">Preservation Work Type</a></li>
                                                            <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                        </ul>
                                                    </div>

                                                </div>

                                            </nav>
                                            <div class="tab-content">
                                                <div id="Type" class="tab-pane fade ">
                                                    <div class="panel">
                                                        <div class="panel-body">

                                                            <table id="tblPropertyType" class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 300px">Name</th>
                                                                        <th>Description</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer" style="float: right; background-color: white">

                                                            <asp:Button ID="Addnew5" class="btn btn-primary" runat="server" Text="Add New" />
                                                            <!-- ModalPopupExtender -->
                                                            <cc1:ModalPopupExtender ID="ModalPopupExtender7" runat="server" PopupControlID="Panel5" TargetControlID="Addnew5"
                                                                BackgroundCssClass="modalBackground">
                                                            </cc1:ModalPopupExtender>
                                                            <asp:Panel ID="Panel5" runat="server" CssClass="modalPopup3" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                <div class="panel">
                                                                    <div class="panel-heading" style="background-color: #4d6578">
                                                                        <div style="color: #fff"><b>Add New PODS</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Name</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtName5" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Description</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtDesc5" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <br />
                                                                <center>
     <asp:Button ID="btnSave5" class="btn btn-primary" runat="server" ClientIDMode="Static"  Text="Save" />
    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button5" class="btn btn-primary" runat="server"  Text="Cancel" />
        </center>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Occupancy" class="tab-pane fade">
                                                    <div class="panel">
                                                        <div class="panel-body">

                                                            <table id="tblOccupancy" class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 300px">Name</th>
                                                                        <th>Description</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer" style="float: right; background-color: white">

                                                            <asp:Button ID="Addnew6" class="btn btn-primary" runat="server" Text="Add New" />
                                                            <!-- ModalPopupExtender -->
                                                            <cc1:ModalPopupExtender ID="ModalPopupExtender8" runat="server" PopupControlID="Panel6" TargetControlID="Addnew6"
                                                                BackgroundCssClass="modalBackground">
                                                            </cc1:ModalPopupExtender>
                                                            <asp:Panel ID="Panel6" runat="server" CssClass="modalPopup3" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                <div class="panel">
                                                                    <div class="panel-heading" style="background-color: #4d6578">
                                                                        <div style="color: #fff"><b>Add New PODS</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Name</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtName6" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Description</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtDesc6" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <br />
                                                                <center>
     <asp:Button ID="btnSave6" class="btn btn-primary" runat="server" ClientIDMode="Static"  Text="Save" />
    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button6" class="btn btn-primary" runat="server"  Text="Cancel" />
        </center>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Status" class="tab-pane fade">
                                                    <div class="panel">
                                                        <div class="panel-body">

                                                            <table id="tblPropertyStatus" class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 300px">Name</th>
                                                                        <th>Description</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer" style="float: right; background-color: white">

                                                            <asp:Button ID="Addnew7" class="btn btn-primary" runat="server" Text="Add New" />
                                                            <!-- ModalPopupExtender -->
                                                            <cc1:ModalPopupExtender ID="ModalPopupExtender9" runat="server" PopupControlID="Panel8" TargetControlID="Addnew7"
                                                                BackgroundCssClass="modalBackground">
                                                            </cc1:ModalPopupExtender>
                                                            <asp:Panel ID="Panel8" runat="server" CssClass="modalPopup3" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                <div class="panel">
                                                                    <div class="panel-heading" style="background-color: #4d6578">
                                                                        <div style="color: #fff"><b>Add New PODS</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Name</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtName7" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Description</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtDesc7" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <br />
                                                                <center>
     <asp:Button ID="btnSave7" class="btn btn-primary" runat="server" ClientIDMode="Static"  Text="Save" />
    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button7" class="btn btn-primary" runat="server"  Text="Cancel" />
        </center>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Preservation" class="tab-pane fade">
                                                    <div class="panel">
                                                        <div class="panel-body">

                                                            <table id="tblPreservation" class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 300px">Name</th>
                                                                        <th>Description</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer" style="float: right; background-color: white">

                                                            <asp:Button ID="Addnew8" class="btn btn-primary" runat="server" Text="Add New" />
                                                            <!-- ModalPopupExtender -->
                                                            <cc1:ModalPopupExtender ID="ModalPopupExtender10" runat="server" PopupControlID="Panel9" TargetControlID="Addnew8"
                                                                BackgroundCssClass="modalBackground">
                                                            </cc1:ModalPopupExtender>
                                                            <asp:Panel ID="Panel9" runat="server" CssClass="modalPopup3" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                <div class="panel">
                                                                    <div class="panel-heading" style="background-color: #4d6578">
                                                                        <div style="color: #fff"><b>Add New PODS</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Name</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtName8" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Description</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtDesc8" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <br />
                                                                <center>
     <asp:Button ID="btnSave8" class="btn btn-primary" runat="server" ClientIDMode="Static"  Text="Save" />
    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button10" class="btn btn-primary" runat="server"  Text="Cancel" />
        </center>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" style="min-height: 590px" id="tab_3">

                                            <nav class="navbar navbar-inverse" style="background-color: #4d6578">
                                                <div class="row">
                                                    <div class="col-sm-1"></div>
                                                    <div class="col-sm-10">
                                                        <ul class="nav navbar-nav">
                                                            <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp| &nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li class="active"><a data-toggle="tab" id="usp" href="#USP">USP Name and Vendor ID</a></li>
                                                            <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li><a data-toggle="tab" href="#Utility" id="utility">Utility Status</a></li>
                                                            <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li><a data-toggle="tab" href="#Work" id="workT">Work Order Type</a></li>
                                                            <p class="navbar-text" style="margin: 10px" >&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                            <li><a data-toggle="tab" href="#WorkStatus" id="workS">Work Order Status</a></li>
                                                            <p class="navbar-text" style="margin: 10px">&nbsp &nbsp &nbsp&nbsp&nbsp|&nbsp &nbsp &nbsp&nbsp&nbsp</p>
                                                        </ul>
                                                    </div> 
                                                </div>

                                            </nav>

                                            <div class="tab-content">
                                                <div id="USP" class="tab-pane fade active in">
                                                    <div class="panel">
                                                        <div class="panel-body">
                                                            <table id="tblUSPNameAndVendor" class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 150px">USP</th>
                                                                        <th style="width: 100px">Vendor ID</th>
                                                                        <th style="width: 130px">Phone Number</th>
                                                                        <th style="width: 200px">Email Address</th>
                                                                        <th>Address</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer" style="float: right; background-color: white">

                                                            <asp:Button ID="btnUtil" class="btn btn-primary" runat="server" Text="Add New" />

                                                            <!-- ModalPopupExtender -->
                                                            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel1" TargetControlID="btnUtil"
                                                                BackgroundCssClass="modalBackground">
                                                            </cc1:ModalPopupExtender>
                                                            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup4" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                <div class="panel">
                                                                    <div class="panel-heading" style="background-color: #4d6578">
                                                                        <div style="color: #fff"><b>Add New Service Location</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>USP Name</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input id="txtUSP" type="text" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>


                                                                <div class="row" style="padding-left: 0px; padding-top: 15px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Vendor ID</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input id="txtVendor" type="text" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>

                                                                <div class="row" style="padding-left: 0px; padding-top: 15px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Phone Number</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input id="txtPhone" type="text" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>

                                                                <div class="row" style="padding-left: 0px; padding-top: 15px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Email Address</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input id="txtEmail" type="text" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 15px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Address</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input id="txtAddress" type="text" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <br />
                                                                <center>
     <asp:Button ID="btnSave10" class="btn btn-primary" ClientIDMode="Static"  runat="server" Text="Save" />
    &nbsp&nbsp&nbsp&nbsp
    <asp:Button ID="Button9" class="btn btn-primary" runat="server" Text="Cancel" />
        </center>
                                                            </asp:Panel>


                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Utility" class="tab-pane fade">
                                                    <div class="panel">
                                                        <div class="panel-body">

                                                            <table id="tblUtilityStatus" class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 300px">Name</th>
                                                                        <th>Description</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer" style="float: right; background-color: white">
                                                            <asp:Button ID="Addnew9" class="btn btn-primary" runat="server" Text="Add New" />
                                                            <!-- ModalPopupExtender -->
                                                            <cc1:ModalPopupExtender ID="ModalPopupExtender11" runat="server" PopupControlID="Panel10" TargetControlID="Addnew9"
                                                                BackgroundCssClass="modalBackground">
                                                            </cc1:ModalPopupExtender>
                                                            <asp:Panel ID="Panel10" runat="server" CssClass="modalPopup3" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                <div class="panel">
                                                                    <div class="panel-heading" style="background-color: #4d6578">
                                                                        <div style="color: #fff"><b>Add New PODS</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Name</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtName9" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Description</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtDesc9" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <br />
                                                                <center>
                                                               <asp:Button ID="btnSave11" class="btn btn-primary" runat="server" ClientIDMode="Static"  Text="Save" />
                                                                  &nbsp&nbsp&nbsp&nbsp
                                                                  <asp:Button ID="Button11" class="btn btn-primary" runat="server"  Text="Cancel" />
                                                                    </center>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Work" class="tab-pane fade">
                                                    <div class="panel">
                                                        <div class="panel-body">

                                                            <table id="tblWorkOrderType" class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 300px">Name</th>
                                                                        <th>Description</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer" style="float: right; background-color: white">
                                                            <asp:Button ID="Addnew10" class="btn btn-primary" runat="server" Text="Add New" />
                                                            <!-- ModalPopupExtender -->
                                                            <cc1:ModalPopupExtender ID="ModalPopupExtender12" runat="server" PopupControlID="Panel11" TargetControlID="Addnew10"
                                                                BackgroundCssClass="modalBackground">
                                                            </cc1:ModalPopupExtender>
                                                            <asp:Panel ID="Panel11" runat="server" CssClass="modalPopup3" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                <div class="panel">
                                                                    <div class="panel-heading" style="background-color: #4d6578">
                                                                        <div style="color: #fff"><b>Add New PODS</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Name</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtName10" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Description</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtDesc10" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <br />
                                                                <center>
                                                               <asp:Button ID="btnSave12" class="btn btn-primary" runat="server" ClientIDMode="Static"  Text="Save" />
                                                                  &nbsp&nbsp&nbsp&nbsp
                                                                  <asp:Button ID="Button13" class="btn btn-primary" runat="server"  Text="Cancel" />
                                                                    </center>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="WorkStatus" class="tab-pane fade">
                                                    <div class="panel">
                                                        <div class="panel-body">

                                                            <table id="tblWorkOrderStatus" class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 300px">Name</th>
                                                                        <th>Description</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer" style="float: right; background-color: white">
                                                            <asp:Button ID="Addnew11" class="btn btn-primary" runat="server" Text="Add New" />
                                                            <!-- ModalPopupExtender -->
                                                            <cc1:ModalPopupExtender ID="ModalPopupExtender13" runat="server" PopupControlID="Panel12" TargetControlID="Addnew11"
                                                                BackgroundCssClass="modalBackground">
                                                            </cc1:ModalPopupExtender>
                                                            <asp:Panel ID="Panel12" runat="server" CssClass="modalPopup3" align="Left" Style="display: none; padding-left: 0px; padding-top: 0px">
                                                                <div class="panel">
                                                                    <div class="panel-heading" style="background-color: #4d6578">
                                                                        <div style="color: #fff"><b>Add New PODS</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Name</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtName11" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="padding-left: 0px; padding-top: 30px">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-4" style="padding-left: 58px">
                                                                        <b>Description</b><div style="float: right">:</div>
                                                                    </div>
                                                                    <div class="col-md-3" style="padding-left: 10px">
                                                                        <input type="text" id="txtDesc11" class="form-control form-group-sm" />
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <br />
                                                                <center>
                                                                    <asp:Button ID="btnSave13" class="btn btn-primary" runat="server" ClientIDMode="Static"  Text="Save" />
                                                                  &nbsp&nbsp&nbsp&nbsp
                                                                  <asp:Button ID="Button14" class="btn btn-primary" runat="server"  Text="Cancel" />
                                                                    </center>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>



                                            </div>


                                        </div>
                                        <!-- /.tab-pane -->
                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <%--   </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                            </div>







                        </div>
                    </div>

                </div>
            <%--------------------------------------------------------------%>

                


            </div>

        </div>

          
    </div>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
    <script src="js/DataList.js"></script>
    
</asp:Content>



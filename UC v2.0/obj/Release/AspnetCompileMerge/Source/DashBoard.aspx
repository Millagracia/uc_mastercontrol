﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" EnableEventValidation="False" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="UC_v2._0.DashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 733px">
        <br />
       <%-- <div class="row">
            <div class="col-sm-2"></div>--%>
            <%-- <div class="col-sm-2">
                <asp:Button ID="btnRUN" ClientIDMode="Static" OnClick="btnRUN_Click" class="btn btn-danger btn-block" runat="server" Text="Run Process" />
            </div>--%>
        <%--    <div class="col-sm-4">
                <div id="Bar" class="main-container collapse">
                    <button id="btnLoading" class="btn btn-default btn-block" type="button"><i class="fa fa-spinner fa-spin"></i>Loading... Please Wait..</button>
                </div>
            </div>
        </div>
        <br />--%>
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_3" data-toggle="tab" aria-expanded="true">USP Process</a></li>
                        <li class=""><a href="#tab_2" id="Tusp" data-toggle="tab" aria-expanded="false">Start Service</a></li>
                        <li class=""><a href="#tab_1" id="Tprio" data-toggle="tab" aria-expanded="true">Stop Service</a></li>
                        <%--<li class=""><a href="#tab_4" id="Tother" data-toggle="tab" aria-expanded="true">Other Settings</a></li>--%>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_3">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <table id="tblUSPprocess" class="table" style="margin-bottom: 5px">
                                            <thead>
                                                <tr>
                                                    <th data-field="USP_Name">USP Name </th>
                                                    <th data-field="Start_Process">Start Service</th>
                                                    <th data-field="Stop_Process">Stop Service</th>
                                                    <th data-field="Invoicing">Invoicing</th>
                                                    <th data-field="Bills_Process">BillsPayment</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>




                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <table id="tblStartService" class="table" style="margin-bottom: 5px"   data-show-columns="true" data-mobile-responsive="true" data-show-export="true" data-page-list="[10, 25, 50, 100, ALL]" data-height="499"> 
                                            <thead>
                                                <tr>
                                                    <th data-field="Start Service" data-sortable="true">Process </th>
                                                    <th data-field="VendorId" data-sortable="true">Vendor ID</th>
                                                    <th data-field="USP" data-sortable="true">USP</th>
                                                    <th data-field="Address" data-sortable="true">Address</th>
                                                    <th data-field="Property ID" data-sortable="true">Property ID</th>
                                                    <th data-field="Utility" data-sortable="true">Utility</th>
                                                    <th data-field="InflowDate" data-sortable="true">Inflow Date</th>
                                                    <th data-field="Status" data-sortable="true">Status</th>
                                                    <th data-field="PostedDate" data-sortable="true">PostedDate</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <table id="tblStopService" class="table" style="margin-bottom: 5px"   data-show-columns="true" data-mobile-responsive="true" data-show-export="true" data-page-list="[10, 25, 50, 100, ALL]" data-height="499"> 
                                            <thead>
                                                <tr>
                                                    <th data-field="Stop Service" data-sortable="true">Process </th>
                                                    <th data-field="USP" data-sortable="true">USP</th>
                                                    <th data-field="VendorId" data-sortable="true">USP</th>
                                                    <th data-field="Property ID" data-sortable="true">Property ID</th>
                                                    <th data-field="Address" data-sortable="true">Address</th>
                                                    <th data-field="Utility" data-sortable="true">Utility</th>
                                                    <th data-field="PreviousStatus" data-sortable="true">PreviousStatus</th>
                                                    <th data-field="PostedDate" data-sortable="true">PostedDate</th>
                                                    <th data-field="Status" data-sortable="true">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--<div class="tab-pane" id="tab_4">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <div class="row">
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                        <div style="text-align: right">
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/dashboard.js"></script>
</asp:Content>

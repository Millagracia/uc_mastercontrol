﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="UpdateData.aspx.cs" Inherits="UC_v2._0.UpdateData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 733px">
        <div class="box-body">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#start" data-toggle="tab" aria-expanded="false">Start Service</a></li>
                <li class=""><a href="#stop" id="Tstop" data-toggle="tab" aria-expanded="true">Stop Service</a></li>
                <li class=""><a href="#invoicing" id="Tinvoice" data-toggle="tab" aria-expanded="true">Invoicing</a></li>
                <li class=""><a href="#bills" id="TBills" data-toggle="tab" aria-expanded="true">Bills Payment</a></li>
            </ul>
            <div class="tab-content">
                <div style="text-align: right">
                </div>
                <div class="tab-pane active" id="start">
                    <br />
                    <br />
                    <button id="btnUpdate" onclick="btnSave()" type="button" class="btn btn-success"  >Update</button>
                        <table id="tblStartService" class="table no-border">
                        <thead>
                            <tr>
                                <th data-field="USP_Name" class="text-center">USP Name </th>
                                <th data-field="ebulk" class="text-center">Bulk Email </th>
                                <th data-field="online" class="text-center">Online Request </th>
                                <th data-field="edi" class="text-center">EDI 814 </th>
                                <th data-field="Call" class="text-center">Call </th>
                            </tr>
                        </thead>
                       
                    </table>
                </div>
                <div class="tab-pane" id="stop">
                    <table class="table table-bordered" id="tblStop" data-toggle="table" data-height="399">
                        <thead>
                            <tr>
                                <th data-field="USP_Name">USP Name </th>
                                <th data-field="bulk">Bulk Email </th>
                                <th data-field="online">Online Request </th>
                                <th data-field="edi">EDI 814 </th>
                                <th data-field="Call">Call </th>
                            </tr>
                        </thead>
                        <tbody style="margin-top: 21px;">
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane " id="invoicing">

                    <table class="table table-bordered" id="tblInvoicing" data-toggle="table" data-height="399">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 122px">USP Name</th>
                                <th class="text-center" style="width: 174px">Summary Billing</th>
                                <th class="text-center" style="width: 88px">E Bill w/ Password</th>
                                <th class="text-center" style="width: 88px">E Bill w/o Password</th>
                                <th class="text-center" style="width: 88px">E-Bill Via Link</th>
                                <th class="text-center" style="width: 88px">EDI 810</th>
                                <th class="text-center" style="width: 88px">Paper Bill</th>
                            </tr>
                        </thead>
                        <tbody style="margin-top: 21px;">
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane " id="bills">
                    <table class="table table-bordered" id="tblbills" data-toggle="table" data-height="399">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 122px">USP Name</th>
                                <th class="text-center" style="width: 174px">ACH</th>
                                <th class="text-center" style="width: 88px">Wire Transfer</th>
                                <th class="text-center" style="width: 88px">Credit Card</th>
                                <th class="text-center" style="width: 88px">Check</th>
                            </tr>
                        </thead>
                        <tbody style="margin-top: 21px;">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/UpdateData.js"></script>
</asp:Content>

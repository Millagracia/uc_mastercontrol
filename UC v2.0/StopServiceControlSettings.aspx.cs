﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class StopServiceControlSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string script = @"$('#ListBox1').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
enableFiltering: true,
                maxHeight: 200});" + @"$('#ListBox2').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
enableFiltering: true,
                maxHeight: 200
            });" + @"$('#ListBox3').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
                enableFiltering: true,
                maxHeight: 200
            });"
               + @"$('#ListBox4').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
enableFiltering: true,
                maxHeight: 200
            });" + @"$('#ListBox5').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
enableFiltering: true,
                maxHeight: 200
            });" + @"$('#ListBox6').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
                enableFiltering: true,
                maxHeight: 200
            });" + @"$('#ListBox7').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
                enableFiltering: true,
                maxHeight: 200
            });"
               + @"$('#ListBox8').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
                enableFiltering: true,
                maxHeight: 200
            });" + @"$('#ListBox9').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
                enableFiltering: true,
                maxHeight: 200
            });" + @"$('#ListBox10').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
                enableFiltering: true,
                maxHeight: 200
            });" + @"$('#ListBox11').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
                enableFiltering: true,
                maxHeight: 200
            });"
               + @"$('#ListBox12').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
                enableFiltering: true,
                maxHeight: 200
            });" + @"$('#ListBox13').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
                enableFiltering: true,
                maxHeight: 200
            });" + @"$('#ListBox14').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
                enableFiltering: true,
                maxHeight: 200
            });" + @"$('#ListBox15').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
enableFiltering: true,
                maxHeight: 200
            });";

            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "multiselect", script, true);

        }
        protected void ListBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            StateList();
        }
        private void StateList()
        {


            //using (SqlConnection con = new SqlConnection(constr))
            //{
            //        using ( SqlCommand cmd = new SqlCommand("SELECT [City], [Val_ID] FROM [tbl_UC_State_City_Zip]"))
            //        { 
            //        SqlDataAdapter da = new SqlDataAdapter(cmd);
            //        DataSet ds = new DataSet();
            //        da.Fill(ds);
            //        ListBox4.DataSource = ds;
            //        ListBox4.DataTextField = "City";
            //        ListBox4.DataValueField = "Val_ID";
            //        ListBox4.DataBind();
            //        ListBox4.Items.Insert(0, new ListItem("--Select--", "*"));
            //        }
            //}

            string query = "SELECT [City], [Val_ID] FROM [tbl_UC_State_City_Zip]";
            string constr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            ListBox4.DataSource = dt;
                            ListBox4.DataTextField = "City";
                            ListBox4.DataValueField = "Val_ID";
                            ListBox4.DataBind();
                            ListBox4.Items.Insert(0, new ListItem("--Select--", "*"));
                            //return dt;

                        }
                    }
                }
            }

        }
        [WebMethod]
        public static string vendorID()
        {


            clsConnection cls = new clsConnection();

            DataTable dt = new DataTable();
            string qry = "";
            qry += "SELECT [USP_VendorID], [USP_Name], [Val_ID], [USP_VendorID] + '-' + [USP_Name] as NameID FROM [tbl_UC_USP_Name_Vendor_ID]  ORDER BY [USP_Name] ASC";


            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dt,
                }
            });
        }
        [WebMethod]
        public static string userList(string id)
        {
            string connetionString = null;
            string GetMainRule = "";
            connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";
            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                cnn.Open();
                SqlCommand command = new SqlCommand("select * from tbl_UC_StopService_Main_Rule Where Name = '" + id + "';", cnn);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    GetMainRule = reader["ID_MainRule"].ToString();
                }
                cnn.Close();
            }

            clsConnection cls = new clsConnection();


            DataTable dt, dt2, dt3, dt4, dt5, dt6, dt7, dt8, dt9, dt10, dt11, dt12, dt13, dt14, dt15, dt16, dt17, dt18 = new DataTable();
            string qry = "";
            string qry3 = "";
            string qry4 = "";
            string qry5 = "";
            string qry6 = "";
            string qry7 = "";
            string qry8 = "";
            string qry9 = "";
            string qry10 = "";
            string qry11 = "";
            string qry12 = "";
            string qry13 = "";
            string qry14 = "";
            string qry15 = "";
            qry += "SELECT DISTINCT [State] FROM [tbl_UC_State_City_Zip]";
            qry3 += "SELECT DISTINCT [City] FROM [tbl_UC_State_City_Zip]";
            qry4 += "SELECT  [Zip] FROM [tbl_UC_State_City_Zip]";
            qry5 += "SELECT DISTINCT Service_Name FROM tbl_UC_Service_Location";
            qry6 += "SELECT DISTINCT POD_Name FROM tbl_UC_PODS";
            qry7 += "SELECT Pro_Name FROM [tbl_UC_Property_Type]";
            qry8 += "SELECT Occu_Name FROM [tbl_UC_Occupancy_Status]";
            qry9 += "SELECT Property_Name FROM [tbl_UC_Property_Status]";
            qry10 += "SELECT Work_Name FROM [tbl_UC_Preservation_Work_Type]";
            qry11 += "SELECT DISTINCT [USP_Name] FROM [tbl_UC_USP_Name_Vendor_ID]";
            qry12 += "SELECT [USP_VendorID], [USP_Name], [Val_ID], [USP_VendorID] + '-' + [USP_Name] as NameID FROM [tbl_UC_USP_Name_Vendor_ID]";
            qry13 += "SELECT [Util_Name], [Val_ID] FROM [tbl_UC_Utility_Status]";
            qry14 += "SELECT [Work_Name], [Val_ID] FROM [tbl_UC_Work_Order_Type]";
            qry15 += "SELECT [wStatus_Name], [Val_ID] FROM [tbl_UC_Work_Order_Status] ";


            string qry2 = "";
            string qry16 = "";
            string qry17 = "";
            string qry18 = "";
            qry2 += "SELECT * FROM [tbl_UC_StopService_Main_Rule] WHERE Name = '" + id + "'";
            qry16 += "SELECT USP_Name,Vendor_ID,Utility_status,Work_Order_Type FROM [tbl_UC_StopService_Main_Rule] WHERE Name = '" + id + "' AND Status = 'Valid'";
            qry17 += "SELECT Vendor_ID FROM [tbl_UC_StopService_Main_Rule] WHERE Name = '" + id + "'";
            qry18 += "SELECT Utility_status FROM [tbl_UC_StopService_Main_Rule] WHERE Name = '" + id + "'";


            dt = cls.GetData(qry);
            dt3 = cls.GetData(qry3);
            dt4 = cls.GetData(qry4);
            dt2 = cls.GetData(qry2);
            dt5 = cls.GetData(qry5);
            dt6 = cls.GetData(qry6);
            dt7 = cls.GetData(qry7);
            dt8 = cls.GetData(qry8);
            dt9 = cls.GetData(qry9);
            dt10 = cls.GetData(qry10);
            dt11 = cls.GetData(qry11);
            dt12 = cls.GetData(qry12);
            dt13 = cls.GetData(qry13);
            dt14 = cls.GetData(qry14);
            dt15 = cls.GetData(qry15);
            dt16 = cls.GetData(qry16);
            dt17 = cls.GetData(qry17);
            dt18 = cls.GetData(qry18);
            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dt,
                    record2 = dt2,
                    record3 = dt3,
                    record4 = dt4,
                    record5 = dt5,
                    record6 = dt6,
                    record7 = dt7,
                    record8 = dt8,
                    record9 = dt9,
                    record10 = dt10,
                    record11 = dt11,
                    record12 = dt12,
                    record13 = dt13,
                    record14 = dt14,
                    record15 = dt15
                    ,
                    record16 = dt16
                }
            });
        }
        [WebMethod]
        public static string userList1()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string qry = "";
            qry += "SELECT  [State], [Zip] FROM [tbl_UC_StopService_Main_Rule]";
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetRule(string getID)
        {
            string connetionString = null;
            string GetMainRule = "";
            connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";
            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                cnn.Open();
                SqlCommand command = new SqlCommand("select * from tbl_Client Where Client_Name = '" + getID + "';", cnn);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    GetMainRule = reader["ID_Client"].ToString();
                }
                cnn.Close();
            }
            clsConnection cls = new clsConnection();
            string qry = "SELECT DISTINCT Name,Property_isActive FROM tbl_UC_StopService_Main_Rule Where ID_Client = '" + GetMainRule + "';";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

        [WebMethod]
        public static string GetAA(string getID, string getName)
        {
            string connetionString = null;
            string GetMain = "";
            connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";
            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                cnn.Open();
                SqlCommand command = new SqlCommand("select * from tbl_Client Where Client_Name = '" + getName + "';", cnn);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    GetMain = reader["ID_Client"].ToString();
                }
                cnn.Close();
            }
            clsConnection cls = new clsConnection();
            string qry = "select * from tbl_UC_StopService_Main_Rule Where ID_Client = '" + GetMain + "' AND Name = '" + getID + "' AND Status = 'Valid';";
            string qry1 = "select USP_Name,Vendor_ID,Utility_status,Work_Order_Type from tbl_UC_StopService_Main_Rule Where ID_Client = '"
                + GetMain + "' AND Name = '" + getID + "' AND Status = 'Valid';";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            dt = cls.GetData(qry);
            dt1 = cls.GetData(qry1);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, record = dt1 } });
        }

        [WebMethod]
        public static string GetSched()
        {
            clsConnection cls = new clsConnection();

            string qry = "select * from tbl_Client;";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

        [WebMethod(EnableSession = true)]
        public static string setActive(string Client, string Main, string Property_isActive)
        {
            string connetionString = null;
            string IDClient = "";
            string setAc = "";
            connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";

            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                cnn.Open();
                SqlCommand command = new SqlCommand("Select * From tbl_UC_StopService_Main_Rule Where Name = '" + Main + "'", cnn);
                //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDClient = reader["ID_MainRule"].ToString();
                }
                cnn.Close();

            }
            if (Property_isActive == "1")
            {
                setAc = "0";
            }
            else
            {
                setAc = "1";

            }


            //Utility_status = Utility_status.Replace("'", "\'");
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand updateCMD = new SqlCommand("UPDATE tbl_UC_StopService_Main_Rule SET " +
                    "Property_isActive = '" + setAc +
                    "' WHERE Name = '" + Main + "'", con);
                {
                    con.Open();
                    updateCMD.ExecuteNonQuery();
                    return "True";
                }
            }
        }
        [WebMethod(EnableSession = true)]
        public static string UpdateRule(string Client, string Main, string Name, string Service_Location, string Country, string State, string City,
            string Zip, string Street_Name, string Number_of_Unit, string PODS, string PBD_Val, string PBD_BusCal, string PBD_DMY, string Property_isActive, string Property_Type,
            string Occupancy_status, string Property_status,
            string Property_Value, string Total_Preservation_Cost, string Preservation_Work_Type, string Inspection_isComplete, string Utility_Type, string USP_Name, string Vendor_ID,
            string Utility_status, string Work_Order_Type,
            string Utility_Status_Name, string Work_Order_Value, string Work_Order_Status, string Process_as, string isActive)
        {
            string connetionString = null;
            string IDClient = "";
            connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";

            string[] arrayUSPname = USP_Name.Split(',');
            string[] arrVenID = Vendor_ID.Split(',');
            string[] arrUtilStat = Utility_status.Split(',');
            string[] arrWOtype = Work_Order_Type.Split(',');
            //   string[] arrWOV = Work_Order_Value.Split(',');


            //  int limit1 = arrayUSPname.Count();//2
            int limit2 = arrVenID.Count();//3
            int limit3 = arrUtilStat.Count();//3
            int limit4 = arrWOtype.Count();//2
            //    int limit5 = arrWOV.Count();
            int ALL = limit2 * limit3 * limit4; //36
            int forVendid = ALL / limit2;//18
            //int forVendid = forUSP / limit2;// 18 / 3 = 6
            int forutilstat = forVendid / limit3;// 12 / 3 = 6
            int forwot = forVendid / limit4;//2 / 2 = 6
            List<string> uspName = new List<string>();
            List<string> venID = new List<string>();
            List<string> UtilStat = new List<string>();
            List<string> WOT = new List<string>();

            int min = 0;
            for (int u = 0; u < limit2; u++)//3
            {
                for (int i = 0; i < forVendid; i++)
                {
                    string splitThis = arrVenID[u].ToString();
                    //string[] getFirst = splitThis.Split('-');
                    //uspName.Add(getFirst[1]);
                    venID.Add(splitThis);
                }
            }
            for (int v = 0; v < ALL / limit3; v++)//24 % 3
            {
                for (int i = 0; i < limit3; i++)
                {
                    UtilStat.Add(arrUtilStat[i]);
                }
            }
            for (int s = 0; s < ALL / limit3; s++)//24 % 2 
            {
                for (int i = 0; i < limit3; i++)//2
                {
                    WOT.Add(arrWOtype[min]);
                }
                if (min == limit4 - 1)// 0 <= 2
                {
                    min = 0;
                }
                else
                {
                    min++;
                }
            }



            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                cnn.Open();
                SqlCommand command = new SqlCommand("Select DISTINCT ID_Client From tbl_UC_StopService_Main_Rule Where Name = '" + Main + "' AND Status ='Valid'", cnn);
                //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDClient = reader["ID_Client"].ToString();
                }
                cnn.Close();

                cnn.Open();
                SqlCommand DeleteQry = new SqlCommand("DELETE From tbl_UC_StopService_Main_Rule Where Name = '" + Main + "'", cnn);
                DeleteQry.ExecuteNonQuery();
                cnn.Close();

            }

            //Utility_status = Utility_status.Replace("'", "\'");
            ////SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            //{
            //    SqlCommand updateCMD = new SqlCommand("UPDATE tbl_UC_Main_Rule SET " +
            //        "Name = '" + Name +
            //        "', Service_Location = '" + Service_Location + 
            //        "', Country = '" + Country +
            //        "', State = '" + State +
            //        "', City = '" + City +
            //        "', Zip = '" + Zip +
            //        "', Street_Name = '" + Street_Name +
            //        "', Number_of_Unit = '" + Number_of_Unit +
            //        "', PODS = '" + PODS +
            //        "', VMS_val = '" + PBD_Val +
            //        "', Business_Calendar = '" + PBD_BusCal +
            //        "', VMS_DMY = '" + PBD_DMY +
            //        "', Property_isActive = '" + Property_isActive +
            //        "', Property_Type = '" + Property_Type +
            //        "', Occupancy_status = '" + Occupancy_status +
            //        "', Property_status = '" + Property_status +
            //        "', Property_Value = '" + Property_Value +
            //        "', Total_Preservation_Cost = '" + Total_Preservation_Cost +
            //        "', Preservation_Work_Type = '" + Preservation_Work_Type +
            //        "', Inspection_isComplete = '" + Inspection_isComplete +
            //        "', Utility_Type = '" + Utility_Type +
            //        "', USP_Name = '" + USP_Name +
            //        "', Vendor_ID = '" + Vendor_ID +
            //        "', Utility_status = '" + Utility_status +
            //        "', Work_Order_Type = '" + Work_Order_Type +
            //        "', Utility_Status_Name = '" + Utility_Status_Name +
            //        "', Work_Order_Value = '" + Work_Order_Value +
            //        "', Work_Order_Status = '" + Work_Order_Status +
            //        "', Process_as = '" + Process_as +
            //        "', isActive = '" + isActive + "' WHERE ID_MainRule = '" + IDClient + "'", con);



            //    {
            //        con.Open();
            //        updateCMD.ExecuteNonQuery();
            //        return "True";
            //    }
            //}
            for (int i = 0; i < ALL; i++)
            {
                string MainRule = "";
                string USPname = "";
                using (SqlConnection cnn = new SqlConnection(connetionString))
                {
                    cnn.Open();
                    SqlCommand command1 = new SqlCommand("SELECT USP_Name FROM tbl_UC_USP_Name_Vendor_ID Where USP_VendorID = '" + venID[i].ToString() + "'", cnn);
                    //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                    SqlDataReader reader1 = command1.ExecuteReader();

                    while (reader1.Read())
                    {
                        USPname = reader1["USP_Name"].ToString();
                    }
                    cnn.Close();
                    cnn.Open();
                    SqlCommand command = new SqlCommand("SELECT  *  FROM tbl_UC_StopService_Main_Rule WHERE USP_Name IN('" + USPname + "') " +
                        "AND Vendor_ID IN('" + venID[i].ToString() + "') AND Utility_status IN('" + UtilStat[i].ToString() + "') AND Work_Order_Type IN('" + WOT[i].ToString() + "')" +
                        " AND Status = 'Valid'", cnn);
                    //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        MainRule = reader["ID_MainRule"].ToString();
                    }
                    cnn.Close();



                }
                if (MainRule != "")
                {
                    //  string passVal = uspName[i].ToString() + " " + venID[i].ToString() + " " + UtilStat[i].ToString() + " " + WOT[i].ToString();
                    //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
                    SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
                    {
                        SqlCommand cmd = new SqlCommand("Insert into tbl_UC_StopService_Main_Rule" +
                            "(ID_Client, Name, Service_Location, Country, State, City, " +
                            "Zip, Street_Name, Number_of_Unit, PODS, VMS_val, Business_Calendar, VMS_DMY, " +
                            "Property_isActive, Property_Type, Occupancy_status, Property_status, Property_Value," +
                            "Total_Preservation_Cost, Preservation_Work_Type, Inspection_isComplete, " +
                            "Utility_Type, USP_Name, Vendor_ID, Utility_status, Work_Order_Type, Utility_Status_Name," +
                            " Work_Order_Value, Work_Order_Status, Process_as, isActive, Status) values('"
                            + IDClient + "','" + Name + "','" + Service_Location + "','" + Country + "','" + State + "','" + City + "','"
                            + Zip + "','" + Street_Name + "','" + Number_of_Unit + "','" + PODS + "','" + PBD_Val + "','" + PBD_BusCal + "','" + PBD_DMY + "','"
                            + Property_isActive + "','" + Property_Type + "','" + Occupancy_status + "','" + Property_status + "','" + Property_Value + "','"
                            + Total_Preservation_Cost + "','" + Preservation_Work_Type + "','" + Inspection_isComplete + "','"
                            + Utility_Type + "','" + USPname + "','" + venID[i].ToString() + "','" + UtilStat[i].ToString() + "','" + WOT[i].ToString() + "','" + Utility_Status_Name + "','"

                            + Work_Order_Value + "','" + Work_Order_Status + "','" + Process_as + "','" + isActive + "','Invalid')", con);
                        {
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
                else
                {
                    //  string passVal = uspName[i].ToString() + " " + venID[i].ToString() + " " + UtilStat[i].ToString() + " " + WOT[i].ToString();
                    //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
                    SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
                    {
                        SqlCommand cmd = new SqlCommand("Insert into tbl_UC_StopService_Main_Rule" +
                            "(ID_Client, Name, Service_Location, Country, State, City, " +
                            "Zip, Street_Name, Number_of_Unit, PODS, VMS_val, Business_Calendar, VMS_DMY, " +
                            "Property_isActive, Property_Type, Occupancy_status, Property_status, Property_Value," +
                            " Total_Preservation_Cost, Preservation_Work_Type, Inspection_isComplete, " +
                            "Utility_Type, USP_Name, Vendor_ID, Utility_status, Work_Order_Type, Utility_Status_Name," +
                            " Work_Order_Value, Work_Order_Status, Process_as, isActive, Status) values('"
                            + IDClient + "','" + Name + "','" + Service_Location + "','" + Country + "','" + State + "','" + City + "','"
                            + Zip + "','" + Street_Name + "','" + Number_of_Unit + "','" + PODS + "','" + PBD_Val + "','" + PBD_BusCal + "','" + PBD_DMY + "','"
                            + Property_isActive + "','" + Property_Type + "','" + Occupancy_status + "','" + Property_status + "','" + Property_Value + "','"
                            + Total_Preservation_Cost + "','" + Preservation_Work_Type + "','" + Inspection_isComplete + "','"
                            + Utility_Type + "','" + USPname + "','" + venID[i].ToString() + "','" + UtilStat[i].ToString() + "','" + WOT[i].ToString() + "','" + Utility_Status_Name + "','"

                            + Work_Order_Value + "','" + Work_Order_Status + "','" + Process_as + "','" + isActive + "','Valid')", con);
                        {
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                }



            }
            return "True";
        }



        [WebMethod(EnableSession = true)]
        public static string SaveMainRule(string Client, string Name, string Service_Location, string Client_Status, string Country, string State, string City,
            string Zip, string Street_Name, string Number_of_Unit, string PODS, string PBD_Val, string PBD_BusCal, string PBD_DMY, string Property_isActive, string Property_Type,
            string Occupancy_status, string Property_status,
            string Property_Value, string Total_Preservation_Cost, string Preservation_Work_Type, string Inspection_isComplete, string Utility_Type, string USP_Name, string Vendor_ID,
            string Utility_status, string Work_Order_Type,
            string Utility_Status_Name, string Work_Order_Value, string Work_Order_Status, string Process_as, string isActive)
        {
            string connetionString = null;
            string IDClient = "";
            connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";

            string[] arrayUSPname = USP_Name.Split(',');
            string[] arrVenID = Vendor_ID.Split(',');
            string[] arrUtilStat = Utility_status.Split(',');
            string[] arrWOtype = Work_Order_Type.Split(',');
            //   string[] arrWOV = Work_Order_Value.Split(',');


            //  int limit1 = arrayUSPname.Count();//2
            int limit2 = arrVenID.Count();//3
            int limit3 = arrUtilStat.Count();//3
            int limit4 = arrWOtype.Count();//2
            //    int limit5 = arrWOV.Count();
            int ALL = limit2 * limit3 * limit4; //36
            int forVendid = ALL / limit2;//18
            //int forVendid = forUSP / limit2;// 18 / 3 = 6
            int forutilstat = forVendid / limit3;// 12 / 3 = 6
            int forwot = forVendid / limit4;//2 / 2 = 6
            List<string> uspName = new List<string>();
            List<string> venID = new List<string>();
            List<string> UtilStat = new List<string>();
            List<string> WOT = new List<string>();

            int min = 0;
            for (int u = 0; u < limit2; u++)//3
            {
                for (int i = 0; i < forVendid; i++)
                {
                    string splitThis = arrVenID[u].ToString();
                    string[] getFirst = splitThis.Split('-');
                    uspName.Add(getFirst[1]);
                    venID.Add(getFirst[0]);
                }
            }
            for (int v = 0; v < ALL / limit3; v++)//24 % 3
            {
                for (int i = 0; i < limit3; i++)
                {
                    UtilStat.Add(arrUtilStat[i]);
                }
            }
            for (int s = 0; s < ALL / limit3; s++)//24 % 2 
            {
                for (int i = 0; i < limit3; i++)//2
                {
                    WOT.Add(arrWOtype[min]);
                }
                if (min == limit4 - 1)// 0 <= 2
                {
                    min = 0;
                }
                else
                {
                    min++;
                }
            }


            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                cnn.Open();
                SqlCommand command = new SqlCommand("Select * From tbl_Client Where Client_Name = '" + Client + "'", cnn);
                //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDClient = reader["ID_Client"].ToString();
                }
                cnn.Close();

            }
            //Utility_status = Utility_status.Replace("'", "\'");
            for (int i = 0; i < ALL; i++)
            {
                string MainRule = "";
                string USPname = "";
                using (SqlConnection cnn = new SqlConnection(connetionString))
                {
                    cnn.Open();
                    SqlCommand command1 = new SqlCommand("SELECT USP_Name FROM tbl_UC_USP_Name_Vendor_ID Where USP_VendorID = '" + venID[i].ToString() + "'", cnn);
                    //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                    SqlDataReader reader1 = command1.ExecuteReader();

                    while (reader1.Read())
                    {
                        USPname = reader1["USP_Name"].ToString();
                    }
                    cnn.Close();
                    cnn.Open();
                    SqlCommand command = new SqlCommand("SELECT  *  FROM tbl_UC_StopService_Main_Rule WHERE USP_Name IN('" + USPname + "') " +
                        "AND Vendor_ID IN('" + venID[i].ToString() + "') AND Utility_status IN('" + UtilStat[i].ToString() + "') AND Work_Order_Type IN('" + WOT[i].ToString() + "')" +
                        " AND Status = 'Valid'", cnn);
                    //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        MainRule = reader["ID_MainRule"].ToString();
                    }
                    cnn.Close();



                }
                if (MainRule != "")
                {
                    string passVal = uspName[i].ToString() + " " + venID[i].ToString() + " " + UtilStat[i].ToString() + " " + WOT[i].ToString();
                    //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
                    SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
                    {
                        SqlCommand cmd = new SqlCommand("Insert into tbl_UC_StopService_Main_Rule" +
                            "(ID_Client, Name, Service_Location, Client_Status, Country, State, City, " +
                            "Zip, Street_Name, Number_of_Unit, PODS, VMS_val, Business_Calendar, VMS_DMY, " +
                            "Property_isActive, Property_Type, Occupancy_status, Property_status, Property_Value," +
                            " Total_Preservation_Cost, Preservation_Work_Type, Inspection_isComplete, " +
                            "Utility_Type, USP_Name, Vendor_ID, Utility_status, Work_Order_Type, Utility_Status_Name," +
                            " Work_Order_Value, Work_Order_Status, Process_as, isActive, Status) values('"
                            + IDClient + "','" + Name + "','" + Service_Location + "','" + Client_Status + "','" + Country + "','" + State + "','" + City + "','"
                            + Zip + "','" + Street_Name + "','" + Number_of_Unit + "','" + PODS + "','" + PBD_Val + "','" + PBD_BusCal + "','" + PBD_DMY + "','"
                            + Property_isActive + "','" + Property_Type + "','" + Occupancy_status + "','" + Property_status + "','" + Property_Value + "','"
                            + Total_Preservation_Cost + "','" + Preservation_Work_Type + "','" + Inspection_isComplete + "','"
                            + Utility_Type + "','" + USPname + "','" + venID[i].ToString() + "','" + UtilStat[i].ToString() + "','" + WOT[i].ToString() + "','" + Utility_Status_Name + "','"

                            + Work_Order_Value + "','" + Work_Order_Status + "','" + Process_as + "','" + isActive + "','Invalid')", con);
                        {
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
                else
                {
                    string passVal = uspName[i].ToString() + " " + venID[i].ToString() + " " + UtilStat[i].ToString() + " " + WOT[i].ToString();
                    //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
                    SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
                    {
                        SqlCommand cmd = new SqlCommand("Insert into tbl_UC_StopService_Main_Rule" +
                            "(ID_Client, Name, Service_Location, Client_Status, Country, State, City, " +
                            "Zip, Street_Name, Number_of_Unit, PODS, VMS_val, Business_Calendar, VMS_DMY, " +
                            "Property_isActive, Property_Type, Occupancy_status, Property_status, Property_Value," +
                            " Total_Preservation_Cost, Preservation_Work_Type, Inspection_isComplete, " +
                            "Utility_Type, USP_Name, Vendor_ID, Utility_status, Work_Order_Type, Utility_Status_Name," +
                            " Work_Order_Value, Work_Order_Status, Process_as, isActive, Status) values('"
                            + IDClient + "','" + Name + "','" + Service_Location + "','" + Client_Status + "','" + Country + "','" + State + "','" + City + "','"
                            + Zip + "','" + Street_Name + "','" + Number_of_Unit + "','" + PODS + "','" + PBD_Val + "','" + PBD_BusCal + "','" + PBD_DMY + "','"
                            + Property_isActive + "','" + Property_Type + "','" + Occupancy_status + "','" + Property_status + "','" + Property_Value + "','"
                            + Total_Preservation_Cost + "','" + Preservation_Work_Type + "','" + Inspection_isComplete + "','"
                            + Utility_Type + "','" + USPname + "','" + venID[i].ToString() + "','" + UtilStat[i].ToString() + "','" + WOT[i].ToString() + "','" + Utility_Status_Name + "','"

                            + Work_Order_Value + "','" + Work_Order_Status + "','" + Process_as + "','" + isActive + "','Valid')", con);
                        {
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                }



            }
            return "True";

        }

        protected void chkLinked_CheckedChanged(Object sender, EventArgs args)
        {
            if (chc1.Checked == true)
            {
                txtProperty.Enabled = true;
                ddl1.Enabled = true;
                ddl2.Enabled = true;
                ddl3.Enabled = true;
            }
            else
            {
                txtProperty.Enabled = false;
                ddl1.Enabled = false;
                ddl2.Enabled = false;
                ddl3.Enabled = false;
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 1;

        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 2;
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 3;
        }
        protected void Button7_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 4;
        }
        protected void Button9_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 5;

            txtNameVal.Text = txtName.Text;

            if (DropDownList12.SelectedItem.ToString() == "ACTIVE")
            {
                txtActive.Text = DropDownList12.SelectedItem.ToString();
                txtActiveVal.Text = "1";
                txtActive.BackColor = Color.FromArgb(76, 175, 80);
            }
            else
            {
                txtActive.Text = DropDownList12.SelectedItem.ToString();
                txtActiveVal.Text = "0";
                txtActive.BackColor = Color.FromArgb(217, 83, 79);
            }

            txtProValue.Text = DropDownList14.SelectedItem.ToString() + " " + txtProVal1.Text;

            if (DropDownList14.SelectedItem.ToString() == "Greater Than")
            {
                txtProValueVal.Text = "> " + txtProVal1.Text;
            }
            else if (DropDownList14.SelectedItem.ToString() == "Less Than")
            {
                txtProValueVal.Text = "< " + txtProVal1.Text;
            }
            else
            {
                txtProValueVal.Text = "= " + txtProVal1.Text;
            }


            txtPreser.Text = DropDownList15.SelectedItem.ToString() + " " + txtPreCost.Text;
            if (DropDownList15.SelectedItem.ToString() == "Greater Than")
            {
                txtPreserVal.Text = "> " + txtPreCost.Text;
            }
            else if (DropDownList15.SelectedItem.ToString() == "Less Than")
            {
                txtPreserVal.Text = "< " + txtPreCost.Text;
            }
            else
            {
                txtPreserVal.Text = "= " + txtPreCost.Text;
            }



            //if(cbYes.Checked == true)
            //{
            //    txtInspection.Text = "YES";
            //    txtInspectionVal.Text = "YES";
            //}
            //else if(cbNo.Checked == true)
            //{
            //    txtInspection.Text = "NO";
            //    txtInspectionVal.Text = "NO";
            //}
            //else
            //{
            //    txtInspection.Text = "";
            //    txtInspectionVal.Text = "";
            //}
            txtCountry.Text = DropDownList3.SelectedItem.ToString();
            txtCountryVal.Text = DropDownList3.SelectedItem.ToString();
            txtStreet.Text = DropDownList7.SelectedItem.ToString();
            txtStreetVal.Text = DropDownList7.SelectedItem.ToString();
            txtNumber.Text = DropDownList8.SelectedItem.ToString();
            txtNumberVal.Text = DropDownList8.SelectedItem.ToString();

            cbGas2.Checked = false;
            cbWater2.Checked = false;
            cbElec2.Checked = false;

            //if (cbGas.Checked == true)
            //{ 
            //    txtUtilType.Text = "Gas";
            //    txtUtilTypeVal.Text = "Gas";
            //    cbGas2.Checked = true;
            //    if(cbWater.Checked == true)
            //    {
            //        txtUtilType.Text = txtUtilType.Text + ", " + "Water";
            //        txtUtilTypeVal.Text = txtUtilTypeVal.Text + "," + "Water";
            //        cbGas2.Checked = true;
            //        cbWater2.Checked = true;
            //        if (cbElectric.Checked == true)
            //        {
            //            txtUtilType.Text = txtUtilType.Text + "," + "Electric";
            //            txtUtilTypeVal.Text = txtUtilTypeVal.Text + "," + "Electric";
            //            cbGas2.Checked = true;
            //            cbWater2.Checked = true;
            //            cbElec2.Checked = true;
            //        }
            //        else
            //        { 
            //        }
            //    }
            //    else if(cbElectric.Checked == true)
            //    {
            //        txtUtilType.Text = txtUtilType.Text + ", " + "Electric";
            //        txtUtilTypeVal.Text = txtUtilTypeVal.Text + "," + "Electric";
            //        cbGas2.Checked = true;
            //        cbElec2.Checked = true;
            //    }
            //    else
            //    { 
            //    }
            //}
            //else if (cbWater.Checked == true)
            //{
            //    txtUtilType.Text = "Water";
            //    txtUtilTypeVal.Text = "Water";
            //    cbWater2.Checked = true;
            //    if(cbElectric.Checked == true)
            //    {
            //        txtUtilType.Text = txtUtilType.Text + ", " + "Electric";
            //        txtUtilTypeVal.Text = txtUtilTypeVal.Text + ", " + "Electric";
            //        cbWater2.Checked = true;
            //        cbElec2.Checked = true;
            //    }
            //    else
            //    { 
            //    }
            //}
            //else if(cbElectric.Checked == true)
            //{
            //    txtUtilType.Text = "Electric";
            //    txtUtilTypeVal.Text = "Electric";
            //    cbElec2.Checked = true;
            //}
            //else
            //{
            //    txtUtilType.Text = "NONE";
            //    txtUtilTypeVal.Text = "NONE";
            //    cbGas2.Checked = false;
            //    cbWater2.Checked = false;
            //    cbElec2.Checked = false;

            //}

            //First Line
            txtClientStat.Text = "";
            txtClientStat.ToolTip = "";
            txtClientStatVal.Text = "";
            int[] sum1 = ListBox1.GetSelectedIndices();
            foreach (int i in ListBox1.GetSelectedIndices())
            {
                if (txtClientStat.Text == "")
                {

                    txtClientStat.Text = "1 Selected ";
                    txtClientStat.ToolTip = txtClientStat.ToolTip + ListBox1.Items[i].Text;
                    txtClientStatVal.Text = txtClientStatVal.Text + ListBox1.Items[i].Text;
                }
                else
                {
                    int sumInner = sum1.Count();
                    txtClientStat.Text = sumInner.ToString() + " Selected ";
                    txtClientStat.ToolTip = txtClientStat.ToolTip + ", \n" + ListBox1.Items[i].Text;
                    txtClientStatVal.Text = txtClientStatVal.Text + "," + ListBox1.Items[i].Text;
                }
            }

            txtServiceLoc.Text = "";
            txtServiceLocVal.Text = "";
            txtServiceLoc.ToolTip = "";
            int[] sum2 = ListBox2.GetSelectedIndices();
            foreach (int i in ListBox2.GetSelectedIndices())
            {

                if (txtServiceLoc.Text == "")
                {
                    txtServiceLoc.Text = "1 Selected ";
                    txtServiceLoc.ToolTip = txtServiceLoc.ToolTip + ListBox2.Items[i].Text;
                    txtServiceLocVal.Text = txtServiceLocVal.Text + ListBox2.Items[i].Text;
                }
                else
                {
                    int sumInner = sum2.Count();
                    txtServiceLoc.Text = sumInner.ToString() + " Selected ";
                    txtServiceLoc.ToolTip = txtServiceLoc.ToolTip + ", \n" + ListBox2.Items[i].Text; ;
                    txtServiceLocVal.Text = txtServiceLocVal.Text + "," + ListBox2.Items[i].Text;

                }
            }


            txtState.Text = "";
            txtState.ToolTip = "";
            txtStateVal.Text = "";
            int valID = 0;
            int[] sum3 = ListBox3.GetSelectedIndices();
            foreach (int i in ListBox3.GetSelectedIndices())
            {
                if (txtState.Text == "")
                {
                    txtState.Text = "1 Selected ";
                    txtState.ToolTip = txtState.ToolTip + ListBox3.Items[i].Text;
                    txtStateVal.Text = txtStateVal.Text + ListBox3.Items[i].Text;
                    string query = ListBox13.Items[i].Value;
                }
                else
                {
                    int sumInner = sum3.Count();
                    txtState.Text = sumInner.ToString() + " Selected ";
                    txtState.ToolTip = txtState.ToolTip + ", \n" + ListBox3.Items[i].Text;
                    txtStateVal.Text = txtStateVal.Text + "," + ListBox3.Items[i].Text;
                    string query2 = ListBox13.Items[i].Value;
                    //   val = val + ListBox3.SelectedValue[i].ToString(); 
                }
            }



            //foreach (ListItem li in sum3[])
            //{
            //    string user = li.Value;
            //}0000




            txtPropertyB.Text = txtVMS.Text + " | " + ddlVMS_BC.SelectedItem.ToString() + " | " + ddlVMSday.SelectedItem.ToString();
            txtPropertyVal.Text = txtVMS.Text;
            txtPropertyBusCal.Text = ddlVMS_BC.SelectedItem.ToString();
            txtPropertyDMY.Text = ddlVMSday.SelectedItem.ToString();

            txtProType.Text = "";
            txtProTypeVal.Text = "";
            foreach (int i in ListBox7.GetSelectedIndices())
            {
                if (txtProType.Text == "")
                {
                    txtProType.Text = txtProType.Text + ListBox7.Items[i].Text;
                    txtProTypeVal.Text = txtProTypeVal.Text + ListBox7.Items[i].Text;
                }
                else
                {
                    txtProType.Text = txtProType.Text + ", " + ListBox7.Items[i].Text;
                    txtProTypeVal.Text = txtProTypeVal.Text + "," + ListBox7.Items[i].Text;
                }
            }
            txtOccu.Text = "";
            txtOccuVal.Text = "";
            foreach (int i in ListBox8.GetSelectedIndices())
            {
                if (txtOccu.Text == "")
                {
                    txtOccu.Text = txtOccu.Text + ListBox8.Items[i].Text;
                    txtOccuVal.Text = txtOccuVal.Text + ListBox8.Items[i].Text;
                }
                else
                {
                    txtOccu.Text = txtOccu.Text + ", " + ListBox8.Items[i].Text;
                    txtOccuVal.Text = txtOccuVal.Text + "," + ListBox8.Items[i].Text;
                }
            }
            txtProStatus.Text = "";
            txtProStatusVal.Text = "";
            foreach (int i in ListBox9.GetSelectedIndices())
            {
                if (txtProStatus.Text == "")
                {
                    txtProStatus.Text = txtProStatus.Text + ListBox9.Items[i].Text;
                    txtProStatusVal.Text = txtProStatusVal.Text + ListBox9.Items[i].Text;
                }
                else
                {
                    txtProStatus.Text = txtProStatus.Text + ", " + ListBox9.Items[i].Text;
                    txtProStatusVal.Text = txtProStatusVal.Text + "," + ListBox9.Items[i].Text;
                }
            }
            txtPWT.Text = "";
            txtPWTVal.Text = "";
            foreach (int i in ListBox10.GetSelectedIndices())
            {
                if (txtPWT.Text == "")
                {
                    txtPWT.Text = txtPWT.Text + ListBox10.Items[i].Text;
                    txtPWTVal.Text = txtPWTVal.Text + ListBox10.Items[i].Text;
                }
                else
                {
                    txtPWT.Text = txtPWT.Text + ", " + ListBox10.Items[i].Text;
                    txtPWTVal.Text = txtPWTVal.Text + "," + ListBox10.Items[i].Text;
                }
            }



            txtCity.Text = "";
            txtCityVal.Text = "";
            foreach (int i in ListBox4.GetSelectedIndices())
            {
                if (txtCity.Text == "")
                {
                    txtCity.Text = txtCity.Text + ListBox4.Items[i].Text;
                    txtCityVal.Text = txtCityVal.Text + ListBox4.Items[i].Text;
                }
                else
                {
                    txtCity.Text = txtCity.Text + ", " + ListBox4.Items[i].Text;
                    txtCityVal.Text = txtCityVal.Text + "," + ListBox4.Items[i].Text;
                }
            }

            txtZip.Text = "";
            txtZipVal.Text = "";
            foreach (int i in ListBox5.GetSelectedIndices())
            {
                if (txtZip.Text == "")
                {
                    txtZip.Text = txtZip.Text + ListBox5.Items[i].Text;
                    txtZipVal.Text = txtZipVal.Text + ListBox5.Items[i].Text;
                }
                else
                {
                    txtZip.Text = txtZip.Text + ", " + ListBox5.Items[i].Text;
                    txtZipVal.Text = txtZipVal.Text + "," + ListBox5.Items[i].Text;
                }
            }
            txtPOD.Text = "";
            txtPODVal.Text = "";
            foreach (int i in ListBox6.GetSelectedIndices())
            {
                if (txtPOD.Text == "")
                {
                    txtPOD.Text = txtPOD.Text + ListBox6.Items[i].Text;
                    txtPODVal.Text = txtPODVal.Text + ListBox6.Items[i].Text;
                }
                else
                {
                    txtPOD.Text = txtPOD.Text + ", " + ListBox6.Items[i].Text;
                    txtPODVal.Text = txtPODVal.Text + "," + ListBox6.Items[i].Text;
                }
            }

            txtUtilName.Text = "";
            txtUtilName.Text = txtUtilStatName.Text;
            txtUtilNameVal.Text = txtUtilStatName.Text;

            txtUtilStat.Text = "";
            txtUtilStatVal.Text = "";
            txtUtilTy.Text = "";
            foreach (int i in ListBox13.GetSelectedIndices())
            {
                if (txtUtilStat.Text == "")
                {
                    txtUtilStat.Text = "'" + txtUtilStat.Text + ListBox13.Items[i].Text + "'";
                    txtUtilTy.Text = txtUtilTy.Text + ListBox13.Items[i].Text;
                    txtUtilStatVal.Text = txtUtilStatVal.Text + ListBox13.Items[i].Text;
                }
                else
                {
                    txtUtilStat.Text = txtUtilStat.Text + ",'" + ListBox13.Items[i].Text + "'";
                    txtUtilTy.Text = txtUtilTy.Text + ", " + ListBox13.Items[i].Text;
                    txtUtilStatVal.Text = txtUtilStatVal.Text + "," + ListBox13.Items[i].Text;
                }
            }
            txtUSPname.Text = "";
            txtUSPnameVal.Text = "";
            txtUSP.Text = "";
            foreach (int i in ListBox11.GetSelectedIndices())
            {
                if (txtUSPname.Text == "")
                {
                    txtUSPname.Text = txtUSPname.Text + ListBox11.Items[i].Text;
                    txtUSP.Text = txtUSP.Text + ListBox11.Items[i].Text;
                    txtUSPnameVal.Text = txtUSPnameVal.Text + ListBox11.Items[i].Text;
                }
                else
                {
                    txtUSPname.Text = txtUSPname.Text + ", " + ListBox11.Items[i].Text;
                    txtUSP.Text = txtUSP.Text + ", " + ListBox11.Items[i].Text;
                    txtUSPnameVal.Text = txtUSPnameVal.Text + "," + ListBox11.Items[i].Text;
                }
            }



            txtVendorID.Text = "";
            txtVendorIDVal.Text = "";
            txtVenID.Text = "";
            foreach (int i in ListBox12.GetSelectedIndices())
            {
                if (txtVendorID.Text == "")
                {
                    txtVendorID.Text = txtVendorID.Text + ListBox12.Items[i].Text;
                    txtVenID.Text = txtVenID.Text + ListBox12.Items[i].Text;
                    string splitvenid = "";
                    string[] splitarray;
                    splitvenid = ListBox12.Items[i].Text;
                    splitarray = splitvenid.Split('-');
                    txtVendorIDVal.Text = txtVendorIDVal.Text + splitarray[0];
                }
                else
                {
                    txtVendorID.Text = txtVendorID.Text + ", " + ListBox12.Items[i].Text;
                    txtVenID.Text = txtVenID.Text + ", " + ListBox12.Items[i].Text;
                    string splitvenid = "";
                    string[] splitarray;
                    splitvenid = ListBox12.Items[i].Text;
                    splitarray = splitvenid.Split('-');
                    txtVendorIDVal.Text = txtVendorIDVal.Text + "," + splitarray[0];
                }
            }

            txtWOT.Text = "";
            txtWOTVal.Text = "";
            txtWOT2.Text = "";
            foreach (int i in ListBox14.GetSelectedIndices())
            {
                if (txtWOT.Text == "")
                {
                    txtWOT.Text = txtWOT.Text + ListBox14.Items[i].Text;
                    txtWOT2.Text = txtWOT2.Text + ListBox14.Items[i].Text;
                    txtWOTVal.Text = txtWOTVal.Text + ListBox14.Items[i].Text;
                }
                else
                {
                    txtWOT.Text = txtWOT.Text + ", " + ListBox14.Items[i].Text;
                    txtWOT2.Text = txtWOT2.Text + ", " + ListBox14.Items[i].Text;
                    txtWOTVal.Text = txtWOTVal.Text + "," + ListBox14.Items[i].Text;
                }
            }

            txtWOV.Text = "";
            txtWOVVal.Text = "";
            txtWOV2.Text = "";
            txtWOV3.Text = "";
            txtWOV.Text = DropDownList51.SelectedItem.ToString() + " " + txt51.Text;

            if (DropDownList51.SelectedItem.ToString() == "Greater Than")
            {
                txtWOVVal.Text = "> " + txt51.Text;
            }
            else if (DropDownList51.SelectedItem.ToString() == "Less Than")
            {
                txtWOVVal.Text = "< " + txt51.Text;
            }
            else
            {
                txtWOVVal.Text = "= " + txt51.Text;
            }


            txtWOV2.Text = DropDownList51.SelectedItem.ToString() + " " + txt51.Text;
            txtWOV3.Text = DropDownList51.SelectedItem.ToString() + " " + txt51.Text;


            txtWOS.Text = "";
            txtWOSVal.Text = "";
            txtWOS2.Text = "";
            foreach (int i in ListBox15.GetSelectedIndices())
            {
                if (txtWOS.Text == "")
                {
                    txtWOS.Text = txtWOS.Text + ListBox15.Items[i].Text;
                    txtWOS2.Text = txtWOS2.Text + ListBox15.Items[i].Text;
                    txtWOSVal.Text = txtWOSVal.Text + ListBox15.Items[i].Text;

                }
                else
                {
                    txtWOS.Text = txtWOS.Text + ", " + ListBox15.Items[i].Text;
                    txtWOS2.Text = txtWOS2.Text + ", " + ListBox15.Items[i].Text;
                    txtWOSVal.Text = txtWOSVal.Text + "," + ListBox15.Items[i].Text;
                }
            }

            txtProcess.Text = DropDownList53.SelectedItem.ToString();
            txtProcessVal.Text = DropDownList53.SelectedItem.ToString();
            //string hold = " ";
            //foreach (ListItem li in ListBox4.Items)
            //{
            //    if (li.Selected)
            //    {
            //        txtCity.Text = txtCity.Text + li.ToString();
            //    }
            //}

            //txtCity.Text = ListBox14.SelectedItem.ToString();





        }
        protected void Button11_Click(object sender, EventArgs e)
        {
            //string connetionString = null;
            //string sql = null;
            //connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";

            //using (SqlConnection cnn = new SqlConnection(connetionString))
            //{
            //    sql = "insert into tbl_UC_Main_Rule ([rule_Name], [rule_desc], [rule_sched], [Period_WDM], [Period_HM], [Start_Run_Rule], "
            //       + " [Stop_Run_Rule])" +
            //       " values(@Name,@Desc,@sched,@WDM,@HM,@Start,@Stop)";

            //      using (SqlCommand cmd = new SqlCommand(sql, cnn))
            //      {
            //          cmd.Parameters.AddWithValue("@Name", "");
            //      }
            //}
        }

        // BACK
        protected void Button3_Click(object sender, EventArgs e)
        {
            lbladdnew.Visible = true;
            MultiView1.ActiveViewIndex = 0;
        }
        protected void Button4_Click(object sender, EventArgs e)
        {
            lbladdnew.Visible = true;
            MultiView1.ActiveViewIndex = 1;
        }
        protected void Button6_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 2;
        }
        protected void Button8_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 3;



        }
        protected void Button10_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 4;
        }

        //Page
        protected void page1_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 5;
            //Button12.Visible = false;
        }
        protected void page2_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 6;
            //Button17.Visible = false; 
        }

        protected void closeView_Click(object sender, EventArgs e)
        {

            MultiView1.ActiveViewIndex = 6;
        }

        protected void AddNew_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;


        }
        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {

        }
        protected void viewMore_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 7;
        }

        protected void CB4_CheckedChanged(object sender, EventArgs e)
        {
            if (CB4.Checked == true)
            {
                txtUtil.Enabled = true;
                ddl25.Enabled = true;
                ddl26.Enabled = true;
                ddl27.Enabled = true;
            }
            else
            {
                txtUtil.Enabled = false;
                ddl25.Enabled = false;
                ddl26.Enabled = false;
                ddl27.Enabled = false;
            }
        }
        protected void CheckBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5.Checked == true)
            {
                txtWork.Enabled = true;
                ddl28.Enabled = true;
                ddl29.Enabled = true;
                ddl30.Enabled = true;
            }
            else
            {
                txtWork.Enabled = false;

                ddl28.Enabled = false;
                ddl29.Enabled = false;
                ddl30.Enabled = false;
            }
        }
        protected void CheckBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6.Checked == true)
            {
                txtClient.Enabled = true;
                ddl31.Enabled = true;
                ddl32.Enabled = true;
                ddl33.Enabled = true;
            }
            else
            {
                txtClient.Enabled = false;
                ddl31.Enabled = false;
                ddl32.Enabled = false;
                ddl33.Enabled = false;
            }
        }
        protected void CheckBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7.Checked == true)
            {
                txtType.Enabled = true;
                ddl34.Enabled = true;
                ddl35.Enabled = true;
                ddl36.Enabled = true;
            }
            else
            {
                txtType.Enabled = false;
                ddl34.Enabled = false;
                ddl35.Enabled = false;
                ddl36.Enabled = false;
            }
        }
        protected void CheckBox8_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8.Checked == true)
            {
                txtReceive.Enabled = true;
                ddl37.Enabled = true;
                ddl38.Enabled = true;
                ddl39.Enabled = true;
            }
            else
            {
                txtReceive.Enabled = false;
                ddl37.Enabled = false;
                ddl38.Enabled = false;
                ddl39.Enabled = false;
            }
        }
        protected void CheckBox9_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9.Checked == true)
            {
                txtboarding.Enabled = true;
                ddl40.Enabled = true;
                ddl41.Enabled = true;
                ddl42.Enabled = true;
            }
            else
            {
                txtboarding.Enabled = false;
                ddl40.Enabled = false;
                ddl41.Enabled = false;
                ddl42.Enabled = false;
            }
        }
        protected void CheckBox10_CheckedChanged(object sender, EventArgs e)
        {
            if (cb10.Checked == true)
            {
                txtReferred.Enabled = true;
                ddl43.Enabled = true;
                ddl44.Enabled = true;
                ddl45.Enabled = true;
            }
            else
            {
                txtReferred.Enabled = false;
                ddl43.Enabled = false;
                ddl44.Enabled = false;
                ddl45.Enabled = false;
            }
        }
        //protected void DropDownList46_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if(TextBox1.Text == "")
        //    { 
        //    TextBox1.Text = DropDownList46.SelectedItem.ToString();
        //    }
        //    else
        //    {
        //        TextBox1.Text = TextBox1.Text + ", " + DropDownList46.SelectedItem.ToString();
        //    }
        //}


        protected void DropDownList12_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList12.SelectedItem.ToString() == "ACTIVE")
            {
                DropDownList12.BackColor = Color.FromArgb(76, 175, 80);
            }
            else
            {
                DropDownList12.BackColor = Color.FromArgb(217, 83, 79);
            }
        }
        protected void cbNo_CheckedChanged(object sender, EventArgs e)
        {
            //cbYes.Checked = false;
            txtComplete.Enabled = false;
            //txtCompleteDate.Enabled = false;
            ddlComplete_BC.Enabled = false;
            ddlComplete_DMY.Enabled = false;
        }
        protected void cbYes_CheckedChanged(object sender, EventArgs e)
        {
            //cbNo.Checked = false;
            txtComplete.Enabled = true;
            ddlComplete_BC.Enabled = true;
            ddlComplete_DMY.Enabled = true;
            //txtCompleteDate.Enabled = true;
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string selected = DropDownList2.SelectedItem.ToString();

            string script = @"alert(asd);";

            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "selected", script, true);

        }


        //protected void Button11_Click1(object sender, EventArgs e)
        //{

        //    string connetionString = null;
        //    string IDClient = "";
        //    connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";



        //    string Serloc;
        //    string Client;
        //    string Ex;
        //    string SessionRule = "";
        //    int cSerloc;
        //    int cClient;



        //    Serloc = txtServiceLocVal.Text;
        //    Client = txtClientStatVal.Text;

        //    string[] AserLoc = Serloc.Split(',');
        //    string[] Aclient = Client.Split(',');

        //    cSerloc = AserLoc.Count(); 
        //    cClient = Aclient.Count();


        //    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        //    var stringChars = new char[8];
        //    var random = new Random();

        //    for (int i = 0; i < stringChars.Length; i++)
        //    {
        //        stringChars[i] = chars[random.Next(chars.Length)];
        //    }

        //    var finalString = new String(stringChars);
        //    SessionRule = finalString.ToString();


        //    //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
        //    {


        //        for (int i = 0; i <= cSerloc; i++)
        //        {
        //            SqlCommand cmd1 = new SqlCommand("Insert into tbl_UC_Main_Rule" +
        //                " (Service_Location,isActive,SessionRuleID ) values('"

        //                + AserLoc[i].ToString() + "','" + 0 + "','" + SessionRule + "')", con);
        //            {
        //                con.Open();
        //                cmd1.ExecuteNonQuery();
        //                con.Close();

        //            }
        //        }

        //        //con.Open();
        //        //SqlCommand command = new SqlCommand("Select ID_MainRule From tbl_UC_Main_Rule Where SessionRuleID = '" + SessionRule + "'", con);
        //        //SqlDataReader reader = command.ExecuteReader();
        //        //while (reader.Read())
        //        //{
        //        //    Session["MainRuleID"] = reader["ID_MainRule"].ToString(); 
        //        //}
        //        //con.Close();


        //        //for (int i = 0; i < 2; i++) 
        //        //{





        //        //    //using (SqlCommand cmd2 = new SqlCommand("UPDATE tbl_UC_Main_Rule SET ([Service_Location], [rule_Name]) values(@SerLoc,@rName)"))
        //        //    //{
        //        //    //    using (SqlDataAdapter sda = new SqlDataAdapter())
        //        //    //    {
        //        //    //        cmd2.CommandType = CommandType.Text;
        //        //    //        cmd2.Parameters.AddWithValue("@SerLoc", Serloc[i]);
        //        //    //        //cmd2.Parameters.AddWithValue("@rName", Client[i]);
        //        //    //        cmd2.Connection = con;
        //        //    //        con.Open();
        //        //    //        cmd2.ExecuteNonQuery();
        //        //    //        con.Close();
        //        //    //    }
        //        //    //}
        //        //    //int MainID =  (Int32)Session["MainRuleID"] + 1;
        //        //} 
        //    }

        //} 
        [WebMethod]
        public static string vendorID(string itemSelec)
        {
            string[] arrItem;
            string newItem = "";
            arrItem = itemSelec.Split(',');

            for (int i = 0; i <= arrItem.Count(); i++)
            {
                if (i == 0)
                {
                    newItem = "'" + arrItem[i] + "'";
                }
                else if (i == arrItem.Count())
                {

                }
                else
                {
                    newItem = newItem + ",'" + arrItem[i] + "'";
                }
            }





            clsConnection cls = new clsConnection();

            string qry = "SELECT [USP_VendorID], [USP_Name], [Val_ID], [USP_VendorID] + '-' + [USP_Name] as NameID FROM [tbl_UC_USP_Name_Vendor_ID] " +
                " WHERE USP_Name IN (" + newItem + ")  ORDER BY [USP_Name] ASC";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        protected void ListBox11_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SqlConnection conS = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a"))
            {
                string getName = "";
                string[] getNameSplit = { "" };
                int countArr = 0;
                string query = "";
                string AndQry = " AND  USP_Name = '";
                foreach (int i in ListBox11.GetSelectedIndices())
                {
                    if (getName == "")
                    {
                        getName = "'" + getName + ListBox11.Items[i].Text + "'";
                    }
                    else
                    {
                        getName = getName + ", '" + ListBox11.Items[i].Text + "'";
                        getNameSplit = getName.Split(',');
                    }
                }

                countArr = getNameSplit.Count();
                for (int i = 0; i <= countArr; i++)
                {
                    if (i == 0)
                    {
                        query = "SELECT [USP_VendorID], [USP_Name], [Val_ID], [USP_VendorID] + '-' + [USP_Name] as NameID FROM [tbl_UC_USP_Name_Vendor_ID] WHERE [USP_Name] ="
                            + getName + "";
                        countArr = countArr - 1;
                    }
                    else
                    {
                        AndQry = AndQry + getNameSplit[i].ToString() + "' ";
                        query = "SELECT [USP_VendorID], [USP_Name], [Val_ID], [USP_VendorID] + '-' + [USP_Name] as NameID FROM [tbl_UC_USP_Name_Vendor_ID] WHERE [USP_Name] IN ("
                          + getName + ")";
                    }
                }

                SqlCommand cmd = new SqlCommand(query, conS);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                ListBox12.DataSource = ds;
                ListBox12.DataTextField = "NameID";
                ListBox12.DataValueField = "Val_ID";
                ListBox12.DataBind();
                //ListBox12.Items.Insert(0, new ListItem("--Select--", "*"));

            }

        }
        protected void lbedit11_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SqlConnection conS = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a"))
            {
                string getName = "";
                string[] getNameSplit = { "" };
                int countArr = 0;
                string query = "";
                string AndQry = " AND  USP_Name = '";
                foreach (int i in lbedit11.GetSelectedIndices())
                {
                    if (getName == "")
                    {
                        getName = "'" + getName + lbedit11.Items[i].Text + "'";
                    }
                    else
                    {
                        getName = getName + ", '" + lbedit11.Items[i].Text + "'";
                        getNameSplit = getName.Split(',');
                    }
                }

                countArr = getNameSplit.Count();
                for (int i = 0; i <= countArr; i++)
                {
                    if (i == 0)
                    {
                        query = "SELECT [USP_VendorID], [USP_Name], [Val_ID], [USP_VendorID] + '-' + [USP_Name] as NameID FROM [tbl_UC_USP_Name_Vendor_ID] WHERE [USP_Name] ="
                            + getName + "";
                        countArr = countArr - 1;
                    }
                    else
                    {
                        AndQry = AndQry + getNameSplit[i].ToString() + "' ";
                        query = "SELECT [USP_VendorID], [USP_Name], [Val_ID], [USP_VendorID] + '-' + [USP_Name] as NameID FROM [tbl_UC_USP_Name_Vendor_ID] WHERE [USP_Name] IN ("
                          + getName + ")";
                    }
                }

                SqlCommand cmd = new SqlCommand(query, conS);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                lbedit12.DataSource = ds;
                lbedit12.DataTextField = "NameID";
                lbedit12.DataValueField = "Val_ID";
                lbedit12.DataBind();
                //ListBox12.Items.Insert(0, new ListItem("--Select--", "*"));

            }

        }
    }
}
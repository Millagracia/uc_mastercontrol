﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Excel = Microsoft.Office.Interop.Excel;


namespace UC_v2._0
{
    public partial class USPCapabilities : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();


            //string qry = "Select EmpLevel FROm tbl_HRMS_EmployeeMaster where NTID = '" + Session["ntid"].ToString() + "'";
            //dt = cls.GetData(qry);

            //if (dt.Rows[0][0].ToString() == "SR. MGR" || dt.Rows[0][0].ToString() == "MGR" || dt.Rows[0][0].ToString() == "MANAGER" || dt.Rows[0][0].ToString() == "AM"
            //    || dt.Rows[0][0].ToString() == "DIR" || dt.Rows[0][0].ToString() == "VP" || dt.Rows[0][0].ToString() == "TL")
            //{

            //}
            //else if (Session["ntid"].ToString() == "solimanj" || Session["ntid"].ToString() == "torrmary" || Session["ntid"].ToString() == "mendiola"
            //    || Session["ntid"].ToString() == "lopezlad" || Session["ntid"].ToString() == "llorenge" || Session["ntid"].ToString() == "chavezar")
            //{

            //}
            //else
            //{
            //    Response.Redirect("Usersettings.aspx");
            //}
        }
        [WebMethod]
        public static string ExtractExcel()
        {
            clsConnection cls = new clsConnection();
            string qry = @"Select DISTINCT a.USP_Name, CASE WHEN b.[Bulk Email USP]  = '0' THEN 'No' ELSE 'Yes' END as 'Bulk Email USP',  CASE WHEN b.[Bulk Email ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'Bulk Email ASPS',
             CASE WHEN b.[Online Request USP]  = '0' THEN 'No' ELSE 'Yes' END as 'Online Request USP',  CASE WHEN b.[Online Request ASPS]  = '0' THEN 'No' ELSE 'Yes' 
            END as 'Online Request ASPS'
            , CASE WHEN b.[Call USP]  = '0' THEN 'No' ELSE 'Yes' END as 'Call USP',  CASE WHEN b.[Call ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'Call ASPS',
            CASE WHEN b.[EDI 814 USP]  = '0' THEN 'No' ELSE 'Yes' END as 'EDI 814 USP',  CASE WHEN b.[EDI 814 ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'EDI 814 ASPS'
            From [dbo].[tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [dbo].[tbl_UC_USPcapabilities_Start_Service] b 
            ON b.USP_ID = a.USP_ID Where a.isDeleted = 0 ORDER BY a.USP_Name";



            string qryExport = "Select DISTINCT a.USP_Name, CASE WHEN b.[Bulk Email USP]  = '0' THEN 'No' ELSE 'Yes' END as 'Bulk Email USP',  CASE WHEN b.[Bulk Email ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'Bulk Email ASPS',";
            qryExport += " CASE WHEN b.[Online Request USP]  = '0' THEN 'No' ELSE 'Yes' END as 'Online Request USP',  CASE WHEN b.[Online Request ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'Online Request ASPS'";
            qryExport += " , CASE WHEN b.[Call USP]  = '0' THEN 'No' ELSE 'Yes' END as 'Call USP',  CASE WHEN b.[Call ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'Call ASPS',";
            qryExport += " CASE WHEN b.[EDI 814 USP]  = '0' THEN 'No' ELSE 'Yes' END as 'EDI 814 USP',  CASE WHEN b.[EDI 814 ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'EDI 814 ASPS'";
            qryExport += " From [dbo].[tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [dbo].[tbl_UC_USPcapabilities_Stop_Service] b ";
            qryExport += " ON b.USP_ID = a.USP_ID Where a.isDeleted = 0 ORDER BY a.USP_Name";


            string qryInvoice = "Select DISTINCT a.USP_Name, CASE WHEN b.[Summary Billing USP]  = '0' THEN 'No' ELSE 'Yes' END as 'Summary Billing USP',  CASE WHEN b.[Summary Billing ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'Summary Billing ASPS', ";
            qryInvoice += "CASE WHEN b.[E-Bill w/ Password USP]  = '0' THEN 'No' ELSE 'Yes' END as 'E-Bill w/ Password USP',  CASE WHEN b.[E-Bill w/ Password ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'E-Bill w/ Password ASPS' ";
            qryInvoice += ", CASE WHEN b.[E-Bill w/o Password USP]  = '0' THEN 'No' ELSE 'Yes' END as 'E-Bill w/o Password USP',  CASE WHEN b.[E-Bill w/o Password ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'E-Bill w/o Password ASPS', ";
            qryInvoice += "CASE WHEN b.[E-Bill Via Link USP]  = '0' THEN 'No' ELSE 'Yes' END as 'E-Bill Via Link USP',  CASE WHEN b.[E-Bill Via Link ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'E-Bill Via Link ASPS', ";
            qryInvoice += "CASE WHEN b.[EDI 810 USP]  = '0' THEN 'No' ELSE 'Yes' END as 'EDI 810 USP',  CASE WHEN b.[EDI 810 ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'EDI 810 ASPS', ";
            qryInvoice += "CASE WHEN b.[Paper Bill USP]  = '0' THEN 'No' ELSE 'Yes' END as 'Paper Bill USP',  CASE WHEN b.[Paper Bill ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'Paper Bill ASPS'";
            qryInvoice += "From [dbo].[tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [dbo].[tbl_UC_USPcapabilities_Invoicing] b  ";
            qryInvoice += "ON b.USP_ID = a.USP_ID Where a.isDeleted = 0 ORDER BY a.USP_Name ";


            string qryBill = "Select DISTINCT a.USP_Name, CASE WHEN b.[ACH USP]  = '0' THEN 'No' ELSE 'Yes' END as 'ACH USP',  CASE WHEN b.[ACH ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'ACH ASPS', ";
            qryBill += "CASE WHEN b.[Wire Transfer USP]  = '0' THEN 'No' ELSE 'Yes' END as 'Wire Transfer USP',  CASE WHEN b.[Wire Transfer ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'Wire Transfer ASPS' ";
            qryBill += ", CASE WHEN b.[Credit Card USP]  = '0' THEN 'No' ELSE 'Yes' END as 'Credit Card USP',  CASE WHEN b.[Credit Card ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'Credit Card ASPS', ";
            qryBill += "CASE WHEN b.[Check USP]  = '0' THEN 'No' ELSE 'Yes' END as 'Check USP',  CASE WHEN b.[Check ASPS]  = '0' THEN 'No' ELSE 'Yes' END as 'Check ASPS' ";
            qryBill += "From [dbo].[tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [dbo].[tbl_UC_USPcapabilities_BillsPayment] b  ";
            qryBill += "ON b.USP_ID = a.USP_ID Where a.isDeleted = 0 ORDER BY a.USP_Name ";

            DataTable dt = new DataTable();
            DataTable dtExport = new DataTable();
            DataTable dtInvoicing = new DataTable();
            DataTable dtBills = new DataTable();
            dt = cls.GetData(qry);
            dtExport = cls.GetData(qryExport);
            dtInvoicing = cls.GetData(qryInvoice);
            dtBills = cls.GetData(qryBill);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    asd = dt,
                    asd2 = dtExport,
                    asd3 = dtInvoicing,
                    asd4 = dtBills
                }
            });
        }



        //Set PRIO in Start Service
        [WebMethod]
        public static string SetPrio(string Name, string preference, string PRIO)
        {
            clsConnection cls = new clsConnection();
            string update = "";
            if (PRIO == "Start")
            {
                update = "UPDATE tbl_UC_USP_StartService_PRIO SET Prio = '" + preference + "' Where Method IN ('" + Name + "')";
            }
            else if (PRIO == "Stop")
            {
                update = "UPDATE tbl_UC_USP_StopService_PRIO SET Prio = '" + preference + "' Where Method IN ('" + Name + "')";
            }
            else if (PRIO == "Invoice")
            {
                update = "UPDATE tbl_UC_USP_Invoice_PRIO SET Prio = '" + preference + "' Where Method IN ('" + Name + "')";
            }
            else if (PRIO == "Bills")
            {
                update = "UPDATE tbl_UC_USP_BillsPayment_PRIO SET Prio = '" + preference + "' Where Method IN ('" + Name + "')";
            }
            else
            {

            }

            cls.ExecuteQuery(update);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { } });
        }

        [WebMethod]
        public static string UpdateStatus(string editVal)
        {
            ArrayList arrPages = new ArrayList();
            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();
            string[] process_ID = editVal.Split(',');
            string UpdateQry = "";
            string qry = "";
            string YesNO = "";
            string ReoResi = "";

            qry = "Select * FROm tbl_UC_Client_Status where Val_ID = " + process_ID[1].ToString();
            dt = cls.GetData(qry);

            if (process_ID[0].ToString() == "Yes_Start")
            {
                UpdateQry = "Update tbl_UC_Client_Status Set Start_Service = 'Yes' Where Val_ID = " + process_ID[1].ToString();
                YesNO = " Start Service to Yes";
            }
            else if (process_ID[0].ToString() == "No_Start")
            {
                UpdateQry = "Update tbl_UC_Client_Status Set Start_Service = 'No' Where Val_ID = " + process_ID[1].ToString();
                YesNO = " Start Service to No";
            }
            else if (process_ID[0].ToString() == "Yes_Stop")
            {
                UpdateQry = "Update tbl_UC_Client_Status Set Stop_Service = 'Yes' Where Val_ID = " + process_ID[1].ToString();
                YesNO = " Stop Service to Yes";
            }
            else if (process_ID[0].ToString() == "No_Stop")
            {
                UpdateQry = "Update tbl_UC_Client_Status Set Stop_Service = 'No' Where Val_ID = " + process_ID[1].ToString();
                YesNO = " Stop Service to No";
            }
            else
            {
            }

            cls.ExecuteQuery(UpdateQry);

            if (dt.Rows[0][0].ToString() == "31")
            {
                ReoResi = "REO";
            }
            else
            {
                ReoResi = "RESI";
            }

            arrPages.Add("Eligible Client Statuses");
            arrPages.Add(ReoResi);
            cls.FUNC.Audit("Update " + dt.Rows[0][1].ToString() + YesNO, arrPages);

            return "True";
        }
        [WebMethod]
        public static string UpdateStatusPFC(string editVal)
        {
            ArrayList arrPages = new ArrayList();
            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();
            string[] process_ID = editVal.Split(',');
            string UpdateQry = "";
            string qry = "";
            string YesNO = "";

            qry = "Select * FROm tbl_UC_Client_Status_PFC where Val_ID = " + process_ID[1].ToString();
            dt = cls.GetData(qry);

            if (process_ID[0].ToString() == "Yes_Start")
            {
                UpdateQry = "Update tbl_UC_Client_Status_PFC Set Start_Service = 'Yes' Where Val_ID = " + process_ID[1].ToString();
                YesNO = " Start Service to Yes";
            }
            else if (process_ID[0].ToString() == "No_Start")
            {
                UpdateQry = "Update tbl_UC_Client_Status_PFC Set Start_Service = 'No' Where Val_ID = " + process_ID[1].ToString();
                YesNO = " Start Service to No";
            }
            else if (process_ID[0].ToString() == "Yes_Stop")
            {
                UpdateQry = "Update tbl_UC_Client_Status_PFC Set Stop_Service = 'Yes' Where Val_ID = " + process_ID[1].ToString();
                YesNO = " Stop Service to Yes";
            }
            else if (process_ID[0].ToString() == "No_Stop")
            {
                UpdateQry = "Update tbl_UC_Client_Status_PFC Set Stop_Service = 'No' Where Val_ID = " + process_ID[1].ToString();
                YesNO = " Stop Service to No";
            }
            else
            {
            }

            cls.ExecuteQuery(UpdateQry);



            arrPages.Add("Eligible Client Statuses");
            arrPages.Add("PFC");
            cls.FUNC.Audit("Update " + dt.Rows[0][1].ToString() + YesNO, arrPages);


            return "True";
        }

        [WebMethod]
        public static string UpdateStartStatus(string editVal)
        {
            ArrayList arrPages = new ArrayList();
            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();
            string[] process_ID = editVal.Split(',');
            string UpdateQry = "";
            UpdateQry = "Update tbl_UC_USP_Start_Service_Status Set isCheck = '" + process_ID[0].ToString() + "' Where id = " + process_ID[1].ToString();
            cls.ExecuteQuery(UpdateQry);


            string qry = "Select * FROM tbl_UC_USP_Start_Service_Status where id = " + process_ID[1].ToString();
            dt = cls.GetData(qry);


            arrPages.Add("Other Settings");
            arrPages.Add("-");
            //Update USP :  USPName on Call ASPS to Yes/No 
            cls.FUNC.Audit("Update Start Service Status " + dt.Rows[0][1].ToString() + " to " + process_ID[0].ToString(), arrPages);


            return "True";
        }
        [WebMethod]
        public static string UpdateTask(string editVal)
        {
            ArrayList arrPages = new ArrayList();
            clsConnection cls = new clsConnection();
            string[] process_ID = editVal.Split(',');
            string UpdateQry = "";
            string Task = "";
            UpdateQry = "Update [tbl_UC_USP_Task] Set isCheck = '" + process_ID[0].ToString() + "' Where id = " + process_ID[1].ToString();

            if (process_ID[1].ToString() == "1")
            {
                Task = "Start";
            }
            else if (process_ID[1].ToString() == "2")
            {
                Task = "Stop";
            }
            else
            {
                Task = "Water Payout";
            }




            arrPages.Add("Other Settings");
            arrPages.Add("-");
            //Update USP :  USPName on Call ASPS to Yes/No 
            cls.FUNC.Audit("Update Task " + Task + " to " + process_ID[0].ToString(), arrPages);


            cls.ExecuteQuery(UpdateQry);

            return "True";
        }


        //Not Used
        [WebMethod]
        public static string GetUtilityVal()
        {
            clsConnection cls = new clsConnection();
            string qry = "select * from tbl_UC_USP_StartService_PRIO a Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where a.Prio_ID = '1'";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetAtoZ()
        {
            clsConnection cls = new clsConnection();
            string qry = "SELECT Distinct SUBSTRING(USP_Name, 1, 1) As NewColumn from tbl_UC_USP_Name_Vendor_ID";
            DataTable dt = new DataTable();
        
            dt = cls.GetData(qry);
         

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt} });
        }


        [WebMethod]
        public static string GetUtilityStatusDDl()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select * From tbl_UC_USP_Start_Service_Status Where isCheck = 'Yes'";
            string qryCLient = "Select * From tbl_UC_Client";
            string qry1 = "select * from tbl_UC_USP_StartService_PRIO a Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID where b.Start_Stop_Invoice_Bills " +
                " = 1 ORDER BY a.Prio_ID ";
            string qryClientPrio = "select * from tbl_UC_USP_StartService_PRIO a Inner Join tbl_UC_USP_Prio_Client b ON b.Prio_ID = a.Prio_ID where b.Start_Stop_Invoice_Bills " +
                " = 1 ORDER BY a.Prio_ID ";
            //string qry1 = "select * from tbl_UC_USP_StartService_PRIO a Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where a.Prio_ID = '1'";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dtClient = new DataTable();
            DataTable dtClientPrio = new DataTable();
            dt = cls.GetData(qry);
            dt1 = cls.GetData(qry1);
            dtClient = cls.GetData(qryCLient);
            dtClientPrio = cls.GetData(qryClientPrio);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd1 = dt1, asd2 = dtClient, asd3 = dtClientPrio } });
        }
        [WebMethod]
        public static string GetUtilityStatusDDlStop()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select * From tbl_UC_USP_Start_Service_Status Where isCheck = 'Yes'";
            string qryCLient = "Select * From tbl_UC_Client";
            string qry1 = "select * from tbl_UC_USP_StopService_PRIO a Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID where b.Start_Stop_Invoice_Bills " +
                " = 2 ORDER BY a.Prio_ID ";
            string qryClientPrio = "select * from tbl_UC_USP_StartService_PRIO a Inner Join tbl_UC_USP_Prio_Client b ON b.Prio_ID = a.Prio_ID where b.Start_Stop_Invoice_Bills " +
                " = 2 ORDER BY a.Prio_ID ";
            //string qry1 = "select * from tbl_UC_USP_StartService_PRIO a Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where a.Prio_ID = '1'";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dtClient = new DataTable();
            DataTable dtClientPrio = new DataTable();
            dt = cls.GetData(qry);
            dt1 = cls.GetData(qry1);
            dtClient = cls.GetData(qryCLient);
            dtClientPrio = cls.GetData(qryClientPrio);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd1 = dt1, asd2 = dtClient, asd3 = dtClientPrio } });
        }
        [WebMethod]
        public static string GetUtilityStatusDDlInvoice()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select * From tbl_UC_USP_Start_Service_Status Where isCheck = 'Yes'";
            string qryCLient = "Select * From tbl_UC_Client";
            string qry1 = "select * from tbl_UC_USP_Invoice_PRIO a Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID where b.Start_Stop_Invoice_Bills " +
                " = 3 ORDER BY a.Prio_ID ";
            string qryClientPrio = "select * from tbl_UC_USP_Invoice_PRIO a Inner Join tbl_UC_USP_Prio_Client b ON b.Prio_ID = a.Prio_ID where b.Start_Stop_Invoice_Bills " +
                " = 3 ORDER BY a.Prio_ID ";
            //string qry1 = "select * from tbl_UC_USP_StartService_PRIO a Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where a.Prio_ID = '1'";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dtClient = new DataTable();
            DataTable dtClientPrio = new DataTable();
            dt = cls.GetData(qry);
            dt1 = cls.GetData(qry1);
            dtClient = cls.GetData(qryCLient);
            dtClientPrio = cls.GetData(qryClientPrio);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd1 = dt1, asd2 = dtClient, asd3 = dtClientPrio } });
        }
        [WebMethod]
        public static string GetUtilityStatusDDlBills()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select * From tbl_UC_USP_Start_Service_Status";
            string qryCLient = "Select * From tbl_UC_Client";
            string qry1 = "select * from tbl_UC_USP_BillsPayment_PRIO a Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID where b.Start_Stop_Invoice_Bills " +
                " = 4 ORDER BY a.Prio_ID ";
            string qryClientPrio = "select * from tbl_UC_USP_BillsPayment_PRIO a Inner Join tbl_UC_USP_Prio_Client b ON b.Prio_ID = a.Prio_ID where b.Start_Stop_Invoice_Bills " +
                " = 4 ORDER BY a.Prio_ID ";
            //string qry1 = "select * from tbl_UC_USP_StartService_PRIO a Inner Join tbl_UC_USP_Prio_Utility b ON b.Prio_ID = a.Prio_ID Where a.Prio_ID = '1'";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dtClient = new DataTable();
            DataTable dtClientPrio = new DataTable();
            dt = cls.GetData(qry);
            dt1 = cls.GetData(qry1);
            dtClient = cls.GetData(qryCLient);
            dtClientPrio = cls.GetData(qryClientPrio);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd1 = dt1, asd2 = dtClient, asd3 = dtClientPrio } });
        }
        [WebMethod]
        public static string UpdateUtilityType(string editVal)
        {

            clsConnection cls = new clsConnection();
            string[] process_ID = editVal.Split(',');
            string UpdateQry = "";
            UpdateQry = "Update [tbl_UC_USP_UtilityType_OtherSettings] Set isCheck = '" + process_ID[0].ToString() + "' Where id = " + process_ID[1].ToString();
            cls.ExecuteQuery(UpdateQry);





            return "True";
        }



        [WebMethod]
        public static string InsertUtilityStatus(int PrioID, string itemSelec, string itemSelecClient)
        {
            ArrayList arrPages = new ArrayList();
            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();
            string deleteStatus = "Delete tbl_UC_USP_Prio_Utility where Prio_ID =" + PrioID + " AND Start_Stop_Invoice_Bills = 1";
            string deleteStatusClient = "Delete tbl_UC_USP_Prio_Client where Prio_ID =" + PrioID + " AND Start_Stop_Invoice_Bills = 1";
            cls.ExecuteQuery(deleteStatus);
            cls.ExecuteQuery(deleteStatusClient);
            string[] itemSelecArry = itemSelec.Split(',');
            string[] itemSelecClientArry = itemSelecClient.Split(',');
            string qry = "INSERT INTO tbl_UC_USP_Prio_Utility (Prio_ID, Util_Name,Start_Stop_Invoice_Bills) values ";
            string qryClient = "INSERT INTO tbl_UC_USP_Prio_Client (Prio_ID, Client_Name,Start_Stop_Invoice_Bills) values ";

            for (int i = 0; i <= itemSelecArry.Count(); i++)
            {
                if (i == itemSelecArry.Count())
                {
                    i = 100;
                }
                else if (itemSelecArry[i].ToString() == "")
                {

                }
                else
                {
                    qry += "('" + PrioID + "','" + itemSelecArry[i].ToString() + "','1'),";
                }
            }

            for (int i = 0; i <= itemSelecClientArry.Count(); i++)
            {
                if (i == itemSelecClientArry.Count())
                {
                    i = 100;
                }
                else if (itemSelecClientArry[i].ToString() == "")
                {

                }
                else
                {
                    qryClient += "('" + PrioID + "','" + itemSelecClientArry[i].ToString() + "','1'),";
                }
            }
            string Selectqry = "Select * FROM tbl_UC_USP_StartService_PRIO where Prio_ID = " + PrioID;
            dt = cls.GetData(Selectqry);


            arrPages.Add("Prioritization");
            arrPages.Add("Start Service");
            //Update USP :  USPName on Call ASPS to Yes/No 
            cls.FUNC.Audit("Update Qualified Utility Status and Client on " + dt.Rows[0][2].ToString(), arrPages);


            qry = qry.Remove(qry.Length - 1, 1);
            cls.ExecuteQuery(qry);

            qryClient = qryClient.Remove(qryClient.Length - 1, 1);
            cls.ExecuteQuery(qryClient);

            return "True";
        }
        [WebMethod]
        public static string InsertUtilityStatusStop(int PrioID, string itemSelec, string itemSelecClient)
        {
            ArrayList arrPages = new ArrayList();
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string deleteStatus = "Delete tbl_UC_USP_Prio_Utility where Prio_ID =" + PrioID + " AND Start_Stop_Invoice_Bills = 2";
            string deleteStatusClient = "Delete tbl_UC_USP_Prio_Client where Prio_ID =" + PrioID + " AND Start_Stop_Invoice_Bills = 2";
            cls.ExecuteQuery(deleteStatus);
            cls.ExecuteQuery(deleteStatusClient);
            string[] itemSelecArry = itemSelec.Split(',');
            string[] itemSelecClientArry = itemSelecClient.Split(',');
            string qry = "INSERT INTO tbl_UC_USP_Prio_Utility (Prio_ID, Util_Name,Start_Stop_Invoice_Bills) values";
            string qryClient = "INSERT INTO tbl_UC_USP_Prio_Client (Prio_ID, Client_Name,Start_Stop_Invoice_Bills) values ";

            for (int i = 0; i <= itemSelecArry.Count(); i++)
            {
                if (i == itemSelecArry.Count())
                {
                    i = 100;
                }
                else if (itemSelecArry[i].ToString() == "")
                {

                }
                else
                {
                    qry += "('" + PrioID + "','" + itemSelecArry[i].ToString() + "','2'),";
                }
            }

            for (int i = 0; i <= itemSelecClientArry.Count(); i++)
            {
                if (i == itemSelecClientArry.Count())
                {
                    i = 100;
                }
                else if (itemSelecClientArry[i].ToString() == "")
                {

                }
                else
                {
                    qryClient += "('" + PrioID + "','" + itemSelecClientArry[i].ToString() + "','2'),";
                }
            }

            qry = qry.Remove(qry.Length - 1, 1);
            cls.ExecuteQuery(qry);

            qryClient = qryClient.Remove(qryClient.Length - 1, 1);
            cls.ExecuteQuery(qryClient);

            string Selectqry = "Select * FROM tbl_UC_USP_StopService_PRIO where Prio_ID =" + PrioID;
            dt = cls.GetData(Selectqry);


            arrPages.Add("Prioritization");
            arrPages.Add("Stop Service");
            //Update USP :  USPName on Call ASPS to Yes/No 
            cls.FUNC.Audit("Update Qualified Utility Status and Client on " + dt.Rows[0][2].ToString(), arrPages);

            return "True";
        }
        [WebMethod]
        public static string InsertUtilityStatusInvoice(int PrioID, string itemSelec, string itemSelecClient)
        {
            ArrayList arrPages = new ArrayList();
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string deleteStatus = "Delete tbl_UC_USP_Prio_Utility where Prio_ID =" + PrioID + " AND Start_Stop_Invoice_Bills = 3";
            string deleteStatusClient = "Delete tbl_UC_USP_Prio_Client where Prio_ID =" + PrioID + " AND Start_Stop_Invoice_Bills = 3";
            cls.ExecuteQuery(deleteStatus);
            cls.ExecuteQuery(deleteStatusClient);
            string[] itemSelecArry = itemSelec.Split(',');
            string[] itemSelecClientArry = itemSelecClient.Split(',');
            string qry = "INSERT INTO tbl_UC_USP_Prio_Utility (Prio_ID, Util_Name,Start_Stop_Invoice_Bills) values";
            string qryClient = "INSERT INTO tbl_UC_USP_Prio_Client (Prio_ID, Client_Name,Start_Stop_Invoice_Bills) values ";

            for (int i = 0; i <= itemSelecArry.Count(); i++)
            {
                if (i == itemSelecArry.Count())
                {
                    i = 100;
                }
                else if (itemSelecArry[i].ToString() == "")
                {

                }
                else
                {
                    qry += "('" + PrioID + "','" + itemSelecArry[i].ToString() + "','3'),";
                }
            }

            for (int i = 0; i <= itemSelecClientArry.Count(); i++)
            {
                if (i == itemSelecClientArry.Count())
                {
                    i = 100;
                }
                else if (itemSelecClientArry[i].ToString() == "")
                {

                }
                else
                {
                    qryClient += "('" + PrioID + "','" + itemSelecClientArry[i].ToString() + "','3'),";
                }
            }


            qry = qry.Remove(qry.Length - 1, 1);
            cls.ExecuteQuery(qry);

            qryClient = qryClient.Remove(qryClient.Length - 1, 1);
            cls.ExecuteQuery(qryClient);

            string Selectqry = "Select * FROM tbl_UC_USP_Invoice_PRIO where Prio_ID =" + PrioID;
            dt = cls.GetData(Selectqry);

            arrPages.Add("Prioritization");
            arrPages.Add("Invoice");
            //Update USP :  USPName on Call ASPS to Yes/No 
            cls.FUNC.Audit("Update Qualified Utility Status and Client on " + dt.Rows[0][2].ToString(), arrPages);


            return "True";
        }
        [WebMethod]
        public static string InsertUtilityStatusBills(int PrioID, string itemSelec, string itemSelecClient)
        {
            ArrayList arrPages = new ArrayList();
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string deleteStatus = "Delete tbl_UC_USP_Prio_Utility where Prio_ID =" + PrioID + " AND Start_Stop_Invoice_Bills = 4";
            string deleteStatusClient = "Delete tbl_UC_USP_Prio_Client where Prio_ID =" + PrioID + " AND Start_Stop_Invoice_Bills = 4";
            cls.ExecuteQuery(deleteStatus);
            cls.ExecuteQuery(deleteStatusClient);
            string[] itemSelecArry = itemSelec.Split(',');
            string[] itemSelecClientArry = itemSelecClient.Split(',');
            string qry = "INSERT INTO tbl_UC_USP_Prio_Utility (Prio_ID, Util_Name,Start_Stop_Invoice_Bills) values";
            string qryClient = "INSERT INTO tbl_UC_USP_Prio_Client (Prio_ID, Client_Name,Start_Stop_Invoice_Bills) values ";

            for (int i = 0; i <= itemSelecArry.Count(); i++)
            {
                if (i == itemSelecArry.Count())
                {
                    i = 100;
                }
                else if (itemSelecArry[i].ToString() == "")
                {

                }
                else
                {
                    qry += "('" + PrioID + "','" + itemSelecArry[i].ToString() + "','3'),";
                }
            }

            for (int i = 0; i <= itemSelecClientArry.Count(); i++)
            {
                if (i == itemSelecClientArry.Count())
                {
                    i = 100;
                }
                else if (itemSelecClientArry[i].ToString() == "")
                {

                }
                else
                {
                    qryClient += "('" + PrioID + "','" + itemSelecClientArry[i].ToString() + "','3'),";
                }
            }
            qry = qry.Remove(qry.Length - 1, 1);
            cls.ExecuteQuery(qry);

            qryClient = qryClient.Remove(qryClient.Length - 1, 1);
            cls.ExecuteQuery(qryClient);

            string Selectqry = "Select * FROM tbl_UC_USP_BillsPayment_PRIO where Prio_ID =" + PrioID;
            dt = cls.GetData(Selectqry);

            arrPages.Add("Prioritization");
            arrPages.Add("BillsPayment");
            //Update USP :  USPName on Call ASPS to Yes/No 
            cls.FUNC.Audit("Update Qualified Utility Status on " + dt.Rows[0][2].ToString(), arrPages);

            return "True";
        }



        [WebMethod]
        public static string UpdateStopStatus(string editVal)
        {
            ArrayList arrPages = new ArrayList();
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string[] process_ID = editVal.Split(',');
            string UpdateQry = "";
            UpdateQry = "Update tbl_UC_USP_Stop_Service_Status Set isCheck = '" + process_ID[0].ToString() + "' Where id = " + process_ID[1].ToString();
            cls.ExecuteQuery(UpdateQry);



            string qry = "Select * FROM tbl_UC_USP_Stop_Service_Status where id = " + process_ID[1].ToString();
            dt = cls.GetData(qry);


            arrPages.Add("Other Settings");
            arrPages.Add("-");
            //Update USP :  USPName on Call ASPS to Yes/No 
            cls.FUNC.Audit("Update Status " + dt.Rows[0][1].ToString() + " to " + process_ID[0].ToString(), arrPages);

            return "True";
        }
        //Bind Start Service Prioritization
        [WebMethod]
        public static string GetPrioStart()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select * From tbl_UC_USP_StartService_PRIO Order By Prio";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        //Bind Stop Service Prioritization
        [WebMethod]
        public static string GetPrioStop()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select * From tbl_UC_USP_StopService_PRIO Order By Prio";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        //Bind Invoicing Prioritization
        [WebMethod]
        public static string GetPrioInvoice()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select * From tbl_UC_USP_Invoice_PRIO Order By Prio";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        //Bind BillsPayment Prioritization
        [WebMethod]
        public static string GetPrioBills()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select * From tbl_UC_USP_BillsPayment_PRIO Order By Prio";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }


        [WebMethod]
        public static string GetClientStatus()
        {
            clsConnection cls = new clsConnection();
            string qry1 = "Select * From tbl_UC_Client_Status Where ID_Client = 31 Order BY Val_ID";
            string qry2 = "Select * From tbl_UC_Client_Status Where ID_Client = 32 Order BY Val_ID";
            string qry3 = "Select * From tbl_UC_Client_Status_PFC Where ID_Client = 34 Order BY Val_ID";
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            dt1 = cls.GetData(qry1);
            dt2 = cls.GetData(qry2);
            dt3 = cls.GetData(qry3);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt1, asd2 = dt2, asd3 = dt3 } });
        }
        [WebMethod]
        public static string GetStartStatus()
        {
            clsConnection cls = new clsConnection();
            string qry1 = "Select * From tbl_UC_USP_Start_Service_Status";
            DataTable dt1 = new DataTable();
            dt1 = cls.GetData(qry1);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt1 } });
        }
        [WebMethod]
        public static string GetTask()
        {
            clsConnection cls = new clsConnection();
            string qry1 = "Select * From [tbl_UC_USP_Task]";
            DataTable dt1 = new DataTable();
            dt1 = cls.GetData(qry1);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt1 } });
        }
        [WebMethod]
        public static string GetUtilityType()
        {
            clsConnection cls = new clsConnection();
            string qry1 = "Select * From [tbl_UC_USP_UtilityType_OtherSettings]";
            DataTable dt1 = new DataTable();
            dt1 = cls.GetData(qry1);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt1 } });
        }

        [WebMethod]
        public static string GetStopStatus()
        {
            clsConnection cls = new clsConnection();
            string qry1 = "Select * From tbl_UC_USP_Stop_Service_Status";
            DataTable dt1 = new DataTable();
            dt1 = cls.GetData(qry1);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt1 } });
        }
        [WebMethod]
        public static string GetStart(string Alpha)
        {
            clsConnection cls = new clsConnection();
            string qry = @"Select DISTINCT a.USP_ID, a.USP_Name,a.isDeleted, b.[Bulk Email USP] as 'bulkUSP', b.[Online Request USP] as 'onlineUSP', b.[EDI 814 USP] as 'ediUSP', b.[Call USP] as CallUSP 
             , b.[Bulk Email ASPS] as 'bulkASPS', b.[Online Request ASPS] as 'onlineASPS', b.[EDI 814 ASPS] as 'ediASPS', b.[Call ASPS] as CallASPS
             From [dbo].[tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [dbo].[tbl_UC_USPcapabilities_Start_Service] b 
             ON b.USP_ID = a.USP_ID Where a.isDeleted = 0 and SUBSTRING(a.USP_Name, 1, 1) = '" + Alpha + "' ORDER BY a.USP_Name  ";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetStop(string Alpha)
        {
            clsConnection cls = new clsConnection();

            string qry = "Select DISTINCT a.USP_ID, a.USP_Name,a.isDeleted, b.[Bulk Email USP] as 'bulkUSP', b.[Online Request USP] as 'onlineUSP', b.[EDI 814 USP] as 'ediUSP', b.[Call USP] as CallUSP ";
            qry += " , b.[Bulk Email ASPS] as 'bulkASPS', b.[Online Request ASPS] as 'onlineASPS', b.[EDI 814 ASPS] as 'ediASPS', b.[Call ASPS] as CallASPS";
            qry += " From [dbo].[tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [dbo].[tbl_UC_USPcapabilities_Stop_Service] b ";
            qry += " ON b.USP_ID = a.USP_ID Where a.isDeleted = 0 and SUBSTRING(a.USP_Name, 1, 1) = '" + Alpha + "'  ORDER BY a.USP_Name";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetInvoice(string Alpha)
        {
            clsConnection cls = new clsConnection();

            //string qry = " Select USP_Name, [Summary Billing] As 'summary',[E-Bill w/ Password] As 'password1', [E-Bill w/o Password] as 'password2'," +
            //    " [E-Bill Via Link] as 'Link', [EDI 810] as 'edi', [Paper Bill] as 'bill' FROM [dbo].[tbl_UC_USPcapabilities_Invoicing]";

            string qry = "Select DISTINCT a.USP_ID,a.USP_Name,a.isDeleted, b.[Summary Billing USP] As 'summaryUSP', b.[E-Bill w/ Password USP] As 'password1USP', b.[E-Bill w/o Password USP] as 'password2USP', ";
            qry += " b.[E-Bill Via Link USP] as 'LinkUSP', b.[EDI 810 USP] as 'ediUSP', b.[Paper Bill USP] as 'billUSP'";
            qry += ", b.[Summary Billing ASPS] As 'summaryASPS', b.[E-Bill w/ Password ASPS] As 'password1ASPS', b.[E-Bill w/o Password ASPS] as 'password2ASPS', ";
            qry += " b.[E-Bill Via Link ASPS] as 'LinkASPS', b.[EDI 810 ASPS] as 'ediASPS', b.[Paper Bill ASPS] as 'billASPS'";
            qry += " From [dbo].[tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [dbo].[tbl_UC_USPcapabilities_Invoicing] b ";
            qry += " ON b.USP_ID = a.USP_ID Where a.isDeleted = 0 and SUBSTRING(a.USP_Name, 1, 1) = '" + Alpha + "'  ORDER BY a.USP_Name";


            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetBills(string Alpha)
        {
            clsConnection cls = new clsConnection();
            //string qry = "Select USP_Name, [ACH], [Wire Transfer] As 'wire', [Credit Card] as 'credit'," +
            //    " [Check] FROM [dbo].[tbl_UC_USPcapabilities_BillsPayment]";

            string qry = "Select DISTINCT a.USP_ID, a.USP_Name, a.isDeleted, b.[ACH USP] as ACHUSP, b.[Wire Transfer USP] As 'wireUSP', b.[Credit Card USP] as 'creditUSP',  b.[Check USP] as CheckUSP";
            qry += ", b.[ACH ASPS] as ACHASPS, b.[Wire Transfer ASPS] As 'wireASPS', b.[Credit Card ASPS] as 'creditASPS',  b.[Check ASPS] as CheckASPS";
            qry += " From [dbo].[tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [dbo].[tbl_UC_USPcapabilities_BillsPayment] b ";
            qry += " ON b.USP_ID = a.USP_ID Where a.isDeleted = 0 and SUBSTRING(a.USP_Name, 1, 1) = '" + Alpha + "'  ORDER BY a.USP_Name";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string UPdateUSP(string val, string USP)
        {

            string[] arrVal = val.Split(',');
            string valstr = "";
            string Change = "";
            string YesNo = "";
            ArrayList arrPages = new ArrayList();
            if (arrVal[1] == "1")
            {
                valstr = "0";
                YesNo = "No";
            }
            else
            {
                valstr = "1";
                YesNo = "Yes";
            }

            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();



            string selectqry = " Select * FROM tbl_UC_USP_Name_Vendor_ID Where replace(USP_Name,' ','') Like  replace('"
                + arrVal[0] + "%',' ','') and isDeleted = 0";
            dt = cls.GetData(selectqry);

            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog=MIS_MLA_UAT;Integrated Security=True");
            ////SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            {
                if (USP == "Bulk")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Start_Service SET " +
                   "[Bulk Email USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();
                    }
                    Change = " on Bulk Email USP to " + YesNo + "";
                }
                else if (USP == "Online")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Start_Service SET " +
                   "[Online Request USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();

                    }
                    Change = " on Online Request USP to " + YesNo + "";
                }
                else if (USP == "EDI")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Start_Service SET " +
                   "[EDI 814 USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();

                    }
                    Change = " on EDI 814 USP to " + YesNo + "";
                }
                else if (USP == "Call")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Start_Service SET " +
                   "[Call USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();
                    }
                    Change = " on Call USP to " + YesNo + "";
                }


                //ASPS UPDATE
                if (USP == "BulkASPS")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Start_Service SET " +
                   "[Bulk Email ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();
                    }
                    Change = " on Bulk Email ASPS to " + YesNo + "";
                }
                else if (USP == "OnlineASPS")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Start_Service SET " +
                   "[Online Request ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();

                    }
                    Change = " on Online Request ASPS to " + YesNo + "";
                }
                else if (USP == "EDIASPS")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Start_Service SET " +
                   "[EDI 814 ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();

                    }
                    Change = " on EDI 814 ASPS to " + YesNo + "";
                }
                else if (USP == "CallASPS")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Start_Service SET " +
                   "[Call ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();
                    }
                    Change = " on Call ASPS to " + YesNo + "";
                }
                else
                {

                }

                //12/15/2017
                string qryGetPrio = "SELECT * FROM [dbo].[tbl_UC_USP_StartService_PRIO] Order By Prio ";
                DataTable dtprio = new DataTable();
                dtprio = cls.GetData(qryGetPrio);

                string qryprioUSP = @"Select * from vw_UC_USP_StartPrio where USP_ID = '" + dt.Rows[0][0].ToString() + "' Order by Prio ";
                DataTable dtUSPprio = new DataTable();
                dtUSPprio = cls.GetData(qryprioUSP);

                string qryUpdatePrio = "";
                if (dtUSPprio.Rows.Count == 0)
                {
                    qryUpdatePrio = "Update [dbo].[tbl_UC_USP_ProcessUSP] Set Start_Process = '' Where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                    cls.ExecuteQuery(qryUpdatePrio);
                }
                else
                {
                    string qryCheck = "";
                    for (int i = 0; i < dtprio.Rows.Count; i++)
                    {
                        if (dtprio.Rows[i][2].ToString() == "Bulk Email" && dtUSPprio.Rows[0][1].ToString() == "Bulk Email")
                        {
                            qryCheck = "Select * from [dbo].[tbl_UC_USP_ProcessUSP] where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            DataTable dtCheckCount = new DataTable();
                            dtCheckCount = cls.GetData(qryCheck);

                            if(dtCheckCount.Rows.Count == 0)
                            {
                                qryUpdatePrio = @"INSERT INTO [dbo].[tbl_UC_USP_ProcessUSP] (USP_ID,Start_Process,isDeleted) VALUES ('" + dt.Rows[0][0].ToString() + @"','Bulk Email',0) ";
                            }
                            else
                            {
                                qryUpdatePrio = "Update [dbo].[tbl_UC_USP_ProcessUSP] Set Start_Process = 'Bulk Email' Where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            }
                            break;
                        }
                        else if (dtprio.Rows[i][2].ToString() == "Online Request" && dtUSPprio.Rows[0][1].ToString() == "Online Request")
                        {
                            qryCheck = "Select * from [dbo].[tbl_UC_USP_ProcessUSP] where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            DataTable dtCheckCount = new DataTable();
                            dtCheckCount = cls.GetData(qryCheck);

                            if (dtCheckCount.Rows.Count == 0)
                            {
                                qryUpdatePrio = "INSERT INTO [dbo].[tbl_UC_USP_ProcessUSP] (USP_ID,Start_Process,isDeleted) VALUES ('" + dt.Rows[0][0].ToString() + @"','Online Request',0) ";
                            }
                            else
                            {
                                qryUpdatePrio = "Update [dbo].[tbl_UC_USP_ProcessUSP] Set Start_Process = 'Online Request' Where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            }

                            break;
                        }
                        else if (dtprio.Rows[i][2].ToString() == "Call" && dtUSPprio.Rows[0][1].ToString() == "Call")
                        {
                            qryCheck = "Select * from [dbo].[tbl_UC_USP_ProcessUSP] where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            DataTable dtCheckCount = new DataTable();
                            dtCheckCount = cls.GetData(qryCheck);

                            if (dtCheckCount.Rows.Count == 0)
                            {
                                qryUpdatePrio = "INSERT INTO [dbo].[tbl_UC_USP_ProcessUSP] (USP_ID,Start_Process,isDeleted) VALUES ('" + dt.Rows[0][0].ToString() + @"','Call',0)";
                            }
                            else
                            {
                                qryUpdatePrio = "Update [dbo].[tbl_UC_USP_ProcessUSP] Set Start_Process = 'Call' Where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            }

                            break;
                        }
                        else if (dtprio.Rows[i][2].ToString() == "EDI 814" && dtUSPprio.Rows[0][1].ToString() == "EDI 814")
                        {
                            qryCheck = "Select * from [dbo].[tbl_UC_USP_ProcessUSP] where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            DataTable dtCheckCount = new DataTable();
                            dtCheckCount = cls.GetData(qryCheck);

                            if (dtCheckCount.Rows.Count == 0)
                            {
                                qryUpdatePrio = "INSERT INTO [dbo].[tbl_UC_USP_ProcessUSP] (USP_ID,Start_Process,isDeleted)  VALUES ('" + dt.Rows[0][0].ToString() + @"','EDI 814',0) ";
                            }
                            else
                            {
                                qryUpdatePrio = "Update [dbo].[tbl_UC_USP_ProcessUSP] Set Start_Process = 'EDI 814' Where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            }

                            break;
                        }
                        else
                        {

                        }
                    }
                    cls.ExecuteQuery(qryUpdatePrio);
                }
                





            }
            DataTable dtgetData = new DataTable();
            string getData = "Select * FROM tbl_UC_USP_Name_Vendor_ID where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
            dtgetData = cls.GetData(getData);

            arrPages.Add("USP Capabilities");
            arrPages.Add("Start Service");
            //Update USP :  USPName on Call ASPS to Yes/No 
            cls.FUNC.Audit("Update USP : " + dtgetData.Rows[0][1].ToString() + Change, arrPages);

            return "true";
            //clsConnection cls = new clsConnection();
            //string qry = " Select USP_Name, [Bulk Email] As 'bulk',[Online Request] As 'online', [EDI 814] as 'edi', [Call] FROM [dbo].[tbl_UC_USPcapabilities_Start_Service]";
            //DataTable dt = new DataTable();
            //dt = cls.GetData(qry);
            //return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string UPdateUSPstop(string val, string USP)
        {

            string[] arrVal = val.Split(',');
            string valstr = "";
            string Change = "";
            string YesNo = "";
            ArrayList arrPages = new ArrayList();
            if (arrVal[1] == "1")
            {
                valstr = "0";
                YesNo = "No";
            }
            else
            {
                valstr = "1";
                YesNo = "Yes";
            }

            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string selectqry = " Select * FROM tbl_UC_USP_Name_Vendor_ID Where replace(USP_Name,' ','') Like  replace('"
                + arrVal[0] + "%',' ','')  and isDeleted = 0";
            dt = cls.GetData(selectqry);

            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog=MIS_MLA_UAT;Integrated Security=True");
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            {
                if (USP == "Bulk")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Stop_Service SET " +
                   "[Bulk Email USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();

                    }
                    Change = " on Bulk Email USP to " + YesNo + "";
                }
                else if (USP == "Online")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Stop_Service SET " +
                   "[Online Request USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();

                    }
                    Change = " on Online Request USP to " + YesNo + "";
                }
                else if (USP == "EDI")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Stop_Service SET " +
                   "[EDI 814 USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();

                    }
                    Change = " on EDI 814 USP to " + YesNo + "";
                }
                else if (USP == "Call")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Stop_Service SET " +
                   "[Call USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);
                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();
                    }
                    Change = " on Call USP to " + YesNo + "";
                }
                else
                {

                }
                //ASPS
                if (USP == "BulkASPS")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Stop_Service SET " +
                   "[Bulk Email ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();

                    }
                    Change = " on Bulk Email ASPS to " + YesNo + "";
                }
                else if (USP == "OnlineASPS")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Stop_Service SET " +
                   "[Online Request ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();

                    }
                    Change = " on Online Request ASPS to " + YesNo + "";
                }
                else if (USP == "EDIASPS")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Stop_Service SET " +
                   "[EDI 814 ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();

                    }
                    Change = " on EDI 814 ASPS to " + YesNo + "";
                }
                else if (USP == "CallASPS")
                {
                    SqlCommand updateBulk = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Stop_Service SET " +
                   "[Call ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateBulk.ExecuteNonQuery();

                    }
                    Change = " on Call ASPS to " + YesNo + "";
                }
                else
                {

                }

                //12/15/2017
                string qryGetPrio = "SELECT * FROM [dbo].[tbl_UC_USP_StopService_PRIO] Order By Prio ";
                DataTable dtprio = new DataTable();
                dtprio = cls.GetData(qryGetPrio);

                string qryprioUSP = @"Select * from vw_UC_USP_StopPrio where USP_ID = '" + dt.Rows[0][0].ToString() + "' Order by Prio ";
                DataTable dtUSPprio = new DataTable();
                dtUSPprio = cls.GetData(qryprioUSP);

                string qryUpdatePrio = "";

                if (dtUSPprio.Rows.Count == 0)
                {
                    qryUpdatePrio = "Update [dbo].[tbl_UC_USP_ProcessUSP] Set Stop_Process = '' Where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                    cls.ExecuteQuery(qryUpdatePrio);
                }
                else{

                    string qryCheck = "";

                    for (int i = 0; i < dtprio.Rows.Count; i++)
                    {
                        if (dtprio.Rows[i][2].ToString() == "Bulk Email" && dtUSPprio.Rows[0][1].ToString() == "Bulk Email")
                        {

                            qryCheck = "Select * from [dbo].[tbl_UC_USP_ProcessUSP] where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            DataTable dtCheckCount = new DataTable();
                            dtCheckCount = cls.GetData(qryCheck);

                            if (dtCheckCount.Rows.Count == 0)
                            {
                                qryUpdatePrio = @"INSERT INTO [dbo].[tbl_UC_USP_ProcessUSP] (USP_ID,Stop_Process,isDeleted) VALUES ('" + dt.Rows[0][0].ToString() + @"','Bulk Email',0) ";
                            }
                            else
                            {
                                qryUpdatePrio = "Update [dbo].[tbl_UC_USP_ProcessUSP] Set Stop_Process = 'Bulk Email' Where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            }
                            break;
                        }
                        else if (dtprio.Rows[i][2].ToString() == "Online Request" && dtUSPprio.Rows[0][1].ToString() == "Online Request")
                        {
                            qryCheck = "Select * from [dbo].[tbl_UC_USP_ProcessUSP] where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            DataTable dtCheckCount = new DataTable();
                            dtCheckCount = cls.GetData(qryCheck);

                            if (dtCheckCount.Rows.Count == 0)
                            {
                                qryUpdatePrio = @"INSERT INTO [dbo].[tbl_UC_USP_ProcessUSP] (USP_ID,Stop_Process,isDeleted) VALUES ('"
                                    + dt.Rows[0][0].ToString() + @"','Online Request',0) ";
                            }
                            else
                            {
                                qryUpdatePrio = "Update [dbo].[tbl_UC_USP_ProcessUSP] Set Stop_Process = 'Online Request' Where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            }

                            break;
                        }
                        else if (dtprio.Rows[i][2].ToString() == "Call" && dtUSPprio.Rows[0][1].ToString() == "Call")
                        {
                            qryCheck = "Select * from [dbo].[tbl_UC_USP_ProcessUSP] where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            DataTable dtCheckCount = new DataTable();
                            dtCheckCount = cls.GetData(qryCheck);

                            if (dtCheckCount.Rows.Count == 0)
                            {
                                qryUpdatePrio = @"INSERT INTO [dbo].[tbl_UC_USP_ProcessUSP] (USP_ID,Stop_Process,isDeleted) VALUES ('"
                                    + dt.Rows[0][0].ToString() + @"','Call',0) ";
                            }
                            else
                            {
                                qryUpdatePrio = "Update [dbo].[tbl_UC_USP_ProcessUSP] Set Stop_Process = 'Call' Where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            }
                            break;
                        }
                        else if (dtprio.Rows[i][2].ToString() == "EDI 814" && dtUSPprio.Rows[0][1].ToString() == "EDI 814")
                        {
                            qryCheck = "Select * from [dbo].[tbl_UC_USP_ProcessUSP] where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            DataTable dtCheckCount = new DataTable();
                            dtCheckCount = cls.GetData(qryCheck);

                            if (dtCheckCount.Rows.Count == 0)
                            {
                                qryUpdatePrio = @"INSERT INTO [dbo].[tbl_UC_USP_ProcessUSP] (USP_ID,Stop_Process,isDeleted) VALUES ('"
                                    + dt.Rows[0][0].ToString() + @"','EDI 814',0) ";
                            }
                            else
                            {
                                qryUpdatePrio = "Update [dbo].[tbl_UC_USP_ProcessUSP] Set Stop_Process = 'EDI 814' Where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                            }
                            break;
                        }
                        else
                        {

                        }
                    }

                    cls.ExecuteQuery(qryUpdatePrio);
                }


                
            }
            DataTable dtgetData = new DataTable();
            string getData = "Select * FROM tbl_UC_USP_Name_Vendor_ID where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
            dtgetData = cls.GetData(getData);

            arrPages.Add("USP Capabilities");
            arrPages.Add("Stop Service");
            //Update USP :  USPName on Call ASPS to Yes/No 
            cls.FUNC.Audit("Update USP : " + dtgetData.Rows[0][1].ToString() + Change, arrPages);

            return "true";
            //clsConnection cls = new clsConnection();
            //string qry = " Select USP_Name, [Bulk Email] As 'bulk',[Online Request] As 'online', [EDI 814] as 'edi', [Call] FROM [dbo].[tbl_UC_USPcapabilities_Start_Service]";
            //DataTable dt = new DataTable();
            //dt = cls.GetData(qry);
            //return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string UPdateUSPinvoice(string val, string USP)
        {

            string[] arrVal = val.Split(',');
            string valstr = "";
            string Change = "";
            string YesNo = "";
            ArrayList arrPages = new ArrayList();
            if (arrVal[1] == "1")
            {
                valstr = "0";
                YesNo = "No";
            }
            else
            {
                valstr = "1";
                YesNo = "Yes";
            }

            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string selectqry = " Select * FROM tbl_UC_USP_Name_Vendor_ID Where replace(USP_Name,' ','') Like  replace('"
                + arrVal[0] + "%',' ','')  and isDeleted = 0";
            dt = cls.GetData(selectqry);

            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog=MIS_MLA_UAT;Integrated Security=True");
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            {
                if (USP == "Summary")
                {
                    SqlCommand updateSummary = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[Summary Billing USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateSummary.ExecuteNonQuery();

                    }
                    Change = " on Summary Billing USP to " + YesNo + "";
                }
                else if (USP == "Password1")
                {
                    SqlCommand updatePassword1 = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[E-Bill w/ Password USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updatePassword1.ExecuteNonQuery();

                    }
                    Change = " on E-Bill w/ Password USP to " + YesNo + "";
                }
                else if (USP == "Password2")
                {
                    SqlCommand updatePassword2 = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[E-Bill w/o Password USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updatePassword2.ExecuteNonQuery();

                    }
                    Change = " on E-Bill w/o Password USP to " + YesNo + "";
                }
                else if (USP == "Link")
                {
                    SqlCommand updateLink = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[E-Bill Via Link USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateLink.ExecuteNonQuery();

                    }
                    Change = " on E-Bill Via Link USP to " + YesNo + "";
                }
                else if (USP == "EDI")
                {
                    SqlCommand updateEDI = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[EDI 810 USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateEDI.ExecuteNonQuery();
                    }
                    Change = " on EDI 810 USP to " + YesNo + "";
                }
                else if (USP == "Bill")
                {
                    SqlCommand updateBill = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[Paper Bill USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);
                    {
                        con.Open();
                        updateBill.ExecuteNonQuery();
                    }
                    Change = " on Paper Bill USP to " + YesNo + "";
                }
                else
                {

                }



                //ASPS
                if (USP == "SummaryASPS")
                {
                    SqlCommand updateSummary = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[Summary Billing ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);
                    {
                        con.Open();
                        updateSummary.ExecuteNonQuery();

                    }
                    Change = " on Summary Billing ASPS to " + YesNo + "";
                }
                else if (USP == "Password1ASPS")
                {
                    SqlCommand updatePassword1 = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[E-Bill w/ Password ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updatePassword1.ExecuteNonQuery();

                    }
                    Change = " on E-Bill w/ Password ASPS to " + YesNo + "";
                }
                else if (USP == "Password2ASPS")
                {
                    SqlCommand updatePassword2 = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[E-Bill w/o Password ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updatePassword2.ExecuteNonQuery();

                    }
                    Change = " on E-Bill w/o Password ASPS to " + YesNo + "";
                }
                else if (USP == "LinkASPS")
                {
                    SqlCommand updateLink = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[E-Bill Via Link ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateLink.ExecuteNonQuery();

                    }
                    Change = " on E-Bill Via Link ASPS to " + YesNo + "";
                }
                else if (USP == "EDIASPS")
                {
                    SqlCommand updateEDI = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[EDI 810 ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateEDI.ExecuteNonQuery();
                    }
                    Change = " on EDI 810 ASPS to " + YesNo + "";
                }
                else if (USP == "BillASPS")
                {
                    SqlCommand updateBill = new SqlCommand("UPDATE tbl_UC_USPcapabilities_Invoicing SET " +
                   "[Paper Bill ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);
                    {
                        con.Open();
                        updateBill.ExecuteNonQuery();
                    }
                    Change = " on Paper Bill ASPS to " + YesNo + "";
                }
                else
                {

                }
            }


            DataTable dtgetData = new DataTable();
            string getData = "Select * FROM tbl_UC_USP_Name_Vendor_ID where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
            dtgetData = cls.GetData(getData);

            arrPages.Add("USP Capabilities");
            arrPages.Add("Invoicing");
            //Update USP :  USPName on Call ASPS to Yes/No 
            cls.FUNC.Audit("Update USP : " + dtgetData.Rows[0][1].ToString() + Change, arrPages);

            return "true";
            //clsConnection cls = new clsConnection();
            //string qry = " Select USP_Name, [Bulk Email] As 'bulk',[Online Request] As 'online', [EDI 814] as 'edi', [Call] FROM [dbo].[tbl_UC_USPcapabilities_Start_Service]";
            //DataTable dt = new DataTable();
            //dt = cls.GetData(qry);
            //return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string UPdateUSPBills(string val, string USP)
        {

            string[] arrVal = val.Split(',');
            string valstr = "";
            string Change = "";
            string YesNo = "";
            ArrayList arrPages = new ArrayList();
            if (arrVal[1] == "1")
            {
                valstr = "0";
                YesNo = "No";
            }
            else
            {
                valstr = "1";
                YesNo = "Yes";
            }


            //Last Update
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string selectqry = " Select * FROM tbl_UC_USP_Name_Vendor_ID Where replace(USP_Name,' ','') Like  replace('"
                + arrVal[0] + "%',' ','')  and isDeleted = 0";
            dt = cls.GetData(selectqry);

            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog=MIS_MLA_UAT;Integrated Security=True");
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            {
                if (USP == "ACH")
                {
                    SqlCommand updateACH = new SqlCommand("UPDATE tbl_UC_USPcapabilities_BillsPayment SET " +
                   " [ACH USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);
                    {
                        con.Open();
                        updateACH.ExecuteNonQuery();

                    }
                    Change = " on ACH USP to " + YesNo + "";
                }
                else if (USP == "Wire")
                {
                    SqlCommand updateWire = new SqlCommand("UPDATE tbl_UC_USPcapabilities_BillsPayment SET " +
                   " [Wire Transfer USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateWire.ExecuteNonQuery();

                    }
                    Change = " on Wire Transfer USP to " + YesNo + "";
                }
                else if (USP == "Credit")
                {
                    SqlCommand updateCredit = new SqlCommand("UPDATE tbl_UC_USPcapabilities_BillsPayment SET " +
                   "[Credit Card USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);
                    string checkqry = "UPDATE tbl_UC_USPcapabilities_BillsPayment SET " +
                   "[Credit Card USP] = '" + valstr + "',[NTID] = '" + HttpContext.Current.Session["ntid"] + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'";
                    {
                        con.Open();
                        updateCredit.ExecuteNonQuery();

                    }
                    Change = " on Credit Card USP to " + YesNo + "";
                }
                else if (USP == "Check")
                {
                    SqlCommand updateCheck = new SqlCommand("UPDATE tbl_UC_USPcapabilities_BillsPayment SET " +
                   "[Check USP] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateCheck.ExecuteNonQuery();

                    }
                    Change = " on Check USP to " + YesNo + "";
                }
                else
                {

                }


                //ASPS
                if (USP == "ACHASPS")
                {
                    SqlCommand updateACH = new SqlCommand("UPDATE tbl_UC_USPcapabilities_BillsPayment SET " +
                   "[ACH ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateACH.ExecuteNonQuery();

                    }
                    Change = " on ACH ASPS to " + YesNo + "";
                }
                else if (USP == "WireASPS")
                {
                    SqlCommand updateWire = new SqlCommand("UPDATE tbl_UC_USPcapabilities_BillsPayment SET " +
                   "[Wire Transfer ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateWire.ExecuteNonQuery();

                    }
                    Change = " on Wire Transfer ASPS to " + YesNo + "";
                }
                else if (USP == "CreditASPS")
                {
                    SqlCommand updateCredit = new SqlCommand("UPDATE tbl_UC_USPcapabilities_BillsPayment SET " +
                   "[Credit Card ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateCredit.ExecuteNonQuery();

                    }
                    Change = " on Credit Card ASPS to " + YesNo + "";
                }
                else if (USP == "CheckASPS")
                {
                    SqlCommand updateCheck = new SqlCommand("UPDATE tbl_UC_USPcapabilities_BillsPayment SET " +
                   "[Check ASPS] = '" + valstr + "',[NTID] = '"
                   + HttpContext.Current.Session["ntid"] + "',[Last_Modified] = '"
                   + DateTime.Now.ToString() + "' WHERE USP_ID = '" + dt.Rows[0][0].ToString() + "'", con);

                    {
                        con.Open();
                        updateCheck.ExecuteNonQuery();

                    }
                    Change = " on Check ASPS to " + YesNo + "";
                }
                else
                {

                }
            }
            DataTable dtgetData = new DataTable();
            string getData = "Select * FROM tbl_UC_USP_Name_Vendor_ID where USP_ID = '" + dt.Rows[0][0].ToString() + "'";
            dtgetData = cls.GetData(getData);

            arrPages.Add("USP Capabilities");
            arrPages.Add("BillsPayment");
            //Update USP :  USPName on Call ASPS to Yes/No 
            cls.FUNC.Audit("Update USP : " + dtgetData.Rows[0][1].ToString() + Change, arrPages);
            return "true";
        }



    }
}
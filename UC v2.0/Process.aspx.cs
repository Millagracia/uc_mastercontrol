﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class Process : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetList(int val)
        {
            clsConnection cls = new clsConnection();
            DataTable dtList = new DataTable();


            if(val == 1)
            {
                string query = "SELECT  Task, USP, [Property ID] as PropertyID, Utility, Address, Status, VendorID," +
                " AccountNo, NoOfUnits, Category FROM vw_UC_ActDeactInv WHERE VendorID IN(SELECT Vendor_ID FROM vw_UC_Capabilities_StartService WHERE [Bulk Email]=1)" +
                " AND Status IN('Research In Progress','Out Of REO','','NULL')" +
                " order by Status";
                dtList = cls.GetData(query);
            }
            else if (val ==2)
            {
                string query = "SELECT  Task, USP, [Property ID] as PropertyID, Utility, Address, Status, VendorID," +
                " AccountNo, NoOfUnits, Category FROM vw_UC_ActDeactInv WHERE VendorID IN(SELECT Vendor_ID FROM vw_UC_Capabilities_StartService WHERE [Bulk Email]=1)" +
                " order by Status";
                dtList = cls.GetData(query);
            }
            else
            {
                string query = "SELECT 'Start Service' AS Task, USPNormalizedName AS USP, VendorId, [Property ID] AS PropertyCode, Utility, Address, Status, DateUploaded, AgentNTID, OnlineFlag," +
                   " Updateflag, Skip" +
                   " FROM         dbo.view_1UC_REO_Activation" +
                   " WHERE     Status IN ('Research In Progress', '', 'NULL','Out of REO') AND " +
                   " VendorId IN(SELECT Vendor_ID FROM vw_UC_Capabilities_StartService WHERE [Online Request]=1) AND USPNormalizedName<>''" +
                   " UNION ALL" +
                   " SELECT     'Stop Service' AS Task, USPNormalizedName, VendorId, [Property ID], Utility, Address, Status, DateUploaded, AgentNTID, OnlineFlag, UpdateFlag, Skip" +
                   " FROM         vw_UC_Deactivation" +
                   " WHERE     Status IN ('Research In Progress', '', 'NULL','Out of REO')  AND" +
                   " VendorId IN(SELECT Vendor_ID FROM vw_UC_Capabilities_StartService WHERE [Online Request]=1) AND USPNormalizedName<>''" ;
                dtList = cls.GetData(query);
            }
            

            

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtList } });
        }
    }
}
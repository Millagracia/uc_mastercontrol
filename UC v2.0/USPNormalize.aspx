﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="USPNormalize.aspx.cs" Inherits="UC_v2._0.USPNormalize" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-3">
            </div>
            <div class="col-lg-5">
                <br />
                <br />
                <br />
                <br />
                <br />
                <div class="box box-success">
                    <br />
                    <br />
                    <asp:FileUpload ID="fileUpload" ClientIDMode="Static" runat="server" />
                    <br />
                    <br />
                    <button type="button" class="btn btn-primary form-control" id="btnUpload"><i class="fa fa-upload" aria-hidden="true"></i>Upload</button>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/UploadExcel.js"></script>
</asp:Content>

﻿using Master_Control;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class Usersettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static List<AccessRole> loadAccess(string type)
        {
            List<AccessRole> ls = new List<AccessRole>();
            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where isActive = 1 and type='" + type + "' and tag = 'UC';");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AccessRole ar = new AccessRole();
                ar.id = dt.Rows[i][0].ToString();
                ar.name = dt.Rows[i][1].ToString();
                ls.Add(ar);
            }

            return ls;
        }

        [WebMethod]
        public static List<AccessFunc> loadFunction(string id, string type)
        {
            List<AccessFunc> ls = new List<AccessFunc>();
            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();

            string qry = "Select * from tbl_VPR_Access_Role_Type_Function where tag = 'UC' and role_id =" + id;
            dt = cls.GetData(qry);
            if (dt.Rows.Count > 0)
            {
                string avl = dt.Rows[0][2].ToString();

                //current function
                qry = "Select * from tbl_VPR_Access_Role_Function where tag = 'UC' and id in (" + avl + ")";
                dt = cls.GetData(qry);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Console.WriteLine(dt.Rows[i][1].ToString());
                    AccessFunc ar = new AccessFunc();
                    ar.id = dt.Rows[i][0].ToString();
                    ar.name = dt.Rows[i][1].ToString();
                    ar.isOn = "True";
                    ls.Add(ar);
                }
                //Available function
                Console.WriteLine("\n");
                qry = "Select * from tbl_VPR_Access_Role_Function where tag = 'UC' and id not in (" + avl + ") and functype='" + type + "'";
                dt = cls.GetData(qry);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AccessFunc ar = new AccessFunc();
                    ar.id = dt.Rows[i][0].ToString();
                    ar.name = dt.Rows[i][1].ToString();
                    ar.isOn = "False";
                    ls.Add(ar);
                }
            }
            else
            {
                //if null
                qry = "Select * from tbl_VPR_Access_Role_Function where functype='" + type + "' and tag = 'UC'";
                dt = cls.GetData(qry);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AccessFunc ar = new AccessFunc();
                    ar.id = dt.Rows[i][0].ToString();
                    ar.name = dt.Rows[i][1].ToString();
                    ar.isOn = "False";
                    ls.Add(ar);
                }
            }


            return ls;
        }


        [WebMethod]
        public static AccessRole loadLineAccess(string id)
        {

            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where tag = 'UC' and id=" + id);
            AccessRole ar = new AccessRole();
            ar.id = dt.Rows[0][0].ToString();
            ar.name = dt.Rows[0][1].ToString();
            ar.desc = dt.Rows[0][2].ToString();
            return ar;
        }

        [WebMethod]
        public static void addAccess(string name, string desc, string type)
        {
            clsConnection cls = new clsConnection();
            cls.ExecuteQuery("INSERT INTO tbl_VPR_Access_Role_Type(accessname,accessdesc,type,isActive, tag) Values ('" + name + "', '" + desc + "','" + type + "',1, 'UC')");
            //cls.ExecuteQuery("INSERT INTO tbl_VPR_Access_Role_Function(role_id,AI,QM,CH,MC,RE) Values ((select MAX(ID) from tbl_VPR_Access_Role_Type) , 0,0,0,0,0)");
            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("Access");
            cls.FUNC.Audit("Create New Role/Skill: " + name, arrPages);
        }
        [WebMethod]
        public static string editAccess(string id, string name, string desc)
        {
            clsConnection cls = new clsConnection();
            cls.ExecuteQuery("Update tbl_VPR_Access_Role_Type set accessname = '" + name + "', accessdesc='" + desc + "' where id=" + id);
            DataTable dt = new DataTable();
            dt = cls.GetData("Select type from tbl_VPR_Access_Role_Type where id =" + id);

            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("Access");

            cls.FUNC.Audit("Update Role/Skill: " + name, arrPages);


            return dt.Rows[0][0].ToString();


        }
        [WebMethod]
        public static string deleteLine(string id)
        {
            clsConnection cls = new clsConnection();
            cls.ExecuteQuery("Update tbl_VPR_Access_Role_Type set isActive = 0 where id=" + id);
            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("Access");
            DataTable dt = new DataTable();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where tag = 'UC' and id =" + id);

            cls.FUNC.Audit("Deleted Role/Skill: " + dt.Rows[0][1].ToString(), arrPages);
            return dt.Rows[0][3].ToString();

        }
        [WebMethod]
        public static void updateFunction(string id, string newStr)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            dt = cls.GetData("Select * from tbl_VPR_Access_Role_Type_Function where tag = 'UC' and role_id =" + id);
            if (dt.Rows.Count > 0)
            {
                cls.ExecuteQuery("Update tbl_VPR_Access_Role_Type_Function set function_on = '" + newStr + "' where role_id=" + id);
            }
            else
            {
                cls.ExecuteQuery("INSERT INTO tbl_VPR_Access_Role_Type_Function(role_id,function_on, tag) VALUES (" + id + ",'" + newStr + "', 'UC')");
            }

            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("Access");

            DataTable dtRole = new DataTable();
            dtRole = cls.GetData("Select * from tbl_VPR_Access_Role_Type_Function where tag = 'UC' and role_id =" + id);

            cls.FUNC.Audit("Updated Role/Skill Function: " + dtRole.Rows[0][1].ToString(), arrPages);

        }
        [WebMethod]
        public static UserList userList()
        {
            clsConnection cls = new clsConnection();
            UserList ul = new UserList();
            List<UserDetails> lud = new List<UserDetails>();
            DataTable dt = new DataTable();
            string qry = "";
            qry += "select a.id [id],b.empname [empname],b.emplevel [emplevel], ";
            qry += "case when role_id is null then '-' else (Select accessname from tbl_VPR_Access_Role_Type where id = role_id and tag = 'UC') end [role_id], ";
            qry += "case when skill_id is null then '-' else (Select accessname from tbl_VPR_Access_Role_Type where id = skill_id and tag = 'UC') end [skill_id], ";
            qry += "MngrName [immsupp]";
            qry += "from tbl_UC_Access_User a ";
            qry += "inner join tbl_HRMS_EmployeeMaster b on a.ntid = b.NTID ";
            qry += "order by empname asc";

            dt = cls.GetData(qry);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserDetails ud = new UserDetails();
                    ud.id = dt.Rows[i][0].ToString();
                    ud.empname = dt.Rows[i][1].ToString();
                    ud.emplevel = dt.Rows[i][2].ToString();
                    ud.role_id = dt.Rows[i][3].ToString();
                    ud.skill_id = dt.Rows[i][4].ToString();
                    ud.immsupp = dt.Rows[i][5].ToString();
                    lud.Add(ud);
                }
                ul.tblUserList = lud;

            }
            else
            {

            }

            return ul;
        }

        [WebMethod]
        public static UserList immediateSuperior()
        {
            clsConnection cls = new clsConnection();
            UserList ul = new UserList();
            List<UserDetails> lud = new List<UserDetails>();
            DataTable dt = new DataTable();
            string qry = "";
            qry += "select a.id [id],b.empname [empname] ";
            qry += "from tbl_UC_Access_User a ";
            qry += "inner join tbl_HRMS_EmployeeMaster b on a.ntid = b.NTID AND b.emplevel <> 'EE' ";
            qry += "order by empname asc";

            dt = cls.GetData(qry);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserDetails ud = new UserDetails();
                    ud.id = dt.Rows[i][0].ToString();
                    ud.empname = dt.Rows[i][1].ToString();

                    lud.Add(ud);
                }
                ul.tblUserList = lud;

            }
            else
            {

            }

            return ul;
        }
        [WebMethod]
        public static UserList filterLIST(string immediate, string userid, string username)
        {
            clsConnection cls = new clsConnection();
            UserList ul = new UserList();
            List<UserDetails> lud = new List<UserDetails>();
            DataTable dt = new DataTable();
            string qry = "";
            qry += "select a.id [id],b.empname [empname],b.emplevel [emplevel], ";
            qry += "case when role_id is null then '-' else (Select accessname from tbl_VPR_Access_Role_Type where id = role_id and tag = 'UC') end [role_id], ";
            qry += "case when skill_id is null then '-' else (Select accessname from tbl_VPR_Access_Role_Type where id = skill_id and tag = 'UC') end [skill_id], ";
            qry += "MngrName [immsupp]";
            qry += "from tbl_UC_Access_User a ";
            qry += "inner join tbl_HRMS_EmployeeMaster b on a.ntid = b.NTID ";
            if (username == "")
            {
                if (immediate != "Select All" || userid != "0")
                {
                    qry += "WHERE ";
                    if (immediate != "Select All")
                    {
                        qry += "MngrName = '" + immediate + "' ";
                    }
                    if (userid != "0")
                    {
                        qry += " AND a.id = '" + userid + "' ";
                    }
                }
            }
            else
            {
                qry += "WHERE b.empname LIKE '%" + username + "%' ";
            }
            qry += "order by empname asc";
            dt = cls.GetData(qry);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserDetails ud = new UserDetails();
                    ud.id = dt.Rows[i][0].ToString();
                    ud.empname = dt.Rows[i][1].ToString();
                    ud.emplevel = dt.Rows[i][2].ToString();
                    ud.role_id = dt.Rows[i][3].ToString();
                    ud.skill_id = dt.Rows[i][4].ToString();
                    ud.immsupp = dt.Rows[i][5].ToString();
                    lud.Add(ud);
                }
                ul.tblUserList = lud;

            }
            else
            {

            }

            return ul;
        }
        [WebMethod]
        public static List<RoleSkill> popRS()
        {
            clsConnection cls = new clsConnection();
            List<RoleSkill> lrs = new List<RoleSkill>();
            DataTable dt = new DataTable();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where isActive = 1 and tag = 'UC'");
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RoleSkill rs = new RoleSkill();
                    rs.id = dt.Rows[i][0].ToString();
                    rs.rsname = dt.Rows[i][1].ToString();
                    rs.rstype = dt.Rows[i][3].ToString();

                    lrs.Add(rs);
                }
            }

            return lrs;
        }

        [WebMethod]
        public static void applyAction(string role_id, string skill_id, string emp_id)
        {
            clsConnection cls = new clsConnection();
            cls.ExecuteQuery("Update tbl_UC_Access_User set role_id =" + role_id + ", skill_id=" + skill_id + " where id in (" + emp_id + ")");
            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("User");
            DataTable dt = new DataTable();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where tag = 'UC' and id =" + role_id);
            string role = dt.Rows[0][1].ToString();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where  tag = 'UC' and id =" + skill_id);
            string skill = dt.Rows[0][1].ToString();
            cls.FUNC.Audit("Applied Role: " + role + " and Skill: " + skill + " to certain users", arrPages);
        }


        [WebMethod]
        public static string GetEmp()
        {
            clsConnection cls = new clsConnection();


            string qry = "";
            qry += "select a.id [id],b.empname [empname],b.emplevel [emplevel], ";
            qry += "case when role_id is null then '-' else (Select accessname from tbl_VPR_Access_Role_Type where id = role_id and tag = 'UC') end [role_id], ";
            qry += "case when skill_id is null then '-' else (Select accessname from tbl_VPR_Access_Role_Type where id = skill_id and tag = 'UC') end [skill_id], ";
            qry += "MngrName [immsupp]";
            qry += "from tbl_UC_Access_User a ";
            qry += "inner join tbl_HRMS_EmployeeMaster b on a.ntid = b.NTID ";
            qry += "order by empname asc";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }

    }
}
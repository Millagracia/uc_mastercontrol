﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class DataList : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void openThis(object sender, EventArgs e)
        {
            Response.Redirect("#");

        }




        [WebMethod]
        public static string GetSched()
        {
            clsConnection cls = new clsConnection();

            string qry = "select * from tbl_Client;";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetServiceLoc(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();


            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;
            }

            clsConnection cls = new clsConnection();


            string qry = "select * from tbl_UC_Service_Location where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });

        }
        [WebMethod]
        public static string GetClientStatus(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();


            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;
            }


            clsConnection cls = new clsConnection();


            string qry = "select * from tbl_UC_Client_Status where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });

        }
        [WebMethod]
        public static string GetPODS(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();


            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;
            }

            clsConnection cls = new clsConnection();


            string qry = "select * from tbl_UC_PODS where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });

        }
        [WebMethod]
        public static string GetStateCityZip(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();


            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;
            }

            clsConnection cls = new clsConnection();


            string qry = "select * from tbl_UC_State_City_Zip where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });

        }
        [WebMethod]
        public static string GetPropertyType(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();


            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;
            }

            clsConnection cls = new clsConnection();


            string qry = "select * from tbl_UC_Property_Type where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });

        }
        [WebMethod]
        public static string GetOccupancy(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();


            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;
            }

            clsConnection cls = new clsConnection();


            string qry = "select * from tbl_UC_Occupancy_Status where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });

        }
        [WebMethod]
        public static string GetPropertyStatus(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();

            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;
            }

            clsConnection cls = new clsConnection();

            string qry = "select * from tbl_UC_Property_Status where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });

        }
        [WebMethod]
        public static string GetPreservation(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();

            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;
            }

            clsConnection cls = new clsConnection();

            string qry = "select * from tbl_UC_Preservation_Work_Type where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });

        }
        [WebMethod]
        public static string getUSPNameAndVendor(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();


            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;
            }

            clsConnection cls = new clsConnection();


            string qry = "select * from tbl_UC_USP_Name_Vendor_ID_Test where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });

        }
        [WebMethod]
        public static string GetUtilityStat(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();

            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;
            }

            clsConnection cls = new clsConnection();

            string qry = "select * from tbl_UC_Utility_Status where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });

        }
        [WebMethod]
        public static string GetWorkOrderType(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();

            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;
            }

            clsConnection cls = new clsConnection();

            string qry = "select * from tbl_UC_Work_Order_Type where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });

        }
        [WebMethod]
        public static string GetWorkOrderStatus(string Client)
        {
            int val;
            string str = "";
            SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            openCon.Open();

            SqlCommand command = new SqlCommand("Select ID_Client From tbl_Client Where Client_Name = '" + Client + "'", openCon);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                str = reader["ID_Client"].ToString();
                HttpContext.Current.Session["ID"] = str;


            }

            clsConnection cls = new clsConnection();

            string qry = "select * from tbl_UC_Work_Order_Status where ID_Client = '" + str + "'";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });




        }


        //INSERT 
        [WebMethod(EnableSession = true)]
        public static string InsertMethod(string Client)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into Tbl_Client values('" + Client + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public static string SaveService(string Name, string Description)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_Service_Location (ID_Client, Service_Name, Service_Desc) values('"
                    + HttpContext.Current.Session["ID"] + "','" + Name + "','" + Description + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public static string ClientStatus(string Name, string Description)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_Client_Status (ID_Client, Client_Name, Client_Desc) values('"
                    + HttpContext.Current.Session["ID"] + "','" + Name + "','" + Description + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }



            }
        }
        [WebMethod(EnableSession = true)]
        public static string PODS(string Name, string Description)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_PODS (ID_Client, POD_Name, POD_Desc) values('"
                    + HttpContext.Current.Session["ID"] + "','" + Name + "','" + Description + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }



            }
        }
        [WebMethod(EnableSession = true)]
        public static string StateCityZip(string State, string City, string Zip)
        {
            if (HttpContext.Current.Session["ID"] == null)
            {

            }
            else
            {
                //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
                SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
                {
                    SqlCommand cmd = new SqlCommand("Insert into tbl_UC_State_City_Zip (ID_Client, State, City, Zip) values('"
                        + HttpContext.Current.Session["ID"] + "','" + State + "','" + City + "','" + Zip + "')", con);
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();

                    }



                }
            }


            return "True";

        }
        [WebMethod(EnableSession = true)]
        public static string PropertyType(string Name, string Description)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_Property_Type (ID_Client, Pro_Name, Pro_Desc) values('"
                    + HttpContext.Current.Session["ID"] + "','" + Name + "','" + Description + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }



            }
        }
        [WebMethod(EnableSession = true)]
        public static string Occupancy(string Name, string Description)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_Occupancy_Status (ID_Client, Occu_Name, Occu_Desc) values('"
                    + HttpContext.Current.Session["ID"] + "','" + Name + "','" + Description + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }



            }
        }
        [WebMethod(EnableSession = true)]
        public static string PropertyStatus(string Name, string Description)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_Property_Status (ID_Client, Property_Name, Property_Desc) values('"
                    + HttpContext.Current.Session["ID"] + "','" + Name + "','" + Description + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }



            }
        }
        [WebMethod(EnableSession = true)]
        public static string PreservationWork(string Name, string Description)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_Preservation_Work_Type (ID_Client, Work_Name, Work_Desc) values('"
                    + HttpContext.Current.Session["ID"] + "','" + Name + "','" + Description + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }



            }
        }
        [WebMethod(EnableSession = true)]
        public static string USPName(string Name, string Vendor, string Phone, string Email, string address)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_USP_Name_Vendor_ID_Test (ID_Client, USP_Name, USP_VendorID, USP_PhoneNo, USP_Email, USP_Address) values('"
                    + HttpContext.Current.Session["ID"] + "','" + Name + "','" + Vendor + "','" + Phone + "','" + Email + "','" + address + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }



            }
        }
        [WebMethod(EnableSession = true)]
        public static string UtilityStatus(string Name, string Description)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_Utility_Status (ID_Client, Util_Name, Util_Desc) values('"
                    + HttpContext.Current.Session["ID"] + "','" + Name + "','" + Description + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }



            }
        }
        [WebMethod(EnableSession = true)]
        public static string WorkOrderType(string Name, string Description)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_Work_Order_Type (ID_Client, Work_Name, Work_Desc) values('"
                    + HttpContext.Current.Session["ID"] + "','" + Name + "','" + Description + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }



            }
        }
        [WebMethod(EnableSession = true)]
        public static string WorkOrderStatus(string Name, string Description)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_Work_Order_Status (ID_Client, wStatus_Name, wStatus_Desc) values('"
                    + HttpContext.Current.Session["ID"] + "','" + Name + "','" + Description + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }





            }
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class DashBoardUC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            //string qry = "Select EmpLevel FROm tbl_HRMS_EmployeeMaster where NTID = '" + Session["ntid"].ToString() + "'";
            //dt = cls.GetData(qry);

            //if (dt.Rows[0][0].ToString() == "SR. MGR" || dt.Rows[0][0].ToString() == "MGR" || dt.Rows[0][0].ToString() == "MANAGER" || dt.Rows[0][0].ToString() == "AM"
            //    || dt.Rows[0][0].ToString() == "DIR" || dt.Rows[0][0].ToString() == "VP" || dt.Rows[0][0].ToString() == "TL")
            //{

            //}
            //else if (Session["ntid"].ToString() == "solimanj")
            //{

            //}
            //else
            //{
            //    Response.Redirect("Usersettings.aspx");
            //}
        }
        [WebMethod]
        public static string GetDashBoard()
        {
            clsConnection cls = new clsConnection();
            //string connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";
            string connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";

            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            //using (SqlConnection cnn = new SqlConnection(connetionString))
            //{
            //string qry = "Select * FROM vw_UC_DashBoard ";
            //string qry2 = "Select replace(convert(NVARCHAR, GETDATE(), 106), ' ', '-')  as DateNow,SUM([Workable Count]) as 'sum' FROM vw_UC_DashBoard ";
            //    cnn.Open();
            //    SqlCommand command = new SqlCommand(qry, cnn);
            //    SqlCommand command2 = new SqlCommand(qry2, cnn);
            //    using (SqlDataReader reader = command.ExecuteReader())
            //    {
            //        dt.Load(reader);

            //    }
            //    using (SqlDataReader reader2 = command2.ExecuteReader())
            //    {
            //        dt2.Load(reader2);

            //    }
            //    cnn.Close();
            //}
            string qry = "Select Solution,isNull([Workable Count],'0') as 'WorkableCount',isNull([Workable Count Left],'0') as "+
                " 'WorkableCountLeft',isNull([Start Service],'0') as 'StartService',isNull([Start Service Age],'0') as 'StartServiceAge', "+
                " isNull([Stop Service],'0') as 'StopService',isNull([Stop Service Age],'0') as 'StopServiceAge' FROM vw_UC_DashBoard";
            string qry2 = "Select replace(convert(NVARCHAR, GETDATE(), 106), ' ', '-')  as DateNow,SUM([Workable Count Left]) as 'sum' FROM vw_UC_DashBoard ";
            dt = cls.GetData(qry);
            dt2 = cls.GetData(qry2);


            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd2 = dt2 } });
        }
        [WebMethod]
        public static string GetTotal()
        {
            clsConnection cls = new clsConnection();
            string connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";
            DataTable dt = new DataTable();
            //using (SqlConnection cnn = new SqlConnection(connetionString))
            //{
            //    string qry = "Select SUM([Workable Count]) FROM vw_UC_DashBoard ";

            //    cnn.Open();
            //    SqlCommand command = new SqlCommand(qry, cnn);

            //    using (SqlDataReader reader = command.ExecuteReader())
            //    {
            //        dt.Load(reader);

            //    }
            //    cnn.Close();
            //}
            string qry = "Select SUM([Workable Count Left]) FROM vw_UC_DashBoard ";
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetProcess(string process)
        {
            clsConnection cls = new clsConnection();
            string qry = "";
            if(process == "Bulk Email")
            {
                 //qry = "Select * FROM vw_UC_DashBoard_Bulk";
                qry = @"Select *,'Pending' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 0 AND Process = 'Bulk Email' AND [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Elec') And Utility ='Elec'
                Union All
                Select *,'Pending' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 0 AND Process = 'Bulk Email' AND [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Gas') And Utility ='Gas'
                Union All
                Select *,'Pending' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 0 AND Process = 'Bulk Email' AND [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Water') And Utility ='Water'
                Union All
                Select *,'Done' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 1  AND Process = 'Bulk Email' AND Not [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Elec') And Utility ='Elec'
                Union All
                Select *,'Done' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 1 AND Process = 'Bulk Email' AND Not [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Gas') And Utility ='Gas'
                Union All
                 Select *,'Done' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 1 AND Process = 'Bulk Email' AND Not [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Water') And Utility ='Water'";
            
            }
            else if(process == "Online Request")
            {
                 //qry = "Select * FROM vw_UC_DashBoard_OnlineRequest";
                qry = "Select *,'Pending' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 0 AND Process = 'Online Request' AND [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Elec') And Utility ='Elec'";
                qry += " Union All";
                qry += " Select *,'Pending' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 0 AND Process = 'Online Request' AND [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Gas') And Utility ='Gas'";
                qry += " Union All";
                qry += " Select *,'Pending' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 0 AND Process = 'Online Request' AND [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Water') And Utility ='Water'";
                qry += " Union All";
                qry += " Select *,'Done' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 1  AND Process = 'Online Request' AND Not [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Elec') And Utility ='Elec'";
                qry += " Union All";
                qry += " Select *,'Done' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 1 AND Process = 'Online Request' AND Not [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Gas') And Utility ='Gas'";
                qry += " Union All";
                qry += " Select *,'Done' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 1 AND Process = 'Online Request' AND Not [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Water') And Utility ='Water'";
            
            }
            else if(process == "Callout")
            {
                 //qry = "Select * FROM vw_UC_DashBoard_Call";
               // qry = "Select * FROM tbl_UC_USP_ReportsImpo where Process = 'Call'";
                qry = @"Select *,'Pending' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 0 AND Process = 'Call' AND [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Elec') And Utility ='Elec'
                Union All
                Select *,'Pending' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 0 AND Process = 'Call' AND [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Gas') And Utility ='Gas'
              Union All
               Select *,'Pending' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 0 AND Process = 'Call' AND [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Water') And Utility ='Water'
               Union All
                 Select *,'Done' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 1  AND Process = 'Call' AND Not [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Elec') And Utility ='Elec'
                 Union All
                Select *,'Done' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 1 AND Process = 'Call' AND Not [Property ID] IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Gas') And Utility ='Gas'
                 Union All
                 Select *,'Done' as 'Workable' FROm tbl_UC_USP_ReportsImpo Where FirstRun = 1 AND Process = 'Call' AND Not [Property ID] 
                IN (Select [Property ID] FROm tbl_UC_USP_ReportsImpo Where FirstRun =0 And Utility = 'Water') And Utility ='Water'";
            
            }
            else
            {

            }

            //string connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";
            //DataTable dt = new DataTable();
            //using (SqlConnection cnn = new SqlConnection(connetionString))
            //{
            //    cnn.Open();
            //    SqlCommand command = new SqlCommand(qry, cnn);

            //    using (SqlDataReader reader = command.ExecuteReader())
            //    {
            //        dt.Load(reader);

            //    }
            //    cnn.Close();
            //}
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetHistorical(string Date, string Process)
        {
            DateTime d = DateTime.ParseExact(Date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            clsConnection cls = new clsConnection();

            if(Process == "All")
            {
                Process = "0','1','2','3";
            }
            else if(Process == "Bulk Email")
            {
                Process = "1','2";
            }
            else if (Process == "Online Request")
            {
                Process = "1','2";
            }
            else if (Process == "Call")
            {
                Process = "0','3";
            }


            string qry = " Select ProcessType,[Client Status],VendorId,USP,Address,[Property ID], Utility,convert(NVARCHAR, InflowDate, 110) as InflowDate," +
               " Status,PreviousStatus,REOStatus,PostedBy,convert(NVARCHAR, PostedDate, 110) as PostedDate,Aging,Skip,SkipReason,UpdateFlag," +
               "convert(NVARCHAR, Historical_Date, 110) as Historical_Date FROm tbl_UC_USP_Historical " +
               " Where convert(NVARCHAR, Historical_Date, 110) = '" + d.ToString("MM-dd-yyyy")
               + "'"; 
               //+ "' AND UpdateFlag IN ('" + Process + "') Order BY Process1";
              
            

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"  CodeBehind="InventoryControl.aspx.cs" Inherits="UC_v2._0.InventoryControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="plugins/multiselect/bootstrap-multiselect.css" rel="stylesheet" />

    <script src="plugins/multiselect/bootstrap-multiselect.js"></script>
      <script src="js/InventoryControl.js"></script>  
  <script type="text/javascript">
      $(document).ready(function () {

          $('#lb1').multiselect();


          $("[id*=gvLocations]").sortable({
              items: 'tr:not(tr:first-child)',
              cursor: 'pointer',
              axis: 'y',
              dropOnEmpty: false,
              start: function (e, ui) {
                  ui.item.addClass("selected");
              },
              stop: function (e, ui) {
                  ui.item.removeClass("selected");
              },
              receive: function (e, ui) {
                  $(this).find("tbody").append(ui.item);
              }
          });
      }); 
</script>

   
    <style>
        .navbar {
    position: relative;
    min-height: 26px;
    margin-bottom: 4px;
    border: 1px solid transparent;
}
        .navbar-nav > li > a {
    padding-top: 11px;
    padding-bottom: 11px;
  }
        .navbar-text {
    margin-top: 9px;
    margin-bottom: 11px;
}
        .navbar-inverse {
    background-color: #274761;
    border-color: #2c628a;
}

          table tbody tr:hover {
            background: #8ce196;
            cursor: pointer;
        }

        table tbody tr.selected {
            background: #81c341;
        }

        #ulSETTINGS li input[type='text'] {
            width: 51px;
            padding: 3px 8px;
            height: 28px;
        }

        select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            padding: 3px 10px;
        }
         
    </style> 
</asp:Content>
 
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div class="content-wrapper" style="min-height:733px">
        <!-- Content Header (Page header) --> 
        <div class="row">
            <div class="col-sm-2">

            </div>
             
            </div> 
        <div class="row">
            
            <div class="col-sm-12" style="background-color:#2c628a;"><h4>Inventory Controls</h4></div>
        </div> 
        <div class="row">
            <div class="col-sm-3">

                <div class="panel panel-New">
                    <div class="panel-heading"><center>Rules</center></div>
                    <div class="panel-body">


                        <table id="tblRules" class="rowHover table"  >
                            <thead>
                                <tr> 
                                    <th >Name</th>
                                    <th>Last Run</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                    <center>
                       
                             <table id="asdasd" onclick="ChangeUtil2(this)"  >
                            <thead>
                            </thead>
                            <tbody>
                               <tr>
                                   <td>
                                        <button type="button"  class="btn btn-primary btn-sm">Add New Rule</button>
                                   </td>
                               </tr>
                            </tbody>
                        </table>
                     
                    </center><br />
                </div>



            </div>
            <div class="col-lg-9" >
                <nav class="navbar navbar-inverse" >
                     <div class="row">
                        
          <div class="col-sm-3"></div>
          <div class="col-sm-7">
  <ul class="nav navbar-nav">
      <p class="navbar-text">|</p>
     <li class="active"><a data-toggle="tab" href="#setting">Total Inventory Setting</a></li>
      <p class="navbar-text">|</p>
    <li><a data-toggle="tab" href="#prediction">Prediction</a></li>
    <p class="navbar-text">|</p>
  </ul>
              </div>
         
      </div>
  
</nav>  

                <div class="panel panel-New">
                    
                    <div class="tab-content" >
                  
                        <div id="setting" class="tab-pane fade active in" >
                    <div class="panel-body">
                        <br />

                     
                           <div   id="divMAINPANEL">
                                   <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                                  <div class="row">
                                <div class="col-sm-2"></div> 
                                <div class="col-sm-2"><label for="usr">Title</label></div> 
                                <div class="col-sm-5"> 
                                    <asp:TextBox ID="txtdisTitle" Width="106px" Enabled="false"  class="form-control form-group-sm" ClientIDMode="Static" runat="server"></asp:TextBox>
                                   
                                </div> 
                            </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div> 
                                <div class="col-sm-2"><label for="usr">Description</label></div> 
                                <div class="col-sm-5">
                                    <%--<textarea class="form-control" rows="5" id="comment"></textarea>--%>
                                    <asp:TextBox ID="txtdisDesc" Enabled="false" Width="150px"    runat="server" class="form-control"  TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                                </div> 
                            </div>  
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div> 
                                <div class="col-sm-2"><label for="usr">Schedule</label></div> 
                                <div class="col-sm-5"> 
                        <asp:TextBox ID="txtdisSched" Enabled="false" Width="106px"   class="form-control form-group-sm" ClientIDMode="Static" runat="server"></asp:TextBox>
                                   
                     
                                </div> 
                            </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div> 
                                <div class="col-sm-2"><label for="usr">Period</label></div> 
                                <div class="col-sm-5"> 
                                    <div class="row" style="padding-top:8px">
                                        <div class="col-sm-8">  
                                             <asp:TextBox ID="txtdisPer" Width="106px" Enabled="false"   class="form-control form-group-sm" ClientIDMode="Static" runat="server"></asp:TextBox>
                          
                                            <asp:Label ID="Label1" runat="server" Text="of the week"></asp:Label> 

                                        </div>
                                        <div class="col-sm-4" style="padding-left:0px">
                                            
                                        </div>
                                        
                                    </div> 
                                   
                                    <div class="row" style="padding-top:8px">
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtdisHM" Width="106px" Enabled="false"   class="form-control form-group-sm" ClientIDMode="Static" runat="server"></asp:TextBox>
                          
                                           <%-- <input type="time" id="myTime" value="22:15:00">--%>
                                        <%--<input type="text"  class="form-control form-group-sm" id="usr1"/> --%>
                                        </div>
                                        <div class="col-sm-2">                                         
    
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div> 
                                </div> 
                            </div>  
                             <div class="row" style="padding-top:8px">
                                 <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><label for="usr" >Start to run rule</label>
                                            </div>
                                        <div class="col-sm-5">

                                            <div class="row">
                                                <div class="col-sm-3">
                                                 <asp:TextBox ID="txtdisStart" Width="106px" Enabled="false"   class="form-control form-group-sm" ClientIDMode="Static" runat="server"></asp:TextBox>
                          
                                                </div>
                                                <div class="col-sm-3"> <asp:DropDownList ID="DropDownList3" runat="server">
        <asp:ListItem>EST</asp:ListItem>
    </asp:DropDownList></div> 
                                            </div>

                                            </div>
                                        
                                    </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-2"><label for="usr" >Stop to run rule</label></div>
                                <div class="col-sm-5">

                                            <div class="row">
                                                <div class="col-sm-3">
                                                  <asp:TextBox ID="txtdisStop" Width="106px" Enabled="false"   class="form-control form-group-sm" ClientIDMode="Static" runat="server"></asp:TextBox>
                          
                                                </div>
                                                <div class="col-sm-3"> <asp:DropDownList ID="DropDownList6" runat="server">
        <asp:ListItem>EST</asp:ListItem>
    </asp:DropDownList></div> 
                                            </div>

                                            </div>
                            </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-2"><label for="usr" >Read from</label></div>
                                <div class="col-sm-5">
                                    <asp:FileUpload ID="FileUpload3" runat="server" /></div>
                            </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-2"><label for="usr" >Save to</label></div>
                                <div class="col-sm-5"><asp:FileUpload ID="FileUpload4" runat="server" /></div>
                            </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-2"><label for="usr" >Send Error To</label></div>
                                <div class="col-sm-2"><input type="text"  class="form-control form-group-sm" id="usr1"/></div> 
                            </div>
                        <br />
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-5">

                                    <div class="row" >
                                        <div class="col-sm-2">
                                            


                                            <table id="asdasda" onclick="ChangeUtil3(this)"  >
                            <thead>
                            </thead>
                            <tbody>
                               <tr>
                                   <td>
                                       <asp:Button ID="Button8" ClientIDMode="Static" class="btn btn-primary btn-sm" 
                                                 runat="server" Text="Edit" />
                                   </td>
                               </tr>
                            </tbody>
                        </table>




                                         </div>
                                        
                                        <div class="col-sm-2">
                                            <button type="button" class="btn btn-primary btn-sm">AAAA</button></div>
                                    
                                        </div>
                                    </div>

                                </div> 
                           
                        </ContentTemplate>
                             </asp:UpdatePanel>
                           

                        </div>

                        <div style="display: none;"  id="divMAINPANEL2">
                                   <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:MultiView ID="MultiView2" runat="server">
                            <asp:View ID="View5"  runat="server">
                                <div class="row"> 
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <div class="alert alert-warning fade in">

                            <strong>Warning!</strong> Name/Title has been use
                        </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                            </asp:View>
                        
                        </asp:MultiView>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-2">
                                <label for="usr">Client</label></div>
                            <div class="col-sm-5">
                                <asp:DropDownList ID="ddlClient" runat="server" DataSourceID="SqlDataSource1" DataTextField="Client_Name" DataValueField="ID_Client"></asp:DropDownList>

                                <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Client_Name], [ID_Client] FROM [tbl_Client]"></asp:SqlDataSource>
                            </div>
                        </div> 

                        
                        
                                 <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div> 
                                <div class="col-sm-2"><label for="usr">Title</label></div> 
                                <div class="col-sm-5"> 
                                    <div style="display: none">
                                        <asp:TextBox ID="txtTitleVal" ClientIDMode="Static" runat="server"></asp:TextBox>
                                    </div>
                                    <asp:TextBox ID="txtTitle"  class="form-control form-group-sm" ClientIDMode="Static" runat="server"></asp:TextBox>
                                   
                                </div> 
                            </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div> 
                                <div class="col-sm-2"><label for="usr">Description</label></div> 
                                <div class="col-sm-5">
                                    <%--<textarea class="form-control" rows="5" id="comment"></textarea>--%>
                                    <asp:TextBox ID="txtDesc" runat="server" class="form-control"  TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                                </div> 
                            </div>  
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div> 
                                <div class="col-sm-2"><label for="usr">Schedule</label></div> 
                                <div class="col-sm-5"> 
                                   <asp:DropDownList ID="ddl1" ClientIDMode="Static" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl1_SelectedIndexChanged">
        <asp:ListItem>Daily</asp:ListItem>
        <asp:ListItem>Weekly</asp:ListItem>
        <asp:ListItem>Monthly</asp:ListItem> 
    </asp:DropDownList> 
                     
                                </div> 
                            </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div> 
                                <div class="col-sm-2"><label for="usr">Period</label></div> 
                                <div class="col-sm-5"> 
                                    <div class="row" style="padding-top:8px">
                                        <div class="col-sm-8">  

                                            <asp:ListBox ID="lb1" runat="server" SelectionMode="Multiple" multiple="multiple" ClientIDMode="Static"> 
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                            </asp:ListBox>


                                            <asp:Label ID="lblShow" runat="server" Text="of the week"></asp:Label> 

                                        </div>
                                        <div class="col-sm-4" style="padding-left:0px">
                                            
                                        </div>
                                        
                                    </div> 
                                   
                                    <div class="row" style="padding-top:8px">
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtHourMin" runat="server" class="form-control" TextMode="Number"></asp:TextBox>
                                            
                                           <%-- <input type="time" id="myTime" value="22:15:00">--%>
                                        <%--<input type="text"  class="form-control form-group-sm" id="usr1"/> --%>
                                        </div>
                                        <div class="col-sm-2">                                         
    <asp:DropDownList ID="ddl3" runat="server" AutoPostBack="false" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
        <asp:ListItem>Hour</asp:ListItem>
        <asp:ListItem>Minute</asp:ListItem> 
    </asp:DropDownList> 
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div> 
                                </div> 
                            </div>  
                             <div class="row" style="padding-top:8px">
                                 <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><label for="usr" >Start to run rule</label>
                                            </div>
                                        <div class="col-sm-5">

                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="myTime" type="time" runat="server" value="00:00:00"></asp:TextBox> 
                                                </div>
                                                <div class="col-sm-3"> <asp:DropDownList ID="DropDownList4" runat="server">
        <asp:ListItem>EST</asp:ListItem>
    </asp:DropDownList></div> 
                                            </div>

                                            </div>
                                        
                                    </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-2"><label for="usr" >Stop to run rule</label></div>
                                <div class="col-sm-5">

                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="stopTime" type="time" runat="server" value="00:00:00"></asp:TextBox> 

                                                </div>
                                                <div class="col-sm-3"> <asp:DropDownList ID="DropDownList5" runat="server">
        <asp:ListItem>EST</asp:ListItem>
    </asp:DropDownList></div> 
                                            </div>

                                            </div>
                            </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-2"><label for="usr" >Read from</label></div>
                                <div class="col-sm-5">
                                    <asp:FileUpload ID="FileUpload1" runat="server" /></div>
                            </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-2"><label for="usr" >Save to</label></div>
                                <div class="col-sm-5"><asp:FileUpload ID="FileUpload2" runat="server" /></div>
                            </div> 
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-2"><label for="usr" >Send Error To</label></div>
                                <div class="col-sm-2"><input type="text"  class="form-control form-group-sm" id="usr1"/></div> 
                            </div>
                        <br />
                            <div class="row" style="padding-top:8px">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-5">

                                    <div class="row" >
                                        <div class="col-sm-2">
                                            <asp:Button ID="btnApply" ClientIDMode="Static" class="btn btn-primary btn-sm" OnClick="btnEdit_Click" runat="server" Text="Apply" />
                                         </div>
                                        
                                        <div class="col-sm-2">
                                            <button type="button" class="btn btn-primary btn-sm">Reset</button></div>
                                        </div>
                                    </div>

                                </div> 
                          
                        

                           </ContentTemplate>
                             </asp:UpdatePanel>
                             </div>
   
                        </div>
                            </div>
                         
                        <div id="prediction" class="tab-pane fade" >
                    <div class="panel-body">
                      
                       


                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
         <ContentTemplate>
              
                
                     <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <div class="row">
                <div class="col-md-4"> 
                  
                </div>
                <div class="col-md-2" style="display:none;">
                    <asp:TextBox ID="txtValName" ClientIDMode="Static" runat="server"></asp:TextBox>
                <div class="col-md-2"></div>
            </div>
            
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <table id="tblParameter" class="table" style="margin-bottom:5px">
                <thead>
                    <tr>
                        <th>Priority #</th>
                        <th>Prediction Parameter</th>
                        <th>Logic Description</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>

            </table>
                          <asp:Button ID="Button2" class="btn btn-link btn-sm" runat="server" Text="+Add New" OnClick="Button2_Click" />
                    </div>
                    <div class="col-md-1">   
                        <br /> 
                        <button type="button" id="AAA" onclick="Change(this)"   class="btn btn-primary btn-sm">Set Priority</button> 
                    </div>
                </div>
             
           <%-- <asp:Button ID="btnSet" class="btn btn-primary" runat="server" ClientIDMode="Static" Text="Set Priority" />--%>
               
            <%-- <asp:GridView ID="gvLocations" runat="server"  BorderColor="White" AutoGenerateColumns="false" Width="820px">
<Columns> 
    <asp:TemplateField  ItemStyle-Width="30" HeaderText="Priority">
        <ItemTemplate>
          <%# Eval("PriorityID") %>
            <input type="hidden" name="LocationId" value='<%# Eval("ID_Pre") %>' />
        </ItemTemplate>
    </asp:TemplateField>
    <asp:BoundField DataField="Pre_Name" HeaderText="Prediction Parameter" ItemStyle-Width="100" />
    <asp:BoundField DataField="Pre_Desc" HeaderText="Logic Description" ItemStyle-Width="150" /> 
</Columns>
</asp:GridView>--%>

             
            <br />
            <div class="row">
                <div class="col-md-2">
                     
                </div>
                <div class="col-md-3"> 
                </div>
                <div class="col-md-2">
              
              
                </div>
            </div>



                           
               <br />
        </asp:View>


        <asp:View ID="View2" runat="server">


            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-6">
                    <img src="img/step1.PNG" class="img-thumbnail" alt="Cinque Terre" width="520" height="60">
                        <asp:Button ID="Button7" Visible="false" Enabled="false"  runat="server" Text="Button" /> 
                    <br />
                    <br />
                       <div class="progress">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:30%">
      30% Complete
    </div>
  </div>
                        
                    <br />
         
               
                     </div>
                    <div class="col-sm-2">
                        <asp:Button ID="Button10" runat="server" Text="X" class="btn btn-danger" OnClick="close_Click" ></asp:Button>
                        
                           </div>

                </div>

            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                     <div class="panel">            
                                                <!-- /.box-header -->
                                                <div class="panel-body" style="min-height:423px">
                                                    <br />
                                                    <br />

                                                    <div class="row">
                                                        <div class="col-sm-1"></div>
                                                        <div class="col-sm-2"> Parameter Name </div>
                                                        <div class="col-sm-4">
                                                           <%--<input type="text" id="txtParaName" class="form-control form-group-sm" />--%>
                                                            <asp:TextBox ID="txtParaName"  runat="server" class="form-control"></asp:TextBox>
                                                        </div> 
                                                    </div>
                                                    <br />
                                                    <br /> 
                                                    <div class="row">
                                                        <div class="col-sm-1"></div>
                                                        <div class="col-sm-2"><center>Logic Description</center></div>
                                                        <div class="col-sm-4"> <asp:TextBox ID="txtLogicDesc" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox></textarea></div>
                                                      </div>
                                                    <div class="row">
                                                        <div class="col-sm-2"></div>
                                                       
                                                        <div class="col-sm-6">

                                                            <div class="panel">
                                                                <div class="panel-body" style="float:right">
                                                                   
                                                                      </div>
                                                            </div>


                                                        </div>
                                                         <div class="col-sm-2"> 
                                                             <br />
                                                             <br />
                                                    </div>

                                                      


                                                    <br />
                                                    <br />
                                                    


                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.box-body -->
                                            </div>


                </div>
                <div class="col-sm-2"></div>
            </div>
            
                                  <div class="col-sm-2"></div>
                              </div>
             <div class ="row">
                                  <div class="col-sm-2"></div>
                                  <div class="col-sm-6">

                                      <div class="panel">
                                          <div class="panel-body" style="float:right">
                                                  <asp:Button ID="Button1" runat="server" class="btn btn-primary"   Text="Next" OnClick="Button1_Click" /></div>
                                               </div>
                                      </div>

                                  </div>
               <br /> 
        </asp:View> 
                          <asp:View ID="View3" runat="server">
                              <div class="row">
                                  <div class="col-sm-2"></div>
                                  <div class="col-sm-6">
                                      <img src="img/step2.PNG" class="img-thumbnail" alt="Cinque Terre" width="520" height="60">

                                      <br />
                                      <br />
                                      <div class="progress">
                                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                              60% Complete
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-2">
                                      <asp:Button ID="Button11" runat="server" Text="X" class="btn btn-danger" OnClick="close_Click"></asp:Button>
                                  </div>

                              </div>
                              <div class ="row">
                                  <div class="col-sm-2">  </div>
                                  <div class="col-sm-6">
                                      <br /><div class="panel panel-default">
                                         <div class="panel-heading">
                                            
                                            
                                              <div class="row">
                                                  <div class="col-sm-5"><b> Parameters</b></div>
                                                  <div class="col-sm-2"><b>Logic</b></div>
                                              </div>
                                         </div>
                                            </div></div>
                                  <div class="col-sm-4"></div>
                              </div>
                              <br />
                              
                              <div class ="row" style="min-height:363px">
                                  <div class="col-sm-1"></div>
                                  <div class="col-sm-7" style="width:585px">
                                       <div class="row" style="padding-left: 100px">
                                            <div class="col-md-4">
                                               <asp:CheckBox ID="cb1" runat="server" AutoPostBack="true" OnCheckedChanged="cb1_CheckedChanged" />
                                      <asp:Label ID="lblState" runat="server" Text="State"></asp:Label>

                                           </div>
                                           <div class="col-md-3" >
                                               <asp:DropDownList ID="ddl6" Enabled="false" BackColor="#d9d9d9" AutoPostBack="true" runat="server" Width="150px" OnSelectedIndexChanged="ddl6_SelectedIndexChanged">
                                                   <asp:ListItem>-----------Select----------</asp:ListItem>
                                                   <asp:ListItem>A-Z</asp:ListItem>
                                                   <asp:ListItem>Z-A</asp:ListItem>
                                                   <asp:ListItem>Nearest to Farthest</asp:ListItem>
                                                   <asp:ListItem>Farthest to Nearest</asp:ListItem>
                                               </asp:DropDownList>
                                           </div>
                                       </div>
                                   <%----------------------------------------------%>
                                  
                                      <div class="row" style="padding-left: 100px; padding-top:3px">
                                          <div class="col-md-4">
                                              <asp:CheckBox ID="cb2" runat="server" AutoPostBack="true" OnCheckedChanged="cb2_CheckedChanged"/>
                                       <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>

                                           </div>
                                           <div class="col-md-2">
                                               <asp:DropDownList ID="ddl7" Enabled="false" BackColor="#d9d9d9" AutoPostBack="true" OnSelectedIndexChanged="ddl7_SelectedIndexChanged" runat="server" Width="150px">
                                                   <asp:ListItem>-----------Select----------</asp:ListItem>
                                                   <asp:ListItem>A-Z</asp:ListItem>
                                                   <asp:ListItem>Z-A</asp:ListItem>
                                                   <asp:ListItem>Nearest to Farthest</asp:ListItem>
                                                   <asp:ListItem>Farthest to Nearest</asp:ListItem>

                                               </asp:DropDownList>
                                           </div>
                                       </div>
                                      <%----------------------------------------------%>

                                       <div class="row" style="padding-left: 100px; padding-top:3px">
                                           <div class="col-md-4">
                                             <asp:CheckBox ID="cb3" runat="server" AutoPostBack="true" OnCheckedChanged="cb3_CheckedChanged" />
                                        <asp:Label ID="lblZip" runat="server" Text="Zip"></asp:Label>

                                           </div>
                                           <div class="col-md-2">
                                               <asp:DropDownList ID="ddl8" Enabled="false" BackColor="#d9d9d9" AutoPostBack="true" OnSelectedIndexChanged="ddl8_SelectedIndexChanged" runat="server" Width="150px">
                                                   <asp:ListItem>-----------Select----------</asp:ListItem>
                                                   <asp:ListItem>Highest to Lowest</asp:ListItem>
                                                   <asp:ListItem>Lowest to Highest</asp:ListItem>
                                                   <asp:ListItem>Nearest to Farthest</asp:ListItem>
                                                   <asp:ListItem>Farthest to Nearest</asp:ListItem>
                                               </asp:DropDownList>
                                           </div>
                                       </div>
                                      <%----------------------------------------------%>

                                      

                                     <div class="row" style="padding-left: 100px; padding-top:3px">
                                             <div class="col-md-4">
                                              <asp:CheckBox ID="cb4" runat="server" AutoPostBack="true" OnCheckedChanged="cb4_CheckedChanged"/>
                                        <asp:Label ID="lblSAdd" runat="server" Text="Street Address"></asp:Label>

                                           </div>
                                           <div class="col-md-2">
                                               <asp:DropDownList ID="ddl9" Enabled="false" BackColor="#d9d9d9" AutoPostBack="true" OnSelectedIndexChanged="ddl9_SelectedIndexChanged" runat="server" Width="150px">
                                                   <asp:ListItem>-----------Select----------</asp:ListItem>
                                                   <asp:ListItem>A-Z</asp:ListItem>
                                                   <asp:ListItem>Z-A</asp:ListItem>
                                                   <asp:ListItem>Nearest to Farthest</asp:ListItem>
                                                   <asp:ListItem>Farthest to Nearest</asp:ListItem>
                                               </asp:DropDownList>
                                           </div>
                                       </div>
                                      <%----------------------------------------------%>

                                     

                                     <div class="row" style="padding-left: 100px; padding-top:3px">
                                          <div class="col-md-4">
                                      <asp:CheckBox ID="cb5" runat="server" AutoPostBack="true" OnCheckedChanged="cb5_CheckedChanged" />
                                        <asp:Label ID="lblProRev" runat="server" Text="Initial Property Review"></asp:Label>

                                           </div>
                                           <div class="col-md-2">
                                                   <asp:DropDownList ID="ddl10" Enabled="false" BackColor="#d9d9d9" AutoPostBack="true" OnSelectedIndexChanged="ddl10_SelectedIndexChanged"  runat="server" Width="150px">
                                          <asp:ListItem>-----------Select----------</asp:ListItem>
                                          <asp:ListItem>A-Z</asp:ListItem>
                                          <asp:ListItem>Z-A</asp:ListItem>
                                      </asp:DropDownList>
                                           </div>
                                       </div>
                                      <%----------------------------------------------%>


                                      <div class="row" style="padding-left: 100px; padding-top:3px">
                                             <div class="col-md-4">
                                       <asp:CheckBox ID="cb6" runat="server"  AutoPostBack="true" OnCheckedChanged="cb6_CheckedChanged"/>
                                        <asp:Label ID="lblUtil" runat="server" Text="Utility Type"></asp:Label>

                                           </div>
                                           <div class="col-md-2">
                                               <asp:DropDownList ID="ddl11" Enabled="false" BackColor="#d9d9d9" AutoPostBack="true" OnSelectedIndexChanged="ddl11_SelectedIndexChanged" runat="server" Width="150px">
                                                   <asp:ListItem>-----------Select----------</asp:ListItem>
                                                   <asp:ListItem>A-Z</asp:ListItem>
                                                   <asp:ListItem>Z-A</asp:ListItem>
                                               </asp:DropDownList>
                                           </div>
                                       </div>
                                      <%----------------------------------------------%>

                                    

                                        <div class="row" style="padding-left: 100px; padding-top:3px">
                                         <div class="col-md-4">
                                        <asp:CheckBox ID="cb7" runat="server" AutoPostBack="true" OnCheckedChanged="cb7_CheckedChanged" />
                                        <asp:Label ID="lblProxi" runat="server" Text="Proximity"></asp:Label>

                                           </div>
                                           <div class="col-md-2">
                                               <asp:DropDownList ID="ddl12" Enabled="false" BackColor="#d9d9d9" AutoPostBack="true" OnSelectedIndexChanged="ddl12_SelectedIndexChanged" runat="server" Width="150px">
                                                   <asp:ListItem>-----------Select----------</asp:ListItem>
                                                   <asp:ListItem>Nearest to Farthest</asp:ListItem>
                                                   <asp:ListItem>Farthest to Nearest</asp:ListItem>
                                               </asp:DropDownList>
                                           </div>
                                       </div>
                                      <%----------------------------------------------%>

                                    

                                      <br />
                                      <br />
                                       
                                  </div>
                                 
                                  <div class="col-sm-3" style="padding-left:0px">

                                      <div class="panel">
                                          <div class="panel-body" style="min-height:300px; background-color:lightblue">
                                              <div style="text-align:center">
                                               
                                                  <%--FOR STATE--%>
                                              <asp:Label ID="lbl1"   runat="server" Text="Summary of Logic" Font-Bold="True" Font-Size="20px"></asp:Label>
                                                  </div>
                                      
                                               <asp:Label ID="lbl2" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="lbl3" Visible="false" runat="server" Font-Bold="True" Text="State"></asp:Label>
                                              <asp:Label ID="lbl4" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="lblOutput" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="lbl5" Visible="false" runat="server" Text="Order"></asp:Label>
                                         
                                              <%--FOR CITY--%>
                                              <asp:Label ID="lbl6" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="lbl7" Visible="false" runat="server" Font-Bold="True" Text="City"></asp:Label>
                                              <asp:Label ID="lbl8" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="lblOutput2" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="lbl9" Visible="false" runat="server" Text="Order"></asp:Label>
                                            
                                              <%--FOR ZIP--%>
                                              <asp:Label ID="lbl10" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="lbl11" Visible="false" runat="server" Font-Bold="True" Text="Zip"></asp:Label>
                                              <asp:Label ID="lbl12" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="lblOutput3" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="lbl13" Visible="false" runat="server" Text="Order"></asp:Label>
                                     
                                              <%--FOR STREET ADDRESS--%>
                                              <asp:Label ID="lbl14" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="lbl15" Visible="false" runat="server" Font-Bold="True" Text="Street Address"></asp:Label>
                                              <asp:Label ID="lbl16" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="lblOutput4" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="lbl17" Visible="false" runat="server" Text="Order"></asp:Label>
                                         
                                              <%--FOR INITIAL PROPERTY--%>
                                              <asp:Label ID="lbl18" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="lbl19" Visible="false" runat="server" Font-Bold="True" Text="Initial Property"></asp:Label>
                                              <asp:Label ID="lbl20" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="lblOutput5" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="lbl21" Visible="false" runat="server" Text="Order"></asp:Label>
                                          
                                              <%--FOR UTILITY TYPE--%>
                                              <asp:Label ID="lbl22" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="lbl23" Visible="false" runat="server" Font-Bold="True" Text="Utility Type"></asp:Label>
                                              <asp:Label ID="lbl24" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="lblOutput6" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="lbl25" Visible="false" runat="server" Text="Order"></asp:Label>
                                       
                                               <%--FOR PROXIMITY--%>
                                              <asp:Label ID="lbl26" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="lbl27" Visible="false" runat="server" Font-Bold="True" Text="Proximity"></asp:Label>
                                              <asp:Label ID="lbl28" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="lblOutput7" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="lbl29" Visible="false" runat="server" Text="Order"></asp:Label>


                                               <asp:Label ID="lblThen" Visible="false" runat="server" Font-Bold="True" Text="THEN"></asp:Label>
                                              <asp:Label ID="lblUSP" Visible="false" runat="server" Text="Predict the USP"></asp:Label>
                                          </div>
                                      </div>

                                  </div>
                              </div>
                             
                              
                             
                             
                              <div class ="row">
                                  <div class="col-sm-2"></div>
                                  <div class="col-sm-6">

                                      <div class="panel">
                                          <div class="panel-body" style="float:right">
                                               <asp:Button ID="Button5" class="btn btn-primary" runat="server" Text="Back" OnClick="Button5_Click" />
                    <asp:Button ID="Button3" runat="server" Text="Next" class="btn btn-primary" OnClick="Button3_Click" />
                                          </div>
                                      </div>

                                  </div>
                                  <div class="col-sm-2"></div>
                              </div>
                                 <br />


        </asp:View>



                         <asp:View ID="View4" runat="server">
                               <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-6">
                     <img src="img/step3.PNG" class="img-thumbnail" alt="Cinque Terre" width="520" height="60">
             <br />
                    <br />
                       <div class="progress">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">
      100% Complete
    </div>
                    
                    </div>
                    

                </div>
                                   <div class="col-sm-2">
                          <asp:Button ID="Button12" runat="server" Text="X" class="btn btn-danger" OnClick="close_Click" ></asp:Button>
                    </div>
                                   </div>
                             <br />
                             <br />
                             <br />
                             <div class="panel">
                                 <div class="panel-body"  style="min-height:400px">
                                     <div class="row">
                                 <div class="col-sm-2"></div>
                                 <div class="col-sm-6">
                                      <div class="panel panel-default">
                                 <div class="panel-heading" >
                                     <br />
                                     <br />
                                     <div class="row" style="min-height:50px">
                                 <div class="col-sm-1"></div>
                                 <div class="col-sm-4"><b>Prediction Parameter</b></div>
                                 <div class="col-sm-2">
                                     <asp:Label ID="lblParaName" runat="server" Text=""></asp:Label>
                                     </div>
                             </div>

                             
                             <div class="row" style="min-height:52px">
                                  <div class="col-sm-1"></div>
                                 <div class="col-sm-4"><b>Logic Description</b></div>
                                 <div class="col-sm-2">
                                     <asp:Label ID="lblParaDesc" runat="server" Text=""></asp:Label>
                                      
                                 </div> 
                             </div>
                                     <br />
                             
                                     <div class="row" style="min-height:160px">
                                 <div class="col-sm-1"></div>
                                 <div class="col-sm-8">
                                                <%--ALL--%>

                                              <%--FOR STATE--%>
                                              
                                               <asp:Label ID="Label3" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="Label4" Visible="false" runat="server" Font-Bold="True" Text="State"></asp:Label>
                                              <asp:Label ID="Label5" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="Label6" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="Label7" Visible="false" runat="server" Text="Order"></asp:Label> 

                                              <%--FOR CITY--%>
                                              <asp:Label ID="Label8" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="Label9" Visible="false" runat="server" Font-Bold="True" Text="City"></asp:Label>
                                              <asp:Label ID="Label10" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="Label11" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="Label12" Visible="false" runat="server" Text="Order"></asp:Label>
                                            
                                              <%--FOR ZIP--%>
                                              <asp:Label ID="Label13" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="Label14" Visible="false" runat="server" Font-Bold="True" Text="Zip"></asp:Label>
                                              <asp:Label ID="Label15" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="Label16" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="Label17" Visible="false" runat="server" Text="Order"></asp:Label>
                                     
                                              <%--FOR STREET ADDRESS--%>
                                              <asp:Label ID="Label18" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="Label19" Visible="false" runat="server" Font-Bold="True" Text="Street Address"></asp:Label>
                                              <asp:Label ID="Label20" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="Label21" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="Label22" Visible="false" runat="server" Text="Order"></asp:Label>
                                         
                                              <%--FOR INITIAL PROPERTY--%>
                                              <asp:Label ID="Label23" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="Label24" Visible="false" runat="server" Font-Bold="True" Text="Initial Property"></asp:Label>
                                              <asp:Label ID="Label25" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="Label26" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="Label27" Visible="false" runat="server" Text="Order"></asp:Label>
                                          
                                              <%--FOR UTILITY TYPE--%>
                                              <asp:Label ID="Label28" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="Label29" Visible="false" runat="server" Font-Bold="True" Text="Utility Type"></asp:Label>
                                              <asp:Label ID="Label30" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="Label31" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="Label32" Visible="false" runat="server" Text="Order"></asp:Label>
                                       
                                               <%--FOR PROXIMITY--%>
                                              <asp:Label ID="Label33" Visible="false" runat="server"  >If the </asp:Label><asp:Label ID="Label34" Visible="false" runat="server" Font-Bold="True" Text="Proximity"></asp:Label>
                                              <asp:Label ID="Label35" Visible="false" runat="server"  >is in </asp:Label><asp:Label ID="Label36" runat="server" Font-Bold="True" Text=""></asp:Label>
                                              <asp:Label ID="Label37" Visible="false" runat="server" Text="Order"></asp:Label>


                                               <asp:Label ID="lblThen2" Visible="false" runat="server" Font-Bold="True" Text="THEN"></asp:Label>
                                              <asp:Label ID="lblUSP2" Visible="false" runat="server" Text="Predict the USP"></asp:Label>







                                 </div>
                                 <div class="col-sm-2"></div>
                             </div>
                                     

                                 </div>
                                          <div class="panel-body" style="float:right">
                                              <br />
                                              <br />
                                              <asp:Button ID="Button6" class="btn btn-primary" runat="server" Text="Back" OnClick="Button6_Click" />
                   
                                                <asp:Button ID="Button4" class="btn btn-primary"  runat="server" Text="Save" OnClick="btnSave_Click"/>
                                           
                                          </div> 
                             </div>
                                 </div>
                                 <div class="col-sm-3"  style="float:right"></div>
                             </div> 
                                 </div> 
                             </div> 
                            <br />
                          
                             <div class="row">
                                  <div class="col-sm-1"></div>
                                 <div class="col-sm-6"></div>
                                 <div class="col-sm-2"></div>
                             <div class="col-sm-1"></div>
                             </div>

        </asp:View>

    </asp:MultiView> 
                
        </ContentTemplate>
        </asp:UpdatePanel> 
<div class="modal fade" id="confirm" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add New Prediction</h4>
                                        </div>
                                        <div class="modal-body">
                                            <br />
                                            <br />
                                             
                                             <div class="row">
                                                 <div class="col-sm-3"></div>
                                                 <div class="col-sm-4">
                                                     <asp:Button ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click" runat="server" Text="Yes"  />
                                                 </div> 
                                                 <div class="col-sm-3">
                                                     &nbsp
                                                     <asp:Button ID="Button9" class="btn btn-primary" runat="server" Text="No"/>
                                                 </div>
                                                 <div class="col-sm-3"></div>
                                             </div>
                                        </div>

                                    </div>

                                </div>
                            </div>


                           
                        </div> 
                        </div> 
                        </div> 
                    </div>
                </div>
            </div> 
        </div> 
                <div class="col-sm-2">
                    <br />
                   
                      
                </div>

            <div class="col-sm-2">
               <br />   
               

            </div>
     

</asp:Content>


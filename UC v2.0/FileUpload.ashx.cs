﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace UC_v2._0
{
    /// <summary>
    /// Summary description for FileUpload
    /// </summary>
    public class FileUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            string domain = System.Web.HttpContext.Current.User.Identity.Name.Replace(@"\", "/");
            if (domain == "")
            {
                domain = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"\", "/");
            }

            string[] sUser = domain.Split('/');
            string sUserId = sUser[1].ToLower();
           

            clsConnection cls = new clsConnection();

            //string query = "select top 1 id, [filename] from tbl_UC_Mapping_PDF where modified_by = '" + sUserId + "'  order by date_modified desc";

            //DataTable dt = new DataTable();
            //dt = cls.GetData(query);

            //string fn = dt.Rows[0][1].ToString();

            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                string fname = files.AllKeys[0].ToString();

                try
                {
                    if (fname == "Signature")
                    {
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFile file = files[i];

                            //string fname = "";
                            //if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                            //{
                            //    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            //}
                            //else
                            //{
                            //    fname = file.FileName;
                            //}

                            string path = Path.Combine(context.Server.MapPath("~/Files/PDF Files/"), fname + ".jpg");
                            //string path = Path.Combine(@"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\PDF Files\" + dt.Rows[0]["filename"].ToString() + ".pdf");

                            file.SaveAs(path);

                            ////string update = "update tbl_VPR_Mapping_PDF set pdf_file = '" + fname + "' where id = " + dt.Rows[0][0].ToString();

                            //try
                            //{
                            //	cls.ExecuteQuery(update);
                            //}
                            //catch (Exception) { }

                        }
                    }
                    else
                    {
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFile file = files[i];

                            //string fname = "";
                            //if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                            //{
                            //    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            //}
                            //else
                            //{
                            //    fname = file.FileName;
                            //}

                            string path = Path.Combine(context.Server.MapPath("~/Files/PDF Files/"), fname + ".pdf");
                            //string path = Path.Combine(@"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\PDF Files\" + dt.Rows[0]["filename"].ToString() + ".pdf");

                            file.SaveAs(path);

                            ////string update = "update tbl_VPR_Mapping_PDF set pdf_file = '" + fname + "' where id = " + dt.Rows[0][0].ToString();

                            //try
                            //{
                            //	cls.ExecuteQuery(update);
                            //}
                            //catch (Exception) { }

                        }
                    }
                }
                catch
                {

                }
                



               
            }
            context.Response.ContentType = "text/plain";
            context.Response.Write("File Uploaded Successfully!");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class OnlineRequestSetUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        [WebMethod]
        public static string GetThis()
        {
            clsConnection cls = new clsConnection();
            string qry = "Select * fRom tbl_UC_USP_Name_Vendor_ID where isDeleted = 0";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string GetUSPs()
        {
            clsConnection cls = new clsConnection();
            string qry = " Select a.USP_ID, USP_Name,a.Task,Website,a.NoOfPage,a.iPages,[Login Name],Password "+
                " FROm tbl_UC_USPListOnlineRequest_MasterControl a INNER JOIN tbl_UC_USP_Name_Vendor_ID b ON b.USP_ID = a.USP_ID";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }
        [WebMethod]
        public static string Save(string USP, string Task, string Pages, string iPages)
        {
            clsConnection cls = new clsConnection();
            string qryInsert = "Insert INTO tbl_UC_USPListOnlineRequest_MasterControl (USP_ID, Task, NoOfPage,iPages) values( ";
            qryInsert += "'" + USP + "','" + Task + "','" + Pages + "','" + iPages + "')";
            cls.ExecuteQuery(qryInsert);

            return "True";
        }
        [WebMethod]
        public static string EditThis(string USP)
        {
            clsConnection cls = new clsConnection();
            string qry2 = "Select * fRom tbl_UC_USP_Name_Vendor_ID where isDeleted = 0";
            string qry = " Select a.USP_ID, USP_Name,a.Task,Website,a.NoOfPage,a.iPages,[Login Name],Password FROm tbl_UC_USPListOnlineRequest_MasterControl a INNER JOIN tbl_UC_USP_Name_Vendor_ID b ON b.USP_ID = a.USP_ID Where a.USP_ID = " + USP;
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            dt = cls.GetData(qry);
            dt2 = cls.GetData(qry2);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, asd2 = dt2 } });
        }
        [WebMethod]
        public static string Update(string USP,string USP_ID, string Task, string Pages, string iPages)
        {
            clsConnection cls = new clsConnection();

            string UpdateQry = "Update tbl_UC_USPListOnlineRequest_MasterControl Set USP_ID = '" + USP + "', Task = '" + Task + "', NoOfPage = '" + Pages + "', iPages = '" + iPages
                + "'  Where USP_ID = '" + USP_ID + "'";
            cls.ExecuteQuery(UpdateQry);

            return "True";
        }



    }
}
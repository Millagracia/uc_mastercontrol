﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Drawing;
using System.Data.SqlClient;
using System.Web.Services;
using System.Data;
using Newtonsoft.Json;
using System.Configuration;

namespace UC_v2._0
{
    public partial class InventoryControl : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string script = @"$('#lb1').multiselect();";
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "multiselect", script, true);


            //if (!IsPostBack)
            //{

            //    this.BindGrid(); 
            //} 

            if (IsPostBack)
            {
                MultiView2.ActiveViewIndex = -1;
                //  this.BindGrid(Session["IDrules"].ToString());
            }
            //if (Session["IDrules"] == null)
            //{ 
            //}
            //else
            //{
            //     this.BindGrid(Session["IDrules"].ToString());
            //} 
        }
        protected void btnSet_Click(object sender, EventArgs e)
        {
            //   this.BindGrid(Session["IDrules"].ToString());
        }
        //private void BindGrid(string IDrules)
        //{
        //    //IDrules = Session["IDrules"].ToString();


        //    string query = "SELECT ID_Pre, Pre_Name, Pre_Desc, PriorityID FROM tbl_UC_Prediction  Where ID_Rules = '"+ IDrules +"' ORDER BY PriorityID";
        //    string constr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(constr))
        //    {
        //        using (SqlCommand cmd = new SqlCommand(query))
        //        {
        //            using (SqlDataAdapter sda = new SqlDataAdapter())
        //            {
        //                cmd.CommandType = CommandType.Text;
        //                cmd.Connection = con;
        //                sda.SelectCommand = cmd;
        //                using (DataTable dt = new DataTable())
        //                {
        //                    sda.Fill(dt);

        //                    gvLocations.DataSource = dt;
        //                    gvLocations.DataBind();
        //                    //return dt;

        //                }
        //            }
        //        }
        //    }
        //}

        //protected void UpdatePreference(object sender, EventArgs e)
        //{
        //    int[] locationIds = (from p in Request.Form["LocationId"].Split(',')
        //                         select int.Parse(p)).ToArray();
        //    int preference = 1;
        //    foreach (int locationId in locationIds)
        //    {
        //        this.UpdatePreference(locationId, preference);
        //        preference += 1;
        //    }

        //    Response.Redirect(Request.Url.AbsoluteUri);
        //}

        //private void UpdatePreference(int locationId, int preference)
        //{
        //    string constr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(constr))
        //    {
        //        using (SqlCommand cmd = new SqlCommand("UPDATE tbl_UC_Prediction SET PriorityID = @PriorityID WHERE ID_Pre = @ID_Pre"))
        //        {
        //            using (SqlDataAdapter sda = new SqlDataAdapter())
        //            {
        //                cmd.CommandType = CommandType.Text;
        //                cmd.Parameters.AddWithValue("@ID_Pre", locationId);
        //                cmd.Parameters.AddWithValue("@PriorityID", preference);
        //                cmd.Connection = con;
        //                con.Open();
        //                cmd.ExecuteNonQuery();
        //                con.Close();
        //            }
        //        }
        //    }
        //}

        [WebMethod]
        public static string SetPrio(string IDrule, string preference)
        {
            string constr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE tbl_UC_Prediction SET PriorityID = @PriorityID WHERE ID_Pre = @ID_Pre"))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@ID_Pre", IDrule);
                        cmd.Parameters.AddWithValue("@PriorityID", preference);
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { } });
        }

        [WebMethod]
        public static string SaveTo(string ParaName, string LogicDesc)
        {
            //SqlConnection con = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            SqlConnection con = new SqlConnection("Data Source=dav8dbhsnd01;Initial Catalog = MIS_MLA_UAT;uid=mis414;pwd=juchUt4a");
            {
                SqlCommand cmd = new SqlCommand("Insert into tbl_UC_Prediction (Pre_Name, Pre_Desc, State, City, Zip, Street_Add, Unitial_Pro, Utility_Type, Proximity) values('" +
                    ParaName + "','" + LogicDesc + "')", con);
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return "True";
                }

            }


        }
        [WebMethod]
        public static string getRule()
        {
            clsConnection cls = new clsConnection();

            string qry = "select rule_Name, CONVERT(varchar, Start_Run_Rule, 107) as Start_Run_Rule, CONVERT(varchar(5), Start_Run_Rule, 108 ) as Time  from tbl_UC_Total_Inventory;";
            //"select * from tbl_HRMS_Employee_Activity where LoginID = '" + myData[0].ToString() + "' and SchedDate between '" + myData[1].ToString() + "' and '" + myData[2].ToString() + "';";

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);
            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
        }



        [WebMethod]
        public static string GetRuleInfoDisplay(string Client)
        {
            string connetionString = null;
            string IDRule = "";
            connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";

            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                cnn.Open();
                SqlCommand command = new SqlCommand("Select * From tbl_UC_Total_Inventory Where rule_Name = '" + Client + "'", cnn);
                //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDRule = reader["ID_Rules"].ToString();
                    HttpContext.Current.Session["IDrules"] = IDRule;
                }
                cnn.Close();

            }

            clsConnection cls = new clsConnection();




            string qry = "SELECT ID_Pre, Pre_Name, Pre_Desc, PriorityID FROM tbl_UC_Prediction Where ID_Rules = '" + IDRule + "' ORDER BY PriorityID ";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });


        }


        [WebMethod]
        public static string GetRuleInfo(string Client)
        {
            //SqlConnection openCon = new SqlConnection("Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a");
            //openCon.Open();

            //SqlCommand command = new SqlCommand("Select * From tbl_UC_Total_Inventory Where rule_Name = '" + Client + "'", openCon);
            //SqlDataReader reader = command.ExecuteReader();

            //while (reader.Read())
            //{
            //    string asdasd = reader["rule_Name"].ToString();

            //}

            HttpContext.Current.Session["RuleName"] = Client;

            clsConnection cls = new clsConnection();
            //InventoryControl AA = new InventoryControl();
            //GridView gv = (GridView)AA.FindControl("gvLocations");
            //AA.BindGrid();
            //gv.DataSource = BindGrid();
            //gv.DataBind();

            //BindGrid();



            string qry = "select rule_Name, rule_desc, rule_sched, Period_WDM, Period, Time, CONVERT(varchar(5), Start_Run_Rule, 108 ) as Start_Run_Rule, CONVERT(varchar(5), Stop_Run_Rule, 108 ) as Stop_Run_Rule from tbl_UC_Total_Inventory where rule_Name = '" + Client + "'";
            DataTable dt = new DataTable();
            dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });


        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            string connetionString = null;
            string sql = null;
            string sqlUpdate = null;
            string sqlUniq = null;
            string week = "";
            string HourMin = "";
            string Uniq = txtValName.Text;
            string Name = "";
            string IDrule = "";
            connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";

            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                cnn.Open();
                SqlCommand command = new SqlCommand("Select * From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'", cnn);
                //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Name = reader["rule_Name"].ToString();
                    IDrule = reader["ID_Rules"].ToString();
                }
                cnn.Close();


                sqlUpdate = "Update tbl_UC_Total_Inventory " +
                "SET rule_Name = @Name, rule_desc = @Desc, rule_sched = @sched, Period_WDM = @WDM, Period = @Period, Time = @Time, Start_Run_Rule = @Start, " +
                "Stop_Run_Rule = @Stop where ID_Rules = '" + IDrule + "'";

                sql = "update  tbl_UC_Total_Inventory ([ID_Client], [rule_Name], [rule_desc], [rule_sched], [Period_WDM], [Period], [Time], [Start_Run_Rule], "
               + " [Stop_Run_Rule])" +
               " values(@Client,@Name,@Desc,@sched,@WDM,@Period,@Time,@Start,@Stop) where ID_Rules = '" + IDrule + "'";

                using (SqlCommand cmd = new SqlCommand(sqlUpdate, cnn))
                {
                    cmd.Parameters.AddWithValue("@Name", txtTitle.Text);
                    cmd.Parameters.AddWithValue("@Desc", txtDesc.Text);
                    cmd.Parameters.AddWithValue("@sched", ddl1.SelectedItem.ToString());

                    foreach (int i in lb1.GetSelectedIndices())
                    {
                        if (week == "")
                        {
                            week = lb1.Items[i].Text;
                        }
                        else
                        {
                            week = week + ", " + lb1.Items[i].Text;
                        }
                    }
                    cmd.Parameters.AddWithValue("@WDM", week);


                    cmd.Parameters.AddWithValue("@Period", txtHourMin.Text);
                    cmd.Parameters.AddWithValue("@Time", ddl3.SelectedItem.ToString());
                    cmd.Parameters.AddWithValue("@Start", Convert.ToDateTime(myTime.Text));
                    cmd.Parameters.AddWithValue("@Stop", Convert.ToDateTime(stopTime.Text));
                    cmd.Connection = cnn;
                    cnn.Open();
                    cmd.ExecuteNonQuery();
                    cnn.Close();
                }
                Response.Redirect("InventoryControl.aspx");
            }






        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            string connetionString = null;
            string sql = null;
            string sqlUniq = null;
            string week = "";
            string HourMin = "";
            string Uniq = txtTitle.Text;
            string Name = "";
            connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";

            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                cnn.Open();
                SqlCommand command = new SqlCommand("Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'", cnn);
                //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Name = reader["rule_Name"].ToString();

                }
                cnn.Close();

                if (Name == Uniq)
                {
                    MultiView2.ActiveViewIndex = 0;
                }
                else
                {
                    sql = "insert into tbl_UC_Total_Inventory ([ID_Client], [rule_Name], [rule_desc], [rule_sched], [Period_WDM], [Period], [Time], [Start_Run_Rule], "
                   + " [Stop_Run_Rule])" +
                   " values(@Client,@Name,@Desc,@sched,@WDM,@Period,@Time,@Start,@Stop)";

                    using (SqlCommand cmd = new SqlCommand(sql, cnn))
                    {
                        cmd.Parameters.AddWithValue("@Client", ddlClient.SelectedValue);
                        cmd.Parameters.AddWithValue("@Name", txtTitle.Text);
                        cmd.Parameters.AddWithValue("@Desc", txtDesc.Text);
                        cmd.Parameters.AddWithValue("@sched", ddl1.SelectedItem.ToString());


                        foreach (int i in lb1.GetSelectedIndices())
                        {
                            if (week == "")
                            {
                                week = lb1.Items[i].Text;
                            }
                            else
                            {
                                week = week + ", " + lb1.Items[i].Text;
                            }


                        }
                        cmd.Parameters.AddWithValue("@WDM", week);




                        cmd.Parameters.AddWithValue("@Period", txtHourMin.Text);
                        cmd.Parameters.AddWithValue("@Time", ddl3.SelectedItem.ToString());
                        cmd.Parameters.AddWithValue("@Start", Convert.ToDateTime(myTime.Text));
                        cmd.Parameters.AddWithValue("@Stop", Convert.ToDateTime(stopTime.Text));
                        cmd.Connection = cnn;
                        cnn.Open();
                        cmd.ExecuteNonQuery();
                        cnn.Close();
                    }
                    Response.Redirect("InventoryControl.aspx");
                }




            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string connetionString = null;
            string sql = null;

            string state = "";
            string city = "";
            string zip = "";
            string Street = "";
            string initial = "";
            string utility = "";
            string proximity = "";
            string IDval = "";
            connetionString = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI_UAT;uid=mis414;pwd=juchUt4a";
            using (SqlConnection cnn = new SqlConnection(connetionString))
            {
                sql = "insert into tbl_UC_Prediction ([ID_Rules], [Pre_Name], [Pre_Desc], [State], [City], [Zip], [Street_Add], [Initial_Pro], [Utility_type], [Proximity])" +
                    " values(@Rules,@Name,@Desc,@State,@City,@Zip,@Street,@Initial,@Utility,@Proximity)";
                cnn.Open();

                SqlCommand command = new SqlCommand("Select ID_Rules From tbl_UC_Total_Inventory Where rule_Name = '" + txtValName.Text + "'", cnn);
                //sqlUniq = "Select rule_Name From tbl_UC_Total_Inventory Where rule_Name = '" + Uniq + "'";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDval = reader["ID_Rules"].ToString();
                }
                cnn.Close();
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, cnn))
                {
                    if (ddl6.SelectedIndex == 0)
                    {
                        state = "";
                    }
                    else
                    {
                        state = ddl6.SelectedItem.ToString();
                    }

                    if (ddl7.SelectedIndex == 0)
                    {
                        city = "";
                    }
                    else
                    {
                        city = ddl7.SelectedItem.ToString();
                    }

                    if (ddl8.SelectedIndex == 0)
                    {
                        zip = "";
                    }
                    else
                    {
                        zip = ddl8.SelectedItem.ToString();
                    }

                    if (ddl9.SelectedIndex == 0)
                    {
                        Street = "";
                    }
                    else
                    {
                        Street = ddl9.SelectedItem.ToString();
                    }

                    if (ddl10.SelectedIndex == 0)
                    {
                        initial = "";
                    }
                    else
                    {
                        initial = ddl10.SelectedItem.ToString();
                    }

                    if (ddl11.SelectedIndex == 0)
                    {
                        utility = "";
                    }
                    else
                    {
                        utility = ddl11.SelectedItem.ToString();
                    }

                    if (ddl12.SelectedIndex == 0)
                    {
                        proximity = "";
                    }
                    else
                    {
                        proximity = ddl12.SelectedItem.ToString();
                    }
                    cmd.Parameters.AddWithValue("@Rules", IDval);
                    cmd.Parameters.AddWithValue("@Name", txtParaName.Text);
                    cmd.Parameters.AddWithValue("@Desc", txtLogicDesc.Text);
                    cmd.Parameters.AddWithValue("@State", state);
                    cmd.Parameters.AddWithValue("@City", city);
                    cmd.Parameters.AddWithValue("@Zip", zip);
                    cmd.Parameters.AddWithValue("@Street", Street);
                    cmd.Parameters.AddWithValue("@Initial", initial);
                    cmd.Parameters.AddWithValue("@Utility", utility);
                    cmd.Parameters.AddWithValue("@Proximity", proximity);
                    cmd.ExecuteNonQuery();

                }
            }

            Response.Redirect("InventoryControl.aspx");
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 1;
            txtParaName.Text = "";
            txtLogicDesc.Text = "";
            cb1.Checked = false;
            ddl6.Enabled = false;
            ddl6.BackColor = Color.FromArgb(217, 217, 217);
            ddl6.SelectedIndex = 0;
            lbl2.Visible = false;
            lbl3.Visible = false;
            lbl4.Visible = false;
            lbl5.Visible = false;
            lblOutput.Visible = false;

            cb2.Checked = false;
            ddl7.Enabled = false;
            ddl7.BackColor = Color.FromArgb(217, 217, 217);
            ddl7.SelectedIndex = 0;
            lbl6.Visible = false;
            lbl7.Visible = false;
            lbl8.Visible = false;
            lbl9.Visible = false;
            lblOutput2.Visible = false;

            cb3.Checked = false;
            ddl8.Enabled = false;
            ddl8.BackColor = Color.FromArgb(217, 217, 217);
            ddl8.SelectedIndex = 0;
            lbl10.Visible = false;
            lbl11.Visible = false;
            lbl12.Visible = false;
            lbl13.Visible = false;
            lblOutput3.Visible = false;

            cb4.Checked = false;
            ddl9.Enabled = false;
            ddl9.BackColor = Color.FromArgb(217, 217, 217);
            ddl9.SelectedIndex = 0;
            lbl14.Visible = false;
            lbl15.Visible = false;
            lbl16.Visible = false;
            lbl17.Visible = false;
            lblOutput4.Visible = false;

            cb5.Checked = false;
            ddl10.Enabled = false;
            ddl10.BackColor = Color.FromArgb(217, 217, 217);
            ddl10.SelectedIndex = 0;
            lbl18.Visible = false;
            lbl19.Visible = false;
            lbl20.Visible = false;
            lbl21.Visible = false;
            lblOutput5.Visible = false;

            cb6.Checked = false;
            ddl11.Enabled = false;
            ddl11.BackColor = Color.FromArgb(217, 217, 217);
            ddl11.SelectedIndex = 0;
            lbl22.Visible = false;
            lbl23.Visible = false;
            lbl24.Visible = false;
            lbl25.Visible = false;
            lblOutput6.Visible = false;

            cb7.Checked = false;
            ddl12.Enabled = false;
            ddl12.BackColor = Color.FromArgb(217, 217, 217);
            ddl12.SelectedIndex = 0;
            lbl26.Visible = false;
            lbl27.Visible = false;
            lbl28.Visible = false;
            lbl29.Visible = false;
            lblOutput7.Visible = false;





        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 2;
        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 3;
            lblParaName.Text = txtParaName.Text;
            lblParaDesc.Text = txtLogicDesc.Text;
        }

        //Back
        protected void Button5_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 1;
        }
        protected void Button6_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 2;
        }
        protected void message_Click(object sender, EventArgs e)
        {


        }
        protected void close_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;

        }
        protected void cb1_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1.Checked == true)
            {
                ddl6.Enabled = true;
                ddl6.BackColor = Color.White;


            }
            else
            {
                ddl6.Enabled = false;
                ddl6.BackColor = Color.FromArgb(217, 217, 217);
                ddl6.SelectedIndex = 0;
                lbl2.Visible = false;
                lbl3.Visible = false;
                lbl4.Visible = false;
                lbl5.Visible = false;
                lblOutput.Visible = false;
            }
        }
        protected void cb2_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2.Checked == true)
            {
                ddl7.Enabled = true;
                ddl7.BackColor = Color.White;

            }
            else
            {
                ddl7.Enabled = false;
                ddl7.BackColor = Color.FromArgb(217, 217, 217);
                ddl7.SelectedIndex = 0;
                lbl6.Visible = false;
                lbl7.Visible = false;
                lbl8.Visible = false;
                lbl9.Visible = false;
                lblOutput2.Visible = false;
            }
        }
        protected void cb3_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3.Checked == true)
            {
                ddl8.Enabled = true;
                ddl8.BackColor = Color.White;

            }
            else
            {
                ddl8.Enabled = false;
                ddl8.BackColor = Color.FromArgb(217, 217, 217);
                ddl8.SelectedIndex = 0;
                lbl10.Visible = false;
                lbl11.Visible = false;
                lbl12.Visible = false;
                lbl13.Visible = false;
                lblOutput3.Visible = false;
            }
        }
        protected void cb4_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4.Checked == true)
            {
                ddl9.Enabled = true;
                ddl9.BackColor = Color.White;

            }
            else
            {
                ddl9.Enabled = false;
                ddl9.BackColor = Color.FromArgb(217, 217, 217);
                ddl9.SelectedIndex = 0;
                lbl14.Visible = false;
                lbl15.Visible = false;
                lbl16.Visible = false;
                lbl17.Visible = false;
                lblOutput4.Visible = false;
            }
        }
        protected void cb5_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5.Checked == true)
            {
                ddl10.Enabled = true;
                ddl10.BackColor = Color.White;

            }
            else
            {
                ddl10.Enabled = false;
                ddl10.BackColor = Color.FromArgb(217, 217, 217);
                ddl10.SelectedIndex = 0;
                lbl18.Visible = false;
                lbl19.Visible = false;
                lbl20.Visible = false;
                lbl21.Visible = false;
                lblOutput5.Visible = false;
            }
        }
        protected void cb6_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6.Checked == true)
            {
                ddl11.Enabled = true;
                ddl11.BackColor = Color.White;

            }
            else
            {
                ddl11.Enabled = false;
                ddl11.BackColor = Color.FromArgb(217, 217, 217);
                ddl11.SelectedIndex = 0;
                lbl22.Visible = false;
                lbl23.Visible = false;
                lbl24.Visible = false;
                lbl25.Visible = false;
                lblOutput6.Visible = false;
            }
        }
        protected void cb7_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7.Checked == true)
            {
                ddl12.Enabled = true;
                ddl12.BackColor = Color.White;

            }
            else
            {
                ddl12.Enabled = false;
                ddl12.BackColor = Color.FromArgb(217, 217, 217);
                ddl12.SelectedIndex = 0;
                lbl26.Visible = false;
                lbl27.Visible = false;
                lbl28.Visible = false;
                lbl29.Visible = false;
                lblOutput7.Visible = false;
            }
        }
        protected void ddl6_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl6.SelectedItem.ToString() == "-----------Select----------")
            {
                lbl2.Visible = false;
                lbl3.Visible = false;
                lbl4.Visible = false;
                lbl5.Visible = false;
                lblOutput.Visible = false;

                Label3.Visible = false;
                Label4.Visible = false;
                Label5.Visible = false;
                Label6.Visible = false;
                Label7.Visible = false;
            }
            else if (ddl6.SelectedItem.ToString() != "-----------Select----------")
            {

                lblOutput.Visible = true;
                lbl2.Visible = true;
                lbl3.Visible = true;
                lbl4.Visible = true;
                lblOutput.Text = ddl6.SelectedItem.ToString();
                lbl5.Visible = true;
                lblUSP.Visible = true;
                lblThen.Visible = true;

                Label3.Visible = true;
                Label4.Visible = true;
                Label4.Text = ddl6.SelectedItem.ToString();
                Label5.Visible = true;
                Label6.Visible = true;
                Label7.Visible = true;
                lblUSP2.Visible = true;
                lblThen2.Visible = true;


            }

        }
        protected void ddl7_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl7.SelectedItem.ToString() == "-----------Select----------")
            {
                lbl6.Visible = false;
                lbl7.Visible = false;
                lbl8.Visible = false;
                lbl9.Visible = false;
                lblOutput2.Visible = false;

                Label8.Visible = false;
                Label9.Visible = false;
                Label10.Visible = false;
                Label11.Visible = false;
                Label12.Visible = false;
            }
            else if (ddl7.SelectedItem.ToString() != "-----------Select----------")
            {
                if (ddl6.SelectedItem.ToString() != "-----------Select----------")
                {

                    lbl6.Text = "AND If the ";
                    lblOutput2.Visible = true;
                    lbl6.Visible = true;
                    lbl7.Visible = true;
                    lbl8.Visible = true;
                    lblOutput2.Text = ddl7.SelectedItem.ToString();
                    lbl9.Visible = true;
                    lblUSP.Visible = true;
                    lblThen.Visible = true;

                    Label8.Text = "AND If the ";
                    Label8.Visible = true;
                    Label9.Visible = true;
                    Label10.Text = ddl7.SelectedItem.ToString();
                    Label10.Visible = true;
                    Label11.Visible = true;
                    Label12.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;
                }
                else
                {

                    lbl6.Text = "If the ";
                    lblOutput2.Visible = true;
                    lbl6.Visible = true;
                    lbl7.Visible = true;
                    lbl8.Visible = true;
                    lblOutput2.Text = ddl7.SelectedItem.ToString();
                    lbl9.Visible = true;
                    lblUSP.Visible = true;
                    lblThen.Visible = true;

                    Label8.Text = "If the ";
                    Label8.Visible = true;
                    Label9.Visible = true;
                    Label10.Text = ddl7.SelectedItem.ToString();
                    Label10.Visible = true;
                    Label11.Visible = true;
                    Label12.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;
                }

            }
        }
        protected void ddl8_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl8.SelectedItem.ToString() == "-----------Select----------")
            {
                lbl10.Visible = false;
                lbl11.Visible = false;
                lbl12.Visible = false;
                lbl13.Visible = false;
                lblOutput3.Visible = false;

                Label13.Visible = false;
                Label14.Visible = false;
                Label15.Visible = false;
                Label16.Visible = false;
                Label17.Visible = false;
            }
            else if (ddl8.SelectedItem.ToString() != "-----------Select----------")
            {
                if (ddl6.SelectedItem.ToString() != "-----------Select----------" || ddl7.SelectedItem.ToString() != "-----------Select----------")
                {

                    lbl10.Text = "AND If the ";
                    lblOutput2.Visible = true;
                    lbl10.Visible = true;
                    lbl11.Visible = true;
                    lbl12.Visible = true;
                    lblOutput3.Text = ddl8.SelectedItem.ToString();
                    lbl13.Visible = true;
                    lblUSP.Visible = true;
                    lblThen.Visible = true;

                    Label13.Text = "AND If the ";
                    Label13.Visible = true;
                    Label14.Visible = true;
                    Label15.Text = ddl8.SelectedItem.ToString();
                    Label15.Visible = true;
                    Label16.Visible = true;
                    Label17.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;
                }
                else
                {

                    lbl10.Text = "If the ";
                    lblOutput2.Visible = true;
                    lbl10.Visible = true;
                    lbl11.Visible = true;
                    lbl12.Visible = true;
                    lblOutput3.Text = ddl8.SelectedItem.ToString();
                    lbl13.Visible = true;
                    lblUSP.Visible = true;
                    lblThen.Visible = true;

                    Label13.Text = "If the ";
                    Label13.Visible = true;
                    Label14.Visible = true;
                    Label15.Text = ddl8.SelectedItem.ToString();
                    Label15.Visible = true;
                    Label16.Visible = true;
                    Label17.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;
                }

            }
        }
        protected void ddl9_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl9.SelectedItem.ToString() == "-----------Select----------")
            {
                lbl14.Visible = false;
                lbl15.Visible = false;
                lbl16.Visible = false;
                lbl17.Visible = false;
                lblOutput4.Visible = false;

                Label18.Visible = false;
                Label19.Visible = false;
                Label20.Visible = false;
                Label21.Visible = false;
                Label22.Visible = false;

            }
            else if (ddl9.SelectedItem.ToString() != "-----------Select----------")
            {
                if (ddl6.SelectedItem.ToString() != "-----------Select----------" || ddl7.SelectedItem.ToString() != "-----------Select----------"
                    || ddl8.SelectedItem.ToString() != "-----------Select----------")
                {

                    lbl14.Text = "AND If the ";
                    lblOutput2.Visible = true;
                    lbl14.Visible = true;
                    lbl15.Visible = true;
                    lbl16.Visible = true;
                    lblOutput4.Text = ddl9.SelectedItem.ToString();
                    lbl17.Visible = true;
                    lblUSP.Visible = true;
                    lblThen.Visible = true;

                    Label18.Text = "AND If the ";
                    Label18.Visible = true;
                    Label19.Visible = true;
                    Label20.Text = ddl9.SelectedItem.ToString();
                    Label20.Visible = true;
                    Label21.Visible = true;
                    Label22.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;

                }
                else
                {

                    lbl14.Text = "If the ";
                    lblOutput2.Visible = true;
                    lbl14.Visible = true;
                    lbl15.Visible = true;
                    lbl16.Visible = true;
                    lblOutput4.Text = ddl9.SelectedItem.ToString();
                    lbl17.Visible = true;
                    lblUSP.Visible = true;
                    lblThen.Visible = true;

                    Label18.Text = "If the ";
                    Label18.Visible = true;
                    Label19.Visible = true;
                    Label20.Text = ddl9.SelectedItem.ToString();
                    Label20.Visible = true;
                    Label21.Visible = true;
                    Label22.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;


                }

            }
        }
        protected void ddl10_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl10.SelectedItem.ToString() == "-----------Select----------")
            {
                lbl18.Visible = false;
                lbl19.Visible = false;
                lbl20.Visible = false;
                lbl21.Visible = false;
                lblOutput5.Visible = false;

                Label23.Visible = false;
                Label24.Visible = false;
                Label25.Visible = false;
                Label26.Visible = false;
                Label27.Visible = false;
            }
            else if (ddl10.SelectedItem.ToString() != "-----------Select----------")
            {
                if (ddl6.SelectedItem.ToString() != "-----------Select----------" || ddl7.SelectedItem.ToString() != "-----------Select----------"
                    || ddl8.SelectedItem.ToString() != "-----------Select----------" || ddl9.SelectedItem.ToString() != "-----------Select----------")
                {

                    lbl18.Text = "AND If the ";
                    lblOutput2.Visible = true;
                    lbl18.Visible = true;
                    lbl19.Visible = true;
                    lbl20.Visible = true;
                    lblOutput5.Text = ddl10.SelectedItem.ToString();
                    lbl21.Visible = true;
                    lblUSP.Visible = true;
                    lblThen.Visible = true;

                    Label23.Text = "AND If the ";
                    Label23.Visible = true;
                    Label24.Visible = true;
                    Label25.Text = ddl10.SelectedItem.ToString();
                    Label25.Visible = true;
                    Label26.Visible = true;
                    Label27.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;
                }
                else
                {
                    lbl18.Text = "If the ";
                    lblOutput2.Visible = true;
                    lbl18.Visible = true;
                    lbl19.Visible = true;
                    lbl20.Visible = true;
                    lblOutput5.Text = ddl10.SelectedItem.ToString();
                    lbl21.Visible = true;
                    lblUSP.Visible = true;
                    lblThen.Visible = true;

                    Label23.Text = "If the ";
                    Label23.Visible = true;
                    Label24.Visible = true;
                    Label25.Text = ddl10.SelectedItem.ToString();
                    Label25.Visible = true;
                    Label26.Visible = true;
                    Label27.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;
                }

            }
        }
        protected void ddl11_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl11.SelectedItem.ToString() == "-----------Select----------")
            {
                lbl22.Visible = false;
                lbl23.Visible = false;
                lbl24.Visible = false;
                lbl25.Visible = false;
                lblOutput6.Visible = false;

                Label28.Visible = false;
                Label29.Visible = false;
                Label30.Visible = false;
                Label31.Visible = false;
                Label32.Visible = false;
            }
            else if (ddl11.SelectedItem.ToString() != "-----------Select----------")
            {
                if (ddl6.SelectedItem.ToString() != "-----------Select----------" || ddl7.SelectedItem.ToString() != "-----------Select----------"
                    || ddl8.SelectedItem.ToString() != "-----------Select----------" || ddl9.SelectedItem.ToString() != "-----------Select----------"
                    || ddl10.SelectedItem.ToString() != "-----------Select----------")
                {

                    lbl22.Text = "AND If the ";
                    lblOutput2.Visible = true;
                    lbl22.Visible = true;
                    lbl23.Visible = true;
                    lbl24.Visible = true;
                    lblOutput6.Text = ddl11.SelectedItem.ToString();
                    lbl25.Visible = true;
                    lblUSP.Visible = true;
                    lblThen.Visible = true;


                    Label28.Text = "AND If the ";
                    Label28.Visible = true;
                    Label29.Visible = true;
                    Label30.Text = ddl11.SelectedItem.ToString();
                    Label30.Visible = true;
                    Label31.Visible = true;
                    Label32.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;
                }
                else
                {
                    lbl22.Text = "If the ";
                    lblOutput2.Visible = true;
                    lbl22.Visible = true;
                    lbl23.Visible = true;
                    lbl24.Visible = true;
                    lblOutput6.Text = ddl11.SelectedItem.ToString();
                    lbl25.Visible = true;
                    lblThen.Visible = true;

                    Label28.Text = "If the ";
                    Label28.Visible = true;
                    Label29.Visible = true;
                    Label30.Text = ddl11.SelectedItem.ToString();
                    Label30.Visible = true;
                    Label31.Visible = true;
                    Label32.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;
                }
            }
        }
        protected void ddl12_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl12.SelectedItem.ToString() == "-----------Select----------")
            {
                lbl26.Visible = false;
                lbl27.Visible = false;
                lbl28.Visible = false;
                lbl29.Visible = false;
                lblOutput7.Visible = false;

                Label33.Visible = false;
                Label34.Visible = false;
                Label35.Visible = false;
                Label36.Visible = false;
                Label37.Visible = false;

            }
            else if (ddl12.SelectedItem.ToString() != "-----------Select----------")
            {
                if (ddl6.SelectedItem.ToString() != "-----------Select----------" || ddl7.SelectedItem.ToString() != "-----------Select----------"
                    || ddl8.SelectedItem.ToString() != "-----------Select----------" || ddl9.SelectedItem.ToString() != "-----------Select----------"
                    || ddl10.SelectedItem.ToString() != "-----------Select----------" || ddl11.SelectedItem.ToString() != "-----------Select----------")
                {

                    lbl26.Text = "AND If the ";
                    lblOutput2.Visible = true;
                    lbl26.Visible = true;
                    lbl27.Visible = true;
                    lbl28.Visible = true;
                    lblOutput7.Text = ddl12.SelectedItem.ToString();
                    lbl29.Visible = true;
                    lblUSP.Visible = true;
                    lblThen.Visible = true;

                    Label33.Text = "AND If the ";
                    Label33.Visible = true;
                    Label34.Visible = true;
                    Label35.Text = ddl12.SelectedItem.ToString();
                    Label35.Visible = true;
                    Label36.Visible = true;
                    Label37.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;
                }
                else
                {
                    lbl26.Text = "If the ";
                    lblOutput2.Visible = true;
                    lbl26.Visible = true;
                    lbl27.Visible = true;
                    lbl28.Visible = true;
                    lblOutput7.Text = ddl12.SelectedItem.ToString();
                    lbl29.Visible = true;
                    lblUSP.Visible = true;
                    lblThen.Visible = true;

                    Label33.Text = "If the ";
                    Label33.Visible = true;
                    Label34.Visible = true;
                    Label35.Text = ddl12.SelectedItem.ToString();
                    Label35.Visible = true;
                    Label36.Visible = true;
                    Label37.Visible = true;
                    lblUSP2.Visible = true;
                    lblThen2.Visible = true;

                }
            }
        }
        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        protected void ddl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl11.SelectedItem.ToString() == "Weekly")
            {
                lblShow.Text = "of the Week";
            }
            else if (ddl1.SelectedItem.ToString() == "Monthly")
            {
                lblShow.Text = "of the Month";
            }
            else
            {
                lblShow.Text = "of the Week";
            }
        }


    }
}
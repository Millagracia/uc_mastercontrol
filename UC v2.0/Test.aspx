﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="UC_v2._0.Test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 733px">
        <div class="content">
             <h2>Greetings</h2>
        <div class="col-lg-2">
            <div class="form-group">
                <label for="usr">Select Sort:</label>
                <asp:ListBox ID="lblAlpha" runat="server" class="form-control" SelectionMode="Multiple" multiple="multiple" ClientIDMode="Static"></asp:ListBox>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label for="usr">USP:</label>
                <asp:ListBox ID="lbUSP" runat="server" class="form-control" SelectionMode="Multiple" multiple="multiple" ClientIDMode="Static"></asp:ListBox>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
            </div>
        </div>
        </div>
       

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>

        $(document).ready(function () {
            $('#lbUSP').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 200
            });
            ddlAplha();
        });
        function ddlVendor()
        {
            $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            var select = document.getElementById("lblAlpha");
            var itemSelec = "";
            var listLength = select.options.length;
            for (var i = 0; i < listLength; i++) {
                if (select.options[i].selected)
                    if (itemSelec == "") {
                        itemSelec = select.options[i].value ;
                    }
                    else {
                        itemSelec = itemSelec + ',' + select.options[i].value  ;
                    }
            }
          

            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: 'Test.aspx/GetVendorList',
                dataType: "json",
                data: '{"Alpha" : "' + itemSelec + '"}',
                success: function (data) {
                   
                    var d = $.parseJSON(data.d);
                    if (d.Success) {
                        $('#lbUSP').empty();
                        $('#lbUSP').multiselect('destroy');
                        var records = d.data.asd;
                        $.each(records, function (idx, val) {
                            var selected = 'selected="selected"';
                            ta2 = "<option value='" + val.VendorID + "' >" + val.VendorID + "&nbsp&nbsp&nbsp--&nbsp&nbsp&nbsp   " + val.VendorName.toUpperCase() + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + "</option>";
                            $('#lbUSP').append(ta2);
                        });
                        $('#lbUSP').multiselect({
                            numberDisplayed: 0,
                            includeSelectAllOption: true,
                            enableFiltering: true,
                            enableCaseInsensitiveFiltering: true,
                            maxHeight: 200
                        });
                    }
                    $('#modalLoading').modal('hide');
                }, error: function (response) {
                    console.log(response.responseText);
                }
            });
        }
        function ddlAplha() {
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: 'Test.aspx/GetAlpha',
                dataType: "json",
               
                success: function (data) {
                    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
                    var d = $.parseJSON(data.d);
                    if (d.Success) {
                        $('#lblAlpha').empty();
                        $('#lblAlpha').multiselect('destroy');
                        var records = d.data.asd;
                        $.each(records, function (idx, val) {
                            var selected = 'selected="selected"';
                            var valueStr = val.NewColumnfrom;
                            ta2 = "<option value='" + val.NewColumnfrom + "' >" + val.NewColumnfrom + "</option>";
                            $('#lblAlpha').append(ta2);
                        });
                        $('#lblAlpha').multiselect({
                            onChange: function(option, checked, select) {
                                ddlVendor();
                            },
                            numberDisplayed: 0,
                            includeSelectAllOption: false,
                            maxHeight: 200
                        });
                    }
                    $('#modalLoading').modal('hide');
                }, error: function (response) {
                    console.log(response.responseText);
                }
            });
        }
        
    </script>


</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string connStr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString.ToString();

            DataTable dt = new DataTable();

            string Query = "Select DISTINCT a.USP_ID, a.USP_Name, b.[Bulk Email] as 'bulk', b.[Online Request] as 'online', b.[EDI 814] as 'edi', b.Call ";
            Query += " From [dbo].[tbl_UC_USP_Name_Vendor_ID] a INNER JOIN [dbo].[tbl_UC_USPcapabilities_Start_Service] b ";
            Query += " ON b.USP_ID = a.USP_ID ";

            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(Query, con);
            da.Fill(dt);
            con.Close();
        }
    }
}
﻿using iTextSharp.text.pdf;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UC_v2._0
{
    public partial class PDFMap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string GetPDF()
        {
            clsConnection cls = new clsConnection();
            DataTable dtPDF = new DataTable();

            string query = "select * from tbl_UC_Mapping_PDF where date_deleted is null order by id";

            dtPDF = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtPDF } });
        }

        #region New Update
        [WebMethod]
        public static string Transfer()
        {
            List<string> fileNames = new List<string> { };

            //string source = HttpContext.Current.Server.MapPath("~/Files/PDF Files/" + pdf);
            //string extSrc = HttpContext.Current.Server.MapPath("~/Files/Extracted Files/");


            System.IO.DirectoryInfo d = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~/Files/Mapped Files/"));//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.pdf"); //Getting Text files
            string str = "";
            foreach (FileInfo file in Files)
            {
                str = str + ", " + file.Name;
                Console.WriteLine(file.Name);
                fileNames.Add(file.Name);
            }
            for (int i = 0; i < fileNames.Count; i++)
            {
                #region Copy File
                string sourcePath = HttpContext.Current.Server.MapPath("~/Files/Mapped Files/");
                string targetPath = @"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\Utilities\Internal\UC Project\UC_SideBySide\Mapped Files";

                // Use Path class to manipulate file and directory paths.
                string sourceFile = System.IO.Path.Combine(sourcePath, fileNames[i]);
                string destFile = System.IO.Path.Combine(targetPath, fileNames[i]);


                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }
                System.IO.File.Copy(sourceFile, destFile, true);
                #endregion
            }


            return "True";
        }
        #endregion


        [WebMethod]
        public static string GetMapping()
        {
            clsConnection cls = new clsConnection();
            DataTable dtMap = new DataTable();
            DataTable dtCustom = new DataTable();

            string data = "select * from tbl_UC_Mapping_Main where DateDeleted is null order by GroupId";

            dtMap = cls.GetData(data);

            string custom = "select Distinct(GroupName), CAST(description as varchar(MAX)) [description], modified_by, date_modified from tbl_UC_Mapping_Group where custom_tag = 1 and date_deleted is null order by GroupName";

            dtCustom = cls.GetData(custom);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { dataMap = dtMap, customData = dtCustom } });
        }

        [WebMethod]
        public static string GetFields(ArrayList id)
        {
            clsConnection cls = new clsConnection();
            DataTable dtFields = new DataTable();

            //if (tag == "source")
            //{
            //	string query = "select * from tbl_VPR_Mapping_Group where GroupId = " + id[0].ToString() + " order by FieldId, GroupName";

            //	dtFields = cls.GetData(query);
            //}
            //else if (tag == "cust")
            //{
            string val = "";

            foreach (string group in id)
            {
                val += "'" + group.Trim() + "',";
            }

            val = val.Remove(val.Length - 1, 1);

            //string query = "select * from tbl_UC_Mapping_Group where GroupId in (" + val + ") OR GroupName in (" + val + ") order by FieldId, GroupName";
            string query = "select * from tbl_UC_Mapping_Group where GroupId in ('3') OR GroupName in ('UC PDF Mapper v12') order by FieldId, GroupName";
            dtFields = cls.GetData(query);
            //}

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtFields } });
        }
        [WebMethod]
        public static string Getusps()
        {
            clsConnection cls = new clsConnection();


            DataTable dtusps = new DataTable();
            string query = "Select USP_Name as 'USPName', [Email Address] as EmailAddress,Website,[Login Name],Password From tbl_UC_USP_Name_Vendor_ID Where isDeleted = 0 Order BY USP_Name";

            dtusps = cls.GetData(query);


            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { records = dtusps } });

        }

        [WebMethod]
        public static string GetWebsite(string USP)
        {
            clsConnection cls = new clsConnection();
            string[] splitUSP = USP.Split('_');

            DataTable dtusps = new DataTable();
            string query = "Select USP_Name as 'USPName', [Email Address] as EmailAddress,Website,[Login Name],Password From tbl_UC_USP_Name_Vendor_ID Where isDeleted = 0 ";
            query += " AND USP_Name = '" + splitUSP[0] + "'  Order BY USP_Name";
            dtusps = cls.GetData(query);


            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { records = dtusps } });

        }

        [WebMethod]
        public static void SaveCustom(ArrayList myData, string usr)
        {
            clsConnection cls = new clsConnection();

            string query = "insert into tbl_UC_Mapping_Group (GroupId, GroupName, FieldName, SampleData, custom_tag, description, modified_date, date_modified) values ";

            string[] field = myData[2].ToString().Split(',');
            string[] value = myData[3].ToString().Split(',');

            int index = 0;

            foreach (string f in field)
            {
                query += "('" + myData[0] + "', '" + myData[1] + "', '" + f + "', '" + value[index] + "', 1, '" + myData[4] + "', '" + usr + "', GETDATE()),";
                index += 1;
            }

            query = query.Remove(query.Length - 1, 1);

            try
            {
                int exec = cls.ExecuteQuery(query);

                //if (exec != 0)
                //{
                //    ArrayList arrPages = new ArrayList();
                //    arrPages.Add("PDF Mapping");
                //    arrPages.Add("");

                //    cls.FUNC.Audit("Save Custom", arrPages);
                //}
            }
            catch (Exception) { }
        }

        [WebMethod]
        public static string UploadFile(string filename, string usr)
        {
            clsConnection cls = new clsConnection();

            string slct = "select * from tbl_UC_Mapping_PDF where filename = '" + filename + "' and date_deleted is null";
            DataTable dt = cls.GetData(slct);

            if (dt.Rows.Count == 0)
            {
                string ins = "insert into tbl_UC_Mapping_PDF (filename, modified_by, date_modified) values ('" + filename + "', '" + HttpContext.Current.Session["ntid"] + "', GETDATE())";

                try
                {
                    int exec = cls.ExecuteQuery(ins);

                    //if (exec != 0)
                    //{
                    //    ArrayList arrPages = new ArrayList();
                    //    arrPages.Add("PDF Mapping");
                    //    arrPages.Add("");

                    //    cls.FUNC.Audit("Uplaod PDF", arrPages);
                    //}

                    return "0";
                }
                catch (Exception)
                {
                    return "Error";
                }
            }
            else
            {
                string Update = "Update tbl_UC_Mapping_PDF set modified_by ='" + HttpContext.Current.Session["ntid"] + "', date_modified = GETDATE() where  filename = '" + filename + "'";
                cls.ExecuteQuery(Update);
                return "1";
            }
        }

        [WebMethod]
        public static string GetHeaders(string pdf)
        {
            clsConnection cls = new clsConnection();

            string headers = "";

            string source = HttpContext.Current.Server.MapPath("~/Files/PDF Files/" + pdf);
            string extSrc = HttpContext.Current.Server.MapPath("~/Files/Extracted Files/");

            if (IsValidPdf(source))
            {
                PdfReader pdfReader = new PdfReader(source);

                string filename = pdf;

                using (FileStream stream = new FileStream(string.Concat(extSrc, filename), FileMode.Create))
                {
                    PdfStamper pdfStamper = new PdfStamper(pdfReader, stream);

                    AcroFields formFields = default(AcroFields);
                    formFields = pdfStamper.AcroFields;
                    AcroFields.Item item = new AcroFields.Item();
                    PdfDictionary dict = new PdfDictionary();

                    int? flags;

                    headers += "/Files/Extracted Files/" + filename + ",";

                    foreach (var field in formFields.Fields)
                    {
                        headers += field.Key + ",";
                        formFields.SetField(field.Key, field.Key);

                        item = formFields.Fields[field.Key];
                        dict = item.GetMerged(0);

                        try
                        {
                            flags = dict.GetAsNumber(PdfName.FF).IntValue;

                            if ((flags & BaseField.MULTILINE) > 0)
                            {
                                formFields.SetFieldProperty(field.Key, "textsize", 6f, null);
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    pdfStamper.FormFlattening = false;
                    pdfStamper.Close();
                    stream.Close();
                }
                pdfReader.Close();
            }

            return headers;
        }

        [WebMethod]
        public static string PreviewPDF(Object fields, Object values, string pdf, string filename)
        {
            string fn = filename + ".pdf";

            string source = HttpContext.Current.Server.MapPath("~/Files/PDF Files/" + pdf);
            string extSrc1 = HttpContext.Current.Server.MapPath("~/Files/Extracted Files/");

            if (IsValidPdf(source))
            {
                PdfReader pdfReader = new PdfReader(source);

                using (FileStream stream = new FileStream(string.Concat(extSrc1, fn), FileMode.Create))
                {
                    PdfStamper pdfStamper = new PdfStamper(pdfReader, stream);

                    AcroFields formFields = default(AcroFields);
                    formFields = pdfStamper.AcroFields;
                    AcroFields.Item item = new AcroFields.Item();
                    PdfDictionary dict = new PdfDictionary();

                    int? flags;

                    ArrayList arr = new ArrayList();
                    ArrayList arr2 = new ArrayList();
                    ArrayList arr3 = new ArrayList();

                    // Default Form Fields
                    foreach (var field in formFields.Fields)
                    {
                        formFields.SetField(field.Key, "");

                        arr.Add(field.Key);
                    }


                    int b = 0;

                    // Changed Fields/Values
                    foreach (var obj in (dynamic)values)
                    {
                        foreach (var k in (dynamic)obj)
                        {
                            object key = k.Key;
                            object value = k.Value;
                            int z = 1;

                            formFields.SetField(key.ToString(), value.ToString());

                            item = formFields.Fields[key.ToString()];
                            dict = item.GetMerged(0);

                            try
                            {
                                flags = dict.GetAsNumber(PdfName.FF).IntValue;

                                if ((flags & BaseField.MULTILINE) > 0)
                                {
                                    formFields.SetFieldProperty(key.ToString(), "textsize", 6f, null);
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }

                    int y = 0;

                    foreach (var objF in (dynamic)fields)
                    {
                        foreach (var f in (dynamic)objF)
                        {
                            object key = f.Key;
                            object value = f.Value;

                            int x = 1;

                            string n = "";
                            Regex rx = new Regex(@"(_+[0-9])", RegexOptions.IgnoreCase);

                            Match mt = rx.Match(value.ToString());

                            if (mt.Success)
                            {
                                n = value.ToString().Replace(mt.Groups[1].Value, "");
                            }
                            else
                            {
                                n = value.ToString();
                            }

                            if (!arr.Contains(n) || !arr2.Contains(n))
                            {
                                formFields.RenameField(key.ToString(), n);
                                arr.Add(n);
                                arr2.Add(n);
                            }
                            else
                            {
                                n = n + "__" + (x + 1);

                                while (arr.Contains(n))
                                {
                                    n = n.Remove(n.Length - 1, 1);
                                    x = x + 1;
                                    n = n + x;
                                }

                                formFields.RenameField(key.ToString(), n);
                                arr.Add(n);
                                arr2.Add(n);
                            }
                        }
                    }

                    foreach (var field in formFields.Fields)
                    {
                        arr3.Add(field);
                    }

                    pdfStamper.FormFlattening = false;
                    pdfStamper.Close();
                    stream.Close();
                }
                pdfReader.Close();
            }

            return "/Files/Extracted Files/" + fn;
        }

        [WebMethod]
        public static string SavePDF(ArrayList myData, string usr)
        {

            clsConnection cls = new clsConnection();

            //if (myData[2].ToString() != "new")
            //{
            //if (myData[2].ToString() == "overwrite")
            //{
            //    string qry = "update tbl_UC_Mapping_PDF set deleted_by = '" + usr + "', date_deleted = GETDATE() where client_name = '" + myData[3].ToString() + "' and date_deleted is null";

            //    cls.ExecuteQuery(qry);
            //}
            string[] GetNewFolder = myData[0].ToString().Split('_');
            string A_D = "";
            if (GetNewFolder[1] == "Start")
            {
                A_D = @"\Activation";
            }
            else
            {
                A_D = @"\Deactivation";
            }

            string source1 = HttpContext.Current.Server.MapPath("~/Files/Extracted Files/" + myData[0].ToString() + ".pdf");
            string source2 = HttpContext.Current.Server.MapPath("~/Files/PDF Files/" + myData[0].ToString() + ".pdf");
            string dest = HttpContext.Current.Server.MapPath("~/Files/Mapped Files/"
                + GetNewFolder[0] + A_D + "\\" + myData[0].ToString() + ".pdf");
            //string dest = HttpContext.Current.Server.MapPath("~/Files/Mapped Files/" + myData[0].ToString() + ".pdf");
            string shared = HttpContext.Current.Server.MapPath("~/Files/Shared Files/" + myData[0].ToString() + ".pdf");


            string targetPath = @"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\Utilities\Internal\UC Project\UC_SideBySide\Mapped Files\"
                 + GetNewFolder[0] + A_D + "\\" + myData[0].ToString() + ".pdf";

            string CheckFileFolder = @"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\Utilities\Internal\UC Project\UC_SideBySide\Mapped Files\"
            + GetNewFolder[0] + A_D;


            string targetPath1 = @"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\Utilities\Internal\UC Project\UC_SideBySide\Mapped Files\"
                    + myData[0].ToString() + ".pdf";


            string destCreate = HttpContext.Current.Server.MapPath("~/Files/Mapped Files/"
                    + GetNewFolder[0] + A_D);

            if (!System.IO.Directory.Exists(destCreate))
            {
                System.IO.Directory.CreateDirectory(destCreate);

            }
            else
            {
            }




            try
            {
                try
                {
                    string qryUpdate = "Update tbl_UC_Mapping_PDF Set FileFolder = '"
                + GetNewFolder[0] + A_D + "', isNew  = 1 where filename = '" + myData[0].ToString() + "'";
                    cls.ExecuteQuery(qryUpdate);
                    File.Copy(source1, dest, true);
                    //   File.Copy(source1, targetPath1, true);

                    string qryInsertCopy = "select (select Top 1 id from tbl_UC_Mapping_PDF where isNew = 1) as 'id' ,  id_SBS, '1' as 'inTemp' from tbl_UC_SBS_Users ";
                    DataTable dtInsert = new DataTable();
                    dtInsert = cls.GetData(qryInsertCopy);

                    for (int i = 0; i < dtInsert.Rows.Count; i++)
                    {
                        string checkInTemp = "select * From tbl_UC_SBS_Temp where id = '" + dtInsert.Rows[i][0].ToString() + "' ";
                        checkInTemp += "And id_SBS = '" + dtInsert.Rows[i][1].ToString() + "'";
                        DataTable dtCheckInTemp = new DataTable();

                        dtCheckInTemp = cls.GetData(checkInTemp);

                        if(dtCheckInTemp.Rows.Count != 0 )
                        {
                            if(dtCheckInTemp.Rows[i][2].ToString() == "0")
                            {
                                string qryUpdateTemp = "Update tbl_UC_SBS_Temp Set inTemp = 1 where id = "
                                 + dtCheckInTemp.Rows[i][0].ToString() + " and id_SBS = " + dtCheckInTemp.Rows[i][1].ToString();

                                cls.ExecuteQuery(qryUpdateTemp);
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            string qryInsertTemp = "Insert Into tbl_UC_SBS_Temp (id, id_SBS,inTemp) values('" + dtInsert.Rows[i][0].ToString() +
                                "', '" + dtInsert.Rows[i][1].ToString() + "','1')";
                            cls.ExecuteQuery(qryInsertTemp);
                        }

                    }


                }
                catch (Exception ex)
                {
                    return "Error: " + ex.ToString();
                }

                string updt = "update tbl_UC_Mapping_PDF set description = '" + myData[1].ToString() + "', ";
                updt += "modified_by = '" + usr + "', ";
                updt += "date_modified = GETDATE() where filename = '" + myData[0].ToString() + "'";

                try
                {
                    int exec = cls.ExecuteQuery(updt);

                    //if (exec != 0)
                    //{
                    //    ArrayList arrPages = new ArrayList();
                    //    arrPages.Add("PDF Mapping");
                    //    arrPages.Add("");

                    //    cls.FUNC.Audit("Save PDF", arrPages);
                    //}

                    if (IsValidPdf(source1))
                    {
                        PdfReader pdfReader = new PdfReader(source1);

                        using (FileStream stream = new FileStream(source2, FileMode.Create))
                        {
                            PdfStamper pdfStamper = new PdfStamper(pdfReader, stream);

                            AcroFields formFields = default(AcroFields);
                            formFields = pdfStamper.AcroFields;
                            AcroFields.Item item = new AcroFields.Item();
                            PdfDictionary dict = new PdfDictionary();

                            int? flags;

                            foreach (var field in formFields.Fields)
                            {
                                formFields.SetField(field.Key, "");

                                item = formFields.Fields[field.Key];
                                dict = item.GetMerged(0);

                                try
                                {
                                    flags = dict.GetAsNumber(PdfName.FF).IntValue;

                                    if ((flags & BaseField.MULTILINE) > 0)
                                    {
                                        formFields.SetFieldProperty(field.Key, "textsize", 6f, null);
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }

                            pdfStamper.FormFlattening = false;
                            pdfStamper.Close();
                            stream.Close();
                        }
                        pdfReader.Close();
                    }

                    //File.Copy(source2, source1, true);
                    try
                    {
                        File.Copy(source2, shared, true);
                    }
                    catch (Exception ex)
                    {
                        return "Error: " + ex.ToString();
                    }

                    return "1";
                }
                catch (Exception ex)
                {
                    return "Error: " + ex.ToString();
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.ToString();
            }
            //}
            //else
            //{
            //    string qry = "select * from tbl_UC_Mapping_PDF where municipality = '" + myData[2].ToString() + "' and date_deleted is null";

            //    DataTable dt = cls.GetData(qry);

            //    if (dt.Rows.Count > 0)
            //    {
            //        return "2";
            //    }
            //    else
            //    {
            //        return "3";
            //    }
            //}
        }

        public static bool IsValidPdf(string filepath)
        {
            bool Ret = true;

            PdfReader reader = null;

            try
            {
                reader = new PdfReader(filepath);
                reader.Close();
            }
            catch
            {
                Ret = false;
            }

            return Ret;
        }

        public static int CountStringOccurrences(string text, string pattern)
        {
            // Loop through all instances of the string 'text'.
            int count = 0;
            int i = 0;
            //int x = 0;
            while ((i = text.IndexOf(pattern, i)) != -1)
            {
                i += pattern.Length;
                count++;
            }

            return count;
        }

        [WebMethod]
        public static string SelectState()
        {
            clsConnection cls = new clsConnection();

            string qry = "select DISTINCT(LTRIM(RTRIM(State))) [State] from tbl_DB_US_States order by [State]";

            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }

        [WebMethod]
        public static string SelectMunicipality(string state)
        {
            clsConnection cls = new clsConnection();

            string qry = "select DISTINCT(LTRIM(RTRIM(Municipality))) [Municipality] from tbl_DB_US_States where LTRIM(RTRIM(State)) = '" + state.Trim() + "' order by [Municipality]";

            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }

        [WebMethod]
        public static string SelectZip(string state, string municipality)
        {
            clsConnection cls = new clsConnection();

            string qry = "select DISTINCT(LTRIM(RTRIM(ZipCode))) [Zip] from tbl_DB_US_States where LTRIM(RTRIM(State)) = '" + state.Trim() + "' and LTRIM(RTRIM(Municipality)) = '" + municipality.Trim() + "' order by Zip";

            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }
    }
}
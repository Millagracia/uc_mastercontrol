﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="BusinessRequirements.aspx.cs" Inherits="UC_v2._0.BusinessRequirements" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        table tbody tr:hover {
            background: #81c341;
            cursor: pointer;
        }

        table tbody tr.selected {
            background: #81c341;
        }



        select {
          
            -moz-appearance: none;
            appearance: none;
            padding: 3px 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="content-wrapper">
        &nbsp&nbsp&nbsp
        <asp:Label ID="ntid" Visible="false" runat="server" Text="Label"></asp:Label>
         <div class="content">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_Down" data-toggle="tab" aria-expanded="true">Downstream Rules</a></li>
                    <li class=""><a href="#tab_Busi" id="Tusp" data-toggle="tab" aria-expanded="false">Business Requirements</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_Down">
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                          <div class="box box-default">
                                        <div class="box-body" style="padding: 2px">
                                            <div class="col-xs-2" style="padding-left: 0px; padding-right: 0px;">
                                                <div class="panel panel-primary" style="height: 517px; overflow-y: scroll;">
                                                    <div class="panel-heading text-center">
                                                        <label>Client</label>
                                                    </div>
                                                    <div class="panel-body">
                                                        <table id="tblClient" class="table" data-util="Gas">
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-xs-10" style="padding-left: 0px;">
                                                <div id="dvRules" style="display:none">
                                                    <div class="panel panel-primary" id="pRules" style="height: 517px; overflow-y: scroll; padding-left: 0px;">
                                                        <div class="panel-heading text-center">
                                                            <label>Rules</label>
                                                        </div>
                                                        <div class="panel-body">
                                                            <table id="tblBusinessRule"  class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th data-field="Task">Task</th>
                                                                        <th data-field="Status">Status</th>
                                                                        <th data-field="Utility">Utility</th>
                                                                        <th data-field="Requires_work_Item">Requires work item y/n</th>
                                                                        <th data-field="Caegory_Item">Category Name</th>
                                                                        <th data-field="">Line Item Name</th>
                                                                        <th data-field="Call ASPS">(and/or)</th>
                                                                        <th data-field="WI_Status">WI Status</th>
                                                                        <th data-field="Follow_UP">Follow up Period</th>
                                                                        <th data-field="id" class="text-center" data-formatter="edit"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div id="dvNewRules" style="display: none">
                                                    <div class="panel panel-primary" style="height: 517px; overflow-y: scroll; padding-left: 0px;">
                                                        <div class="panel-heading text-center">
                                                            <label>New Rules</label>
                                                        </div>
                                                        <div class="panel-body">

                                                             <div class="row">
                                                                 <div class="col-lg-9" style="padding:0px">
                                                                      <div class="col-lg-3" style="padding-right: 1px">
                                                                          </div>
                                                                        <div class="col-lg-3" style="padding-right: 1px">
                                                                          </div>
                                                                        <div class="col-lg-3" style="padding-right: 1px">
                                                                          </div>
                                                                       <div class="col-lg-3 text-left" style="padding: 1px">
                                                                              Additional Condition (if needed) :
                                                                          </div>

                                                                 </div>
                                                                  <div class="col-lg-3" style="padding-left:5px; ">
                                                                     Follow up Period :
                                                                  </div>
                                                             </div>

                                                            <div class="row">

                                                                <div class="col-lg-9" style="padding:0px">
                                                                     <div>
                                                                <div class="col-lg-3" style="padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                        <select class="form-control input-sm" style="padding: 0px" id="ddlTask">
                                                                            <option value="" disabled="disabled" selected="selected">Task</option>
                                                                            <option value="1">Start Service</option>
                                                                            <option value="2">Stop Service</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                        <select id="ddlStatus" class="form-control input-sm" style="padding: 0px">
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                        <select class="form-control input-sm" style="padding: 0px" id="ddlUtility">
                                                                            <option value="" disabled="disabled" selected="selected">Utility</option>
                                                                            <option value="1">Gas</option>
                                                                            <option value="2">Electric</option>
                                                                            <option value="2">Water</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                        <select class="form-control input-sm" style="padding: 0px" id="ddlworkItemYN">
                                                                            <option value="" disabled="disabled" selected="selected">Work item (Y/N)</option>
                                                                            <option value="1">Yes</option>
                                                                            <option value="2">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                       <select class="form-control input-sm" style="padding: 0px" id="ddlCategory">
                                                                            <option value="" disabled="disabled" selected="selected">Category Name</option>
                                                                            <option value="1">Utility</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px" id="ddlLineItem">
                                                                        <option value="" disabled="disabled" selected="selected">Line item Name</option>
                                                                        <option value="1">Start Service</option>
                                                                        <option value="2">Stop Service</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-8" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px" id="ddlWorkItemStatus">
                                                                        <option value="" disabled="disabled" selected="selected">Work item Status</option>
                                                                        <option value="1">Start Service</option>
                                                                        <option value="2">Stop Service</option>
                                                                    </select>
                                                                    </div>
                                                                    <div class="col-lg-4" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px" id="ddlAndOr">
                                                                        <option value="" disabled="disabled" selected="selected">And/Or</option>
                                                                        <option value="1">And</option>
                                                                        <option value="2">Or</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding:0px">
                                                                      <div >
                                                                <div class="col-lg-12" style="padding-left: 0px; padding-right: 1px;border-left:solid">
                                                                    <div class="col-lg-2" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px" id="ddlNo">
                                                                        <option value="" disabled="disabled" selected="selected">#</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                             <option value="2">3</option>
                                                                             <option value="2">4</option>
                                                                             <option value="2">5</option>
                                                                             <option value="2">6</option>
                                                                             <option value="2">7</option>
                                                                             <option value="2">8</option>
                                                                             <option value="2">9</option>
                                                                             <option value="2">10</option>
                                                                             <option value="2">11</option>
                                                                             <option value="2">12</option>
                                                                             <option value="2">13</option>
                                                                             <option value="2">14</option>
                                                                             <option value="2">15</option>
                                                                             <option value="2">16</option>
                                                                             <option value="2">17</option>
                                                                             <option value="2">18</option>
                                                                             <option value="2">19</option>
                                                                             <option value="2">20</option>
                                                                    </select>
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px" id="ddlBusCal">
                                                                        <option value="" disabled="disabled" selected="selected">Select Item</option>
                                                                        <option value="1">Business</option>
                                                                        <option value="2">Calendar</option>
                                                                    </select>
                                                                    </div>
                                                                    <div class="col-lg-4" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px" id="ddlDWMY">
                                                                             <option value="" disabled="disabled" selected="selected">Select Item</option>
                                                                             <option value="1">Days</option>
                                                                             <option value="2">Week</option>
                                                                             <option value="2">Months</option>
                                                                             <option value="2">Years</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="dvCondition1" style="display:none">

                                                                <div class="col-lg-9" style="padding:0px">
                                                                     <div>
                                                                <div class="col-lg-3" style="padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                      
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                     
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                   
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                     
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                    
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px">
                                                                        <option value="" disabled="disabled" selected="selected">Line item Name</option>
                                                                        <option value="1">Start Service</option>
                                                                        <option value="2">Stop Service</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-8" style="padding: 1px">
                                                                    
                                                                    </div>
                                                                    <div class="col-lg-4" style="padding: 1px" id="ddlAndOR1">
                                                                         <select class="form-control input-sm" style="padding: 0px">
                                                                        <option value="" disabled="disabled" selected="selected">And/Or</option>
                                                                        <option value="1">And</option>
                                                                        <option value="2">Or</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding:0px">
                                                                      <div >
                                                                <div class="col-lg-12" style="padding-left: 0px; padding-right: 1px;border-left:solid">
                                                                    <div class="col-lg-2" style="padding: 1px">
                                                                
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                 
                                                                    </div>
                                                                    <div class="col-lg-4" style="padding: 1px">
                                                                     
                                                                    </div>
                                                                </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="dvCondition2" style="display:none">
                                                                <div class="col-lg-9" style="padding:0px"><div>
                                                                <div class="col-lg-3" style="padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px"></div>
                                                                    <div class="col-lg-6" style="padding: 1px"></div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px"></div>
                                                                    <div class="col-lg-6" style="padding: 1px"></div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                    
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px">
                                                                        <option value="" disabled="disabled" selected="selected">Line item Name</option>
                                                                        <option value="1">Start Service</option>
                                                                        <option value="2">Stop Service</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-8" style="padding: 1px">
                                                                    
                                                                    </div>
                                                                    <div class="col-lg-4" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px" id="ddlAndOR2">
                                                                        <option value="" disabled="disabled" selected="selected">And/Or</option>
                                                                        <option value="1">And</option>
                                                                        <option value="2">Or</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding:0px">
                                                                      <div >
                                                                <div class="col-lg-12" style="padding-left: 0px; padding-right: 1px;border-left:solid">
                                                                    <div class="col-lg-2" style="padding: 1px"></div>
                                                                    <div class="col-lg-6" style="padding: 1px"></div>
                                                                    <div class="col-lg-4" style="padding: 1px">
                                                                     
                                                                    </div>
                                                                </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="dvCondition3" style="display:none">

                                                                <div class="col-lg-9" style="padding:0px">
                                                                     <div>
                                                                <div class="col-lg-3" style="padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                      
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                     
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                   
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                     
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                    
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px">
                                                                        <option value="" disabled="disabled" selected="selected">Line item Name</option>
                                                                        <option value="1">Start Service</option>
                                                                        <option value="2">Stop Service</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-8" style="padding: 1px">
                                                                    
                                                                    </div>
                                                                    <div class="col-lg-4" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px" id="ddlAndOR3">
                                                                        <option value="" disabled="disabled" selected="selected" >And/Or</option>
                                                                        <option value="1">And</option>
                                                                        <option value="2">Or</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding:0px">
                                                                      <div >
                                                                <div class="col-lg-12" style="padding-left: 0px; padding-right: 1px;border-left:solid">
                                                                    <div class="col-lg-2" style="padding: 1px">
                                                                
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                 
                                                                    </div>
                                                                    <div class="col-lg-4" style="padding: 1px">
                                                                     
                                                                    </div>
                                                                </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="dvCondition4" style="display:none">
                                                                <div class="col-lg-9" style="padding:0px"><div>
                                                                <div class="col-lg-3" style="padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px"></div>
                                                                    <div class="col-lg-6" style="padding: 1px"></div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px"></div>
                                                                    <div class="col-lg-6" style="padding: 1px"></div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                    
                                                                    </div>
                                                                    <div class="col-lg-6" style="padding: 1px">
                                                                         <select class="form-control input-sm" style="padding: 0px">
                                                                        <option value="" disabled="disabled" selected="selected">Line item Name</option>
                                                                        <option value="1">Start Service</option>
                                                                        <option value="2">Stop Service</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding-left: 0px; padding-right: 1px">
                                                                    <div class="col-lg-8" style="padding: 1px">
                                                                    
                                                                    </div>
                                                                    <div class="col-lg-4" style="padding: 1px">
                                                                       <%--  <select class="form-control input-sm" style="padding: 0px" id="ddlAndOR4">
                                                                        <option value="" disabled="disabled" selected="selected">And/Or</option>
                                                                        <option value="1">And</option>
                                                                        <option value="2">Or</option>
                                                                    </select>--%>
                                                                    </div>
                                                                </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3" style="padding:0px">
                                                                      <div >
                                                                <div class="col-lg-12" style="padding-left: 0px; padding-right: 1px;border-left:solid">
                                                                    <div class="col-lg-2" style="padding: 1px"></div>
                                                                    <div class="col-lg-6" style="padding: 1px"></div>
                                                                    <div class="col-lg-4" style="padding: 1px">
                                                                     
                                                                    </div>
                                                                </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div style="float: right; display:none" id="dvAddnew" >
                                                    <button type="button" class="btn3d btn btn-primary btn-sm" id="btnAddNew">Add New</button>
                                                </div>
                                                <div id="dvDone" style="display:none">
                                                    <div class="col-lg-5"></div>
                                                    <div class="col-lg-1">
                                                        <button type="button" class="btn3d btn btn-primary btn-sm form-control" id="btnDone">Done</button>
                                                    </div>
                                                    <div class="col-lg-1">
                                                        <button type="button" class="btn3d btn btn-primary btn-sm form-control" id="btnBack">Back</button>
                                                    </div>
                                                    <div class="col-lg-5"></div>
                                                    
                                                </div>
                                            </div>
                                        </div>

</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_Busi">
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box-body">
                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                <div class="panel panel-primary" style="height: 717px; overflow-y: scroll;">
                                                    <div class="panel-heading text-center">
                                                        <label>USP</label>
                                                    </div>
                                                    <div class="panel-body">
                                                        <table id="tblUTILITY" class="table" data-util="Gas">
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-10" style="padding-left: 0px; display: none;" id="divMAINPANEL">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading text-center">
                                                        <label>USP Control Settings</label>
                                                    </div>
                                                    <div class="panel-body" style="padding: 3px;">
                                                        <%--<div class="col-xs-2" style="padding-left: 0px; padding-right: 3px;">
                                <div class="panel panel-primary" style="height: 648px; overflow-y: scroll;">
                                    <div class="panel-body text-center">
                                        <label style="color: #000">States</label>
                                        <table id="tblSTATE" class="table no-border" data-util="Gas">
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>--%>
                                                        <%--<div class="col-xs-2" style="padding-left: 0px;">
                                <div class="panel panel-primary" style="height: 648px; overflow-y: scroll;">
                                    <div class="panel-body text-center">
                                        <label style="color: #000">Cities</label>
                                        <table id="tblCITIES" class="table no-border" data-util="Gas">
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>--%>

                                                        <div class="col-xs-12" style="padding-left: 0px; height: 648px; overflow-y: scroll; display: none;" id="divBASEPANEL">
                                                            <div class="box-body">
                                                                <div class="nav-tabs-custom">
                                                                    <ul class="nav nav-tabs">

                                                                        <li class="active"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Requirements</a></li>
                                                                        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false" style="display: none">Address</a></li>
                                                                        <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false" style="display: none">Zip Codes</a></li>
                                                                        <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="true" style="display: none">Copy Settings</a></li>
                                                                    </ul>
                                                                    <div class="tab-content">
                                                                        <div class="tab-pane" id="tab_1">
                                                                        </div>
                                                                        <div class="tab-pane active" id="tab_2">
                                                                            <div class="panel panel-primary">
                                                                                <div class="panel-body">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">

                                                                                            <div class="nav-tabs-custom">
                                                                                                <ul class="nav nav-tabs">
                                                                                                    <li class="active"><a href="#PAYMENTDETAILS" data-toggle="tab" aria-expanded="true">Payment Details</a></li>
                                                                                                    <li class=""><a href="#DOCUMENTS" data-toggle="tab" aria-expanded="false">Documents</a></li>
                                                                                                    <li class=""><a href="#CITYINSPECTION" data-toggle="tab" aria-expanded="false">City of Inspection</a></li>
                                                                                                    <li class=""><a href="#ACTIVATION" data-toggle="tab" aria-expanded="false">Activation of other Utilities</a></li>
                                                                                                    <li class=""><a href="#REQUIREMENTS" data-toggle="tab" aria-expanded="false">Inspection Requirements</a></li>
                                                                                                </ul>
                                                                                                <div class="tab-content">

                                                                                                    <div class="tab-pane active" id="PAYMENTDETAILS">
                                                                                                        <div class="row form-group">
                                                                                                            <div class="col-md-3">
                                                                                                                Status
                                                                                                            </div>
                                                                                                            <div class="col-md-3 ">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="active">
                                                                                                                        Active
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-3">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="inactive">
                                                                                                                        Inactive
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group">
                                                                                                            <div class="col-md-12">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row ">

                                                                                                            <div class="col-md-3  ">
                                                                                                                <div class="checkbox chkSETINACTIVE123" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkDeposit">
                                                                                                                        Deposit Amount
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-sm-4">
                                                                                                                Standard Amount 
                                                                                 <asp:TextBox ID="txtStandard1" class="form-control" ClientIDMode="Static" runat="server" TextMode="Number"></asp:TextBox>
                                                                                                            </div>


                                                                                                        </div>
                                                                                                        <%--<div class="row " style="display: none">
                                                                            <div class="col-md-3 col-md-offset-1 chkSETINACTIVE123 ">
                                                                                Calculation
                                                                                 <div class="checkbox pull-right chkSETINACTIVE123" style="margin: 0;">
                                                                                     <label>
                                                                                         <input type="checkbox" >
                                                                                     </label>
                                                                                 </div>
                                                                            </div>
                                                                            <div class="col-md-8 chkSETINACTIVE123">
                                                                                <input type="text">
                                                                                <select class="">
                                                                                    <option>With</option>
                                                                                    <option>Without</option>
                                                                                </select>
                                                                                <input type="text" />
                                                                            </div>
                                                                        </div>--%>
                                                                                                        <%--<div class="row form-group chkSETINACTIVE123">
                                                                            <div class="col-md-12">
                                                                                
                                                                            </div>
                                                                        </div>--%>
                                                                                                        <div class="row form-group chkSETINACTIVEACT chkSETINACTIVE123">
                                                                                                            <hr />
                                                                                                            <div class="col-md-3 ">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkactivation">
                                                                                                                        Activation Fee
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-4">
                                                                                                                Standard Amount 
                                                                                 <asp:TextBox ID="txtStandard2" class="form-control" ClientIDMode="Static" runat="server" TextMode="Number"></asp:TextBox>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                        <%-- <div class="row form-group chkSETINACTIVEACT chkSETINACTIVE123" style="display: none">
                                                                            <div class="col-md-3 col-md-offset-1">
                                                                                Calculation
                                                                                <div class="checkbox pull-right" style="margin: 0;">
                                                                                    <label>
                                                                                        <input type="checkbox">
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <input type="text">
                                                                                <select class="">
                                                                                    <option>With</option>
                                                                                    <option>Without</option>
                                                                                </select>
                                                                                <input type="text" />
                                                                            </div>
                                                                        </div>--%>
                                                                                                        <div class="row form-group chkSETINACTIVELIN chkSETINACTIVE123">
                                                                                                            <hr />
                                                                                                            <div class="col-md-3 ">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkLien">
                                                                                                                        Lien Search Fee
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-4">
                                                                                                                Standard Amount 
                                                                                 <asp:TextBox ID="txtStandard3" class="form-control" ClientIDMode="Static" runat="server" TextMode="Number"></asp:TextBox>

                                                                                                            </div>

                                                                                                        </div>

                                                                                                    </div>
                                                                                                    <div class="tab-pane" id="DOCUMENTS">
                                                                                                        <div class="row form-group">
                                                                                                            <div class="col-md-3">
                                                                                                                Status
                                                                                                            </div>
                                                                                                            <div class="col-md-3 ">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkActive">
                                                                                                                        Active
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-3">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkInactive" checked="checked">
                                                                                                                        Inactive
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                        <div class="row form-group chkDOCUMENTS">
                                                                                                            <div class="col-md-12">

                                                                                                                <div class="col-md-9">
                                                                                                                    What is the email Address? 
                                                                                    <asp:TextBox ID="txtEmailAdd" class="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div class="col-md-3">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkDOCUMENTS">
                                                                                                            <div class="col-md-12">

                                                                                                                <div class="col-md-9">
                                                                                                                    To whom it is attention to? 
                                                                                    <asp:TextBox ID="txtAttention" class="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div class="col-md-3">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkDOCUMENTS">
                                                                                                            <div class="col-md-12">

                                                                                                                <div class="col-md-9">
                                                                                                                    What is the FAX text? 
                                                                                    <asp:TextBox ID="txtFax" class="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div class="col-md-3">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkDOCUMENTS">
                                                                                                            <div class="col-md-12">

                                                                                                                <label class="" style="font-weight: bold">Documents Required</label>
                                                                                                                <hr style="margin-bottom: 0; margin-top: 0" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkDOCUMENTS">
                                                                                                            <div class="col-md-6" style="padding: 10px;">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkDeed" disabled="disabled">
                                                                                                                        Foreclosure Deed
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-6" style="padding: 10px;">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkRequest" disabled="disabled">
                                                                                                                        Letter of Request
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-6" style="padding: 10px;">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkPOA" disabled="disabled">
                                                                                                                        Investor-Ocwen POA
                                                                                                                    </label>

                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-6" style="padding: 10px;">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkAuthorization" disabled="disabled">
                                                                                                                        Letter of Authorization

                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-6" style="padding: 10px;">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkOcwenPOA" disabled="disabled">
                                                                                                                        Ocwen-Altisource POA

                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-6" style="padding: 10px;">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkASFI" disabled="disabled">
                                                                                                                        ASFI W9
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-6" style="padding: 10px;">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkForm" disabled="disabled">
                                                                                                                        Application form
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-6" style="padding: 10px;">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkListing" disabled="disabled">
                                                                                                                        Listing Agreement
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="tab-pane" id="CITYINSPECTION">
                                                                                                        <div class="row form-group">
                                                                                                            <div class="col-md-3">
                                                                                                                Status
                                                                                                            </div>
                                                                                                            <div class="col-md-3 ">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkActive1">
                                                                                                                        Active
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-3">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkInactive1">
                                                                                                                        Inactive
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkADDRESS">
                                                                                                            <div class="col-md-12">

                                                                                                                <div class="col-md-9">
                                                                                                                    Where should the report/clearance be sent?
                                                                                    <asp:TextBox ID="txtReport" class="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div class="col-md-3">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkADDRESS">
                                                                                                            <div class="col-md-12">

                                                                                                                <div class="col-md-9">
                                                                                                                    What is the email Address? 
                                                                                    <asp:TextBox ID="txtCityEmail" class="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div class="col-md-3">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkADDRESS">
                                                                                                            <div class="col-md-12">

                                                                                                                <div class="col-md-9">
                                                                                                                    To whom it is attention to? 
                                                                                    <asp:TextBox ID="txtAtten" class="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div class="col-md-3">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkADDRESS">
                                                                                                            <div class="col-md-12">

                                                                                                                <div class="col-md-9">
                                                                                                                    What is the FAX text? 
                                                                                    <asp:TextBox ID="txtCityFax" class="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div class="col-md-3">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="tab-pane" id="ACTIVATION">
                                                                                                        <div class="row form-group">
                                                                                                            <div class="col-md-3">
                                                                                                                Status
                                                                                                            </div>
                                                                                                            <div class="col-md-3 ">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkActive2">
                                                                                                                        Active
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-3">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkInactive2">
                                                                                                                        Inactive
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkACTUT">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkElec">
                                                                                                                        To activate Electricity, the following  should be:
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkACTUT">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="col-md-3">
                                                                                                                    Water Should be
                                                                                                                </div>
                                                                                                                <div class="col-md-9">
                                                                                                                    <input type='checkbox' id="togWater1" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkACTUT">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="col-md-3">
                                                                                                                    Gas Should be
                                                                                                                </div>
                                                                                                                <div class="col-md-9">
                                                                                                                    <input type='checkbox' id="togGas1" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkACTUT">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkGas">
                                                                                                                        To activate Gas, the following  should be:
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkACTUT">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="col-md-3">
                                                                                                                    Water Should be
                                                                                                                </div>
                                                                                                                <div class="col-md-9">
                                                                                                                    <input type='checkbox' id="togWater2" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkACTUT">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="col-md-3">
                                                                                                                    Electricity Should be
                                                                                                                </div>
                                                                                                                <div class="col-md-9">
                                                                                                                    <input type='checkbox' id="togElec1" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkACTUT">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" id="chkWater">
                                                                                                                        To activate Water, the following  should be:
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkACTUT">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="col-md-3">
                                                                                                                    Electricity Should be
                                                                                                                </div>
                                                                                                                <div class="col-md-9">
                                                                                                                    <input type='checkbox' id="togElec2" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row form-group chkACTUT">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="col-md-3">
                                                                                                                    Gas Should be
                                                                                                                </div>
                                                                                                                <div class="col-md-9">
                                                                                                                    <input type='checkbox' id="togGas2" data-toggle="toggle" data-style="ios" class="chkBoxActive toggle" />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="tab-pane" id="REQUIREMENTS">
                                                                                                        <div class="row form-group" style="padding: 15px;">
                                                                                                            <div>
                                                                                                                <ul class="list-group" id="ulSETTINGS">
                                                                                                                    <li class="list-group-item">
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-12">
                                                                                                                                <div class="checkbox" style="margin: 0;">
                                                                                                                                    <label>
                                                                                                                                        <input type="checkbox" id="chkRequire" class="pull-left">If the status of the property is vacant and the vacancy is 
                                                                                         <asp:DropDownList ID="ddlGLE" ClientIDMode="Static" Width="120px" runat="server" Style="width: 40px;">
                                                                                             <asp:ListItem><</asp:ListItem>
                                                                                             <asp:ListItem>></asp:ListItem>
                                                                                             <asp:ListItem>=</asp:ListItem>
                                                                                         </asp:DropDownList>
                                                                                                                                        <asp:TextBox ID="txtVacancy" ClientIDMode="Static" runat="server" Text="1" Style="width: 70px;" TextMode="Number"></asp:TextBox>

                                                                                                                                        <asp:DropDownList ID="ddlDWM" ClientIDMode="Static" Width="120px" runat="server" Style="width: 62px;">
                                                                                                                                            <asp:ListItem Value="Days">Days</asp:ListItem>
                                                                                                                                            <asp:ListItem Value="Weeks">Weeks</asp:ListItem>
                                                                                                                                            <asp:ListItem Value="Months">Months</asp:ListItem>
                                                                                                                                        </asp:DropDownList>
                                                                                                                                        then inspection is required 
                                                                                                                                    </label>
                                                                                                                                </div>

                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="col-md-10" style="text-align: right">
                                                                                            </div>
                                                                                            <div class="col-md-2" style="text-align: right">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane " id="tab_3">
                                                                            <div class="panel panel-primary">
                                                                                <div class="panel-body">
                                                                                    <div class="row form-group">
                                                                                        <div class="col-md-12">
                                                                                            <div class="col-md-1">
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                                <div class="panel panel-success">
                                                                                                    <div class="panel-body">
                                                                                                        Physical Address
                                                                <input type="text" class="form-control" style="margin-bottom: 4px" id="PhStreet" placeholder="Street" />
                                                                                                        <input type="text" class="form-control" style="margin-bottom: 4px" id="PhCity" placeholder="City" />
                                                                                                        <input type="text" class="form-control" style="margin-bottom: 4px" id="PhState" placeholder="State" />
                                                                                                        <input type="text" class="form-control" style="margin-bottom: 4px" id="PhZipcode" placeholder="Zipcode" />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                                <div class="panel panel-success">
                                                                                                    <div class="panel-body">
                                                                                                        Payment Address
                                                                <input type="text" class="form-control" style="margin-bottom: 4px" id="Pstreet" placeholder="Street" />
                                                                                                        <input type="text" class="form-control" style="margin-bottom: 4px" id="Pcity" placeholder="City" />
                                                                                                        <input type="text" class="form-control" style="margin-bottom: 4px" id="Pstate" placeholder="State" />
                                                                                                        <input type="text" class="form-control" style="margin-bottom: 4px" id="Pzipcode" placeholder="Zipcode" />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-3">
                                                                                                <div class="panel panel-success">
                                                                                                    <div class="panel-body">
                                                                                                        P.O. Box Address
                                                                <input type="text" class="form-control" style="margin-bottom: 4px" id="POstreet" placeholder="Street" />
                                                                                                        <input type="text" class="form-control" style="margin-bottom: 4px" id="POcity" placeholder="City" />
                                                                                                        <input type="text" class="form-control" style="margin-bottom: 4px" id="POstate" placeholder="State" />
                                                                                                        <input type="text" class="form-control" style="margin-bottom: 4px" id="POzipcode" placeholder="Zipcode" />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row form-group">
                                                                                        <div class="col-md-12">

                                                                                            <div class="col-md-3">
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row form-group">
                                                                                        <div class="col-md-12">

                                                                                            <div class="col-md-3">
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane " id="tab_4">
                                                                            <div class="panel panel-primary">

                                                                                <div class="panel-body" id="divZIPS">
                                                                                    <asp:ListBox ID="lbZip" runat="server" SelectionMode="Multiple" multiple="multiple" ClientIDMode="Static" DataSourceID="SqlDataZip" DataTextField="Zip" DataValueField="Zip"></asp:ListBox>
                                                                                    <asp:SqlDataSource runat="server" ID="SqlDataZip" ConnectionString='<%$ ConnectionStrings:ConnectionString %>' SelectCommand="SELECT [Zip] FROM [tbl_UC_State_City_Zip]"></asp:SqlDataSource>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="align-content: flex-end">
                                                                    <table id="1" onclick="btnSave(this)" style="text-align: right">
                                                                        <thead>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <button type="button" class="btn btn-success">Save</button>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>





            </div>
        </div>
             </div>
    </div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
   <%-- <script src="plugins/multiselect/bootstrap-multiselect.js"></script>--%>
    <script src="js/Downstream.js"></script>
    <script src="js/businessReq.js"></script>
<%--    <script>
        $(document).ready(function () { $("#e1").select2(); });
    </script>--%>
</asp:Content>


